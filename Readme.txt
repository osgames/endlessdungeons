The Endless Dungeons by Brian Conley (https://sourceforge.net/projects/endlessdungeons/)
Imported from Sourceforge file releases (https://sourceforge.net/projects/endlessdungeons/files/)
Licensed under CC BY-NC-SA 2.0 with permission of the author.

---

Current release: 

If you are upgrading from 1.02 to 1.10 or later, please view upgrade.txt first!

AS A GENERAL NOTE: If you are running the install package in your previous game directory, make sure to back up your Saves folder first! 

v1.30d:
Bugfixes:
- The fix for player dodge chance in 1.30c accidentally broke player initiative, which caused the player to never get a turn when fighting a monster with haste. This has now also been fixed.

v1.30c:
Gameplay Improvements:
- Beware! Dragon breath weapon damage has been boosted, and boosted additionally for dragons of level 60 or higher.
- Stairs no longer always favor spawning at the map corners.
- Lowered the kill requirement for the second kill quest in the Hall of Heroes from 1600 to 1200.
- Changed player hit rate calculations. 345 dexterity will get you a base 85% hit rate, with returns dropping off significantly after that point (1% per 50 points). This is mostly to make the Accuracy weapon ability more useful.

Bugfixes:
- Fixed a serious bug where player dodge chance was not working correctly since 1.20.
- Fixed a bug which prevented the eye of eluria from being obtained since 1.30.
- Page x of x displayed in the bags pane is now the correct numbers.

v1.30b:
Bugfixes:
- Deflect value is now once again showing up in the stat pane.
- Improved the mouse movement routines and they now work in the zoomed view as well.

v1.30a:
Gameplay Improvements:
- Zoom in and out (Control-R) now has a graphical effect
- Combat zoom (zoom in automatically during combat) is now the default setting. This can be changed in dungeon.cfg if you prefer.
Bugfixes:
- Fixed a math bug that was causing several of the 1.20 item abilities to not work correctly
- Sceptres are no longer mistakenly shown as 2h items in the info box
- Fixed a bug which made it impossible to claim rewards at the hall of heroes since 1.30

v1.30 (another major release)
Features:
MAJOR GRAPHICS OVERHAUL
- Almost all of the drawing routines have been redone. 
- The game is now intended to be run in widescreen. The default resolution is 1280 x 720.
- Tiles are drawn across the entire width and height of the screen, at native resolution. No more poor quality stretch_blit scaling.
- The game supports both 32x32 tile drawing and 64x64 tile drawing, and you can toggle between them in-game by pressing Control-R.
- Tiles outside of your current field of view, but which you have explored, are now drawn to the screen in a darkened state. This is more in line with how other graphical roguelikes work, and makes good use of the larger screen real estate.
- Overall, UI and tile elements are drawn smaller, and make better use of high PC resolutions.
- In short, the "look" of the game is greatly improved.
Gameplay Improvements:
- Added a help mode to the game (enabled by default). With help mode enabled, the keyboard shortcut for each main dock option will be displayed above the icon. Help mode can be toggled by editing dungeon.cfg or by pressing / in-game.
- Added a fullscreen view of the automap to the game which can be accessed by either clicking the mini-map itself, or by pressing Control-M.
- Increased damage of Meteor and Dark Cloud spells, so that they are now not totally outclassed by the Storm spell.
- Keyboard shortcuts for the Main dock work in town now too.
- Added an experimental feature "combat zoom" that will automatically switch to the zoomed 64x64 view when you enter combat. This is disabled at default but can be enabled by editing dungeon.cfg.
Bugfixes:
- Fixed a longstanding bug regarding mouse movement where sometimes it was impossible to move to the square directly next of you.


v1.20a
Bugfixes:
- Fixed a typo regarding two-handed weapons and Item Info
- Fixed the bug relating to the bugged item that would sometimes appear in the weapon shop

v1.20 (major release):
Features:
- Two new character classes! Monk and Nomad. The Monk is a cloth-wearing melee fighter who uses a Chi ability to avoid damage. The Nomad is a desert warrior skilled in Earth and Wind magics. More info is in the manual (it's been updated).
- MORE INTERESTING ITEMS: A major item revamp. Magic weapons and armor can now spawn a variety of new modifiers. A complete list follows:
Weapons (non-Wand): Enhanced Damage (5%-15%), Lucky, Life Stealing, Accuracy, Slumber.
Weapons (Wand): Enhanced Spell Damage (5%-15%), Potency, 10%-20% Reduced MP Cost.
Armor (Chest and Shield only): +(5%-15%) Maximum HP/MP, Regenerates X HP/Turn, Evasion, Fleet Foot.
These new ablities will appear in purple text under the Item Info pane. More information on these ablities is in the revamped manual under "Rare Weapons and Armor".

Gameplay Improvements:
- Redid the Info pane to use plain text and no icons. Now, the only thing icons are used for in-game is for Boons and Curses (and buttons).
- The info pane enlarges when viewing an item. This allows for more detailed item information, and more complex items in general.
- Damage listed for a weapon in the info pane is now accurate, factoring in both elemental imbue and enhanced damage if they are present.
- Weapon damage has been rebalanced. Two-handed weapons and wands were not changed. For every other weapon type, damage has been increased. In general, the disparity between one-handed and two-handed weapons has been decreased, as well as the disparity between weapon types. In other words, all one handed weapons do about the same damage now (swords have a slight edge), and two-handed weapons do about 1/3rd more base damage than one-handed weapons.
- Resistances have been reworked (again). The maximum is once again 99, but the formula has changed so that 99 resistance equals 75% reduction in damage for that element.
- Changed the way that resistances are assigned to armor. A level 1 armor can now have between 25-50 resistance, and a level 99 armor can have between 50-99 resistance. In general, more resistance is available, and earlier in the game. Also resistance is random and not static by item level.
- Chest and Shield type items now have a chance to spawn with a Prismatic imbue (resist all). 
- Increased the frequency at which weapons with elemental imbues appear in both the dungeon and shops.
- Reduced the HP of boss monsters encountered on the first 20 levels of the dungeon.
- Monsters heal themselves less frequently, especially at lower levels.
- Reduced the number of kills required for defeat X monster missions, and the chosen monster type should be more relevant to your current dungeon level.
- Further corrections to the game font

Bug Fixes:
- Fixed a bug where if you died from poison and resurrected it did not port you back to town.
- May have fixed the bug with the weird item that sometimes appears in the weapon shop (more testing needed).
- Fixed a typo in the Lightblade log entry.



v1.12:
Gameplay Improvements:
- New Game Feature: Missions! At the start of your play session, you will be assigned a random mission. Completing
  this mission will net you a medium amount of experience points; additionally, there is a small chance that one
  of your stats will increase. After completing the mission, you can obtain a new mission by returning to town.
  Level 99 characters who complete a mission have a %50 chance to get either +1 or +2 to their primary stat. The 
  +2 reward is much rarer. 
  Your currently assigned mission and it's progress are listed on page 2 of the Stat pane.
  The philosophy behind this missions feature is to give you something additional to do beyond simply working on
  skills and exploring the dungeon.
  Mission progress is not saved between sessions and if you exit to the title screen or close the program, a 
  new mission will be assigned when you return to town.
- New sound effects! Added a grunt type noise to melee attacks to make them more interesting. The sound effect
  does not change based on class, but it is different for male versus female characters.
- Major Xantos retooling. Overall difficulty increased slightly, but should be more balanced between classes.
- The Item shop is now guaranteed to stock at least one Cleansing Potion.
- Buffed Healing and Mana potions to provide about 700 restoration at level 99 versus 500 before.
- You will now be prompted for confirmation before selling a stack of potions.
- When selling a stack of any item, the confirmation prompt now tells you the quantity you are selling.
- Portal Stone frequency in the dungeon has been further reduced.
- Chests will appear slightly more frequently in large rooms in the dungeon.
- The maximum player resists has been changed from 99 to 95 in order to not completely trivialize certain spells.
- The Warlock innate Siphon ability has been buffed and now displays in the log correctly.
- Changed Lightblade, the level 99 Scholar ability for Paladins. It is now guaranteed to heal with every hit, but 
  the amount was reduced from 1/3 to 1/5 of the damage dealt.
- Changed Art of War, the level 99 Scholar ablity for Warriors. It now has two effects: damage with one-handed 
  weapons is increased by 30%, and the chance for lucky strikes with two-handed weapons is also increased. 

Bug Fixes:
- Fixed an issue where selling stacks of items was not rewarding as much gold as it should.
- Fixed a serious bug where Icy Shield was actually decreasing deflection instead of increasing it.
- Fixed more percentage typos.
- Fixed an issue where Endurance, the level 50 scholar skill for Barbarians, was showing increased deflection
  rating outside of combat.
- Fixed an issue where it was sometimes possible to maintain Berserker rage stacks between encounters.
- Fixed an issue where the M hotkey didn't work if you were viewing the second page of the stat pane.
- Fixed a door tile misalignment in tiles.bmp.
- Fixed an issue where cleansing the Softness curse did not immediately increase deflection to normal.
- Fixed a typo in the ending credits.
- Fixed a border issue when changing trade professions.



v1.11a:
Gameplay Improvement:
- Boosted XP rewarded from monsters slightly (about %10)
Compatibility Improvement:
- Bold font is now automatically selected when scaling below the native 1280 x 900 resolution.
Bugfixes:
- Fixed an issue where the player could be stunned but still prompted for action during combat.
- Fixed an issue where the flee message during combat was not always drawn to the log.
- Fixed a problem with mind of the kraken that was preventing it from working correctly.
- Changed male Warrior icon.
- Fixed an issue where polymorph would sometimes crash the game.
- Additional tweaks to the default font.


v1.11:
Gameplay Improvements:
- Added an option to the Hall of Heroes to exchange scrolls for gems at a rate of 10 to 5.
- Scrolls can no longer be sold at the item vendors.
- Added a fast combat option to the game, which is twice as fast as the default combat speed. Fast combat can be
  toggled in-game by pressing Control-F. There is also an entry in dungeon.cfg.
- Creatures with magic resistance are now guaranteed to be resistant to magic.
- Creatures with an elemental imbue now have appropriate resistances to their element.
- Reduced portal stone appearance in the dungeon as they are not as needed now as they were pre-1.10.
- Boosted volume on some sound files that were too quiet.
- Corrected some issues in both the normal and bold font files, mostly related to punctuation.
- Boss monsters and chests are now much more likely to drop necklaces and rings.
- Boss Monsters have been rebalanced. They have more HP, but reward significantly higher XP than before.
- Scythes have been added to the list of items which use either intelligence or strength (whichever higher) to 
  determine damage. This should allow Warlocks to use scythes effectively into the endgame.

Bug Fixes:
- The player graphic is now correctly redrawn when you cleanse a polymorph effect during combat.
- Corrected some friendly NPC interactions in the dungeon which were providing outdated information.
- Corrected an issue that caused Smash Level 1 to display as Smash Level 2 in the spellbook.
- Improved some combat texts.
- Cleaned up many boon and spell tooltips.


v1.10c:
- Fixed a bug that caused Martial Arts weapon skill to not cap out at 99
- Added a gameplaytips.txt file to the game install with some quick hints on how to play the game

v1.10b:
Caster buffs:
- Wands now get twice as much bonus damage from the intelligence stat as before. This is more in line with how 
  one-handed weapons work for the other classes.
- Staff type weapon damage is now boosted by the higher of either strength or intelligence, making this weapon 
  type actually useful for casters.
Bug Fixes:
- Fixed a shop bug, where having a full inventory before teleporting back to town could cause a bugged item to
  appear in the shop.
- Fixed an issue that caused concussive blast to make two entries in the log during combat
- Removed the low inventory warning message from situations where it was inappropriate (for example, combining
  potions in town).

v1.10a:
- Fixed a bug in dungeon generation that was causing the smaller map sizes to spawn too many rooms.

v1.10:
Gameplay Improvements:
- Added a minimap to the game.
- Changed the stat pane to be more user friendly, with simple text descriptions instead of a wall of icons. You 
  can enable the old icon-based style of stat pane if you prefer by editing dungeon.cfg.
- When entering the dungeon from the town, you may now continue from the lowest dungeon level visited.
- Added difficulty settings of easy, normal, and hard. In easy difficulty, monsters have %33 less HP and reward 
  %20 more experince. Normal difficulty has no special modifiers. Hard difficulty increases monster damage and
  decreases XP rewarded.
- Added a map size setting of Small, Normal, and Large. Small = 64 x 64 squares, Normal = 96 x 96, and 
  Large = 128 x 128. The previous default map size was 128 x 128 and the new default is 96 x 96.
- Added an option to disable random boss encounters in the dungeon.
- Added several new keyboard shortcuts (dungeon only): M = Automap, I = Inventory, E = Equip, B = Spellbook, S =
  Stats, Y = System Menu.
- Increased the number of items generated in the dungeon slightly to offset the smaller default map size.
- Magic users now gain two skillpoints per scroll used until skill level 30 is reached.
- Nerfed the HP bonus provided to monsters from the Colossal Strength boon.
- The game now warns you when your inventory is almost full (9 or fewer slots left).
- Changed the way that having no more inventory slots left is handled. Instead of the old system where the 
  game would prompt you to drop an item in order to pick up the item, instead the game simply doesn't let you 
  pick anything up when your inventory is full. Quest rewards and crafting check for inventory space as well and
  won't let you proceed without inventory space.

Compatiblity Improvements:
- Changed the way that video modes work. Previously, there were three supported video modes: 1600 x 900 fullscreen,
  640 x 480 windowed or fullscreen, and 1280 x 900 windowed. Now, graphics are rendered at 1280 x 900 internally,
  and can be scaled to any video resolution you prefer by changing the resolution setting in dungeon.cfg. Note
  that Allegro 4.4.2 stretch_blit scaling is not very high quality so this may make the graphics blocky 
  (but, at least it can run on more systems).
- Added a configurable bold font setting to dungeon.cfg which may make text more readable when scaling down to
  lower resolutions.

Bug Fixes:
- Fixed a bug relating to bow weapon skill that caused it not to cap at 99.
- Fixed a serious bug relating to the Hall of Heroes where upgrading items would often cause a crash.


v1.02 Alpha:
* More code fixes, mostly to aid compatiblity with Linux: 
a. Replaced all instances of itoa with sprintf as sprintf is standard and itoa is not
b. Modified the routines that read the .dat files in a way that might make it work better on Linux
c. Replaced all \\ in pathnames with // 
c. Corrected a potentially serious bug in a randitem() call
d. Renamed random() to randomnum() 

EndlessDungeonsv102a.sfx.exe
Main game executable and necessary game files, self-extracting archive

TheEndlessDungeonsv102a-upgrade.zip
New EXE only.  If upgrading from v100a or v101a to v102a this is all you need.

TheEndlessDungeonsv102a.rar
Main game executable and necessary game files, winrar archive

TheEndlessDungeonsv102a.zip
Main game executable and necessary game files, Zip archive (larger size)

EndlessEditorv100a.zip
Editor for tile info used by the game.  Needs libgcc dll which is in the main game files.

TheEndlessDungeonsSourcecodev102a.zip
Sourcecode for both the game and editor.  Requires Allegro 4.4.2 library.

The Endless Dungeons.pdf
Manual in PDF format

The Endless Dungeons.docx
Manual in word DOCX format

v1.01 Alpha:
* Increased the duration of Alchemists Giant's Potion from 5 to 10 turns
* Made Alchemist trade a bit easier to level
* Fixed several code issues (should hopefully be more friendly to non-windows O/s now)
* Fixed a bug with macros where spell macros didn't work until you opened the spellbook
* Fixed a bug with enemy Smash spell
* Fixed a bug in the Xantos encounter
