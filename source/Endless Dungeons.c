/* Endless Dungeons 1.2 by Litmus Dragon */
/* Endless Dungeons 1.2 by Litmus Dragon */
/* Coded in Allegro 4.4.2 with MinGW and Code::Blocks */
/* Thanks to Allandy and Beorn from Allegro.cc for many helpful suggestions in revising the code */

#include <allegro.h>
#include <stdio.h>
#include <time.h>

#define BROWN       makecol(102,61,3)
#define GOLD        makecol(255,228,47)
#define WHITE       makecol(255,255,255)
#define BLACK       makecol(0,0,0)
#define GREEN       makecol(80,160,0)
#define RED         makecol(159,7,7)
#define BLUE        makecol(7,7,159)
#define GREY        makecol(136,121,121)
#define PURPLE      makecol(112,0,112)
#define LIGHTBROWN  makecol(132,91,33)
#define DARKGOLD    makecol(245,218,87)
#define SCREEN      0
#define MENU        1
#define LEFT        0
#define CENTER      1
#define RIGHT       2
#define TOP         0
#define BOTTOM      2

typedef struct {
    int x, y;
} playericons;

typedef struct {
    int x, y, sx, sy, cx, cy;
} trackdraw;


typedef struct {
    int statpanex[2], statpaney[2], logx[2], logy[2], infox[2], infoy[2], bagx[2], bagy[2],
    mapx[2], mapy[2];
} guitrack;

typedef struct {
    int wallx[4], wally[4], doorx[4], doory[4], floorx[4], floory[4], walls,
    doors, floors, supx, supy, sdnx, sdny, doorhidx, doorhidy;
} dungeontilestruct;


typedef struct {
    int bonesx[8], bonesy[8], ruinsx, ruinsy, mossx, mossy, pearlx, pearly, nshadex, nshadey, bookx[7], booky[7], sigilx[3], sigily[3],
    scrapx, scrapy, fragx[3], fragy[3], docx[2], docy[2], lantx[2], lanty[2];
} tradestruct;


typedef struct {
    int ulx, uly, h, w, l;
    } RoomArray;


typedef struct {
    int trkrm, automap, trki;
    } MapTrak;


// Dungeon struct
// sq 0 = floor  1 = wall  3 = accent 1  4 = accent 2  5 = accent 3
typedef struct {
    int icnx[128][128], icny[128][128], tset, supx, supy, sdnx, sdny, lgt, door[128],
    doormn[128], doormnlvl[128], doormx[128], doormy[128], explore[128][128], open[128][128],
    doorx[128], doory[128], item[256], itemx[256], itemy[256];
    } DLevel;


// The tile database structure
typedef struct {
    int lvl, type, mtype, cls, elem, treasure, magic, bonus;
    char name[24];
} tilearray;

// array to point to the tile data, for easy searching
typedef struct {
    int x, y;
} tilepointerstruct;

// struct to count item type totals, also for looking up
typedef struct {
    int mons, melee, armor, ammo, food, potion, scroll, gem, ranged, bones;
} countstruct;

// player structure
typedef struct {
    int lvl, xp, xpn, str, dex, itl, chr, resist[7], buffx[10], buffy[10], buffmod[10], buffdur[10], curse[4], cursedur[4],
    spl, dpl, ipl, cpl, magic[6], xpl, hpl, mpl, hp, mp, hpm, mpm, lockpick, wskill[7], equipmw[7][3], equipa[6], equipr[6],
    kills, deaths, hdamage, turns, llevel, rwskill[6], twoh, dextodmg, lucky, berserking, crusader, siphon, poisonexpert, gold,
    invh[100], invl[100], invicnx[100], invicny[100], invnum[100], invlvl[100],  invtype[100], invmtype[100], invcls[100], invelem[100],
    invtreasure[100], invmagic[100], invbonus[100], invadd[100][5], invaddamt[100][5], bkills, levelquest[5], killquest[5], questmodb[5],
    metsqr[64][28], deflect, dmgl, dmgh, dlvl, room, icnx, icny, x, y, equipped[11], adjstr, mpregen, poisontick,
    adjdex, adjitl, adjchr, savedlvl[99], lastx, lasty, bestxantos, pmorx, pmory, sex, arch, alch, scholar, trade, adjhp, adjmp,
    wswap[4][2], combatturn, funcmacrox[12], funcmacroy[12];
    char clname[24], name[24], invname[100][30];
} pstruct;


// monster structure
typedef struct {
    int lvl, xp, str, dex, itl, resist[7], buffx[2], buffy[2], buffdur[2], curse[2], cursedur[2], cls, l, h,
    magic[6], hp, hpm, gold, deflect, dmgl, dmgh, icnx, icny, elem, align, bonus, treasure, mtype, stun;
    char name[32];
} mstruct;


typedef struct {
    int type[8], mtype[8], cls[8], treasure[8], bonus[8], num[8],
    elem[8], magic[8], lvl[8], icnx[8], icny[8], l[8], h[8], add[8][5],
    addamt[8][5], price[8];
    char name[8][24];
} shopstruct;


typedef struct {
    int type, mtype, cls, treasure, bonus, elem, magic, lvl, icnx, icny, l, h,
    add[5], addamt[5], num;
    char name[24];
} tempitemstruct;

// a second temp item is needed for the drop routine
typedef struct {
    int type, mtype, cls, treasure, bonus, elem, magic, lvl, icnx, icny, l, h,
    add[5], addamt[5], num;
    char name[24];
} dropitemstruct;

tilearray tiledata[64][28];  shopstruct shop[3];  tempitemstruct tempitem;
tempitemstruct dropitembuff;

DLevel dgn; MapTrak dmap[128][128]; RoomArray r[128]; dungeontilestruct dtiles[5];
pstruct plyr; tilepointerstruct tlu[1500]; countstruct total; playericons picn[10][2];
mstruct mons; tradestruct trade; guitrack guilocations; trackdraw tiledraw;

MIDI *musicfile;
SAMPLE *soundfile;
BITMAP *buttonl[2], *buttons[2], *screentemp, *menutemp, *menu[2], *bknd, *menubord, *paper, *icons, *buttonbord,
*paperdoll, *iconsi, *tiles, *smalltiles, *maskedtiles, *maskedsmalltiles, *tiletemp, *monsterbuffer, *greyicons,
*redicons, *plusminus, *flipbuttontemp, *darken, *xantosbuffer;
FONT *largefont, *smallfont;
FILE *inputfile;
char menuopt[24][20], plyrlog[40][70], infocapt[60][40], infocapt2[60][40], passtemp[50], lastmidifile[20], charlist[24][24];
int clickx[24][2], clicky[24][2], offsetx, offsety, statpane, logc, tileres, tilepref, playerlog, charcount, infox[70][2], infoy[70][2],
infoct, rightpane, bagsopen, bagpage, spellbutton[6][6], iscombat, skeyx, skeyy, pstonex, pstoney,
gpilex, gpiley, gamescr, cheat, chestx[6], chesty[6], sound, midi, dmusictrack, dmusicpos,
saleoption, runex[3], runey[3], xantosx, xantosy, sfieldx[2], sfieldy[2], difficulty, mapsize,
bossflag, statstyle, fastmode, mission, missiontyp, missioncount, screenresx, screenresy, buttonsize,
guiborders, fontsize, viewt[60][60], moveflag[60][60], minimap, mapscale, combatzoom, movex, movey, help;

int divideround(int x, int y)
{
int a = (x-1)/y + 1;
return a;
}

void zoomout()
{
int x, drawx, drawy;
BITMAP *stretchtemp;
stretchtemp=create_bitmap(screen->w*2,screen->h*2);
stretch_blit(screentemp,stretchtemp,0,0,screentemp->w,screentemp->h,0,0,stretchtemp->w,stretchtemp->h);
drawx=(screen->w/2);
drawy=(screen->h/2);
for (x=1; x<10; ++x)
{
stretch_blit(stretchtemp,screen,drawx-((drawx*x)/10),drawy-((drawy*x)/10),screen->w+(((drawx*x)/10)*2),screen->h+(((drawy*x)/10)*2),0,0,screen->w,screen->h);
}
destroy_bitmap(stretchtemp);
}

void zoomin()
{
int x, drawx, drawy;
BITMAP *stretchtemp;
stretchtemp=create_bitmap(screen->w*2,screen->h*2);
stretch_blit(screentemp,stretchtemp,0,0,screentemp->w,screentemp->h,0,0,stretchtemp->w,stretchtemp->h);
drawx=(screen->w/2);
drawy=(screen->h/2);
for (x=10; x>0; --x)
{
stretch_blit(stretchtemp,screen,drawx-((drawx*x)/10),drawy-((drawy*x)/10),screen->w+(((drawx*x)/10)*2),screen->h+(((drawy*x)/10)*2),0,0,screen->w,screen->h);
}
destroy_bitmap(stretchtemp);
}

void clearmoveflags()
{
int x,y;
for (x=0; x<tiledraw.x; ++x)
{
for (y=0; y<tiledraw.y; ++y)
{
moveflag[x][y]=-1;
}
}
movex=-1; movey=-1;
}


void redrawdungeontiles()
{
int x, y, q, z, tn, tw, te, ts;
// Calculate viewable squares
for (x=0; x<tiledraw.x; ++x)
{
for (y=0; y<tiledraw.y; ++y)
{
viewt[x][y]=-1;
}
}
// North first
viewt[tiledraw.cx][tiledraw.cy]=1; viewt[tiledraw.cx][tiledraw.cy-1]=1;
viewt[tiledraw.cx][tiledraw.cy+1]=1; viewt[tiledraw.cx-1][tiledraw.cy]=1;
viewt[tiledraw.cx+1][tiledraw.cy]=1;
tn=-1;
for (y=plyr.y-1; y>plyr.y-tiledraw.y; --y)
{
if ((dmap[plyr.x][y].automap>0)||(dgn.open[plyr.x][y]==-1)) {tn=y; y=plyr.y-tiledraw.y;}
}
if (tn==-1) tn=plyr.y-tiledraw.y;
ts=-1;
for (y=plyr.y+1; y<plyr.y+tiledraw.y; ++y)
{
if ((dmap[plyr.x][y].automap>0)||(dgn.open[plyr.x][y]==-1)) {ts=y; y=plyr.y+tiledraw.y;}
}
if (ts==-1) ts=plyr.y+tiledraw.y;
te=-1;
for (x=plyr.x+1; x<plyr.x+tiledraw.x; ++x)
{
if ((dmap[x][plyr.y].automap>0)||(dgn.open[x][plyr.y]==-1)) {te=x; x=plyr.x+tiledraw.x;}
}
if (te==-1) te=plyr.x+tiledraw.x;
tw=-1;
for (x=plyr.x-1; x>plyr.x-tiledraw.x; --x)
{
if ((dmap[x][plyr.y].automap>0)||(dgn.open[x][plyr.y]==-1)) {tw=x; x=plyr.x-tiledraw.x;}
}
if (te==-1) tw=plyr.x-tiledraw.x;

for (x=-(tiledraw.cx); x<(tiledraw.x-tiledraw.cx); ++x)
{
for (y=-(tiledraw.cy); y<(tiledraw.y-tiledraw.cy); ++y)
{
if ((plyr.x+x>=tw)&&(plyr.x+x<=te)&&(plyr.y+y>=tn)&&(plyr.y+y<=ts))
    {viewt[x+tiledraw.cx][y+tiledraw.cy]=1; dgn.explore[plyr.x+x][plyr.y+y]=0;}
}
}
plyr.x = plyr.x-tiledraw.cx;
plyr.y = plyr.y-tiledraw.cy;
clear_bitmap(screentemp);
for (y=0; y<tiledraw.y; ++y)
{
for (x=0; x<tiledraw.x; ++x)
{
if (tileres==64)
{
if ( ((plyr.x+x)<0) || ((plyr.x+x)>128) || ((plyr.y+y)>128) || ((plyr.y+y)<0)
    || (dgn.icnx[plyr.x+x][plyr.y+y]==-1) || (dgn.explore[plyr.x+x][plyr.y+y]!=0))
{
blit(tiles, tiletemp, (63*tileres), (27*tileres), 0, 0, tileres, tileres);
}
else
{
blit(tiles, tiletemp, ((dgn.icnx[plyr.x+x][plyr.y+y])*tileres), ((dgn.icny[plyr.x+x][plyr.y+y])*tileres), 0, 0, tileres, tileres);
if (dmap[plyr.x+x][plyr.y+y].automap==0)
{
if ((dgn.item[dmap[plyr.x+x][plyr.y+y].trki]!=-1)&&(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<2000))
masked_blit(maskedtiles, tiletemp, tlu[dgn.item[dmap[plyr.x+x][plyr.y+y].trki]].x*tileres, tlu[dgn.item[dmap[plyr.x+x][plyr.y+y].trki]].y*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2000)
masked_blit(maskedtiles, tiletemp, gpilex*tileres, gpiley*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2001)
masked_blit(maskedtiles, tiletemp, skeyx*tileres, skeyy*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2002)
masked_blit(maskedtiles, tiletemp, pstonex*tileres, pstoney*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2003)
masked_blit(maskedtiles, tiletemp, chestx[0]*tileres, chesty[0]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2004)
masked_blit(maskedtiles, tiletemp, chestx[1]*tileres, chesty[1]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2005)
masked_blit(maskedtiles, tiletemp, chestx[2]*tileres, chesty[2]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2006)
masked_blit(maskedtiles, tiletemp, chestx[3]*tileres, chesty[3]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2007)
masked_blit(maskedtiles, tiletemp, chestx[4]*tileres, chesty[4]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2008)
masked_blit(maskedtiles, tiletemp, chestx[5]*tileres, chesty[5]*tileres, 0, 0, tileres, tileres);
else if ((dgn.item[dmap[plyr.x+x][plyr.y+y].trki]>2999)&&(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3999))
{
if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3004)
masked_blit(maskedtiles, tiletemp, (trade.bookx[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3000))*tileres, trade.booky[0]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3008)
masked_blit(maskedtiles, tiletemp, (trade.bookx[1]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3004))*tileres, trade.booky[1]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3012)
masked_blit(maskedtiles, tiletemp, (trade.bookx[2]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3008))*tileres, trade.booky[2]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3016)
masked_blit(maskedtiles, tiletemp, (trade.bookx[3]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3012))*tileres, trade.booky[3]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3020)
masked_blit(maskedtiles, tiletemp, (trade.bookx[4]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3016))*tileres, trade.booky[4]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3024)
masked_blit(maskedtiles, tiletemp, (trade.bookx[5]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3020))*tileres, trade.booky[5]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3028)
masked_blit(maskedtiles, tiletemp, (trade.bookx[6]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3024))*tileres, trade.booky[6]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3035)
masked_blit(maskedtiles, tiletemp, (trade.bookx[0]+4)*tileres, (trade.booky[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3028))*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3042)
masked_blit(maskedtiles, tiletemp, (trade.bookx[0]+5)*tileres, (trade.booky[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3035))*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3049)
masked_blit(maskedtiles, tiletemp, (trade.bookx[0]+6)*tileres, (trade.booky[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3042))*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<4000)
masked_blit(maskedtiles, tiletemp, (trade.bookx[0]+7)*tileres, (trade.booky[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3049))*tileres, 0, 0, tileres, tileres);
}
else if ((dgn.item[dmap[plyr.x+x][plyr.y+y].trki]>3999)&&(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<4008))
masked_blit(maskedtiles, tiletemp, trade.bonesx[(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-4000)]*tileres, trade.bonesy[(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-4000)]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==4008)
masked_blit(maskedtiles, tiletemp, trade.ruinsx*tileres, trade.ruinsy*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==5000)
masked_blit(maskedtiles, tiletemp, trade.nshadex*tileres, trade.nshadey*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==5001)
masked_blit(maskedtiles, tiletemp, trade.mossx*tileres, trade.mossy*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==5002)
masked_blit(maskedtiles, tiletemp, trade.pearlx*tileres, trade.pearly*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]>5999)
masked_blit(maskedtiles, tiletemp, runex[dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-6000]*tileres,
            runey[dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-6000]*tileres, 0, 0, tileres, tileres);
}
if ((plyr.dlvl==99)&&(dgn.sdnx==plyr.x+x)&&(dgn.sdny==plyr.y+y))
masked_blit(maskedtiles, tiletemp, (xantosx*tileres), (xantosy*tileres), 0, 0, tileres, tileres);
if ((x==tiledraw.cx)&&(y==tiledraw.cy))
{
q=-1;
for (z=0; z<4; ++z)
if (plyr.curse[z]==2) q=0;
if (q==-1) masked_blit(maskedtiles, tiletemp, (plyr.icnx*tileres), (plyr.icny*tileres), 0, 0, tileres, tileres);
else masked_blit(maskedtiles, tiletemp, (plyr.pmorx*tileres), (plyr.pmory*tileres), 0, 0, tileres, tileres);
}
// darken tiles that are explored but not in field of view
set_alpha_blender();
if (viewt[x][y]==-1)
draw_trans_sprite(tiletemp,darken,0,0);
// blit the path marker if it's active
if ((moveflag[x][y]!=-1)&&!((x==tiledraw.cx)&&(y==tiledraw.cy))) masked_blit(maskedtiles, tiletemp, ((moveflag[x][y])*tileres), (27*tileres), 0, 0, tileres, tileres);
}
}
else
{
if ( ((plyr.x+x)<0) || ((plyr.x+x)>128) || ((plyr.y+y)>128) || ((plyr.y+y)<0)
    || (dgn.icnx[plyr.x+x][plyr.y+y]==-1) || (dgn.explore[plyr.x+x][plyr.y+y]!=0))
{
blit(smalltiles, tiletemp, (63*tileres), (27*tileres), 0, 0, tileres, tileres);
}
else
{
blit(smalltiles, tiletemp, ((dgn.icnx[plyr.x+x][plyr.y+y])*tileres), ((dgn.icny[plyr.x+x][plyr.y+y])*tileres), 0, 0, tileres, tileres);
if (dmap[plyr.x+x][plyr.y+y].automap==0)
{
if ((dgn.item[dmap[plyr.x+x][plyr.y+y].trki]!=-1)&&(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<2000))
masked_blit(maskedsmalltiles, tiletemp, tlu[dgn.item[dmap[plyr.x+x][plyr.y+y].trki]].x*tileres, tlu[dgn.item[dmap[plyr.x+x][plyr.y+y].trki]].y*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2000)
masked_blit(maskedsmalltiles, tiletemp, gpilex*tileres, gpiley*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2001)
masked_blit(maskedsmalltiles, tiletemp, skeyx*tileres, skeyy*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2002)
masked_blit(maskedsmalltiles, tiletemp, pstonex*tileres, pstoney*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2003)
masked_blit(maskedsmalltiles, tiletemp, chestx[0]*tileres, chesty[0]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2004)
masked_blit(maskedsmalltiles, tiletemp, chestx[1]*tileres, chesty[1]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2005)
masked_blit(maskedsmalltiles, tiletemp, chestx[2]*tileres, chesty[2]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2006)
masked_blit(maskedsmalltiles, tiletemp, chestx[3]*tileres, chesty[3]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2007)
masked_blit(maskedsmalltiles, tiletemp, chestx[4]*tileres, chesty[4]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==2008)
masked_blit(maskedsmalltiles, tiletemp, chestx[5]*tileres, chesty[5]*tileres, 0, 0, tileres, tileres);
else if ((dgn.item[dmap[plyr.x+x][plyr.y+y].trki]>2999)&&(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3999))
{
if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3004)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3000))*tileres, trade.booky[0]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3008)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[1]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3004))*tileres, trade.booky[1]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3012)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[2]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3008))*tileres, trade.booky[2]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3016)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[3]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3012))*tileres, trade.booky[3]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3020)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[4]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3016))*tileres, trade.booky[4]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3024)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[5]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3020))*tileres, trade.booky[5]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3028)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[6]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3024))*tileres, trade.booky[6]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3035)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[0]+4)*tileres, (trade.booky[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3028))*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3042)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[0]+5)*tileres, (trade.booky[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3035))*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<3049)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[0]+6)*tileres, (trade.booky[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3042))*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<4000)
masked_blit(maskedsmalltiles, tiletemp, (trade.bookx[0]+7)*tileres, (trade.booky[0]+(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-3049))*tileres, 0, 0, tileres, tileres);
}
else if ((dgn.item[dmap[plyr.x+x][plyr.y+y].trki]>3999)&&(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]<4008))
masked_blit(maskedsmalltiles, tiletemp, trade.bonesx[(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-4000)]*tileres, trade.bonesy[(dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-4000)]*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==4008)
masked_blit(maskedsmalltiles, tiletemp, trade.ruinsx*tileres, trade.ruinsy*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==5000)
masked_blit(maskedsmalltiles, tiletemp, trade.nshadex*tileres, trade.nshadey*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==5001)
masked_blit(maskedsmalltiles, tiletemp, trade.mossx*tileres, trade.mossy*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]==5002)
masked_blit(maskedsmalltiles, tiletemp, trade.pearlx*tileres, trade.pearly*tileres, 0, 0, tileres, tileres);
else if (dgn.item[dmap[plyr.x+x][plyr.y+y].trki]>5999)
masked_blit(maskedsmalltiles, tiletemp, runex[dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-6000]*tileres,
            runey[dgn.item[dmap[plyr.x+x][plyr.y+y].trki]-6000]*tileres, 0, 0, tileres, tileres);
}
if ((plyr.dlvl==99)&&(dgn.sdnx==plyr.x+x)&&(dgn.sdny==plyr.y+y))
masked_blit(maskedsmalltiles, tiletemp, (xantosx*tileres), (xantosy*tileres), 0, 0, tileres, tileres);
if ((x==tiledraw.cx)&&(y==tiledraw.cy))
{
q=-1;
for (z=0; z<4; ++z)
if (plyr.curse[z]==2) q=0;
if (q==-1) masked_blit(maskedsmalltiles, tiletemp, (plyr.icnx*tileres), (plyr.icny*tileres), 0, 0, tileres, tileres);
else masked_blit(maskedsmalltiles, tiletemp, (plyr.pmorx*tileres), (plyr.pmory*tileres), 0, 0, tileres, tileres);
}
// darken tiles that are explored but not in field of view
set_alpha_blender();
if (viewt[x][y]==-1)
draw_trans_sprite(tiletemp,darken,0,0);
// blit the path marker if it's active
if ((moveflag[x][y]!=-1)&&!((x==tiledraw.cx)&&(y==tiledraw.cy))) masked_blit(maskedsmalltiles, tiletemp, ((moveflag[x][y])*tileres), (27*tileres), 0, 0, tileres, tileres);
}
}
blit(tiletemp, screentemp, 0, 0, (x*tileres)+tiledraw.sx, (y*tileres)+tiledraw.sy, tileres, tileres);
}
}
plyr.x = plyr.x + tiledraw.cx;
plyr.y = plyr.y + tiledraw.cy;
}

void recalcdraw()
{
// determine draw size based on tile size versus resolution

tiledraw.x=screenresx/tileres;
if (!(tiledraw.x % 2)) ++tiledraw.x;
if ((tiledraw.x*tileres)<=screen->w) tiledraw.x=tiledraw.x+2;
tiledraw.y=screenresy/tileres;
if (!(tiledraw.y % 2)) ++tiledraw.y;
if ((tiledraw.y*tileres)<=screen->h) tiledraw.y=tiledraw.y+2;

tiledraw.cx=(tiledraw.x/2);
tiledraw.cy=(tiledraw.y/2);

// determine where to start drawing tiles

tiledraw.sx=(screen->w-(tiledraw.x*tileres))/2;
tiledraw.sy=(screen->h-(tiledraw.y*tileres))/2;

}

void deletechar()
{
int x, y;
char temp[80], temp2[10];
for (y=0; ((y<charcount)&&(strcmp(charlist[y],plyr.name)!=0)); ++y);
if (y==charcount) {allegro_message("Error");}
else
{
for (x=y; x<23; ++x)
strcpy(charlist[x],charlist[x+1]);
strcpy(charlist[23],"none");
--charcount;
inputfile = fopen("Saves//charlist.dat","w");

      if (!inputfile) {
      set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
      allegro_message("Can't open Saves\\charlist.dat.  Exiting.");
      return 1;}

for (x=0; x<24; ++x) fprintf(inputfile, "%s\n", charlist[x]);
fclose(inputfile);
strcpy(temp,"Saves//");
y=strlen(plyr.name);
if (y>8) y=8;
strncat(temp, plyr.name, y);
strcat(temp, ".dat");
delete_file(temp);
strcpy(temp,"Saves//");
y=strlen(plyr.name);
if (y>8) y=8;
strncat(temp, plyr.name, y);
strcat(temp, ".log");
delete_file(temp);
for (x=1; x<100; ++x)
{
strcpy(temp,"Saves//");
y=strlen(plyr.name);
if (y>8) y=8;
strncat(temp, plyr.name, y);
strcat(temp, ".L");
sprintf(temp2,"%d",x);
strcat(temp,temp2);
delete_file(temp);
}
}
}

void equiprecalc()
{
int x, y, z;
plyr.adjstr=0; plyr.adjchr=0; plyr.adjitl=0; plyr.adjdex=0; plyr.deflect=0;
plyr.adjhp=0; plyr.adjmp=0;
if (plyr.equipped[9]!=-1) {plyr.dmgl=plyr.invl[plyr.equipped[9]]; plyr.dmgh=plyr.invh[plyr.equipped[9]];}
else {plyr.dmgl=1; plyr.dmgh=1;}
for (x=0; x<7; ++x) plyr.resist[x]=0;
for (x=0; x<10; ++x)
{
if (plyr.equipped[x]!=-1)
{
if ((x<5)||(x==8))
{
if ((x==8)&&(strcmp(plyr.clname,"Cleric")==0)&&(plyr.trade==1)&&(plyr.scholar==99)) plyr.deflect=plyr.deflect+(plyr.invh[plyr.equipped[x]]*2);
else plyr.deflect=plyr.deflect+plyr.invh[plyr.equipped[x]];
}
if (x<9)
{
z=0;
for (y=0; y<5; ++y)
if (plyr.invadd[plyr.equipped[x]][y]==4)
if (plyr.invaddamt[plyr.equipped[x]][y]>z) z=plyr.invaddamt[plyr.equipped[x]][y];
if (z!=0)
{
plyr.adjhp=plyr.adjhp+((plyr.hpm*z)/100);
plyr.adjmp=plyr.adjmp+((plyr.mpm*z)/100);
}
}
if ((x<9)&&(plyr.invelem[plyr.equipped[x]]!=-1))
{
if (plyr.invelem[plyr.equipped[x]]==7)
{
for (y=0; y<7; ++y)
plyr.resist[y]=plyr.resist[y]+20;
}
else if (plyr.invelem[plyr.equipped[x]]<100)
plyr.resist[plyr.invelem[plyr.equipped[x]]]=plyr.resist[plyr.invelem[plyr.equipped[x]]]+10+(plyr.invlvl[plyr.equipped[x]]*0.707);
else
{
// new code for 1.13 resist changes
if (plyr.invelem[plyr.equipped[x]]<200)
plyr.resist[0]=plyr.resist[0]+(plyr.invelem[plyr.equipped[x]]-100);
else if (plyr.invelem[plyr.equipped[x]]<300)
plyr.resist[1]=plyr.resist[1]+(plyr.invelem[plyr.equipped[x]]-200);
else if (plyr.invelem[plyr.equipped[x]]<400)
plyr.resist[2]=plyr.resist[2]+(plyr.invelem[plyr.equipped[x]]-300);
else if (plyr.invelem[plyr.equipped[x]]<500)
plyr.resist[3]=plyr.resist[3]+(plyr.invelem[plyr.equipped[x]]-400);
else if (plyr.invelem[plyr.equipped[x]]<600)
plyr.resist[4]=plyr.resist[4]+(plyr.invelem[plyr.equipped[x]]-500);
else if (plyr.invelem[plyr.equipped[x]]<700)
plyr.resist[5]=plyr.resist[5]+(plyr.invelem[plyr.equipped[x]]-600);
else if (plyr.invelem[plyr.equipped[x]]<800)
plyr.resist[6]=plyr.resist[6]+(plyr.invelem[plyr.equipped[x]]-700);
else if (plyr.invelem[plyr.equipped[x]]<900)
{
for (y=0; y<7; ++y)
plyr.resist[y]=plyr.resist[y]+(plyr.invelem[plyr.equipped[x]]-800);
}
}
}
for (y=0; y<5; ++y)
{
if (plyr.invadd[plyr.equipped[x]][y]==0) plyr.adjstr=plyr.adjstr+plyr.invaddamt[plyr.equipped[x]][y];
if (plyr.invadd[plyr.equipped[x]][y]==1) plyr.adjdex=plyr.adjdex+plyr.invaddamt[plyr.equipped[x]][y];
if (plyr.invadd[plyr.equipped[x]][y]==2) plyr.adjitl=plyr.adjitl+plyr.invaddamt[plyr.equipped[x]][y];
if (plyr.invadd[plyr.equipped[x]][y]==3) plyr.adjchr=plyr.adjchr+plyr.invaddamt[plyr.equipped[x]][y];
}
}
}
if (plyr.poisonexpert!=-1) plyr.resist[6]=plyr.resist[6]+20;
for (x=0; x<10; ++x)
{
if (plyr.buffx[x]!=-1)
{
if ((plyr.buffx[x]==4)&&(plyr.buffy[x]==2)) plyr.adjstr=plyr.adjstr+(plyr.str/10);
if ((plyr.buffx[x]==5)&&(plyr.buffy[x]==2)) plyr.adjdex=plyr.adjdex+(plyr.dex/10);
if ((plyr.buffx[x]==6)&&(plyr.buffy[x]==2)) plyr.adjitl=plyr.adjitl+(plyr.itl/10);
if ((plyr.buffx[x]==7)&&(plyr.buffy[x]==2)) plyr.adjchr=plyr.adjchr+(plyr.chr/10);
if ((plyr.buffx[x]==4)&&(plyr.buffy[x]==13)) {plyr.dmgl=plyr.dmgl+(plyr.dmgl/10)+(plyr.dmgl/20); plyr.dmgh=plyr.dmgh+(plyr.dmgh/10)+(plyr.dmgh/20); plyr.deflect=plyr.deflect-(plyr.deflect/10)-(plyr.deflect/20);}
if ((plyr.buffx[x]==9)&&(plyr.buffy[x]==3)) {plyr.resist[0]=plyr.resist[0]+20;}
if ((plyr.buffx[x]<6)&&(plyr.buffy[x]==4)) {plyr.resist[plyr.buffx[x]+1]=plyr.resist[plyr.buffx[x]+1]+20;}
if ((plyr.buffx[x]==1)&&(plyr.buffy[x]==11)) {plyr.resist[6]=plyr.resist[6]+30;}
if ((plyr.buffx[x]==6)&&(plyr.buffy[x]==7)&&(plyr.buffmod[x]==2)) {plyr.adjdex=plyr.adjdex+(plyr.dex/10); plyr.adjitl=plyr.adjitl+(plyr.itl/10);}
if ((plyr.buffx[x]==7)&&(plyr.buffy[x]==9)&&(plyr.buffmod[x]==2)) plyr.deflect=plyr.deflect+(plyr.deflect/5);
else if ((plyr.buffx[x]==7)&&(plyr.buffy[x]==9)) plyr.deflect=plyr.deflect+(plyr.deflect/10);
if ((plyr.buffx[x]==0)&&(plyr.buffy[x]==8)&&(plyr.buffmod[x]==1)) plyr.adjstr=plyr.adjstr+(plyr.str/5);
else if ((plyr.buffx[x]==0)&&(plyr.buffy[x]==8)) plyr.adjstr=plyr.adjstr+(plyr.str/10);
if ((plyr.buffx[x]==1)&&(plyr.buffy[x]==5)) plyr.deflect=plyr.deflect-(plyr.deflect/10);
if ((plyr.buffx[x]==2)&&(plyr.buffy[x]==9)&&(plyr.buffmod[x]==2)) plyr.adjitl=plyr.adjitl+(plyr.itl/10);
if ((plyr.buffx[x]==0)&&(plyr.buffy[x]==10)) plyr.adjitl=plyr.adjitl+(plyr.itl/5);
if ((plyr.buffx[x]==6)&&(plyr.buffy[x]==6)&&(plyr.buffmod[x]==1)) {plyr.adjstr=plyr.adjstr+(plyr.str/10)+(plyr.str/20); plyr.adjitl=plyr.adjitl+(plyr.itl/10)+(plyr.itl/20);}
else if ((plyr.buffx[x]==6)&&(plyr.buffy[x]==6)) {plyr.adjstr=plyr.adjstr+(plyr.str/10); plyr.adjitl=plyr.adjitl+(plyr.itl/10);}
if ((plyr.buffx[x]==9)&&(plyr.buffy[x]==4))
{
plyr.adjstr=plyr.adjstr+plyr.str;
plyr.adjdex=plyr.adjdex+plyr.dex;
plyr.adjitl=plyr.adjitl+plyr.itl;
plyr.adjchr=plyr.adjchr+plyr.chr;
}
}
}
//if ((strcmp(plyr.clname,"Warrior")==0)&&(plyr.trade==1)&&(plyr.scholar==99)) plyr.adjstr=plyr.adjstr+(plyr.str/10)+(plyr.str/20);
if ((strcmp(plyr.clname,"Thief")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) plyr.adjdex=plyr.adjdex+(plyr.dex/10);
if ((strcmp(plyr.clname,"Barbarian")==0)&&(plyr.trade==1)&&(plyr.scholar>49)&&(iscombat!=-1)) plyr.deflect=plyr.deflect+(plyr.berserking*(plyr.deflect/10));
if ((strcmp(plyr.clname,"Monk")==0)&&(plyr.scholar==99)) plyr.deflect=plyr.deflect*1.3;
if ((plyr.invmagic[plyr.equipped[8]]==3)&&(plyr.invbonus[plyr.equipped[8]]==1))
{
plyr.adjhp=plyr.hpm/5;
plyr.adjmp=plyr.mpm/5;
}
else if (plyr.invmagic[plyr.equipped[8]]==3)
{
plyr.adjhp=(plyr.hpm/10)+(plyr.hpm/20);
plyr.adjmp=(plyr.mpm/10)+(plyr.mpm/20);
}
if (plyr.invmagic[plyr.equipped[9]]==3)
{
plyr.adjhp=plyr.adjhp+(plyr.hpm/10)+(plyr.hpm/20);
if ((strcmp(plyr.clname,"Warrior")==0)||(strcmp(plyr.clname,"Barbarian")==0)||
    (strcmp(plyr.clname,"Paladin")==0)||(strcmp(plyr.clname,"Spellblade")==0)||
    (strcmp(plyr.clname,"Nomad")==0))
plyr.adjstr=plyr.adjstr+(plyr.str/5);
if ((strcmp(plyr.clname,"Cleric")==0)||(strcmp(plyr.clname,"Mage")==0)||(strcmp(plyr.clname,"Warlock")==0))
plyr.adjitl=plyr.adjitl+(plyr.itl/5);
if ((strcmp(plyr.clname,"Thief")==0)||(strcmp(plyr.clname,"Monk")==0))
plyr.adjdex=plyr.adjdex+(plyr.dex/5);
}
for (x=0; x<4; ++x)
{
if (plyr.curse[x]==1)
{
plyr.adjstr=plyr.adjstr-(plyr.str/4);
plyr.adjdex=plyr.adjdex-(plyr.dex/4);
plyr.adjitl=plyr.adjitl-(plyr.itl/4);
plyr.adjchr=plyr.adjchr-(plyr.chr/4);
}
if (plyr.curse[x]==2)
{
plyr.adjstr=(-plyr.str)+1;
plyr.adjdex=(-plyr.dex)+1;
plyr.adjitl=(-plyr.itl)+1;
plyr.adjchr=(-plyr.chr)+1;
}
if (plyr.curse[x]==3)
{
plyr.deflect=plyr.deflect/2;
}
}
for (x=0; x<7; ++x)
{
if (plyr.resist[x]>99) plyr.resist[x]=99;
}
}

void lostbuff(int which)
{
int x;
for (x=which; (x<9); ++x)
{
plyr.buffx[x]=plyr.buffx[x+1];
plyr.buffy[x]=plyr.buffy[x+1];
plyr.buffmod[x]=plyr.buffmod[x+1];
plyr.buffdur[x]=plyr.buffdur[x+1];
}
plyr.buffx[9]=-1; plyr.buffmod[9]=0;
equiprecalc();
}


void lostdebuff(int which)
{
int x;
for (x=which; (x<3); ++x)
{
plyr.curse[x]=plyr.curse[x+1];
plyr.cursedur[x]=plyr.cursedur[x+1];
}
plyr.curse[3]=-1;
equiprecalc();
}


int endofturn()
{
// checks stairs, opens doors on newly moved squares, decreases buff durations and
// increments the plyr.turn
int x, y, z, spec;
char temp[40], temp2[6];
    z=-1; spec=-1;
    ++plyr.turns;
    plyr.mpregen=plyr.mpregen+1+((plyr.mpm+plyr.adjmp)/100);
    if (plyr.mpregen>=10)
    {if (plyr.mp<(plyr.mpm+plyr.adjmp)) plyr.mp=plyr.mp+1+((plyr.mpm+plyr.adjmp)/250);
    plyr.mpregen=0;}
    if (plyr.mp>(plyr.mpm+plyr.adjmp)) plyr.mp=(plyr.mp+plyr.adjmp);
    for (x=0; x<10; ++x)
    {
    if (plyr.buffx[x]!=-1) {--plyr.buffdur[x]; if (plyr.buffdur[x]==0) lostbuff(x);}
    }
    for (x=0; x<4; ++x)
    {
    if (plyr.curse[x]==0) {++plyr.poisontick; if (plyr.poisontick==5) {plyr.poisontick=0; --plyr.hp;}}
    if (plyr.curse[x]!=-1) {--plyr.cursedur[x]; if (plyr.cursedur[x]==0) lostdebuff(x);}
    }
    for (x=0; x<128; ++x)
    {
    if ((dgn.doorx[x]==plyr.x)&&(dgn.doory[x]==plyr.y)&&
        (dgn.door[x]==0)) z=x;
    }
    if (z!=-1) {dgn.door[z]=4; ++dgn.icnx[plyr.x][plyr.y];}

    if ((plyr.x==dgn.supx)&&(plyr.y==dgn.supy))
    {
    if (plyr.dlvl==1) x=draw_menu(CENTER, CENTER, 23, 1, "Take the stairs back upto town?", 2);
    else x=draw_menu(CENTER, CENTER, 25, 1, "There are stairs up here.Take them?", 2);
    if ((x==0)&&(plyr.dlvl==1))
    {
    savedlvl();
    savechar();
    if (plyr.curse[0]!=-1)
    {
    curedebuff(-1);
    addlog("The healing aura of Velia removes your curses.",0);
    }
    return 1;
    }
    else if (x==0)
    {
    savedlvl();
    savechar();
    --plyr.dlvl;
    if (plyr.savedlvl[plyr.dlvl-1]==-1)
    {
    randdungeon();
    savedlvl();
    plyr.savedlvl[plyr.dlvl-1]=0;
    }
    else
    loaddlvl();
    plyr.x=dgn.sdnx;
    plyr.y=dgn.sdny;
    }
    }
    else if ((plyr.x==dgn.sdnx)&&(plyr.y==dgn.sdny))
    {
    if (plyr.dlvl==99) x=draw_menu(CENTER, CENTER, 21, 1, "Confront Lord Xantos?", 2);
    else x=draw_menu(CENTER, CENTER, 27, 1, "There are stairs down here.Take them?", 2);
    if ((x==0)&&(plyr.dlvl==99))
    {
    plyr.x=dgn.sdnx;
    plyr.y=dgn.sdny+1;
    doxantosintro();
    drawxantostiles();
    y=combatevent(9999,0);
    iscombat=-1;
    equiprecalc();
    if (y==0)
    {
    gamecredits(0);
    savedlvl();
    savechar();
    return 2;
    }
    else spec=1;
    }
    else if (x==0)
    {
    savedlvl();
    savechar();
    if (plyr.dlvl==plyr.llevel)
    {
    sprintf(temp, "Reached dungeon level %d.", plyr.dlvl+1);
    addlog(temp,0);
    playsound("dlevelup.wav");
    if (mission==2) missioncomplete();
    }
    if (((plyr.dlvl==4)&&(plyr.llevel==4))||((plyr.dlvl==24)&&(plyr.llevel==24))
        ||((plyr.dlvl==59)&&(plyr.llevel==59))||((plyr.dlvl==98)&&(plyr.llevel==98)))
    {
    addlog("Return to the Hall of Heroes for a reward.",0);
    }
    ++plyr.dlvl;
    if (plyr.llevel<plyr.dlvl) plyr.llevel=plyr.dlvl;
    if (plyr.savedlvl[plyr.dlvl-1]==-1)
    {
    randdungeon();
    savedlvl();
    plyr.savedlvl[plyr.dlvl-1]=0;
    }
    else
    loaddlvl();
    plyr.x=dgn.supx;
    plyr.y=dgn.supy;
    }
    }
    if ((plyr.hp<1)&&(spec!=1)) {x=playerdied(-1); if (x==0) return 3; else return 2;}
    else if (plyr.hp<1)
    {
    playerdied(1);
    plyr.hp=(plyr.hpm+plyr.adjhp);
    plyr.mp=(plyr.mpm+plyr.adjmp);
    plyr.gold=plyr.gold/2;
    for (x=0; x<10; ++x)
    {
    plyr.buffx[x]=-1;
    if (x<4) plyr.curse[x]=-1;
    }
    savechar();
    randshops();
    equiprecalc();
    return 1;
    }
    if ((plyr.berserking>0)&&(plyr.trade==1)&&(plyr.scholar>49)) equiprecalc();
    return 0;
}


void missionprogress()
{
int x, y;
char tempstr[50];
--missioncount;
if (missioncount==0) missioncomplete();
else
{
if (missiontyp==0)
sprintf(tempstr,"Mission Progressed. Defeat %d more beasts.",missioncount);
if (missiontyp==1)
sprintf(tempstr,"Mission Progressed. Defeat %d more humanoids.",missioncount);
if (missiontyp==2)
sprintf(tempstr,"Mission Progressed. Defeat %d more reptiles.",missioncount);
if (missiontyp==3)
sprintf(tempstr,"Mission Progressed. Defeat %d more insects.",missioncount);
if (missiontyp==4)
sprintf(tempstr,"Mission Progressed. Defeat %d more undead.",missioncount);
if (missiontyp==5)
sprintf(tempstr,"Mission Progressed. Defeat %d more apparitions.",missioncount);
if (missiontyp==6)
sprintf(tempstr,"Mission Progressed. Defeat %d more golems.",missioncount);
if (missiontyp==7)
sprintf(tempstr,"Mission Progressed. Defeat %d more dragons.",missioncount);
if (missiontyp==8)
sprintf(tempstr,"Mission Progressed. Defeat %d more elementals.",missioncount);
if (missiontyp==9)
sprintf(tempstr,"Mission Progressed. Defeat %d more demons.",missioncount);
if (missiontyp==10)
sprintf(tempstr,"Mission Progressed. Defeat %d more ooze.",missioncount);
addlog(tempstr,0);
}
}

void missioncomplete()
{
int x, y, bonus, c;
char tempstr[50];
mission=-1; bonus=30;
for (x=0; x<plyr.lvl; ++x)
{
y=(bonus*0.093); if (y<1) y=1;
bonus=bonus+y;
}
if (plyr.lvl<99)
{
plyr.xp=plyr.xp+bonus;
c=randomnum(1,20);
if (c!=20)
sprintf(tempstr,"Mission complete. Gained %d XP.",bonus);
else
{
if ((plyr.itl>plyr.str)&&(plyr.itl>plyr.dex))
{
++plyr.itl;
sprintf(tempstr,"Mission complete. Gained %d XP and +1 intelligence.",bonus);
}
else if (plyr.dex>plyr.str)
{
++plyr.dex;
sprintf(tempstr,"Mission complete. Gained %d XP and +1 dexterity.",bonus);
}
else
{
++plyr.str;
sprintf(tempstr,"Mission complete. Gained %d XP and +1 strength.",bonus);
}
}
addlog(tempstr,0);
if (plyr.xp>plyr.xpn) gainedlevel();
}
else
{
c=randomnum(0,1);
if (c!=1)
{
addlog("Mission complete.",0);
}
else
{
c=randomnum(1,4);
if (c=4) x=2;
else x=1;
if ((plyr.itl>plyr.str)&&(plyr.itl>plyr.dex))
{
plyr.itl=plyr.itl+x;
sprintf(tempstr,"Mission complete. Gained +%d intelligence.",x);
}
else if (plyr.dex>plyr.str)
{
plyr.dex=plyr.dex+x;
sprintf(tempstr,"Mission complete. Gained +%d dexterity.",x);
}
else
{
plyr.str=plyr.str+x;
sprintf(tempstr,"Mission complete. Gained +%d strength.",x);
}
addlog(tempstr);
}
}
addlog("Return to town for a new mission.",1);
}


void assignfuncmacro(int which, int mx, int my)
{
int x;
for (x=0; x<12; ++x)
if (((plyr.funcmacrox[x]==mx)&&(plyr.funcmacroy[x]==-1))
    ||((plyr.funcmacrox[x]==mx)&&(plyr.funcmacroy[x]==my)))
     {plyr.funcmacrox[x]=-1; plyr.funcmacroy[x]=-1;}
plyr.funcmacrox[which]=mx;
plyr.funcmacroy[which]=my;
}

int checkfunckeys()
{
if ((key[KEY_F1])||(key[KEY_F2])||(key[KEY_F3])||(key[KEY_F4])||(key[KEY_F5])||
    (key[KEY_F6])||(key[KEY_F7])||(key[KEY_F8])||(key[KEY_F9])||(key[KEY_F10])||
    (key[KEY_F11])||(key[KEY_F12]))
    return 1;
else
    return 0;
}

int checknumkeys()
{
if ((key[KEY_1])||(key[KEY_2])||(key[KEY_3])||(key[KEY_4]))
{
if (key[KEY_1]) return 0;
if (key[KEY_2]) return 1;
if (key[KEY_3]) return 2;
if (key[KEY_4]) return 3;
}
    return -1;
}


int spelldamage(int x)
{
int a, y;
if ((plyr.equipped[9]!=-1)&&((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4)))
{
for (a=0; a<5; ++a)
if (plyr.invadd[plyr.equipped[9]][a]==4)
x=x+((x*plyr.invaddamt[plyr.equipped[9]][a])/100);
}
for (a=0; a<10; ++a)
{
if ((plyr.buffx[a]==7)&&(plyr.buffy[a]==5)) x=x+(x/2);
if ((plyr.buffx[a]==2)&&(plyr.buffy[a]==9)&&(plyr.buffmod[a]>0))
{
y=5+randomnum((plyr.itl+plyr.adjitl)/30,(plyr.itl+plyr.adjitl)/15);
if (mons.resist[4]>0) y=y-((mons.resist[4]*y)/132);
x=x+y;
}
}
a=x;
if ((strcmp(plyr.clname,"Mage")==0)&&(plyr.trade==1)&&(plyr.scholar==99))
{
y=randomnum(1,100);
if (y<16)
{
x=x*2;
addlog("Spellblast triggers and damage is boosted.",0);
}
}
if (x==a)
{
if ((plyr.equipped[9]!=-1)&&((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4)))
{
for (a=0; a<5; ++a)
if (plyr.invadd[plyr.equipped[9]][a]==5)
{
y=randomnum(1,10);
if (y==10)
{
x=x*2;
addlog("Potency triggers and damage is boosted.",0);
}
}
}
}
return x;
}

void namejewelry()
{
char temp[30], temp2[30];
if (tempitem.cls==5) strcpy(temp,"Ring");
if (tempitem.cls==6) strcpy(temp,"Pendant");
if ((tempitem.elem==-1)&&(tempitem.add[0]==-1))
{
strcpy(temp2,"Simple ");
strcat(temp2, temp);
strcpy(tempitem.name,temp2);
}
else
{
if (tempitem.elem!=-1)
{
if (tempitem.elem<100)
{
if (tempitem.elem==0) strcpy(temp2," of Wind");
if (tempitem.elem==1) strcpy(temp2," of Fire");
if (tempitem.elem==2) strcpy(temp2," of Water");
if (tempitem.elem==3) strcpy(temp2," of Earth");
if (tempitem.elem==4) strcpy(temp2," of Shadow");
if (tempitem.elem==5) strcpy(temp2," of Light");
if (tempitem.elem==6) strcpy(temp2," of Venom");
}
else
{
if (tempitem.elem<200)
strcpy(temp2," of Wind");
else if (tempitem.elem<300)
strcpy(temp2," of Fire");
else if (tempitem.elem<400)
strcpy(temp2," of Water");
else if (tempitem.elem<500)
strcpy(temp2," of Earth");
else if (tempitem.elem<600)
strcpy(temp2," of Shadow");
else if (tempitem.elem<700)
strcpy(temp2," of Light");
else if (tempitem.elem<800)
strcpy(temp2," of Venom");
else if (tempitem.elem<900)
strcpy(temp2," of Prism");
}
strcat(temp, temp2);
}
if (tempitem.add[0]!=-1)
{
if (tempitem.add[0]==0) strcpy(temp2,"Strong ");
if (tempitem.add[0]==1) strcpy(temp2,"Agile ");
if (tempitem.add[0]==2) strcpy(temp2,"Sly ");
if (tempitem.add[0]==3) strcpy(temp2,"Bold ");
strcat(temp2, temp);
}
else
strcpy(temp2, temp);
strcpy(tempitem.name,temp2);
}
}

int fetchfkeynum()
{
if (key[KEY_F1]) return 0;
else if (key[KEY_F2]) return 1;
else if (key[KEY_F3]) return 2;
else if (key[KEY_F4]) return 3;
else if (key[KEY_F5]) return 4;
else if (key[KEY_F6]) return 5;
else if (key[KEY_F7]) return 6;
else if (key[KEY_F8]) return 7;
else if (key[KEY_F9]) return 8;
else if (key[KEY_F10]) return 9;
else if (key[KEY_F11]) return 10;
else if (key[KEY_F12]) return 11;
else return -1;
}

int fetchmousex()
{
int x;
//return (1280*mouse_x)/screenresx;
return mouse_x;
}

int fetchmousey()
{
//return (mouse_y*900)/screenresy;
return mouse_y;
}

int scaley(int x)
{
return (screenresy*x)/900;
}

int scalex(int y)
{
int z;
z = (screenresy / 3) * 4;
return (z*y)/1280;
}

void playsound(char *fname)
{
char temp[40];
if (sound==1)
{
strcpy(temp,"sounds//");
strcat(temp,fname);
soundfile=load_sample(temp);
play_sample(soundfile,255,127,1000,0);
}
}


void playmusic(char *fname, int flag)
{
char temp[20];
if (midi==1)
{
strcpy(temp,"music//");
strcat(temp,fname);
if ((flag==0)||(strcmp(temp,lastmidifile)!=0))
{
musicfile = load_midi(temp);
play_midi(musicfile,TRUE);
strcpy(lastmidifile,temp);
}
}
}

void resumedungeonmusic()
{
char temp[24], temp2[3];
int x;
if (midi==1)
{
play_midi(NULL,FALSE);
strcpy(temp,"music//Dungeon");
sprintf(temp2, "%d", dmusictrack);
strcat(temp,temp2);
strcat(temp,".mid");
musicfile=load_midi(temp);
play_midi(musicfile,FALSE);
while (midi_pos<0)
{
}
x=midi_seek(dmusicpos);
}
}

void pausedungeonmusic()
{
if (midi==1)
{
dmusicpos=midi_pos;
}
}


void dungeonmusicloop()
{
char temp[24], temp2[3];
if (midi==1)
{
if (midi_pos<0)
{
strcpy(temp,"music//Dungeon");
++dmusictrack;
if (dmusictrack>8) dmusictrack=1;
sprintf(temp, "music//Dungeon%d.mid", dmusictrack);
musicfile=load_midi(temp);
play_midi(musicfile,FALSE);
}
}
}


void blitscreen()
{
int x;
show_mouse(NULL);
blit(screentemp,screen,0,0,0,0,screen->w,screen->h);
show_mouse(screen);
}

void clearplayervar()
{
int x, y;
        // clear out player variables
    plyr.lvl = 1; plyr.xp = 0; plyr.xpn=100; plyr.str=10; plyr.dex=10; plyr.itl=10; plyr.chr=10; plyr.spl=3; plyr.dpl=2; plyr.ipl=1; plyr.cpl=1;
    plyr.xpl=100; plyr.hpl=15; plyr.mpl=5; plyr.hp=20; plyr.mp=10; plyr.hpm=0; plyr.mpm=0; plyr.lockpick=-1; plyr.kills=0;
    plyr.deaths=0; plyr.hdamage=0; plyr.turns=0; plyr.llevel=1; plyr.twoh=-1;
    plyr.dextodmg=-1; plyr.lucky=-1; plyr.berserking=-1; plyr.crusader=-1; plyr.siphon=-1;
    plyr.poisonexpert=-1; plyr.gold=100; plyr.mpregen=0; plyr.poisontick=0;
    plyr.bestxantos=0; plyr.combatturn=0;
    for (x=0; x<100; ++x)
    {
    plyr.invh[x]=-1; plyr.invl[x]=-1; plyr.invicnx[x]=-1; plyr.invicny[x]=-1; plyr.invnum[x]=-1; plyr.invlvl[x]=-1;  plyr.invtype[x]=-1;
    plyr.invmtype[x]=-1; plyr.invcls[x]=-1; plyr.invelem[x]=-1;
    plyr.invtreasure[x]=-1; plyr.invmagic[x]=-1; plyr.invbonus[x]=-1;
    for (y=0; y<5; ++y) {plyr.invadd[x][y]=-1; plyr.invaddamt[x][y]=-1;}
    strcpy(plyr.invname[x],"");
    }
    plyr.bkills=0;
    for (x=0; x<5; ++x)
    {
    plyr.levelquest[x]=-1; plyr.killquest[x]=-1; plyr.questmodb[x]=-1;
    }
    for (x=0; x<64; ++x)
    {
    for (y=0; y<28; ++y)
    {
    plyr.metsqr[x][y]=-1;
    }
    }
    plyr.deflect=0; plyr.dmgl=1; plyr.dmgh=1;
    for (x=0; x<10; ++x)
    {
    if (x<4) {plyr.curse[x]=-1; plyr.cursedur[x]=0;}
    plyr.buffx[x]=-1; plyr.buffdur[x]=0; plyr.buffmod[x]=0;
        if (x<7) {plyr.resist[x]=0; plyr.wskill[x]=1;}
    if (x<6) {plyr.magic[x]=-1; plyr.equipa[x]=-1; plyr.equipr[x]=-1; plyr.rwskill[x]=1;}
    for (y=0; y<10; ++y)
    {
    if ((y<3)&&(x<7)) plyr.equipmw[x][y]=-1;
    }
    }
    plyr.dlvl = 1;
    for (y=0; y<11; ++y) {plyr.equipped[y]=-1; if (x<10) {plyr.buffx[y]=-1; plyr.buffdur[y]=0; plyr.buffmod[y]=0;}}
    for (y=0; y<99; ++y) plyr.savedlvl[y]=-1;
    plyr.arch=0; plyr.alch=0; plyr.scholar=0; plyr.trade=-1; plyr.sex=0;
    for (x=0; x<4; ++x)
    {
    plyr.wswap[x][0]=-1; plyr.wswap[x][1]=-1;
    }
    for (x=0; x<12; ++x)
    {
    plyr.funcmacrox[x]=-1;
    plyr.funcmacroy[x]=-1;
    }
    equiprecalc();
}

void addtoinfo(int x, int y, int maxx, int maxy, char *icaption, char *icaption2)
{
infox[infoct][0]=x; infox[infoct][1]=maxx; infoy[infoct][0]=y; infoy[infoct][1]=maxy;
strcpy(infocapt[infoct],icaption);
if (strlen(icaption2)>1) strcpy(infocapt2[infoct],icaption2);
else strcpy(infocapt2[infoct],"");
++infoct;
}

void halficon(int bx, int by, int x, int y, int flag)
{
if (flag==2)
stretch_blit(redicons,screentemp,(bx*buttonsize),(by*buttonsize),buttonsize,buttonsize,x,y,buttonsize,buttonsize);
else if (flag==1)
stretch_blit(greyicons,screentemp,(bx*buttonsize),(by*buttonsize),buttonsize,buttonsize,x,y,buttonsize,buttonsize);
else
stretch_blit(icons,screentemp,(bx*buttonsize),(by*buttonsize),buttonsize,buttonsize,x,y,buttonsize,buttonsize);
}


void usefkeymacro(int which)
{
if (plyr.funcmacrox[which]!=-1)
{
if (plyr.funcmacroy[which]==-1)
{
equipuse(plyr.funcmacrox[which]);
}
else
{
castspell(plyr.funcmacrox[which],plyr.funcmacroy[which]);
}
}
}

void usehotkey(int which)
{
char temp[40], temp2[10];
if (plyr.wswap[which][0]==-1)
{
sprintf(temp, "Hotkey for Ctrl-%d not set.", which+1);
addlog(temp,1);
}
else
{
plyr.equipped[9]=plyr.wswap[which][0];
plyr.equipped[8]=plyr.wswap[which][1];
strcpy(temp,"Equipped ");
strcat(temp,plyr.invname[plyr.equipped[9]]);
strcat(temp,".");
addlog(temp,0);
if (plyr.equipped[8]!=-1)
{
strcpy(temp,"Equipped ");
strcat(temp,plyr.invname[plyr.equipped[8]]);
strcat(temp,".");
addlog(temp,0);
}
equiprecalc();
}
if (iscombat!=-1)
{
drawlog(2); blitscreen();
}
playsound("loot2.wav");
while (checknumkeys()!=-1)
{
}
}

void hotkey(int squarex, int squarey, int which)
{
int z;
char temp[40], temp2[10];
if (squarex!=-1)
{
z=squarey-1;  if (squarex==6) z=z+10;
z=z+(bagpage*20);
if ((plyr.wswap[which][0]==z)||(plyr.wswap[which][1]==z))
{
if (plyr.wswap[which][1]==z) plyr.wswap[which][1]=-1;
else
{
plyr.wswap[which][0]=-1; plyr.wswap[which][1]=-1;
}
strcpy(temp,plyr.invname[z]);
strcat(temp," removed from hotkey.");
addlog(temp,1);
}
else if ((plyr.invmtype[z]>2)||((plyr.invmtype[z]==2)&&(plyr.invcls[z]!=7))
    ||((plyr.invmtype[z]==2)&&(plyr.invcls[z]==7)&&(plyr.invmagic[z]!=3)&&(plyr.equipa[5]==-1)))
addlog("Can't hotkey this item.",1);
else if ((plyr.invmtype[z]==2)&&(plyr.invcls[z]==7)&&(plyr.wswap[which][0]==-1))
addlog("You must hotkey the weapon first.",1);
else if ((plyr.invmtype[z]==2)&&(plyr.invcls[z]==7)&&(plyr.invmtype[plyr.wswap[which][0]]==1)
         &&(plyr.invcls[plyr.wswap[which][0]]<2))
addlog("Can't hotkey a shield with a bow.",1);
else if ((plyr.invmtype[z]==2)&&(plyr.invcls[z]==7)&&(plyr.invmtype[plyr.wswap[which][0]]==0)
         &&(plyr.invtreasure[plyr.wswap[which][0]]==1))
addlog("Can't hotkey a shield with a 2H weapon.",1);
else if ((plyr.invmtype[z]==2)&&(plyr.invcls[z]==7))
{
sprintf(temp,"%s hotkeyed to Ctrl-%d.",plyr.invname[z],which+1);
addlog(temp,1);
plyr.wswap[which][1]=z;
}
else if (((plyr.invmtype[z]==1)&&(plyr.equipr[plyr.invcls[z]]==-1))||
         ((plyr.invmtype[z]==0)&&(plyr.equipmw[plyr.invcls[z]][plyr.invbonus[z]]==-1))||
         ((plyr.invmtype[z]==0)&&(plyr.twoh==-1)&&(plyr.invtreasure[z]==1)))
addlog("Can't equip this item.",1);
else if (plyr.invmtype[z]<2)
{
sprintf(temp,"%s hotkeyed to Ctrl-%d.",plyr.invname[z],which+1);
addlog(temp,1);
plyr.wswap[which][0]=z;
plyr.wswap[which][1]=-1;
}
}
if (iscombat!=-1)
{
drawlog(2); blitscreen();
}
while (checknumkeys()!=-1)
{
}
}

int equipuse(int item)
{
int x, y, c, z;
char temp[50], strint[10];
if (plyr.invtype[item]==2)
{
/*
plyr.equpped

   0 = head
   1 = chest
   2 = hands
   3 = feet
   4 = back
   5 = ring
   6 = ring2
   7 = necklace
   8 = shield
   9 = weapon
*/
// equippable items
if ((plyr.invmtype[item]<4)&&(plyr.invcls[item]<1000))
{
// melee weapon
if (plyr.invmtype[item]==0)
{
if (plyr.equipmw[plyr.invcls[item]][plyr.invbonus[item]]!=1)
    addlog("You can't equip that weapon.",1);
else if ((plyr.invmagic[item]==3)&&(plyr.trade!=0))
    addlog("That weapon requires Archaeology.",1);
else
{
if (plyr.equipped[9]!=item)
{
plyr.equipped[9]=item;
strcpy(temp, "Equipped ");
strcat(temp, plyr.invname[item]);
strcat(temp, ".");
addlog(temp,0);
if (plyr.invtreasure[item]==1) plyr.equipped[8]=-1;
equiprecalc();
playsound("loot1.wav");
}
}
}
//ranged weapon
if (plyr.invmtype[item]==1)
{
if (plyr.equipr[plyr.invcls[item]]!=1)
    addlog("You can't equip that weapon.",1);
else
{
if (plyr.equipped[9]!=item)
{
plyr.equipped[9]=item;
plyr.dmgl=plyr.invl[item]; plyr.dmgh=plyr.invh[item];
strcpy(temp, "Equipped ");
strcat(temp, plyr.invname[item]);
strcat(temp, ".");
playsound("loot1.wav");
addlog(temp,0);
if (plyr.invcls[item]<2) plyr.equipped[8]=-1;
equiprecalc();
}
}
}
// armor
if (plyr.invmtype[item]==2)
{
if ((plyr.invcls[item]<5)&&(plyr.equipa[plyr.invtreasure[item]]!=1))
    addlog("You can't equip that armor.",1);
else if ((plyr.invcls[item]==7)&&(plyr.invtreasure[item]==1)&&(plyr.trade!=0))
    addlog("That item requires Archaeology.",1);
else if ((plyr.invcls[item]==7)&&(plyr.equipa[5]!=1)&&(plyr.invtreasure[item]!=1))
    addlog("You can't equip shields.",1);
else if ((plyr.invtreasure[plyr.equipped[9]]==1)&&(plyr.invmtype[item]==2)&&(plyr.invcls[item]==7))
    addlog("Can't equip a shield with a 2h weapon.",0);
else
{
c=0;
for (x=0; x<9; ++x) if (plyr.equipped[x]==item) c=1;
if (c==0)
{
if (plyr.invcls[item]<5) plyr.equipped[plyr.invcls[item]]=item;
else if (plyr.invcls[item]==5)
{
if (plyr.equipped[5]==-1) plyr.equipped[5]=item;
else if (plyr.equipped[6]==-1) plyr.equipped[6]=item;
else
{
plyr.equipped[6]=plyr.equipped[5];
plyr.equipped[5]=item;
}
}
else plyr.equipped[plyr.invcls[item]+1]=item;
strcpy(temp, "Equipped ");
strcat(temp, plyr.invname[item]);
strcat(temp, ".");
addlog(temp,0);
playsound("loot1.wav");
equiprecalc();
}
}
}
if (plyr.invmtype[item]==3)
{
if (plyr.equipr[plyr.invcls[item]]==-1)
addlog("You can't equip that item.",0);
else
{
plyr.equipped[10]=item;
strcpy(temp, "Equipped ");
strcat(temp, plyr.invname[item]);
strcat(temp, ".");
addlog(temp,0);
playsound("loot1.wav");
equiprecalc();
}
}
}
if ((plyr.invmtype[item]==4)&&(plyr.invcls[item]==0))
{
z=randomnum(plyr.invl[item],plyr.invh[item]);
playerheal(z);
sprintf(temp,"Ate %s. Restored %d HP.",plyr.invname[item],z);
addlog(temp,0);
--plyr.invnum[item];
if (plyr.invnum[item]==0) lostitem(item);
playsound("Spell-Cure.wav");
}
else if ((plyr.invmtype[item]==4)&&(plyr.invcls[item]==1)&&(plyr.invtreasure[item]==7)&&(plyr.trade!=2))
addlog("That item requires Alchemist skill to use.",0);
else if ((plyr.invmtype[item]==4)&&(plyr.invcls[item]==1))
{
playsound("potion.wav");
if ((plyr.invtreasure[item]==0)&&(plyr.invelem[item]==0))
{
gotdebuff(0);
addlog("Drank a potion and was poisoned.",0);
}
if ((plyr.invtreasure[item]==0)&&(plyr.invelem[item]==1))
{
gotdebuff(1);
addlog("Drank a potion and was cursed with weakness.",0);
}
if ((plyr.invtreasure[item]==1)&&(plyr.invelem[item]==2))
{
z=randomnum(plyr.invl[item],plyr.invh[item]);
plyr.hp=plyr.hp-z;
sprintf(temp, "Drank a potion and took %d damage.", z);
addlog(temp,0);
}
if ((plyr.invtreasure[item]==1)&&(plyr.invelem[item]==0))
{
z=randomnum(plyr.invl[item],plyr.invh[item]);
playerheal(z);
sprintf(temp,"Drank a potion and regained %d HP.",z);
addlog(temp,0);
}
if ((plyr.invtreasure[item]==1)&&(plyr.invelem[item]==1))
{
z=randomnum(plyr.invl[item],plyr.invh[item]);
plyr.mp=plyr.mp+z;
if (plyr.mp>(plyr.mpm+plyr.adjmp)) plyr.mp=(plyr.mpm+plyr.adjmp);
sprintf(temp,"Drank a potion and regained %d MP.",z);
addlog(temp,0);
}
if (plyr.invtreasure[item]==2)
{
if (plyr.invelem[item]==0)
{
curedebuff(0);
addlog("Drank a potion and was cured of poison.",0);
if (iscombat==-1) {drawstatpane(0); blitscreen();}
}
else
{
curedebuff(-1);
addlog("Drank a potion and was cleansed.",0);
equiprecalc();
}
}
if (plyr.invtreasure[item]==3)
{
if (plyr.invelem[item]==0) {gotbuff(4,2,0,plyr.invlvl[item]); addlog("Drank a potion and gained might.",0);}
if (plyr.invelem[item]==1) {gotbuff(5,2,0,plyr.invlvl[item]); addlog("Drank a potion and gained agility.",0);}
if (plyr.invelem[item]==2) {gotbuff(6,2,0,plyr.invlvl[item]); addlog("Drank a potion and gained sage's mind.",0);}
if (plyr.invelem[item]==3) {gotbuff(7,2,0,plyr.invlvl[item]); addlog("Drank a potion and gained leadership.",0);}
}
if (plyr.invtreasure[item]==4)
{
if (plyr.invelem[item]==0) {gotbuff(4,13,0,plyr.invlvl[item]); addlog("Drank a potion and gained recklessness.",0);}
if (plyr.invelem[item]==1) {gotbuff(7,5,0,plyr.invlvl[item]); addlog("Drank a potion and gained spellstrike.",0);}
if (plyr.invelem[item]==2) {gotbuff(8,8,0,plyr.invlvl[item]); addlog("Drank a potion and gained luck.",0);}
if (plyr.invelem[item]==3) {gotbuff(8,5,0,plyr.invlvl[item]); addlog("Drank a potion and gained haste.",0);}
}
if (plyr.invtreasure[item]==5)
{
if (plyr.invelem[item]==0) gotbuff(9,3,0,plyr.invlvl[item]);
else if (plyr.invelem[item]<6) gotbuff(0+(plyr.invelem[item]-1),4,0,plyr.invlvl[item]);
else gotbuff(1,11,0,plyr.invlvl[item]);
addlog("Drank a potion and gained resistance.",0);
}
if (plyr.invtreasure[item]==6)
{
if (plyr.invelem[item]==0) {if (plyr.str<700) ++plyr.str; addlog("Drank a potion and strength increased by 1.",0);}
if (plyr.invelem[item]==1) {if (plyr.dex<700) ++plyr.dex; addlog("Drank a potion and dexterity increased by 1.",0);}
if (plyr.invelem[item]==2) {if (plyr.itl<700) ++plyr.itl; addlog("Drank a potion and intelligence increased by 1.",0);}
if (plyr.invelem[item]==3) {if (plyr.chr<700) ++plyr.chr; addlog("Drank a potion and charisma increased by 1.",0);}
}
if ((plyr.invtreasure[item]==7)&&(plyr.invelem[item]==0)&&(plyr.trade==2))
{
addlog("Drank an Elixir and restored HP/MP to full.",0);
plyr.hp=(plyr.hpm+plyr.adjhp);
plyr.mp=(plyr.mpm+plyr.adjmp);
}
if ((plyr.invtreasure[item]==7)&&(plyr.invelem[item]==1)&&(plyr.trade==2))
{gotbuff(9,4,0,plyr.invlvl[item]); addlog("Drank a potion and power increased.",0);}
--plyr.invnum[item];
if (plyr.invnum[item]==0) lostitem(item);
}
else if ((plyr.invmtype[item]==4)&&(plyr.invcls[item]==2))
{
if (plyr.magic[plyr.invelem[item]]!=-1)
{
c=0;
if (plyr.magic[plyr.invelem[item]]<40)
c=1;
else if (plyr.magic[plyr.invelem[item]]<65)
{
z=randomnum(0,1);
if (z==1) c=1;
}
else if (plyr.magic[plyr.invelem[item]]<90)
{
z=randomnum(0,2);
if (z==1) c=1;
}
else
{
z=randomnum(0,3);
if (z==1) c=1;
}
if (plyr.magic[plyr.invelem[item]]==99)
{
strcpy(temp,"Used ");
strcat(temp,plyr.invname[item]);
strcat(temp,", but is already at max level.");
addlog(temp,0);
}
else if (c==1)
{
strcpy(temp,"Used a ");
strcat(temp,plyr.invname[item]);
strcat(temp," and gained skill.");
playsound("Spell-Wind-Boon.wav");
++plyr.magic[plyr.invelem[item]];
if (plyr.magic[plyr.invelem[item]]<30) ++plyr.magic[plyr.invelem[item]];
addlog(temp,0);
if ((plyr.magic[plyr.invelem[item]]==10)&&(plyr.invelem[item]==0))
addlog("Learned the Charged Strike spell.",0);
if ((plyr.magic[plyr.invelem[item]]==20)&&(plyr.invelem[item]==0))
addlog("Learned the Cleanse spell.",0);
if ((plyr.magic[plyr.invelem[item]]==30)&&(plyr.invelem[item]==0))
addlog("Learned the Lightning Bolt spell.",0);
if ((plyr.magic[plyr.invelem[item]]==40)&&(plyr.invelem[item]==0))
addlog("Learned the Zephyr's Boon Level 2 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==55)&&(plyr.invelem[item]==0))
addlog("Learned the Cyclone Strike spell.",0);
if ((plyr.magic[plyr.invelem[item]]==75)&&(plyr.invelem[item]==0))
addlog("Learned the Storm spell.",0);
if ((plyr.magic[plyr.invelem[item]]==99)&&(plyr.invelem[item]==0))
addlog("Learned the Zephyr's Boon Level 3 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==10)&&(plyr.invelem[item]==1))
addlog("Learned the Firebolt spell.",0);
if ((plyr.magic[plyr.invelem[item]]==20)&&(plyr.invelem[item]==1))
addlog("Learned the Concussive Blast spell.",0);
if ((plyr.magic[plyr.invelem[item]]==30)&&(plyr.invelem[item]==1))
addlog("Learned the Inferno spell.",0);
if ((plyr.magic[plyr.invelem[item]]==40)&&(plyr.invelem[item]==1))
addlog("Learned the Fire Weapon Level 2 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==55)&&(plyr.invelem[item]==1))
addlog("Learned the Charged Blast spell.",0);
if ((plyr.magic[plyr.invelem[item]]==75)&&(plyr.invelem[item]==1))
addlog("Learned the Meteor spell.",0);
if ((plyr.magic[plyr.invelem[item]]==99)&&(plyr.invelem[item]==1))
addlog("Learned the Fire Weapon Level 3 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==10)&&(plyr.invelem[item]==2))
addlog("Learned the Lesser Healing Waters spell.",0);
if ((plyr.magic[plyr.invelem[item]]==20)&&(plyr.invelem[item]==2))
addlog("Learned the Wave spell.",0);
if ((plyr.magic[plyr.invelem[item]]==30)&&(plyr.invelem[item]==2))
addlog("Learned the Kraken's Strike spell.",0);
if ((plyr.magic[plyr.invelem[item]]==40)&&(plyr.invelem[item]==2))
addlog("Learned the Icy Shield L2 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==55)&&(plyr.invelem[item]==2))
addlog("Learned the Greater Healing Waters spell.",0);
if ((plyr.magic[plyr.invelem[item]]==75)&&(plyr.invelem[item]==2))
addlog("Learned the Mind of the Kraken spell.",0);
if ((plyr.magic[plyr.invelem[item]]==99)&&(plyr.invelem[item]==2))
addlog("Learned the Icy Shield L3 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==10)&&(plyr.invelem[item]==3))
addlog("Learned the Fortification Level 1 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==20)&&(plyr.invelem[item]==3))
addlog("Learned the Tremor spell.",0);
if ((plyr.magic[plyr.invelem[item]]==30)&&(plyr.invelem[item]==3))
addlog("Learned the Rock Volley spell.",0);
if ((plyr.magic[plyr.invelem[item]]==40)&&(plyr.invelem[item]==3))
addlog("Learned the Fortification Level 2 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==55)&&(plyr.invelem[item]==3))
addlog("Learned the Smash spell.",0);
if ((plyr.magic[plyr.invelem[item]]==75)&&(plyr.invelem[item]==3))
addlog("Learned the Mountain's Might Level 2 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==99)&&(plyr.invelem[item]==3))
addlog("Learned the Smash Level 2 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==10)&&(plyr.invelem[item]==4))
addlog("Learned the Touch of Shadow spell.",0);
if ((plyr.magic[plyr.invelem[item]]==20)&&(plyr.invelem[item]==4))
addlog("Learned the Dark Strike spell.",0);
if ((plyr.magic[plyr.invelem[item]]==30)&&(plyr.invelem[item]==4))
addlog("Learned the Drain Life spell.",0);
if ((plyr.magic[plyr.invelem[item]]==40)&&(plyr.invelem[item]==4))
addlog("Learned the Laughing Shadow L2 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==55)&&(plyr.invelem[item]==4))
addlog("Learned the Crown of the Dead spell.",0);
if ((plyr.magic[plyr.invelem[item]]==75)&&(plyr.invelem[item]==4))
addlog("Learned the Dark Cloud spell.",0);
if ((plyr.magic[plyr.invelem[item]]==99)&&(plyr.invelem[item]==4))
addlog("Learned the Laughing Shadow L3 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==10)&&(plyr.invelem[item]==5))
addlog("Learned the Cure Poison spell.",0);
if ((plyr.magic[plyr.invelem[item]]==20)&&(plyr.invelem[item]==5))
addlog("Learned the Light's Infusion spell.",0);
if ((plyr.magic[plyr.invelem[item]]==30)&&(plyr.invelem[item]==5))
addlog("Learned the Holy Boon spell.",0);
if ((plyr.magic[plyr.invelem[item]]==40)&&(plyr.invelem[item]==5))
addlog("Learned the Sunstrike spell.",0);
if ((plyr.magic[plyr.invelem[item]]==55)&&(plyr.invelem[item]==5))
addlog("Learned the Holy Warrior Level 2 spell.",0);
if ((plyr.magic[plyr.invelem[item]]==75)&&(plyr.invelem[item]==5))
addlog("Learned the Call the Light spell.",0);
if ((plyr.magic[plyr.invelem[item]]==99)&&(plyr.invelem[item]==5))
addlog("Learned the Holy Boon Level 2 spell.",0);
}
else
{
strcpy(temp,"Used a ");
strcat(temp,plyr.invname[item]);
strcat(temp," but didn't get +skill.");
addlog(temp,0);
}
}
else
{
if (plyr.invelem[item]==0) {gotbuff(6,7,0,1); playsound("Spell-Wind-Boon.wav");}
if (plyr.invelem[item]==1) {gotbuff(0,5,0,1); playsound("Spell-Fire-Boon.wav");}
if (plyr.invelem[item]==2) {gotbuff(7,9,0,1); playsound("Spell-Ice.wav");}
if (plyr.invelem[item]==3) {gotbuff(0,8,0,1); playsound("Spell-Earth-Boon.wav");}
if (plyr.invelem[item]==4) {gotbuff(2,9,0,1); playsound("Spell-Shadow-Boon.wav");}
if (plyr.invelem[item]==5) {gotbuff(2,5,0,1); playsound("Spell-Light-Boon.wav");}
addlog("Used a scroll and gained a boon.",0);
}
--plyr.invnum[item];
if (plyr.invnum[item]==0) lostitem(item);
}
if ((plyr.invmtype[item]==4)&&(plyr.invcls[item]==4))
{
if (gamescr==2)
{
strcpy(temp,"Teleport to");
y=strlen(temp);
strcat(temp,"dungeon level ");
sprintf(strint,"%d?",plyr.llevel);
strcat(temp,strint);
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (x==0)
{
plyr.dlvl=plyr.llevel;
savechar();
if (plyr.savedlvl[plyr.dlvl-1]==-1)
{
randdungeon();
plyr.savedlvl[plyr.dlvl-1]=0;
savedlvl();
}
else
loaddlvl();
plyr.x=dgn.supx;
plyr.y=dgn.supy;
--plyr.invnum[item];
if (plyr.invnum[item]==0) lostitem(item);
playsound("Portalstone.wav");
return 254;
}
}
else if (gamescr==4)
{
strcpy(temp,"Use the portal stone");
y=strlen(temp);
strcat(temp,"to return to town?");
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (x==0)
{
savechar();
savedlvl();
gamescr=2;
--plyr.invnum[item];
if (plyr.invnum[item]==0) lostitem(item);
pausedungeonmusic();
playsound("Portalstone.wav");
return 254;
}
}
}
}
if ((plyr.invcls[item]>2999)&&(plyr.invcls[item]<3999)&&(plyr.trade==1))
{
c=0;
if (plyr.scholar<35) c=1;
else if (plyr.scholar<60)
{
if (plyr.invcls[item]<3028) {z=randomnum(0,1); if (z==0) c=1;}
else c=1;
}
else if (plyr.scholar<78)
{
if (plyr.invcls[item]<3028) {z=randomnum(0,2); if (z==0) c=1;}
else if (plyr.invcls[item]<3035) {z=randomnum(0,1); if (z==0) c=1;}
else c=1;
}
else if (plyr.scholar<91)
{
if (plyr.invcls[item]<3035) {z=randomnum(0,3); if (z==0) c=1;}
else if (plyr.invcls[item]<3049) {z=randomnum(0,1); if (z==0) c=1;}
else c=1;
}
else
{
if (plyr.invcls[item]<3028) {z=randomnum(0,9); if (z==0) c=1;}
else if ((plyr.invcls[item]>3027)&&(plyr.invcls[item]<3042)) {z=randomnum(0,4); if (z==0) c=1;}
else if ((plyr.invcls[item]>3041)&&(plyr.invcls[item]<3049)) {z=randomnum(0,2); if (z==0) c=1;}
else {z=randomnum(0,1); if (z==0) c=1;}
}
if ((c==1)&&(plyr.scholar<99))
{
addlog("Read a book and gained a skillpoint.",0);
++plyr.scholar;
if ((plyr.scholar==50)||(plyr.scholar==99))
addlog("You unlocked a new ability.",0);
equiprecalc();
playsound("Spell-Light-Boon.wav");
}
else
{
addlog("Read a book but did not gain a skillpoint.",0);
playsound("loot2.wav");
}
--plyr.invnum[item];
if (plyr.invnum[item]==0) lostitem(item);
}
if (plyr.hp<0) return 255;
else return 0;
}


int playerdied(int flag)
{
int x, y;
char temp[40], strint[10];
clear_bitmap(screentemp);
++plyr.deaths;
sprintf(temp,"%s died on turn %d.  Press any key.",plyr.name,plyr.turns);
drawstatpane(0);
addlog(temp,0);
drawlog(1);
playmusic("death.mid",-1);
regulartext(screen->w/2,screen->h/2,"You have died.",WHITE,SCREEN,CENTER);
blitscreen();
clear_keybuf();
while (!keypressed())
{
}
if (flag!=1)
{
x=resurrectoption();
}
else x=1;
if (x==1) return 0;
else return 1;
}


int resurrectoption()
{
int x, y;
char temp[80], temp2[10];
strcpy(temp,"Resurrect this char?  Lose 1/2");
y=strlen(temp);
strcat(temp,"gold and dungeon map will reset.");
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (x==0)
{
plyr.hp=(plyr.hpm+plyr.adjhp);
plyr.mp=(plyr.mpm+plyr.adjmp);
plyr.gold=plyr.gold/2;
for (y=0; y<99; ++y)
plyr.savedlvl[y]=-1;
for (x=0; x<10; ++x)
{
plyr.buffx[x]=-1;
if (x<4) plyr.curse[x]=-1;
}
savechar();
return 1;
}
else
{
strcpy(temp,"Really delete this character");
y=strlen(temp);
strcat(temp,"instead of resurrecting?");
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (x==0)
{
strcpy(temp,"Delete Character");
y=strlen(temp);
strcat(temp,"Are you sure?");
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (x==0)
{
deletechar();
}
else return 1;
}
else return 1;
return 0;
}
}

int gotbuff(int whichx, int whichy, int whichmod, int level)
{
int x, y, mx, my;
char tempstr[50];
//first check for the same buff and reset it's duration if it's found
y=0;
for (x=0; x<10; ++x)
if ((plyr.buffx[x]==whichx)&&(plyr.buffy[x]==whichy)&&(plyr.buffmod[x]==whichmod)
    &&(!((whichx==9)&&(whichy==4)))) {y=1; plyr.buffdur[x]=200+(level*5);}
if (y!=1)
{
//next check for a lesser version of the buff and replace it if found
if (whichmod!=0)
{
y=-1;
for (x=0; x<10; ++x)
{
if ((plyr.buffx[x]==whichx)&&(plyr.buffy[x]==whichy)&&(plyr.buffmod[x]<whichmod)) y=x;
}
if (y!=-1)
{
plyr.buffmod[y]=whichmod;
plyr.buffdur[y]=200+(level*5);
return 0;
}
}
// Next check if player is already at max buffs
if (plyr.buffx[9]!=-1)
{
addlog("Couldn't apply boon because you are already at the maximum.",0);
return 1;
}
for (x=0; ((plyr.buffx[x]!=-1)&&(x<10)); ++x) ;
if (x!=10)
{
plyr.buffx[x]=whichx;
plyr.buffy[x]=whichy;
plyr.buffmod[x]=whichmod;
plyr.buffdur[x]=200+(level*5);
if ((whichx==9)&&(whichy==4)) plyr.buffdur[x]=10;
}
}
equiprecalc();
return 0;
}

void gotdebuff(int which)
{
int x;
for (x=0; x<4; ++x)
{
if (plyr.curse[x]==which) return;
}
for (x=0; ((plyr.curse[x]!=-1)&&(x<4)); ++x) ;
if (x!=4)
{
plyr.curse[x]=which;
if (which==0) plyr.cursedur[x]=400;
else plyr.cursedur[x]=250;
}
equiprecalc();
}



void curedebuff(int which)
{
int x, y;
y=99;
if (which != -1)
{
for (x=0; x<3; ++x)
{
if (plyr.curse[x]==which) y=x;
if (x>=y) {plyr.curse[x]=plyr.curse[x+1]; plyr.cursedur[x]=plyr.cursedur[x+1];}
}
}
else
{
for (x=0; x<4; ++x)
{
plyr.curse[x]=-1; plyr.cursedur[x]=0;
}
}
equiprecalc();
}

/*
   0 = head
   1 = chest
   2 = hands
   3 = feet
   4 = back
   5 = ring
   6 = ring2
   7 = necklace
   8 = shield
   9 = weapon
   10 = ammo
*/

int drawalignborder(int wherex, int wherey, int sizex, int sizey, char *caption)
{
/*
wherex
0 = left
1 = center
2 = right
wherey
0 = top
1 = center
2 = bottom
*/
if ((wherex==0)&&(wherey==0))
{
drawborder(0,0,sizex,sizey,caption);
return 0;
}
if ((wherex==0)&&(wherey==1))
{
drawborder(0,(screen->h/2)-(sizey/2),sizex,(screen->h/2)-(sizey/2)+sizey,caption);
return 0;
}
if ((wherex==0)&&(wherey==2))
{
drawborder(0,screen->h-sizey,sizex,screen->h,caption);
return 0;
}
if ((wherex==1)&&(wherey==0))
{
drawborder((screen->w/2)-(sizex/2),0,(screen->w/2)-(sizex/2)+sizex,sizey,caption);
return (screen->w/2)-(sizex/2);
}
if ((wherex==1)&&(wherey==1))
{
drawborder((screen->w/2)-(sizex/2),(screen->h/2)-(sizey/2),(screen->w/2)-(sizex/2)+sizex,(screen->h/2)-(sizey/2)+sizey,caption);
return (screen->w/2)-(sizex/2);
}
if ((wherex==1)&&(wherey==2))
{
drawborder((screen->w/2)-(sizex/2),screen->h-sizey,(screen->w/2)-(sizex/2)+sizex,screen->h,caption);
return (screen->w/2)-(sizex/2);
}
if ((wherex==2)&&(wherey==0))
{
drawborder(screen->w-sizex,0,screen->w,sizey,caption);
return screen->w-sizex;
}
if ((wherex==2)&&(wherey==1))
{
drawborder(screen->w-sizex,(screen->h/2)-(sizey/2),screen->w,sizey,caption);
return screen->w-sizex;
}
if ((wherex==2)&&(wherey==2))
{
drawborder(screen->w-sizex,screen->h-sizey,screen->w,screen->h,caption);
return screen->w-sizex;
}
}

void drawborder(int x, int y, int mx, int my, char *caption)
{
int off1, off2, clip;
off1 = scalex(36); clip=scaley(32);
off2 = off1 + text_length(largefont, " ")/2;
blit(bknd,screentemp,0,0,x,y,(mx-x)/2,my-y);
blit(bknd,screentemp,bknd->w-((mx-x)/2),0,x+((mx-x)/2),y,(mx-x)/2,my-y);
if (my-y<bknd->h) blit(bknd,screentemp,0,bknd->h-clip,x,my-clip,(mx-x)/2,clip);
else blit(bknd,screentemp,0,bknd->h-((my-y)/4),x,my-((my-y)/4),(mx-x)/2,((my-y)/4));
if (my-y<bknd->h) blit(bknd,screentemp,bknd->w-((mx-x)/2),bknd->h-clip,x+((mx-x)/2),my-clip,(mx-x)/2,clip);
else blit(bknd,screentemp,bknd->w-((mx-x)/2),bknd->h-((my-y)/4),x+((mx-x)/2),my-((my-y)/4),(mx-x)/2,((my-y)/4));
blit(paper,screentemp,0,0,x+off1,y,text_length(largefont,caption),text_height(largefont));
boldtext(x+off2,y-2,caption,BROWN,SCREEN,LEFT);
}


int randshops()
{
int x, y, c, z, cls, bon;
for (x=0; x<3; ++x)
{
for (y=0; y<8; ++y)
{
shop[x].icnx[y]=-1;
}
}
x=rewarditem(1,0);
if (x==1) {return 1;}
shopgot(0,0);
c=randomnum(6,8);
for (y=1; y<c; ++y)
{
cls=randomnum(0,11);
if (cls==11) cls=12;
if ((cls==0)||(cls==5)) bon=randomnum(0,1);
else if ((cls==3)||(cls>6)) bon=0;
else bon=randomnum(0,2);
if (cls<7) z=randomnum(0,1);
else z=0;
x=makeweapon(cls,bon,z,plyr.lvl,-1,-1,-1);
if (x==1) return 1;
shopgot(0,y);
}
c=randomnum(6,8);
for (y=0; y<c; ++y)
{
cls=randomnum(0,5);
if (cls==5) cls=7;
bon=randomnum(0,4);
x=makearmor(bon,cls,plyr.lvl,-1,-1);
if (x==1) return 1;
shopgot(1,y);
}
x=makeammo(0, 1, -1, -1);
if (x==1) return 1;
shopgot(2,0);
x=makeammo(1, 1, -1, -1);
if (x==1) return 1;
shopgot(2,1);
x=makeammo(2, 1, -1, -1);
if (x==1) return 1;
shopgot(2,2);
x=makepotion(plyr.lvl,1,-1,-1,-1);
if (x==1) return 1;
tempitem.treasure=2;
tempitem.elem=1;
strcpy(tempitem.name,"Cleansing Potion");
shopgot(2,3);
c=randomnum(7,8);
for (y=4; y<c; ++y)
{
z=randomnum(0,1);
if (z==0)
{
x=makepotion(plyr.lvl,1,-1,-1,-1);
if (x==1) return 1;
shopgot(2,y);
}
else
{
x=makefood(plyr.lvl,-1,-1);
if (x==1) return 1;
shopgot(2,y);
}
}
return 0;
}

int makeammo(int kind, int shop, int tx, int ty)
{
int x, c, pickt[10], e;
if (shop==1) e=1;
else e=plyr.dlvl;
if (tx==-1)
{
c=0;
for (x=(total.mons+total.melee+total.ranged+total.armor); x<(total.mons+total.melee+total.ranged+total.armor+total.ammo); ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=e)&&(tiledata[tlu[x].x][tlu[x].y].cls==kind)) {pickt[c]=x; ++c;}
}
if (c==0) {allegro_message("Unable to create ammo type %i.  Exiting.", kind); return 1;}
e=randomnum(0,c-1);
c=pickt[e];
}
else
{
c=-1;
for (x=(total.mons+total.melee+total.ranged+total.armor); x<(total.mons+total.melee+total.ranged+total.armor+total.ammo); ++x)
{
if ((tlu[x].x==tx)&&(tlu[x].y==ty)) c=x;
}
}
makecommon(tlu[c].x, tlu[c].y, 1, -1);
tempitem.lvl=1;
if (shop==1) tempitem.num=randomnum(40,255);
else tempitem.num=randomnum(3,40);
return 0;
}

// 0 = mons  1 = weapon  2 = ranged  3 = armor,
// 4 = ammo 5 = food 6 = potion 7 = scroll 8 = gem


void combinepotions()
{
int x, y, z;
for (z=0; z<2; ++z)
{
for (x=0; x<100; ++x)
{
if ((plyr.invmtype[x]==4)&&(plyr.invcls[x]==1))
{
for (y=x+1; y<100; ++y)
{
if ((plyr.invmtype[y]==4)&&(plyr.invcls[y]==1)&&(plyr.invtreasure[x]==plyr.invtreasure[y])&&(plyr.invelem[x]==plyr.invelem[y]))
{
if (plyr.invlvl[y]>plyr.invlvl[x]) plyr.invlvl[x]=plyr.invlvl[y];
if (plyr.invh[y]>plyr.invh[x]) plyr.invh[x]=plyr.invh[y];
if (plyr.invl[y]>plyr.invl[x]) plyr.invl[x]=plyr.invl[y];
plyr.invnum[x]=plyr.invnum[x]+plyr.invnum[y];
if (plyr.invnum[x]>999) plyr.invnum[x]=999;
lostitem(y);
}
}
}
}
}
}

int makepotion(int level, int shop, int tx, int ty, int flag)
{
int x, c, b, d;
if (tx==-1)
b=randomnum(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food,total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion-1);
else
{
for (x=total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food;
x<total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion; ++x)
{
if ((tlu[x].x==tx)&&(tlu[x].y==ty)) b=x;
}
}
makecommon(tlu[b].x, tlu[b].y, level, flag);
d=94; if (level>59) d=100;
c=0;  if (shop==1) c=15;
b=randomnum(c,d);
if (b<15)
{
// debuff potion
// 0 = poison  1 = weakness  2 = damage
tempitem.treasure=0;
tempitem.elem=randomnum(0,2);
if (tempitem.elem==2)
{
tempitem.l=1+level/4;
tempitem.h=5+(level/2);
}
}
else if (b<35)
{
// restorative potion
// 0 = hp  1 = mp
tempitem.treasure=1;
tempitem.elem=randomnum(0,1);
tempitem.l=20+(level*7);
tempitem.h=25+(level*7);
}
else if (b<50)
{
// curative potion
// 0 = poison  1 = all debuffs
tempitem.treasure=2;
c=randomnum(0,2);
if (c==0) tempitem.elem=1;
else tempitem.elem=0;
tempitem.lvl=1;
}
else if (b<65)
{
// stat buff, temporary
// 0-3 usual stat order
tempitem.treasure=3;
tempitem.elem=randomnum(0,3);
}
else if (b<80)
{
// combat buff
// 0 = recklessness  1 = +spell damage  2 = lucky  3 = haste
tempitem.treasure=4;
tempitem.elem=randomnum(0,3);
}
else if (b<95)
{
// resistance
// 0-6 usual elemental order
tempitem.treasure=5;
tempitem.elem=randomnum(0,6);
}
else
{
// permanent +1 to a stat
// usual stat order
tempitem.treasure=6;
tempitem.elem=randomnum(0,3);
}
if (shop==1) {getpotionname(); tempitem.bonus=0;}
else {strcpy(tempitem.name, "Unidentified Potion"); tempitem.bonus=-1;}
return 0;
}


void getpotionname()
{
if ((tempitem.treasure==0)&&(tempitem.elem==0)) strcpy(tempitem.name,"Poison Potion");
if ((tempitem.treasure==0)&&(tempitem.elem==1)) strcpy(tempitem.name,"Weakening Potion");
if ((tempitem.treasure==0)&&(tempitem.elem==2)) strcpy(tempitem.name,"Acidic Potion");
if ((tempitem.treasure==1)&&(tempitem.elem==0)) strcpy(tempitem.name,"Healing Potion");
if ((tempitem.treasure==1)&&(tempitem.elem==1)) strcpy(tempitem.name,"Mana Potion");
if ((tempitem.treasure==2)&&(tempitem.elem==0)) strcpy(tempitem.name,"Antidote Potion");
if ((tempitem.treasure==2)&&(tempitem.elem==1)) strcpy(tempitem.name,"Cleansing Potion");
if ((tempitem.treasure==3)&&(tempitem.elem==0)) strcpy(tempitem.name,"Mighty Potion");
if ((tempitem.treasure==3)&&(tempitem.elem==1)) strcpy(tempitem.name,"Agile Potion");
if ((tempitem.treasure==3)&&(tempitem.elem==2)) strcpy(tempitem.name,"Sage's Potion");
if ((tempitem.treasure==3)&&(tempitem.elem==3)) strcpy(tempitem.name,"Leadership Potion");
if ((tempitem.treasure==4)&&(tempitem.elem==0)) strcpy(tempitem.name,"Frothy Potion");
if ((tempitem.treasure==4)&&(tempitem.elem==1)) strcpy(tempitem.name,"Kinetic Potion");
if ((tempitem.treasure==4)&&(tempitem.elem==2)) strcpy(tempitem.name,"Lucky Potion");
if ((tempitem.treasure==4)&&(tempitem.elem==3)) strcpy(tempitem.name,"Celerity Potion");
if ((tempitem.treasure==5)&&(tempitem.elem==0)) strcpy(tempitem.name,"Wind Balm");
if ((tempitem.treasure==5)&&(tempitem.elem==1)) strcpy(tempitem.name,"Fire Balm");
if ((tempitem.treasure==5)&&(tempitem.elem==2)) strcpy(tempitem.name,"Water Balm");
if ((tempitem.treasure==5)&&(tempitem.elem==3)) strcpy(tempitem.name,"Earth Balm");
if ((tempitem.treasure==5)&&(tempitem.elem==4)) strcpy(tempitem.name,"Shadow Balm");
if ((tempitem.treasure==5)&&(tempitem.elem==5)) strcpy(tempitem.name,"Light Balm");
if ((tempitem.treasure==5)&&(tempitem.elem==6)) strcpy(tempitem.name,"Poison Balm");
if ((tempitem.treasure==6)&&(tempitem.elem==0)) strcpy(tempitem.name,"Knightly Potion");
if ((tempitem.treasure==6)&&(tempitem.elem==1)) strcpy(tempitem.name,"Acrobatic Potion");
if ((tempitem.treasure==6)&&(tempitem.elem==2)) strcpy(tempitem.name,"Insightful Potion");
if ((tempitem.treasure==6)&&(tempitem.elem==3)) strcpy(tempitem.name,"Beloved Potion");
if ((tempitem.treasure==7)&&(tempitem.elem==0)) strcpy(tempitem.name,"Elixir");
if ((tempitem.treasure==7)&&(tempitem.elem==1)) strcpy(tempitem.name,"Giant's Potion");
}


int makefood(int level, int tx, int ty)
{
int x, c, pick[50], b;
if (tx==-1)
{
c=0;
for (x=(total.mons+total.melee+total.ranged+total.armor+total.ammo); x<(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food); ++x)
{
if (tiledata[tlu[x].x][tlu[x].y].lvl<=level) {pick[c]=x; ++c;}
}
if (c==0) {allegro_message("Unable to create random food.  Exiting."); return 1;}
b=randomnum(0,c-1);
makecommon(tlu[pick[b]].x, tlu[pick[b]].y, level, -1);
}
else
{
b=-1;
for (x=(total.mons+total.melee+total.ranged+total.armor+total.ammo); x<(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food); ++x)
{
if ((tlu[x].x==tx)&&(tlu[x].y==ty)) b=x;
}
makecommon(tlu[b].x, tlu[b].y, level, -1);
}
tempitem.l=10+(level*3);
tempitem.h=10+(level*3)+randomnum(1,5);
tempitem.num=randomnum(1,5);
return 0;
}


int makescroll(int tx, int ty)
{
int x, b;
if (tx==-1)
{
b=randomnum(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion,
         total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll-1);
}
else
{
b=-1;
for (x=(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion); x<(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll); ++x)
{
if ((tlu[x].x==tx)&&(tlu[x].y==ty)) b=x;
}
if (b==-1) {allegro_message("Error fetching scroll."); return 1;}
}
makecommon(tlu[b].x, tlu[b].y, 1, -1);
tempitem.elem=randomnum(0,5);
namescroll();
return 0;
}

void namescroll()
{
if (tempitem.elem==0) strcpy(tempitem.name,"Scroll of Wind");
if (tempitem.elem==1) strcpy(tempitem.name,"Scroll of Flames");
if (tempitem.elem==2) strcpy(tempitem.name,"Scroll of Water");
if (tempitem.elem==3) strcpy(tempitem.name,"Scroll of Earth");
if (tempitem.elem==4) strcpy(tempitem.name,"Scroll of Shadow");
if (tempitem.elem==5) strcpy(tempitem.name,"Scroll of Light");
}


int makegem(int tx, int ty)
{
int x, b;
if (tx==-1)
{
b=randomnum(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll,
         total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+total.gem-1);
}
else
{
b=-1;
for (x=total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll; x<total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+total.gem; ++x)
{
if ((tlu[x].x==tx)&&(tlu[x].y==ty)) b=x;
}
if (b==-1) {allegro_message("Error fetching gem. %i %i",tx,ty); return 1;}
}
makecommon(tlu[b].x, tlu[b].y, 1, -1);
return 0;
}



int randdungeon()
{
int d, lastx, lasty, doors, items, q, y, w, a, x, rr, c, rooms, z, ditems, pick[600], map_size, mapitems, roomsmax;
//The Dungeon generation routine, all rights reserved, not really
d = 1;
//Generate level
//Small = 64 x 64, Medium = 96 x 96, Large = 128 x 128
if (mapsize==0) {map_size=64; mapitems=140;}
else if (mapsize==1) {map_size=96; mapitems=210;}
else {map_size=128; mapitems=280;}
lastx = (map_size/2);
lasty = (map_size/2);
doors = 0;
ditems = 0;
dgn.supx = lastx;
dgn.supy = lasty;
doors = 0;
items = 0;
rooms = 0;
dgn.tset = randomnum(0,4);
for (y=0; y<256; ++y)
{
dgn.item[y]=-1;
dgn.itemx[y]=-1;
dgn.itemy[y]=-1;
}
for (y=0; y<128; ++y)
{
dgn.door[y]=-1;
dgn.doorx[y]=-1;
dgn.doory[y]=-1;
dgn.doormn[y]=-1;
dgn.doormnlvl[y]=-1;
dgn.doormx[y]=-1;
dgn.doormy[y]=-1;
for (w=0; w<128; ++w)
{
dgn.icnx[y][w] = -1;
dgn.icny[y][w] = -1;
dgn.explore[y][w] = -1;
dmap[y][w].trkrm = -1;
dmap[y][w].automap = -1;
dmap[y][w].trki= -1;
dgn.open[y][w] = -1;
}
}
dmap[lastx][lasty].automap=3;
dgn.open[lastx][lasty]=0;
dgn.icnx[lastx][lasty]=dtiles[dgn.tset].supx;
dgn.icny[lastx][lasty]=dtiles[dgn.tset].supy;
//First step - we are in a new dungeon level, looking for the direction to expand
//into (from the staircase)
r[0].h = randomnum(1,10);
r[0].w = randomnum(1,10);
a = randomnum(0,3);
if (a==0)
{
r[0].ulx = lastx - (r[0].w / 2);
r[0].uly = lasty - (r[0].h);
}
if (a == 1)
{
r[0].ulx = lastx - (r[0].w / 2);
r[0].uly = lasty + 1;
}
if (a == 2)
{
r[0].uly = lasty - (r[0].h / 2);
r[0].ulx = lastx + 1;
}
if (a == 3)
{
r[0].uly = lasty - (r[0].h / 2);
r[0].ulx = lastx - (r[0].w);
}
for (x = r[0].ulx; x < r[0].ulx + r[0].w; ++x)
{
for (y = r[0].uly; y < r[0].uly + r[0].h; ++y)
{
dmap[x][y].trkrm = rooms;
}
}
rooms = 1;
//Fist room added.
//Room generation loop.
c = 0;
if (mapsize==2) roomsmax = 128;
else if (mapsize==1) roomsmax = 96;
else roomsmax = 64;
while ((c < 3000)&&(rooms < roomsmax)&&(doors < 128))
{
d = 0;
rr = randomnum(0,rooms-1);
a = randomnum(0,3);
r[rooms].w = randomnum(1,10);
r[rooms].h = randomnum(1,10);
//hallway or room
//50 /50
x = randomnum(0,1);
if (x == 1)
{
if (a < 2)
r[rooms].w = 1;
else
r[rooms].h = 1;
}
if ((a < 2)&&(r[rr].w>0)) lastx = r[rr].ulx + randomnum(0, r[rr].w-1);
//else if ((a < 2)&&(r[rr].w<=1)) lastx = r[rr].ulx;
if (a == 0) lasty = r[rr].uly - 1;
if (a == 1) lasty = r[rr].uly + r[rr].h;
if ((a > 1)&&(r[rr].h>0)) lasty = r[rr].uly + randomnum(0, r[rr].h-1);
//else if ((a > 1)&&(r[rr].h<=1)) lasty = r[rr].uly;
if (a == 2) lastx = r[rr].ulx + r[rr].w;
if (a == 3) lastx = r[rr].ulx - 1;
if (a < 2) r[rooms].ulx = lastx - (r[rooms].w / 2);
if (a == 0) r[rooms].uly = r[rr].uly - 1 - r[rooms].h;
if (a == 1) r[rooms].uly = r[rr].uly + r[rr].h + 1;
if (a > 1) r[rooms].uly = lasty - (r[rooms].h / 2);
if (a == 2) r[rooms].ulx = r[rr].ulx + r[rr].w + 1;
if (a == 3) r[rooms].ulx = r[rr].ulx - 1 - r[rooms].w;
if ((r[rooms].ulx < 0)||(r[rooms].uly < 0) || (r[rooms].uly + r[rooms].h> (map_size-1))
||(r[rooms].ulx + r[rooms].w > (map_size-1))) d = 1;
for (x = r[rooms].ulx - 1; ((x < r[rooms].ulx + r[rooms].w + 1)
                            &&(x < (map_size-1))&&(x > 0)); ++x)
{
for (y = r[rooms].uly - 1; ((y < r[rooms].uly + r[rooms].h + 1)&&(y <(map_size-1))&&(y > 0)); ++y)
{
if (dmap[x][y].trkrm != -1) d = 1;
if ((dgn.supx == x)&&(dgn.supy == y)) d = 1;
}
}
if (d == 0)
{
for (x = r[rooms].ulx-1; x < r[rooms].ulx + r[rooms].w; ++x)
{
for (y = r[rooms].uly-1; y < r[rooms].uly + r[rooms].h; ++y)
{
dmap[x][y].trkrm = rooms;
}
}
dmap[lastx][lasty].automap=2;
dgn.doorx[doors] = lastx;
dgn.doory[doors] = lasty;
z=randomnum(0,99);
if (z<50)
dgn.door[doors] = 0;
else if (z<65)
dgn.door[doors] = 1;
else if (z<94)
dgn.door[doors] = 2;
else
dgn.door[doors] = 3;
doors = doors + 1;
rooms = rooms + 1;
}
c = c + 1;
}
if (c==5000) {allegro_message("Random dungeon routine failed.  Exiting."); return 1;}
//All rooms have been added.  Now fill in the dgn.icnx/y array
//with tile info to match up with the rooms.
for (rr = 0; ((rr < rooms)&&(r[rr].w > 0)); ++rr)
{
for (x = r[rr].ulx - 1; x < r[rr].ulx + r[rr].w + 1; ++x)
{
for (y = r[rr].uly - 1; y < r[rr].uly + r[rr].h + 1; ++y)
{
if ((x > 0)&&(y > 0)&&(x < map_size)&&(y < map_size)&&!((dgn.supx==x)&&(dgn.supy==y)))
{
// first check for door
if (dmap[x][y].automap==2)
{
a=randomnum(0,dtiles[dgn.tset].doors-1);
for (z=0; (((dgn.doorx[z]!=x)||(dgn.doory[z]!=y))&&(z<doors)); ++z);
if (z==doors) return 1;
if (dgn.door[z]==1) dgn.icnx[x][y]=dtiles[dgn.tset].doorx[a]+2;
else dgn.icnx[x][y]=dtiles[dgn.tset].doorx[a];
dgn.icny[x][y]=dtiles[dgn.tset].doory[a];
dgn.open[x][y]=0;
}
else if ((x == r[rr].ulx - 1)||(x == r[rr].ulx + r[rr].w)||(y == r[rr].uly - 1)
    ||(y == r[rr].uly + r[rr].h))
{
//wall
dmap[x][y].automap=1;
a = randomnum(0,10);
if (a == 0)
{
// accent wall
a = randomnum(1,3);
dgn.icnx[x][y]=dtiles[dgn.tset].wallx[a];
dgn.icny[x][y]=dtiles[dgn.tset].wally[a];
}
else
{
// normal wall
dgn.icnx[x][y]=dtiles[dgn.tset].wallx[0];
dgn.icny[x][y]=dtiles[dgn.tset].wally[0];
}
}
else
{
// floor
dgn.open[x][y]=0;
dmap[x][y].automap=0;
a = randomnum(0,10);
if ((a==0)&&(dtiles[dgn.tset].floors>1))
{
//accent floor
a=randomnum(0,dtiles[dgn.tset].floors-1);
dgn.icnx[x][y]=dtiles[dgn.tset].floorx[a];
dgn.icny[x][y]=dtiles[dgn.tset].floory[a];
}
else
{
//regular floor
dgn.icnx[x][y]=dtiles[dgn.tset].floorx[0];
dgn.icny[x][y]=dtiles[dgn.tset].floory[0];
}
}
}
}
}
}
for (rr=1; ((rr < rooms)&&(items<mapitems)); ++rr)
{
//For each room, take a look at the size && then stock that room.
if ((r[rr].h > 4) && (r[rr].w > 4))
{
//Room is a large room
a = randomnum(1,100);
if (a < 60)
{
//Normal room
// %15 of normal large rooms have a rune in them
a=randomnum(1,10);
if (a<4)
{
if (dmap[r[rr].ulx+(r[rr].w/2)][r[rr].uly+(r[rr].h/2)].automap!=0)
{allegro_message("Error: Randdungeon() tried to place a rune in an invalid spot.  Exiting."); return 1;}
a=randomnum(0,2);
dgn.item[items]=6000+a;
dgn.itemx[items]=r[rr].ulx+(r[rr].w/2);
dgn.itemy[items]=r[rr].uly+(r[rr].h/2);
dmap[r[rr].ulx+(r[rr].w/2)][r[rr].uly+(r[rr].h/2)].trki=items;
++items;
}
else
{
a=randomnum(0,1);
if (a==0)
{
a=randomnum(1,4);
randitem(rr, a, 0, items);
items=items+a;
}
}
}
else if (a<76)
{
//Room is a larder
a=randomnum(6,12);
randitem(rr, a, 1, items);
items=items+a;
}
else if (a<83)
{
//Room is an armory
a=randomnum(4,8);
randitem(rr, a, 3, items);
items=items+a;
}
else if (a<92)
{
//treasure room
a=randomnum(6,12);
x=randitem(rr, a, 2, items);
items=items+x;
}
else
{
//Room is a library
a=randomnum(6,12);
randitem(rr, a, 4, items);
items=items+a;
}
}
else if ((r[rr].h > 2)&&(r[rr].w > 2))
{
// room is mid-sized
a=randomnum(0,1);
if (a==0)
{
a=randomnum(1,2);
randitem(rr, a, 0, items);
items=items+a;
}
}
else if ((r[rr].h == 1)&&(r[rr].w == 1))
{
a=randomnum(0,3);
if (a==0)
{
randitem(rr,1,0,items);
++items;
}
}
else
{
a=randomnum(0,2);
if (a==0)
{
a=randomnum(1,2);
randitem(rr, a, 0, items);
items=items+a;
}
}
}
//Now put monsters behind doors
// 4 in 10 chance for a monster
for (x=0; x<doors; ++x)
{
a=randomnum(1,10);
if (a<5)
{
// 1 in 100 chance for a "boss" monster
// boss monsters can be any monster not just the usual level table
// they recive +%25 all stats and drop an item
a=randomnum(1,100);
if (a==1)
{
dgn.doormnlvl[x]=plyr.dlvl+3;
dgn.doormn[x]=1;
y=randomnum(0,total.mons-1);
dgn.doormx[x]=tlu[y].x;
dgn.doormy[x]=tlu[y].y;
}
else
{
dgn.doormnlvl[x]=randomnum(plyr.dlvl-1,plyr.dlvl+2);
if (dgn.doormnlvl[x]<1) dgn.doormnlvl[x]=1;
dgn.doormn[x]=0;
d=0;
if (plyr.dlvl<10) rr=1; else rr=((plyr.dlvl/10)*10);
for (y=0; y<total.mons; ++y)
if (tiledata[tlu[y].x][tlu[y].y].lvl==rr) {pick[d]=y; ++d;}
y=randomnum(0,d-1);
dgn.doormx[x]=tlu[pick[y]].x;
dgn.doormy[x]=tlu[pick[y]].y;
}
}
}
//Put stairs down in a random corner, or middle of map
lastx=-1;
a=randomnum(0,4);
if (a==0)
{
//NW corner
for (x=0; x<(map_size*0.66); ++x)
{
for (y=0; y<(map_size*0.66); ++y)
{
if ((dmap[x][y].automap==1)
    &&(dmap[x+1][y].automap!=2)&&(dmap[x-1][y].automap!=2)
    &&(dmap[x][y+1].automap!=2)&&(dmap[x][y-1].automap!=2)
    &&((dmap[x][y+1].automap==0)||(dmap[x+1][y].automap==0)))
{
lastx=x; lasty=y; x=(map_size*0.66);
}
}
}
}
else if (a==1)
//NE Corner
{
for (x=(map_size-1); x>(map_size/4); --x)
{
for (y=0; y<(map_size*0.66); ++y)
{
if ((dmap[x][y].automap==1)
    &&(dmap[x+1][y].automap!=2)&&(dmap[x-1][y].automap!=2)
    &&(dmap[x][y+1].automap!=2)&&(dmap[x][y-1].automap!=2)
    &&((dmap[x][y+1].automap==0)||(dmap[x-1][y].automap==0)))
{
lastx=x; lasty=y; x=(map_size/4);
}
}
}
}
else if (a==2)
{
// SE Corner
for (x=(map_size-1); x>(map_size/4); --x)
{
for (y=(map_size-1); y>(map_size/4); --y)
{
if ((dmap[x][y].automap==1)
    &&(dmap[x+1][y].automap!=2)&&(dmap[x-1][y].automap!=2)
    &&(dmap[x][y+1].automap!=2)&&(dmap[x][y-1].automap!=2)
    &&((dmap[x][y-1].automap==0)||(dmap[x-1][y].automap==0)))
{
lastx=x; lasty=y; x=(map_size/4);
}
}
}
}
else if (a==3)
{
// SW Corner
for (x=0; x<(map_size*0.66); ++x)
{
for (y=(map_size-1); y>(map_size/4); --y)
{
if ((dmap[x][y].automap==1)
    &&(dmap[x+1][y].automap!=2)&&(dmap[x-1][y].automap!=2)
    &&(dmap[x][y+1].automap!=2)&&(dmap[x][y-1].automap!=2)
    &&((dmap[x][y-1].automap==0)||(dmap[x+1][y].automap==0)))
{
lastx=x; lasty=y; x=(map_size*0.66);
}
}
}
}
else if (a==4)
{
// Middle of map
for (x=(map_size*0.33); x<(map_size*0.66); ++x)
{
for (y=(map_size-1); y>(map_size/4); --y)
{
if ((dmap[x][y].automap==1)
    &&(dmap[x+1][y].automap!=2)&&(dmap[x-1][y].automap!=2)
    &&(dmap[x][y+1].automap!=2)&&(dmap[x][y-1].automap!=2)
    &&((dmap[x][y-1].automap==0)||(dmap[x+1][y].automap==0)))
{
lastx=x; lasty=y; x=(map_size*0.66);
}
}
}
}
if (lastx==-1)
{
allegro_message("Failed to generate stairs down.  Exiting.  A=%i",a);
return 1;
}
else
{
//if dungeon level 99, place Xantos instead of stairs
if (plyr.dlvl==99)
{
if (dgn.open[lastx+1][lasty]==0) lastx=lastx+1;
else if (dgn.open[lastx-1][lasty]==0) lastx=lastx-1;
else if (dgn.open[lastx][lasty+1]==0) lasty=lasty+1;
else if (dgn.open[lastx][lasty-1]==0) lasty=lasty-1;
else
{
allegro_message("Unable to place Xantos.  Exiting.");
}
}
dgn.open[lastx][lasty]=0;
dgn.sdnx = lastx;
dgn.sdny = lasty;
dmap[lastx][lasty].automap=0;
if (plyr.dlvl!=99)
{dgn.icnx[lastx][lasty]=dtiles[dgn.tset].sdnx;
dgn.icny[lastx][lasty]=dtiles[dgn.tset].sdny;
dmap[lastx][lasty].automap=3;}
return 0;
}
}


int randitem(int which, int number, int flag, int slot)
{
// place "number" random items in dungeon room "which"
// flag for kind of items
// 0 = general  1 = food  2 = treasury  3 = armory
int x, y, z, d, b, randx[12], randy[12], pick[40], bp[10], bug;
bug=0;
// first pick out x/y coords that don't overlap for each item
for (x=0; x<12; ++x) {randx[x]=-1; randy[x]=-1;}
for (x=0; x<number; ++x)
{
y=-1;
b=0;
while (y==-1)
{
randx[x]=randomnum(0,r[which].w);
randy[x]=randomnum(0,r[which].h);
z=0;
for (d=0; d<number; ++d)
{
if ((d!=x)&&(randx[d]==randx[x])&&(randy[d]==randy[x])) z=-1;
if ((dmap[r[which].ulx+randx[x]][r[which].uly+randy[x]].automap!=0)) z=-1;
}
if (z==0) y=0;
++bug;
if (bug==10000) {allegro_message("Item placement timed out."); return 1;}
}
}
if (flag==0)
{
// 0 = general
// 1-15 Gold Pile
// 16-30 Food
// 31-38 Ammo
// 39-42 Portal Stone
// 43-52 Skeleton Key
// 53-60 Potion
// 61-72 Scroll
// 73-89 Armor
// 90-95 Melee Weapon
// 96-100 Ranged Weapon
// 101-105 Gem
bp[0]=16; bp[1]=31; bp[2]=39; bp[3]=43; bp[4]=53; bp[5]=61; bp[6]=73; bp[7]=90; bp[8]=96;
bp[9]=101;
}
if (flag==1)
{
// 1 = food (granary)
// 1-10 Gold Pile
// 11-60 Food
// 61-66 Ammo
// 67-71 Portal Stone
// 72-77 Skeleton Key
// 78-86 Potion
// 87-93 Scroll
// 94-96 Armor
// 97-99 Melee Weapon
// 100-102 Ranged Weapon
// 103-105 Gem
bp[0]=11; bp[1]=61; bp[2]=67; bp[3]=72; bp[4]=78; bp[5]=87; bp[6]=94; bp[7]=97; bp[8]=100;
bp[9]=103;
}
if (flag==2)
{
// 2 = treasure room
// 1 Gold Pile
// 2-3 Food
// 4-5 Ammo
// 6-11 Portal Stone
// 12-28 Skeleton Key
// 29-45 Potion
// 46-65 Scroll
// 66-72 Armor
// 73-79 Melee Weapon
// 80-84 Ranged Weapon
// 85-105 Gem
bp[0]=2; bp[1]=4; bp[2]=6; bp[3]=12; bp[4]=29; bp[5]=46; bp[6]=66; bp[7]=73; bp[8]=80;
bp[9]=85;
}
if (flag==3)
{
// 3 = Armory
// 1-10 Gold Pile
// 11-14 Food
// 15-41 Ammo
// 42-43 Portal Stone
// 44-45 Skeleton Key
// 46-48 Potion
// 49-52 Scroll
// 53-77 Armor
// 78-90 Melee Weapon
// 91-103 Ranged Weapon
// 104-105 Gem
bp[0]=11; bp[1]=15; bp[2]=42; bp[3]=44; bp[4]=46; bp[5]=49; bp[6]=53; bp[7]=78; bp[8]=91;
bp[9]=104;
}
if (flag==4)
{
// 4 = Library
// 1-10 Gold Pile
// 11-14 Food
// 15-16 Ammo
// 17-18 Portal Stone
// 19-22 Skeleton Key
// 23-29 Potion
// 30-90 Scroll
// 91-93 Armor
// 94-96 Melee Weapon
// 97-99 Ranged Weapon
// 100-105 Gem
bp[0]=11; bp[1]=15; bp[2]=17; bp[3]=19; bp[4]=23; bp[5]=30; bp[6]=91; bp[7]=94; bp[8]=97;
bp[9]=100;
}
for (x=0; x<number; ++x)
{
// %100 chance in treasure room for a chest as the first item
// 1 in 5 chance in library or armory for chest
d=randomnum(1,10);
if ((x==0)&&(((d<3)&&(flag>2))||((d<11)&&(flag==2))))
{
// 1 in 4 chance for large chest
d=randomnum(1,4);
if (d==1)
{
d=randomnum(0,2);
dgn.item[slot+x]=2004+(d*2);
}
else
{
d=randomnum(0,2);
dgn.item[slot+x]=2003+(d*2);
}
}
else
{
// first check for trade item drop chance
// trade items only drop if you have a trade
z=20;
if ((plyr.trade!=-1)&&(!((plyr.trade==1)&&(plyr.scholar==99)))) z=randomnum(1,20);
if (z<4)
{
if (plyr.trade==0)
{
z=randomnum(0,8);
dgn.item[slot+x]=4000+z;
}
if (plyr.trade==1)
{
z=randomnum(0,55);
dgn.item[slot+x]=3000+z;
}
if (plyr.trade==2)
{
z=randomnum(0,2);
dgn.item[slot+x]=5000+z;
}
}
else
{
if (plyr.dlvl>20) z=randomnum(1,105);
else z=randomnum(1,100);
if (z<bp[0])
{
dgn.item[slot+x]=2000;
}
else if (z<bp[1])
{
y=randomnum(total.mons+total.melee+total.ranged+total.armor+total.ammo,
total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food);
dgn.item[slot+x]=y;
}
else if (z<bp[2])
{
d=0;
for (y=total.mons+total.melee+total.ranged+total.armor;
y<total.mons+total.melee+total.ranged+total.armor+total.ammo; ++y)
if (tiledata[tlu[y].x][tlu[y].y].lvl<=plyr.dlvl) {pick[d]=y; ++d;}
y=randomnum(0,d-1);
dgn.item[slot+x]=pick[y];
}
else if (z<bp[3])
{
dgn.item[slot+x]=2002;
}
else if (z<bp[4])
{
dgn.item[slot+x]=2001;
}
else if (z<bp[5])
{
y=randomnum(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food,
total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion);
dgn.item[slot+x]=y;
}
else if (z<bp[6])
{
y=randomnum(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion,
total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll);
dgn.item[slot+x]=y;
}
else if (z<bp[7])
{
b=randomnum(0,7);
d=0;
for (y=total.mons+total.melee+total.ranged; y<total.mons+total.melee+total.ranged+total.armor; ++y)
{
if ((tiledata[tlu[y].x][tlu[y].y].lvl<=plyr.dlvl)&&(tiledata[tlu[y].x][tlu[y].y].cls==b)
    &&(!((tiledata[tlu[y].x][tlu[y].y].cls==7)&&(tiledata[tlu[y].x][tlu[y].y].treasure==1))))
    {pick[d]=y; ++d;}
}
if (d==0) allegro_message("Errored on armor.  Cls%i",b);
y=randomnum(0,d-1);
dgn.item[slot+x]=pick[y];
}
else if (z<bp[8])
{
b=randomnum(0,6);
d=0;
for (y=total.mons; y<total.mons+total.melee; ++y)
{
if ((tiledata[tlu[y].x][tlu[y].y].lvl<=plyr.dlvl)&&(tiledata[tlu[y].x][tlu[y].y].cls==b))
    {pick[d]=y; ++d;}
}
if (d==0) allegro_message("Errored on melee.  Cls%i",b);
y=randomnum(0,d-1);
dgn.item[slot+x]=pick[y];
}
else if (z<bp[9])
{
if (plyr.dlvl<20) b=randomnum(0,4);
else b=randomnum(0,5);
if ((plyr.dlvl<20)&&(b==4)) b=5;
d=0;
for (y=total.mons+total.melee; y<total.mons+total.melee+total.ranged; ++y)
{
if ((tiledata[tlu[y].x][tlu[y].y].lvl<=plyr.dlvl)&&(tiledata[tlu[y].x][tlu[y].y].cls==b))
    {pick[d]=y; ++d;}
}
if (d==0) allegro_message("Errored on ranged.  Cls%i",b);
y=randomnum(0,d-1);
dgn.item[slot+x]=pick[y];
}
else
{
y=randomnum(total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll,
total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+total.gem-1);
dgn.item[slot+x]=y;
}
}
}
dgn.itemx[slot+x]=r[which].ulx+randx[x];
dgn.itemy[slot+x]=r[which].uly+randy[x];
dmap[r[which].ulx+randx[x]][r[which].uly+randy[x]].trki=slot+x;
}
if (flag==2)
// extra routine for treasure rooms, fill in blank squares with gold piles
{
for (x=0; x<r[which].w; ++x)
{
for (y=0; y<r[which].h; ++y)
{
if ((dmap[r[which].ulx+x][r[which].uly+y].trki==-1)&&(dmap[r[which].ulx+x][r[which].uly+y].automap==0)
    &&(slot+number<255))
{
dgn.item[slot+number]=2000;
dgn.itemx[slot+number]=r[which].ulx+x;
dgn.itemy[slot+number]=r[which].uly+y;
dmap[r[which].ulx+x][r[which].uly+y].trki=slot+number;
++number;
}
}
}
return number;
}
else return 0;
}


void makemonster(int tx, int ty, int level, int boss)
{
int x, y;
if (!((tx==xantosx)&&(ty==xantosy)))
{
if ((boss==1)&&(bossflag==1))
{
strcpy(mons.name,"Boss ");
strcat(mons.name,tiledata[tx][ty].name);
mons.lvl=plyr.lvl+3;
}
else
strcpy(mons.name,tiledata[tx][ty].name);
if (mons.lvl<1) mons.lvl=1;
mons.buffx[0]=-1; mons.buffx[1]=-1;
mons.bonus=tiledata[tx][ty].bonus; mons.elem=tiledata[tx][ty].elem-1;
mons.cls=tiledata[tx][ty].cls; mons.treasure=tiledata[tx][ty].treasure;
mons.icnx=tx; mons.icny=ty; mons.align=tiledata[tx][ty].magic; mons.mtype=tiledata[tx][ty].mtype;
if (mons.treasure==0) mons.gold=10+randomnum(2*level,6*level);
else if (mons.treasure==1) mons.gold=1+randomnum(1*level,3*level);
else if (mons.treasure==2) mons.gold=12+randomnum(3*level,8*level);
else if (mons.treasure==3) mons.gold=15+randomnum(6*level,12*level);
for (x=0; x<6; ++x) mons.magic[x]=-1;
if (tiledata[tx][ty].cls==0)
{
//Warrior
mons.str=(5*mons.lvl); mons.dex=(4*mons.lvl); mons.itl=(1*mons.lvl); mons.hp=(15*mons.lvl);
mons.l=2+(mons.lvl*0.80); mons.h=((mons.l*2)); mons.deflect=5+(2.5*mons.lvl);
mons.xp=12;
for (x=0; x<7; ++x) mons.resist[x]=mons.lvl/3;
}
else if (tiledata[tx][ty].cls==1)
{
//Cleric
mons.str=(2*mons.lvl); mons.dex=(2*mons.lvl); mons.itl=(5*mons.lvl); mons.hp=(15*mons.lvl);
mons.magic[4]=10+(1*mons.lvl); mons.magic[5]=10+(1*mons.lvl);
mons.l=2+(mons.lvl*0.6); mons.h=((mons.l*2)-1); mons.deflect=3+(1.80*mons.lvl);
mons.xp=13;
for (x=0; x<7; ++x) mons.resist[x]=mons.lvl/3;
}
else if (tiledata[tx][ty].cls==2)
{
//Mage
mons.str=(1*mons.lvl); mons.dex=(1*mons.lvl); mons.itl=(6*mons.lvl); mons.hp=(8*mons.lvl);
for (x=0; x<4; ++x) mons.magic[x]=10+(1*mons.lvl);
mons.l=1+(mons.lvl*0.5); mons.h=((mons.l*2)-1); mons.deflect=1+(1.1*mons.lvl);
mons.xp=13;
for (x=0; x<7; ++x) mons.resist[x]=mons.lvl/2;
}
else if ((tiledata[tx][ty].cls==3)||(tiledata[tx][ty].cls==8))
{
//Thief or Monk
mons.str=(1*mons.lvl); mons.dex=(8*mons.lvl); mons.itl=(2*mons.lvl); mons.hp=(10*mons.lvl);
mons.l=2+(mons.lvl*0.80); mons.h=((mons.l*2)-2); mons.deflect=2+(1.50*mons.lvl);
mons.xp=11;
for (x=0; x<7; ++x) mons.resist[x]=mons.lvl/4;
}
else if (tiledata[tx][ty].cls==4)
{
//Barbarian
mons.str=(6*mons.lvl); mons.dex=(3*mons.lvl); mons.itl=(1*mons.lvl); mons.hp=(20*mons.lvl);
mons.l=2+(mons.lvl*0.80); mons.h=((mons.l*2))+1; mons.deflect=2+(1.5*mons.lvl);
mons.xp=12;
for (x=0; x<7; ++x) mons.resist[x]=mons.lvl/4;
}
else if ((tiledata[tx][ty].cls==5)||(tiledata[tx][ty].cls==9))
{
//Spellblade or Nomad
mons.str=(3*mons.lvl); mons.dex=(3*mons.lvl); mons.itl=(3*mons.lvl); mons.hp=(14*mons.lvl);
mons.l=2+(mons.lvl*0.80); mons.h=((mons.l*2)-1); mons.deflect=4+(2.2*mons.lvl);
if (tiledata[tx][ty].cls==5)
{
mons.magic[1]=10+(1*mons.lvl);
mons.magic[2]=10+(1*mons.lvl);
}
else
{
mons.magic[0]=10+(1*mons.lvl);
mons.magic[3]=10+(1*mons.lvl);
}
mons.xp=14;
for (x=0; x<7; ++x) mons.resist[x]=mons.lvl/3;
}
else if (tiledata[tx][ty].cls==6)
{
//Paladin
mons.str=(4*mons.lvl); mons.dex=(2*mons.lvl); mons.itl=(2*mons.lvl); mons.hp=(16*mons.lvl);
mons.l=2+(mons.lvl*0.80); mons.h=((mons.l*2)-1); mons.deflect=5+(2.5*mons.lvl);
mons.magic[5]=10+(1*mons.lvl);
mons.xp=14;
for (x=0; x<7; ++x) mons.resist[x]=mons.lvl/3;
}
else if (tiledata[tx][ty].cls==7)
{
//warlock
mons.str=(2*mons.lvl); mons.dex=(1*mons.lvl); mons.itl=(5*mons.lvl); mons.hp=(10*mons.lvl);
mons.l=2+(mons.lvl*0.6); mons.h=((mons.l*2)-1); mons.deflect=1+(1.1*mons.lvl);
mons.magic[1]=10+(1*mons.lvl); mons.magic[3]=10+(1*mons.lvl); mons.magic[4]=10+(1*mons.lvl);
mons.xp=13;
for (x=0; x<7; ++x) mons.resist[x]=mons.lvl/2;
}
// Add type modifiers
if (mons.mtype==0)
{
//Beast
mons.itl=mons.itl/2;
mons.str=mons.str+(mons.str/5);
mons.l=mons.l+(mons.l/5);
mons.h=mons.h+(mons.h/5);
mons.deflect=mons.deflect-(mons.deflect/5);
mons.hp=mons.hp+(mons.hp/5);
for (x=0; x<7; ++x) mons.resist[x]=mons.resist[x]-mons.resist[x]/3;
}
else if (mons.mtype==2)
{
//reptile
mons.itl=mons.itl/2;
mons.deflect=mons.deflect+(mons.deflect/3);
mons.hp=mons.hp-(mons.hp/3);
mons.dex=mons.dex-(mons.dex/5);
mons.h=mons.h+(mons.h/5);
mons.l=mons.l+(mons.l/5);
mons.resist[2]=mons.resist[2]/2;
mons.resist[1]=mons.resist[1]+mons.resist[1]/2;
}
else if (mons.mtype==3)
{
//Insect
mons.itl=mons.itl/2;
mons.deflect=mons.deflect+(mons.deflect/5);
mons.dex=mons.dex+(mons.dex/4);
mons.hp=mons.hp-(mons.hp/5);
mons.resist[1]=mons.resist[1]/2;
mons.resist[2]=mons.resist[2]+mons.resist[2]/2;
}
else if (mons.mtype==4)
{
//Undead
mons.deflect=mons.deflect+(mons.deflect/5);
mons.str=mons.str+(mons.str/5);
mons.resist[1]=mons.resist[1]/2;
mons.resist[2]=mons.resist[2]+mons.resist[2]/2;
mons.resist[6]=99;
}
else if (mons.mtype==5)
{
//Apparition
mons.hp=mons.hp/2;
mons.str=mons.str-(mons.str/3);
mons.l=mons.l-(mons.l/3); mons.h=mons.h-(mons.h/3);
for (x=0; x<7; ++x) mons.resist[x]=mons.resist[x]-mons.resist[x]/4;
mons.resist[6]=99;
}
else if (mons.mtype==6)
{
//Golem
mons.hp=mons.hp+(mons.hp/3);
mons.deflect=mons.deflect-(mons.deflect/3);
mons.itl=mons.itl/2;
mons.str=mons.str+(mons.str/5);
mons.dex=mons.dex-(mons.dex/4);
mons.resist[0]=mons.resist[0]/2;
mons.resist[2]=mons.resist[2]+mons.resist[2]/2;
mons.resist[3]=mons.resist[3]+mons.resist[3]/2;
mons.resist[6]=99;
}
else if (mons.mtype==7)
{
//Dragon
mons.hp=mons.hp+(mons.hp/3);
mons.deflect=mons.deflect+(mons.deflect/3);
mons.itl=mons.itl+(mons.itl/3);
mons.str=mons.str+(mons.str/4);
for (x=0; x<7; ++x) mons.resist[x]=mons.resist[x]+mons.resist[x]/3;
mons.xp=mons.xp+(mons.xp/3);
}
else if (mons.mtype==8)
{
//Elemental
mons.resist[mons.elem]=99;
if (mons.elem==0) mons.resist[3]=mons.resist[3]/4;
if (mons.elem==1) mons.resist[2]=mons.resist[2]/4;
if (mons.elem==2) mons.resist[1]=mons.resist[1]/4;
if (mons.elem==3) mons.resist[0]=mons.resist[0]/4;
if (mons.elem==4) mons.resist[5]=mons.resist[5]/4;
if (mons.elem==5) mons.resist[4]=mons.resist[4]/4;
if (mons.elem==6) mons.resist[1]=mons.resist[1]/4;
mons.resist[6]=99;
}
else if (mons.mtype==9)
{
// Demon
mons.itl=mons.itl+(mons.itl/4);
mons.str=mons.str+(mons.str/4);
mons.resist[1]=mons.resist[1]+mons.resist[1]/2;
mons.resist[2]=mons.resist[2]-mons.resist[2]/2;
mons.resist[5]=mons.resist[5]-mons.resist[1]/2;
mons.resist[4]=mons.resist[4]+mons.resist[4]/2;
mons.xp=mons.xp+(mons.xp/4);
}
else if (mons.mtype==10)
{
// Ooze
mons.deflect=mons.deflect-mons.deflect/2;
mons.hp=mons.hp+((mons.hp/3)*2);
mons.itl=mons.itl/2;
mons.dex=mons.dex/2;
mons.resist[6]=99;
mons.resist[1]=mons.resist[1]/4;
mons.resist[2]=mons.resist[2]/4;
}
else if (mons.mtype==11)
{
// Plant
mons.dex=mons.dex/3;
mons.deflect=mons.deflect-(mons.deflect/4);
mons.hp=mons.hp+(mons.hp/4);
mons.resist[1]=mons.resist[1]/2;
for (x=0; x<7; ++x) if (x!=1) mons.resist[x]=mons.resist[x]+mons.resist[x]/3;
}
// Add resistances if elemental
// but not if already an elemental
if ((mons.elem!=-1)&&(mons.mtype!=8))
{
mons.resist[mons.elem]=mons.resist[mons.elem]*2;
if (mons.resist[mons.elem]<50) mons.resist[mons.elem]=50;
if (mons.resist[mons.elem]>99) mons.resist[mons.elem]=99;
if (mons.elem==0) mons.resist[3]=mons.resist[3]/2;
if (mons.elem==1) mons.resist[2]=mons.resist[2]/2;
if (mons.elem==2) mons.resist[1]=mons.resist[1]/2;
if (mons.elem==3) mons.resist[0]=mons.resist[0]/2;
if (mons.elem==4) mons.resist[5]=mons.resist[5]/2;
if (mons.elem==5) mons.resist[4]=mons.resist[4]/2;
if (mons.elem==6) mons.resist[1]=mons.resist[1]/2;
}
// Process bonus if applicable
if (mons.bonus!=0)
{
if (mons.bonus==1)
{
mons.str=mons.str+mons.str/3;
mons.dex=mons.dex-mons.dex/2;
mons.hp=mons.hp+(mons.hp/2);
}
else if (mons.bonus==3)
{
mons.deflect=mons.deflect+(mons.deflect/2);
mons.hp=mons.hp-mons.hp/3;
}
else if (mons.bonus==6)
{
mons.dex=mons.dex+(mons.dex/3);
}
else if (mons.bonus==4)
{
for (x=0; x<6; ++x)
{
mons.resist[x]=mons.resist[x]+mons.resist[x];
if (mons.resist[x]<50) mons.resist[x]=50;
if (mons.resist[x]>99) mons.resist[x]=99;
}
}
mons.xp=mons.xp+(mons.xp/3);
}
if (plyr.dlvl>89)
{
//monsters in dungeon level 90 and higher get bonus stats
if (mons.bonus!=1) mons.str=mons.str+(mons.str/4);
mons.dex=mons.dex+(mons.dex/4);
mons.itl=mons.itl+(mons.itl/4);
mons.h=mons.h+(mons.h/4);
mons.l=mons.l+(mons.l/4);
mons.deflect=mons.deflect+(mons.deflect/4);
}
//Finalize XP
// * 0.093 per level
for (x=0; x<mons.lvl; ++x)
{
y=(mons.xp*0.093); if (y<1) y=1;
mons.xp=mons.xp+y;
}
}
else
{
//Xantos stats
strcpy(mons.name,"Xantos");
mons.lvl=102;
mons.str=(12*mons.lvl); mons.dex=(7*mons.lvl); mons.itl=(10*mons.lvl);
mons.hp=20000;
mons.l=2+(mons.lvl*1.00); mons.h=((mons.l*2)); mons.deflect=80;
mons.xp=0; mons.bonus=5; mons.elem=-1;
mons.mtype=12; mons.cls=2;
for (x=0; x<7; ++x) mons.resist[x]=75;
mons.buffx[0]=-1; mons.buffx[1]=-1;
//allegro_message("Xantos dex %i", mons.dex);
}
//Difficulty setting adjustment to HP, XP and Damage
if (difficulty==0)
{
mons.hp=mons.hp*0.66;
mons.xp=mons.xp*1.2;
}
else if (difficulty==2)
{
mons.xp=mons.xp*0.8;
mons.dmgh=mons.dmgh*1.33;
mons.dmgl=mons.dmgl*1.33;
}
if (boss==1)
{
//Bonus stats for boss monster
mons.align=0;
if (mons.lvl>20)
mons.hp=mons.hp*2;
else
mons.hp=mons.hp*1.5;
mons.h=mons.h+(mons.h*0.25);
mons.l=mons.l+(mons.l*0.25);
mons.deflect=mons.deflect+(mons.deflect*0.25);
mons.str=mons.str+(mons.str*0.25);
mons.dex=mons.dex+(mons.dex*0.25);
mons.itl=mons.itl+(mons.itl*0.25);
if (mons.elem==-1)
{
mons.elem=randomnum(0,6);
mons.resist[mons.elem]=50;
}
for (x=0; x<7; ++x)
{
if ((x<6)&&(mons.magic[x]!=-1)) {mons.magic[x]=mons.magic[x]+15; if (mons.magic[x]>99) mons.magic[x]=99;}
mons.resist[x]=mons.resist[x]+(mons.resist[x]*0.25);
if (mons.resist[x]>99) mons.resist[x]=99;
}
mons.gold=mons.gold*2;
mons.xp=mons.xp*3;
}
mons.hpm=mons.hp;
}


void regulartext(int x, int y, char *message, int tcolor, int where, int align)
{
if (align==1)
{
if (where==1)
{
textprintf_centre_ex(menutemp, largefont, x, y, tcolor, -1, message);
}
else
{
textprintf_centre_ex(screentemp, largefont, x, y, tcolor, -1, message);
}
}
else
{
if (where==1)
{
textprintf_ex(menutemp, largefont, x, y, tcolor, -1, message);
}
else
{
textprintf_ex(screentemp, largefont, x, y, tcolor, -1, message);
}
}
}

void smalltext(int x, int y, char *message, int tcolor, int where, int align)
{
if (align==1)
{
if (where==1)
{
textprintf_centre_ex(menutemp, smallfont, x, y, tcolor, -1, message);
}
else
{
textprintf_centre_ex(screentemp, smallfont, x, y, tcolor, -1, message);
}
}
else
{
if (where==1)
{
textprintf_ex(menutemp, smallfont, x, y, tcolor, -1, message);
}
else
{
textprintf_ex(screentemp, smallfont, x, y, tcolor, -1, message);
}
}
}

void boldtext(int x, int y, char *message, int tcolor, int where, int align)
{
if (align==1)
{
if (where==1)
{
textprintf_centre_ex(menutemp, largefont, x, y, tcolor, -1, message);
textprintf_centre_ex(menutemp, largefont, x-1, y, tcolor, -1, message);
}
else
{
textprintf_centre_ex(screentemp, largefont, x, y, tcolor, -1, message);
textprintf_centre_ex(screentemp, largefont, x-1, y, tcolor, -1, message);
}
}
else
{
if (where==1)
{
textprintf_ex(menutemp, largefont, x, y, tcolor, -1, message);
textprintf_ex(menutemp, largefont, x-1, y, tcolor, -1, message);
}
else
{
textprintf_ex(screentemp, largefont, x, y, tcolor, -1, message);
textprintf_ex(screentemp, largefont, x-1, y, tcolor, -1, message);
}
}
}

void boldsmalltext(int x, int y, char *message, int tcolor, int where, int align)
{
if (align==1)
{
if (where==1)
{
textprintf_centre_ex(menutemp, smallfont, x, y, tcolor, -1, message);
textprintf_centre_ex(menutemp, smallfont, x-1, y, tcolor, -1, message);
}
else
{
textprintf_centre_ex(screentemp, smallfont, x, y, tcolor, -1, message);
textprintf_centre_ex(screentemp, smallfont, x-1, y, tcolor, -1, message);
}
}
else
{
if (where==1)
{
textprintf_ex(menutemp, smallfont, x, y, tcolor, -1, message);
textprintf_ex(menutemp, smallfont, x-1, y, tcolor, -1, message);
}
else
{
textprintf_ex(screentemp, smallfont, x, y, tcolor, -1, message);
textprintf_ex(screentemp, smallfont, x-1, y, tcolor, -1, message);
}
}
}


void shadowtext(int x, int y, char *message, int tcolor, int where, int align)
{
if (align==1)
{
if (where==1)
{
textprintf_centre_ex(menutemp, largefont, x-(text_length(largefont,"T")/6), y+(text_height(largefont)/11), makecol(44,44,44), -1, message);
textprintf_centre_ex(menutemp, largefont, x-(text_length(largefont,"T")/6)-1, y+(text_height(largefont)/11)-1, makecol(44,44,44), -1, message);
textprintf_centre_ex(menutemp, largefont, x, y, tcolor, -1, message);
}
else
{
textprintf_centre_ex(screentemp, largefont, x-(text_length(largefont,"T")/6), y+(text_height(largefont)/11), makecol(44,44,44), -1, message);
textprintf_centre_ex(screentemp, largefont, x-(text_length(largefont,"T")/6)-1, y+(text_height(largefont)/11)-1, makecol(44,44,44), -1, message);
textprintf_centre_ex(screentemp, largefont, x, y, tcolor, -1, message);
}
}
else
{
if (where==1)
{
textprintf_ex(menutemp, largefont, x-(text_length(largefont,"T")/6), y+(text_height(largefont)/11), makecol(44,44,44), -1, message);
textprintf_ex(menutemp, largefont, x-(text_length(largefont,"T")/6)-1, y+(text_height(largefont)/11)-1, makecol(44,44,44), -1, message);
textprintf_ex(menutemp, largefont, x, y, tcolor, -1, message);
}
else
{
textprintf_ex(screentemp, largefont, x-(text_length(largefont,"T")/6), y+(text_height(largefont)/11), makecol(44,44,44), -1, message);
textprintf_ex(screentemp, largefont, x-(text_length(largefont,"T")/6)-1, y+(text_height(largefont)/11)-1, makecol(44,44,44), -1, message);
textprintf_ex(screentemp, largefont, x, y, tcolor, -1, message);
}
}
}

void smallshadowtext(int x, int y, char *message, int tcolor, int where, int align)
{
if (align==1)
{
if (where==1)
{
textprintf_centre_ex(menutemp, smallfont, x-(text_length(smallfont,"T")/6), y+(text_height(smallfont)/11), makecol(44,44,44), -1, message);
textprintf_centre_ex(menutemp, smallfont, x-(text_length(smallfont,"T")/6)-1, y+(text_height(smallfont)/11)-1, makecol(44,44,44), -1, message);
textprintf_centre_ex(menutemp, smallfont, x, y, tcolor, -1, message);
}
else
{
textprintf_centre_ex(screentemp, smallfont, x-(text_length(smallfont,"T")/6), y+(text_height(smallfont)/11), makecol(44,44,44), -1, message);
textprintf_centre_ex(screentemp, smallfont, x-(text_length(smallfont,"T")/6)-1, y+(text_height(smallfont)/11)-1, makecol(44,44,44), -1, message);
textprintf_centre_ex(screentemp, smallfont, x, y, tcolor, -1, message);
}
}
else
{
if (where==1)
{
textprintf_ex(menutemp, smallfont, x-(text_length(smallfont,"T")/6), y+(text_height(smallfont)/11), makecol(44,44,44), -1, message);
textprintf_ex(menutemp, smallfont, x-(text_length(smallfont,"T")/6)-1, y+(text_height(smallfont)/11)-1, makecol(44,44,44), -1, message);
textprintf_ex(menutemp, smallfont, x, y, tcolor, -1, message);
}
else
{
textprintf_ex(screentemp, smallfont, x-(text_length(smallfont,"T")/6), y+(text_height(smallfont)/11), makecol(44,44,44), -1, message);
textprintf_ex(screentemp, smallfont, x-(text_length(smallfont,"T")/6)-1, y+(text_height(smallfont)/11)-1, makecol(44,44,44), -1, message);
textprintf_ex(screentemp, smallfont, x, y, tcolor, -1, message);
}
}
}

void addlog(char *entry, int sys)
{
char fname[20] = "Saves//", temp[40];
int x, z;
if (logc==40)
{
for (x=0; x<39; ++x)
{
strcpy(plyrlog[x],plyrlog[x+1]);
}
strcpy(plyrlog[39], entry);
}
else
{
strcpy(plyrlog[logc], entry);
++logc;
}
if ((playerlog==1)&&(sys==0))
{
z=strlen(plyr.name);
if (z>8) z=8;
strncat(fname, plyr.name, z);
strcat(fname, ".log");
inputfile = fopen(fname,"a");
fprintf(inputfile,"Turn %i: %s\n",plyr.turns,entry);
fclose(inputfile);
}
}

void drawlog(int where)
{
int tracky, x, y, width, row, charsperrow;
row=(text_height(smallfont)*0.7);
if (where==1)
{
width=(screen->w/3);
drawalignborder(LEFT,BOTTOM,width,(row*4)+offsety,"Log ");
guilocations.logx[0]=0; guilocations.logx[1]=width;
guilocations.logy[0]=screen->h-(row*4)-offsety; guilocations.logy[1]=screen->h;
if (logc>3) y=logc-3;
else y=0;
tracky=screen->h-(row*3)-offsety+3;
for (x=y; x<logc; ++x)
{
smalltext(offsetx, tracky, plyrlog[x], BROWN, SCREEN, LEFT);
tracky=tracky+row;
}
}
if (where==2)
{
width=(screen->w/3);
drawalignborder(LEFT,BOTTOM,width,(row*9)+offsety,"Log ");
if (logc>8) y=logc-8;
else y=0;
tracky=screen->h-(row*8)-offsety+3;
for (x=y; x<logc; ++x)
{
smalltext(offsetx, tracky, plyrlog[x], BROWN, SCREEN, LEFT);
tracky=tracky+row;
}
}
if (where==3)
{
row=(text_height(largefont)*0.7);
drawalignborder(LEFT,BOTTOM,700,row*34+offsety,"Log ");
tracky=screen->h-(row*33+offsety); y=logc-33;
for (x=y; x<logc; ++x)
{
if (x>-1) regulartext(offsetx, tracky, plyrlog[x], BROWN, SCREEN, LEFT);
tracky=tracky+row;
}
}
}


void doshopinfo(int where, int slot)
{
if ((shop[where].icnx[slot]!=-1)&&(slot<8))
{
shoptotemp(where,slot);
doiteminfo();
}
}


void dobaginfo(int mx, int my)
{
int z;
z=my-1;  if (mx==6) z=z+10;
z=z+(bagpage*20);
if (plyr.invicnx[z]!=-1)
{
invtotemp(z);
doiteminfo();
}
}


void doiteminfo()
{
int x, y, c, rc, cc, totadd[4], totaddct[4], d, z, size;
char name[20], temp[80], temp2[20];
FONT *tempfont;
tempfont=smallfont;
playsound("hoverover.wav");
size=text_length(tempfont,"                                ")+offsetx*2;
drawalignborder(RIGHT,BOTTOM,size,scaley(272),"Info ");
c=(screen->h-scaley(272))+scaley(24);
rc=0;
d=(text_height(tempfont)*0.7);
cc=(screen->w-size)+offsetx;
if (((tempitem.mtype==4)&&(tempitem.cls==2))||(tempitem.mtype==5)||(tempitem.cls>3000))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "%s",tempitem.name);
else if (((tempitem.mtype<2)&&(tempitem.treasure==0))||((tempitem.mtype==1)&&(tempitem.cls==4)))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "%s (L%d) (1h)",tempitem.name,tempitem.lvl);
else if ((tempitem.mtype<2)&&(tempitem.treasure==1))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "%s (L%d) (2h)",tempitem.name,tempitem.lvl);
else
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "%s (L%d)",tempitem.name,tempitem.lvl);
++rc;
if ((tempitem.cls>3000)&&(tempitem.cls<3999))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Book.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Chance to increase Scholar skill.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Becomes increasingly difficult to");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "increase skill as you advance in");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "level.");
}
else if ((tempitem.cls>3999)&&(tempitem.cls<4999))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Archaeological Find.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "The Trade Guild would be interested");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "in this item.");
}
else if (tempitem.cls>4999)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Alchemy ingredient.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Can be used to create potions at the");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Trade Guild.");
}
else if ((tempitem.mtype<4)&&(tempitem.elem!=-1)&&(tempitem.magic!=2))
{
if (tempitem.mtype!=2)
{
if (tempitem.elem==0)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), WHITE, -1, "Imbued with Wind");
else if (tempitem.elem==1)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), RED, -1, "Imbued with Fire");
else if (tempitem.elem==2)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BLUE, -1, "Imbued with Water");
else if (tempitem.elem==3)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), LIGHTBROWN, -1, "Imbued with Earth");
else if (tempitem.elem==4)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BLACK, -1, "Imbued with Shadow");
else if (tempitem.elem==5)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), WHITE, -1, "Imbued with Light");
else if (tempitem.elem==6)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), GREEN, -1, "Imbued with Poison");
}
else
{
if (tempitem.elem<100) x=(10+(tempitem.lvl*0.707));
else if (tempitem.elem<200) x=tempitem.elem-100;
else if (tempitem.elem<300) x=tempitem.elem-200;
else if (tempitem.elem<400) x=tempitem.elem-300;
else if (tempitem.elem<500) x=tempitem.elem-400;
else if (tempitem.elem<600) x=tempitem.elem-500;
else if (tempitem.elem<700) x=tempitem.elem-600;
else if (tempitem.elem<800) x=tempitem.elem-700;
else if (tempitem.elem<900) x=tempitem.elem-800;
if ((tempitem.elem==0)||((tempitem.elem>100)&&(tempitem.elem<200)))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), WHITE, -1, "Imbued with Wind (%i resist)",x);
else if ((tempitem.elem==1)||((tempitem.elem>100)&&(tempitem.elem<300)))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), RED, -1, "Imbued with Fire (%i resist)",x);
else if ((tempitem.elem==2)||((tempitem.elem>100)&&(tempitem.elem<400)))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BLUE, -1, "Imbued with Water (%i resist)",x);
else if ((tempitem.elem==3)||((tempitem.elem>100)&&(tempitem.elem<500)))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), LIGHTBROWN, -1, "Imbued with Earth (%i resist)",x);
else if ((tempitem.elem==4)||((tempitem.elem>100)&&(tempitem.elem<600)))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BLACK, -1, "Imbued with Shadow (%i resist)",x);
else if ((tempitem.elem==5)||((tempitem.elem>100)&&(tempitem.elem<700)))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), WHITE, -1, "Imbued with Light (%i resist)",x);
else if ((tempitem.elem==6)||((tempitem.elem>100)&&(tempitem.elem<800)))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), GREEN, -1, "Imbued with Poison (%i resist)",x);
else if ((tempitem.elem>100)&&(tempitem.elem<900))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), DARKGOLD, -1, "Prismatic (%i resist)",x);
}
++rc;
if (tempitem.mtype==3)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Deals extra damage as element");
++rc;
}
}
else if (tempitem.magic==2)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), DARKGOLD, -1, "Prismatic (20 resist)");
++rc;
}
else if (tempitem.mtype==4)
{
if (tempitem.cls==0)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Food");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Restores %i-%i HP",tempitem.l,tempitem.h);
++rc;
}
if (tempitem.cls==1)
{
if ((tempitem.treasure==0)&&(tempitem.elem==0))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Curses you with poison.");
if ((tempitem.treasure==0)&&(tempitem.elem==1))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Curses you with weakness.");
if ((tempitem.treasure==0)&&(tempitem.elem==2))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Smells evil.");
if ((tempitem.treasure==1)&&(tempitem.elem==0))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Restores %i-%i HP.",tempitem.l,tempitem.h);
if ((tempitem.treasure==1)&&(tempitem.elem==1))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Restores %i-%i MP.",tempitem.l,tempitem.h);
if ((tempitem.treasure==2)&&(tempitem.elem==0))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Cures poison.");
if ((tempitem.treasure==2)&&(tempitem.elem==1))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Cures any curse.");
if ((tempitem.treasure==3)&&(tempitem.elem==0))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Strength boon.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Increases Strength by 10%% for");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "a limited time.");
}
if ((tempitem.treasure==3)&&(tempitem.elem==1))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Dexterity boon.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Increases Dexterity by 10%% for");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "a limited time.");
}
if ((tempitem.treasure==3)&&(tempitem.elem==2))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Intelligence boon.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Increases Intelligence by 10%% for");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "a limited time.");
}
if ((tempitem.treasure==3)&&(tempitem.elem==3))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Charisma boon.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Increases Charisma by 10%% for");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "a limited time.");
}
if ((tempitem.treasure==4)&&(tempitem.elem==0)) strcpy(temp,"Frothy Potion");
if ((tempitem.treasure==4)&&(tempitem.elem==0))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Recklessness.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Increases damage at the cost of");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "deflection for a limited time.");
}
if ((tempitem.treasure==4)&&(tempitem.elem==1))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Spellstrike. Increases");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "spell damage by 50%% for a limited");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "time.");
}
if ((tempitem.treasure==4)&&(tempitem.elem==2))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Lucky. Increases the chance");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "for Lucky Strikes for a limited time.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Lucky Strikes deal extra damage.");
}
if ((tempitem.treasure==4)&&(tempitem.elem==3))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Haste.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants the Haste boon for a limited");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "time. Players with Haste have");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "priority, and sometimes deal double");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "strikes in combat.");
}
if (tempitem.treasure==5)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Gants additional resistances");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "for a limited time.");
}
if ((tempitem.treasure==6)&&(tempitem.elem==0))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+1 Strength permanently.");
if ((tempitem.treasure==6)&&(tempitem.elem==1))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+1 Dexterity permanently.");
if ((tempitem.treasure==6)&&(tempitem.elem==2))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+1 Intelligence permanently.");
if ((tempitem.treasure==6)&&(tempitem.elem==3))
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+1 Charisma permanently.");
if ((tempitem.treasure==7)&&(tempitem.elem==0))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Restores all HP and MP to full.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "You can only carry one of these,");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "and it requires the Alchemy");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "profession to use.");
}
if ((tempitem.treasure==7)&&(tempitem.elem==1))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Doubles all stats for a short time.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "You can only carry one of these,");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "and it requires the Alchemy");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "profession to use.");
}
}
if (tempitem.cls==2)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Spell scroll.");
++rc;
if (plyr.magic[tempitem.elem]!=-1)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Chance to increase magic skill.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "At low levels, always grants one or");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "two skill points. At higher levels,");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "random chance determines whether");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "your skill increases or not.");
}
else
{
if (tempitem.elem==0)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Zephyr's Boon, a form of");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Haste, for a limited time. Players");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "with Haste have initiative, and");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "sometimes deal double strikes in");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "combat.");
}
if (tempitem.elem==1)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Fire Weapon for a limited");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "time. Fire Weapon adds fire damage");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "to your weapon attacks.");
}
if (tempitem.elem==2)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Icy Shield for a limited");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "time. Icy Shield increases your");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "deflection by 10%%.");
}
if (tempitem.elem==3)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Mountain's Might for a");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "limited time. Mountain's Might");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "increases your Strength by 10%%.");
}
if (tempitem.elem==4)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Laughing Shadow for a");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "limited time. Laughing Shadow");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "causes enemy attacks to sometimes");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "backfire.");
}
if (tempitem.elem==5)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Grants Holy Warrior for a limited");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "time. Holy Warrior increases attack");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "damage by 10%%, and attack damage");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "versus undead by an additional 10%%.");
}
}
}
else if (tempitem.cls==3)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Opens any door or chest.");
else if (tempitem.cls==4)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Teleports you back to town.");
}
if (tempitem.mtype==5)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "A precious gem.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Used as a currency at the Hall of");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Heroes for various services.");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Also required as an ingredient for");
++rc;
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "certain powerful, high-level spells.");
}
if ((tempitem.mtype==0)&&(tempitem.cls<3000))
{
if ((tempitem.cls==0)&&(tempitem.bonus==0))
strcpy(temp,"Type: Dagger (Slashing)");
else if ((tempitem.cls==0)&&(tempitem.bonus==1))
strcpy(temp,"Type: Dagger (Piercing)");
else if ((tempitem.cls==1)&&(tempitem.bonus==0))
strcpy(temp,"Type: Blunt (Mace)");
else if ((tempitem.cls==1)&&(tempitem.bonus==1))
strcpy(temp,"Type: Blunt (Hammer)");
else if ((tempitem.cls==1)&&(tempitem.bonus==2))
strcpy(temp,"Type: Blunt (Flail)");
else if ((tempitem.cls==2)&&(tempitem.bonus==0))
strcpy(temp,"Type: Sword (Broad)");
else if ((tempitem.cls==2)&&(tempitem.bonus==1))
strcpy(temp,"Type: Sword (Rapier)");
else if ((tempitem.cls==2)&&(tempitem.bonus==2))
strcpy(temp,"Type: Sword (Katana)");
else if (tempitem.cls==3)
strcpy(temp,"Type: Axe");
else if ((tempitem.cls==4)&&(tempitem.bonus==0))
strcpy(temp,"Type: Spear (Piercing)");
else if ((tempitem.cls==4)&&(tempitem.bonus==1))
strcpy(temp,"Type: Spear (Pitchfork)");
else if ((tempitem.cls==4)&&(tempitem.bonus==2))
strcpy(temp,"Type: Spear (Lance)");
else if ((tempitem.cls==5)&&(tempitem.bonus==0))
strcpy(temp,"Type: Polearm (Halberd)");
else if ((tempitem.cls==5)&&(tempitem.bonus==1))
strcpy(temp,"Type: Polearm (Scythe)");
else if ((tempitem.cls==6)&&(tempitem.bonus==0))
strcpy(temp,"Type: Martial Arts (Whip)");
else if ((tempitem.cls==6)&&(tempitem.bonus==1))
strcpy(temp,"Type: Martial Arts (Nunchuk)");
else if ((tempitem.cls==6)&&(tempitem.bonus==2))
strcpy(temp,"Type: Martial Arts (Staff)");
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, temp);
++rc;
if (plyr.equipmw[tempitem.cls][tempitem.bonus]==-1)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), RED, -1, "(Can't Equip)");
++rc;
}
}
if (tempitem.mtype==1)
{
if (tempitem.cls==0)
strcpy(temp,"Type: Bow");
else if (tempitem.cls==1)
strcpy(temp,"Type: Crossbow");
else if (tempitem.cls==2)
strcpy(temp,"Type: Sling");
else if (tempitem.cls==3)
strcpy(temp,"Type: Darts");
else if ((tempitem.cls==4)&&(tempitem.treasure==0))
strcpy(temp,"Type: Wand (Wand)");
else if ((tempitem.cls==4)&&(tempitem.treasure==1))
strcpy(temp,"Type: Wand (Scepter)");
else if (tempitem.cls==5)
strcpy(temp,"Type: Shruiken");
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, temp);
++rc;
if (plyr.equipr[tempitem.cls]==-1)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), RED, -1, "(Can't Equip)");
++rc;
}
}
if (tempitem.mtype==2)
{
if (tempitem.cls==0)
strcpy(temp2,"Type: Head");
else if (tempitem.cls==1)
strcpy(temp2,"Type: Chest");
else if (tempitem.cls==2)
strcpy(temp2,"Type: Hands");
else if (tempitem.cls==3)
strcpy(temp2,"Type: Feet");
else if (tempitem.cls==4)
strcpy(temp2,"Type: Back");
else if (tempitem.cls==5)
strcpy(temp2,"Type: Ring");
else if (tempitem.cls==6)
strcpy(temp2,"Type: Necklace");
else if ((tempitem.cls==7)&&(tempitem.treasure!=1))
strcpy(temp2,"Type: Shield");
else if ((tempitem.cls==7)&&(tempitem.treasure==1))
strcpy(temp2,"Type: Lantern");
if (tempitem.cls<5)
{
if (tempitem.treasure==0)
sprintf(temp,"%s (Cloth)",temp2);
if (tempitem.treasure==1)
sprintf(temp,"%s (Leather)",temp2);
if (tempitem.treasure==2)
sprintf(temp,"%s (Chain)",temp2);
if (tempitem.treasure==3)
sprintf(temp,"%s (Half-Plate)",temp2);
if (tempitem.treasure==4)
sprintf(temp,"%s (Full Plate)",temp2);
}
else
strcpy(temp,temp2);
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, temp);
++rc;
if (((tempitem.cls<4)&&(plyr.equipa[tempitem.treasure]==-1))||((tempitem.cls==7)&&(plyr.equipa[5]==-1))
    &&(!((tempitem.cls==7)&&(tempitem.treasure==1))))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), RED, -1, "(Can't Equip)");
++rc;
}
}
x=tempitem.l; y=tempitem.h;
for (z=0; z<5; ++z)
if (tempitem.add[z]==4)
{
x=x+((x*tempitem.addamt[z])/100);
y=y+((y*tempitem.addamt[z])/100);
}
if (((tempitem.mtype==0)||(tempitem.mtype==1))&&(tempitem.cls<3000))
{
if (tempitem.elem!=-1)
{
x=x*1.3; y=y*1.3;
if (tempitem.elem==0)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Damage: %i-%i (50%% Wind)",x,y);
else if (tempitem.elem==1)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Damage: %i-%i (50%% Fire)",x,y);
else if (tempitem.elem==2)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Damage: %i-%i (50%% Water)",x,y);
else if (tempitem.elem==3)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Damage: %i-%i (50%% Earth)",x,y);
else if (tempitem.elem==4)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Damage: %i-%i (50%% Shadow)",x,y);
else if (tempitem.elem==5)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Damage: %i-%i (50%% Light)",x,y);
else if (tempitem.elem==6)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Damage: %i-%i (50%% Poison)",x,y);
}
else
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Damage: %i-%i",x,y);
++rc;
}
if ((tempitem.mtype==2)&&(tempitem.cls!=5)&&(tempitem.cls!=6))
{
if (!((tempitem.cls==7)&&(tempitem.treasure==1)))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "Deflection: %i",tempitem.h);
++rc;
}
}
if (tempitem.mtype<3)
{
if ((tempitem.mtype==0)&&(tempitem.magic==3))
{
if ((strcmp(plyr.clname,"Warrior")==0)||(strcmp(plyr.clname,"Barbarian")==0))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+20%% Strength");
textprintf_ex(screentemp, tempfont, cc, c+(d*(rc+1)), BROWN, -1, "+15%% Maximum HP");
}
if ((strcmp(plyr.clname,"Paladin")==0)||(strcmp(plyr.clname,"Spellblade")==0)||(strcmp(plyr.clname,"Nomad")==0))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+20%% Strength");
textprintf_ex(screentemp, tempfont, cc, c+(d*(rc+1)), BROWN, -1, "+15%% Maximum HP");
textprintf_ex(screentemp, tempfont, cc, c+(d*(rc+2)), BROWN, -1, "Regenerate MP in combat");
}
if ((strcmp(plyr.clname,"Cleric")==0)||(strcmp(plyr.clname,"Mage")==0)||(strcmp(plyr.clname,"Warlock")==0))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+20%% Intelligene");
textprintf_ex(screentemp, tempfont, cc, c+(d*(rc+1)), BROWN, -1, "+15%% Maximum HP");
textprintf_ex(screentemp, tempfont, cc, c+(d*(rc+2)), BROWN, -1, "Regenerate MP in combat");
}
if ((strcmp(plyr.clname,"Thief")==0)||(strcmp(plyr.clname,"Monk")==0))
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+20%% Dexterity");
textprintf_ex(screentemp, tempfont, cc, c+(d*(rc+1)), BROWN, -1, "+15%% Maximum HP");
}
}
else if ((tempitem.cls==7)&&(tempitem.treasure==1))
{
if (tempitem.bonus==0)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+15%% HP/MP");
else
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+20%% HP/MP");
}
else
{
// New item properties section
// First, list stat bonuses if any are present
for (x=0; x<4; ++x) {totadd[x]=0; totaddct[x]=0;}
for (x=0; x<5; ++x)
{
if ((tempitem.add[x]!=-1)&&(tempitem.add[x]<4))
{
totadd[tempitem.add[x]]=totadd[tempitem.add[x]]+tempitem.addamt[x];
++totaddct[tempitem.add[x]];
}
}
for (x=0; x<4; ++x)
{
if (totadd[x]!=0)
{
if (totaddct[x]>1)
{
if (x==0)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+%i Strength (%i)",totadd[x],totaddct[x]);
if (x==1)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+%i Dexterity (%i)",totadd[x],totaddct[x]);
if (x==2)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+%i Intelligence (%i)",totadd[x],totaddct[x]);
if (x==3)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+%i Charisma (%i)",totadd[x],totaddct[x]);
}
else
{
if (x==0)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+%i Strength",totadd[x]);
if (x==1)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+%i Dexterity",totadd[x]);
if (x==2)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+%i Intelligence",totadd[x]);
if (x==3)
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), BROWN, -1, "+%i Charisma",totadd[x]);
}
++rc;
}
}
if (tempitem.mtype<2)
{
if ((tempitem.mtype==1)&&(tempitem.cls==4))
{
for (x=0; x<5; ++x)
{
if (tempitem.add[x]==4)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "%d%% Enh. Spell Damage",tempitem.addamt[x]);
++rc;
}
if (tempitem.add[x]==5)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "Potency");
++rc;
}
if (tempitem.add[x]==6)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "%d%% Reduced MP Cost",tempitem.addamt[x]);
++rc;
}
}
}
else
{
for (x=0; x<5; ++x)
{
if (tempitem.add[x]==4)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "%i%% Enhanced Damage",tempitem.addamt[x]);
++rc;
}
if (tempitem.add[x]==5)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "Lucky");
++rc;
}
if (tempitem.add[x]==6)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "Life Stealing");
++rc;
}
if (tempitem.add[x]==7)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "Accuracy");
++rc;
}
if (tempitem.add[x]==8)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "Slumber");
++rc;
}
}
}
}
if (tempitem.mtype==2)
{
for (x=0; x<5; ++x)
{
if (tempitem.add[x]==4)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "+%d%% Maximum HP/MP",tempitem.addamt[x]);
++rc;
}
if (tempitem.add[x]==5)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "Regens %d HP/turn",tempitem.addamt[x]);
++rc;
}
if (tempitem.add[x]==6)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "Evasion");
++rc;
}
if (tempitem.add[x]==7)
{
textprintf_ex(screentemp, tempfont, cc, c+(d*rc), PURPLE, -1, "Fleet Foot");
++rc;
}
}
}
}
}
show_mouse(NULL);
blit(screentemp,screen,guilocations.infox[0],guilocations.infoy[0],guilocations.infox[0],guilocations.infoy[0],
     (guilocations.infox[1]-guilocations.infox[0]),(guilocations.infoy[1]-guilocations.infoy[0]));
show_mouse(screen);
}


void drawinfopane(int x)
{
int c, size;
FONT *tempfont;
if (fontsize==0) tempfont=smallfont;
else tempfont=largefont;
size=text_length(tempfont,"                                ")+offsetx*2;
c=(text_height(tempfont)*0.7);
guilocations.infox[0]=(screen->w-size);
guilocations.infox[1]=screen->w;
guilocations.infoy[0]=(screen->h-scaley(272));
guilocations.infoy[1]=screen->h;
if (x!=-1)
{
if (guiborders==1)
{
drawalignborder(RIGHT,BOTTOM,size,scaley(272),"Info ");
textprintf_ex(screentemp, tempfont, (screen->w-size)+offsetx, (screen->h-scaley(272))+offsety, BROWN, -1, infocapt[x]);
textprintf_ex(screentemp, tempfont, (screen->w-size)+offsetx, (screen->h-scaley(272))+offsety+c, BROWN, -1, infocapt2[x]);
}
else
{
textprintf_centre_ex(screentemp, tempfont, (screen->w-(size/2)), screen->h-(c*3)-offsety, WHITE, BLACK, "Info:");
textprintf_centre_ex(screentemp, tempfont, (screen->w-(size/2)), screen->h-(c*2)-offsety, WHITE, BLACK, infocapt[x]);
textprintf_centre_ex(screentemp, tempfont, (screen->w-(size/2)), screen->h-(c)-offsety, WHITE, BLACK, infocapt2[x]);
}
playsound("hoverover.wav");
}
}


int drawcombatborder(int bx, int by)
{
char temp[40],temp2[25];
int col[4], rowinc, xinc, x, rowat, y, drawx;
drawx=drawalignborder(RIGHT,TOP,312,scaley(632),"Encounter ");
col[0]=drawx+offsetx; col[1]=col[0]+100; col[2]=drawx+112; col[3]=col[2]+100;
rowinc=scaley(44);
strcpy(temp, "Encountered ");
strcat(temp,mons.name);
strcat(temp,".");
smalltext(drawx+offsetx,offsety,temp,BROWN,SCREEN,LEFT);
sprintf(temp,"A ");
if (mons.cls==0)
strcat(temp,"warrior type ");
if (mons.cls==1)
strcat(temp,"cleric type ");
if (mons.cls==2)
strcat(temp,"mage type ");
if (mons.cls==3)
strcat(temp,"thief type ");
if (mons.cls==4)
strcat(temp,"barbarian type ");
if (mons.cls==5)
strcat(temp,"spellblade type ");
if (mons.cls==6)
strcat(temp,"paladin type ");
if (mons.cls==7)
strcat(temp,"warlock type ");
if (mons.cls==8)
strcat(temp,"monk type ");
if (mons.cls==9)
strcat(temp,"nomad type ");
if (mons.mtype==0)
strcat(temp,"beast.");
if (mons.mtype==1)
strcat(temp,"humanoid.");
if (mons.mtype==2)
strcat(temp,"reptile.");
if (mons.mtype==3)
strcat(temp,"insect.");
if (mons.mtype==4)
strcat(temp,"undead.");
if (mons.mtype==5)
strcat(temp,"apparition.");
if (mons.mtype==6)
strcat(temp,"golem.");
if (mons.mtype==7)
strcat(temp,"dragon.");
if (mons.mtype==8)
strcat(temp,"elemental.");
if (mons.mtype==9)
strcat(temp,"demon.");
if (mons.mtype==10)
strcat(temp,"ooze.");
if (mons.mtype==11)
strcat(temp,"plant creature.");
if (mons.mtype==12)
strcat(temp,"strange being.");
smalltext(drawx+offsetx,offsety+text_height(smallfont),temp,BROWN,SCREEN,LEFT);
rowat=2;
smalltext(drawx+offsetx,offsety+text_height(smallfont)*(rowat),"What will you do?",BROWN,SCREEN,LEFT);
++rowat; ++rowat;
sprintf(temp,"Lvl: %d ",mons.lvl);
smalltext(drawx+offsetx,offsety+text_height(smallfont)*(rowat),temp,BROWN,SCREEN,LEFT);
//smalltext(col[2],offsety+text_height(smallfont)*(rowat),"Boons: ",BROWN,SCREEN,LEFT);
rectfill(screentemp,col[2],offsety+text_height(smallfont)*(rowat),col[2]+66,offsety+text_height(smallfont)*(rowat)+66,BROWN);
blit(monsterbuffer,screentemp,0,0,col[2]+2,offsety+text_height(smallfont)*(rowat)+2,64,64);
++rowat;
if (mons.hp<10000)
{
sprintf(temp,"HP: %d ",mons.hp);
}
else
{
sprintf(temp,"HP: %dK",(mons.hp/1000));
}
smalltext(drawx+offsetx,offsety+text_height(smallfont)*(rowat),temp,BROWN,SCREEN,LEFT);
xinc=col[2]+64+4;
if (mons.bonus!=0)
{
if (mons.bonus==1)
{halficon(4,2,xinc,offsety-24+text_height(smallfont)*(rowat),0);
addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Colossal Strength","+HP +Str");}
if (mons.bonus==2)
{halficon(3,10,xinc,offsety-24+text_height(smallfont)*(rowat),0);
addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Paralyzer","Chance to Paralyze");}
if (mons.bonus==3)
{halficon(1,3,xinc,offsety-24+text_height(smallfont)*(rowat),0);
addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Physical Resist","+Deflect");}
if (mons.bonus==4)
{halficon(8,6,xinc,offsety-24+text_height(smallfont)*(rowat),0);
addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Magic Resist","+Resistance");}
if (mons.bonus==5)
{halficon(3,9,xinc,offsety-24+text_height(smallfont)*(rowat),0);
addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Vampirism","Steals life");}
if (mons.bonus==6)
{halficon(3,5,xinc,offsety-24+text_height(smallfont)*(rowat),0);
addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Speed","+Speed");}
xinc=xinc+(32);
}
if (mons.elem!=-1)
{
if (mons.elem==0) halficon(9,3,xinc,offsety-24+text_height(smallfont)*(rowat),0);
else if (mons.elem<6) halficon(0+(mons.elem-1),4,xinc,offsety-24+text_height(smallfont)*(rowat),0);
else halficon(1,11,xinc,offsety-24+text_height(smallfont)*(rowat),0);
if (mons.elem==0) addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Element: Wind","+Resist +Dmg");
if (mons.elem==1) addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Element: Fire","+Resist +Dmg");
if (mons.elem==2) addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Element: Water","+Resist +Dmg");
if (mons.elem==3) addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Element: Earth","+Resist +Dmg");
if (mons.elem==4) addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Element: Shadow","+Resist +Dmg");
if (mons.elem==5) addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Element: Light","+Resist +Dmg");
if (mons.elem==6) addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),"Element: Poison","Chance/poison");
xinc=xinc+(32);
}
for (x=0; x<2; ++x)
{
if (mons.buffx[x]!=-1)
{
halficon(mons.buffx[x],mons.buffy[x],xinc,offsety-24+text_height(smallfont)*(rowat),0);
y=0;
if ((mons.buffx[x]==6)&&(mons.buffy[x]==7)&&(mons.magic[0]<40)) y=0;
else if ((mons.buffx[x]==6)&&(mons.buffy[x]==7)&&(mons.magic[0]<99)) y=1;
else if ((mons.buffx[x]==6)&&(mons.buffy[x]==7)) y=2;
else if ((mons.buffx[x]==0)&&(mons.buffy[x]==5)&&(mons.magic[1]<40)) y=0;
else if ((mons.buffx[x]==0)&&(mons.buffy[x]==5)&&(mons.magic[1]<99)) y=1;
else if ((mons.buffx[x]==0)&&(mons.buffy[x]==5)) y=2;
else if ((mons.buffx[x]==7)&&(mons.buffy[x]==9)&&(mons.magic[2]<40)) y=0;
else if ((mons.buffx[x]==7)&&(mons.buffy[x]==9)&&(mons.magic[2]<99)) y=1;
else if ((mons.buffx[x]==7)&&(mons.buffy[x]==9)) y=2;
else if ((mons.buffx[x]==0)&&(mons.buffy[x]==8)&&(mons.magic[3]<75)) y=0;
else if ((mons.buffx[x]==0)&&(mons.buffy[x]==8)) y=1;
else if ((mons.buffx[x]==1)&&(mons.buffy[x]==5)&&(mons.magic[3]<40)) y=0;
else if ((mons.buffx[x]==1)&&(mons.buffy[x]==5)) y=1;
else if ((mons.buffx[x]==2)&&(mons.buffy[x]==9)&&(mons.magic[4]<40)) y=0;
else if ((mons.buffx[x]==2)&&(mons.buffy[x]==9)&&(mons.magic[4]<99)) y=1;
else if ((mons.buffx[x]==2)&&(mons.buffy[x]==9)) y=2;
else if ((mons.buffx[x]==2)&&(mons.buffy[x]==5)&&(mons.magic[5]<55)) y=0;
else if ((mons.buffx[x]==2)&&(mons.buffy[x]==5)) y=1;
else if ((mons.buffx[x]==6)&&(mons.buffy[x]==6)&&(mons.magic[5]<99)) y=0;
else if ((mons.buffx[x]==6)&&(mons.buffy[x]==6)) y=1;
fetchbuffinfo(mons.buffx[x],mons.buffy[x],0,y); strcpy(temp, passtemp);
fetchbuffinfo(mons.buffx[x],mons.buffy[x],1,y); strcpy(temp2, passtemp);
addtoinfo(xinc,offsety-24+text_height(smallfont)*(rowat),xinc+(32),offsety-24+text_height(smallfont)*(rowat)+(32),temp,temp2);
xinc=xinc+(32);
}
}
rowat=rowat+4;
textprintf_ex(screentemp, smallfont, drawx+offsetx, offsety+text_height(smallfont)*(rowat), BROWN, -1, "Player:");
++rowat;
textprintf_ex(screentemp, smallfont, drawx+offsetx, offsety+text_height(smallfont)*(rowat), BROWN, -1, "HP: %i / %i",plyr.hp,(plyr.hpm+plyr.adjhp));
++rowat;
if (plyr.mpm>0) textprintf_ex(screentemp, smallfont, drawx+offsetx, offsety+text_height(smallfont)*(rowat), BROWN, -1, "MP: %i / %i",plyr.mp,(plyr.mpm+plyr.adjmp));
++rowat;
if (plyr.equipped[9]!=-1) textprintf_ex(screentemp, smallfont, drawx+offsetx, offsety+text_height(smallfont)*(rowat), BROWN, -1, "W: %s",plyr.invname[plyr.equipped[9]]);
else textprintf_ex(screentemp, smallfont, drawx+offsetx, offsety+text_height(smallfont)*(rowat), BROWN, -1, "W: None");
++rowat;
textprintf_ex(screentemp, smallfont, drawx+offsetx, offsety+text_height(smallfont)*(rowat), BROWN, -1, "Boons:");
drawplayerbuffs(drawx+offsetx, offsety+text_height(smallfont)*(rowat+1));
return 0;
}

int weaponskillupchance(int which)
{
int x;
if (plyr.equipped[9]!=-1)
{
if (which>6)
{
which=which-6;
if (plyr.rwskill[which]>89)
x=randomnum(0,6);
else if (plyr.rwskill[which]>59)
x=randomnum(0,3);
else
x=0;
if ((x==0)&&(plyr.rwskill[which]<99))
{
return 1;
}
else return 0;
}
else
{
if (plyr.wskill[which]>89)
x=randomnum(0,6);
else if (plyr.wskill[which]>59)
x=randomnum(0,3);
else
x=0;
if ((x==0)&&(plyr.wskill[which]<99))
{
return 1;
}
else return 0;
}
}
else return 0;
}


void warlockstunchance()
{
int x;
if ((strcmp(plyr.clname,"Warlock")==0)&&(plyr.trade==1)&&(plyr.scholar==99))
{
x=randomnum(1,4);
if (x==1)
{
addlog("Fiery Secret triggers and enemy is stunned.",0);
mons.stun=randomnum(1,3);
}
}
}

void playerheal(int amt)
{
if ((strcmp(plyr.clname,"Cleric")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) amt=amt+(amt/5);
plyr.hp=plyr.hp+amt; if (plyr.hp>(plyr.hpm+plyr.adjhp)) plyr.hp=(plyr.hpm+plyr.adjhp);
rightpane=0;
}

void plyrdamagecheck(int amt)
{
int a, c;
a=-1;
for (c=0; c<10; ++c)
if ((plyr.buffx[c]==1)&&(plyr.buffy[c]==5)&&(plyr.buffmod[c]==1)) a=randomnum(1,10);
if (strcmp(plyr.clname,"Monk")==0)
{
c=randomnum(1,100);
if (c<41) a=1;
}
if (a==1)
{
if (strcmp(plyr.clname,"Monk")==0)
addlog("Your Chi absorbed the hit.",0);
else
addlog("Fortification absorbed the hit.",0);
}
else
plyr.hp=plyr.hp-amt;
}

void monsdamagecheck(int amt)
{
int a;
a=-1;
if ((((mons.buffx[0]==1)&&(mons.buffy[0]==5))||
    ((mons.buffx[1]==1)&&(mons.buffy[1]==5)))&&(mons.magic[3]>39)) a=randomnum(1,10);
if (mons.cls==8) a=randomnum(1,4);
if (a==1)
{
if (mons.cls==8)
addlog("Enemy's Chi absorbed the hit.",0);
else
addlog("Enemy's Fortification absorbed the hit.",0);
}
else
mons.hp=mons.hp-amt;
}

void tradeguild()
{
int done, x, y, z, lastinfo, lastmousex, lastmousey, c, d, e, totalopt, col, row, pickt[15],
cls, h, drawx, drawy;
char temp[120], temp2[10];
done=-1; lastinfo=-1; lastmousex=-1; lastmousey=-1;
while (done==-1)
{
col=0; row=0; totalopt=1;
infoct=0;
//drawborder(0,0,screentemp->w,bknd->h,"Trade Guild  ");
drawx=drawalignborder(CENTER,CENTER,640,400,"Trade Guild  ");
drawx=drawx+offsetx;
drawy=(screen->h/2)-200+offsety;
shadowtext((screenresx/2),drawy,"TRADE GUILD",GOLD,SCREEN,CENTER);
strcpy(temp,"Current skill levels: Archaeologist ");
sprintf(temp,"Current skill levels: Archaeologist %d, Scholar %d, Alchemist %d.",plyr.arch,plyr.scholar,plyr.alch);
shadowtext((screenresx/2),drawy+text_height(smallfont),temp,WHITE,SCREEN,CENTER);
if (plyr.trade==-1)
smalltext(drawx,drawy+text_height(smallfont)*2,"Current Trade: None",WHITE,SCREEN,LEFT);
else if (plyr.trade==0)
smalltext(drawx,drawy+text_height(smallfont)*2,"Current Trade: Archaeologist",WHITE,SCREEN,LEFT);
else if (plyr.trade==1)
smalltext(drawx,drawy+text_height(smallfont)*2,"Current Trade: Scholar",WHITE,SCREEN,LEFT);
else if (plyr.trade==2)
smalltext(drawx,drawy+text_height(smallfont)*2,"Current Trade: Alchemist",WHITE,SCREEN,LEFT);
//shadowtext((screen->w/2)+drawx,drawy+text_height(smallfont)*2,"Upgrades",WHITE,SCREEN,LEFT);
blitbutton(5,11,drawx,drawy+text_height(smallfont)*3,0);
addtoinfo(drawx,drawy+text_height(smallfont)*3,drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+buttonbord->h,"Change/Learn Trade","Cost: 500 Gold");
smalltext(drawx+(buttonbord->w),drawy+text_height(smallfont)*3,"Change/Learn Trade",BROWN,SCREEN,LEFT);
clickx[0][0]=drawx; clickx[0][1]=drawx+buttonbord->w; clicky[0][0]=drawy+text_height(smallfont)*3; clicky[0][1]=drawy+(text_height(smallfont)*3)+buttonbord->h;
if (plyr.trade==0)
{
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(0,12,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Turn in Scraps","Requires 10 scraps");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Turn in Scraps",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(0,12,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Turn in Sigil","");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Turn in Sigils",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(0,12,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Turn in Parchment","");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Turn in Parchments",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(0,12,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Turn in fragments","Requires 10");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Turn in Weapon Fragments",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
}
if (plyr.trade==2)
{
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Create Antidote Potion","Cost Deep Moss");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Create Antidote Potion",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Create Balm","Cost Pearl");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Create Balm",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
if (plyr.alch>19)
{
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Create Healing Potion","Cost Deep Moss");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Create Healing Potion",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Create Mana Potion","Cost Nightshade");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Create Mana Potion",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
}
if (plyr.alch>29)
{
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Create Cleansing Potion","Cost Pearl");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Create Cleansing Potion",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
}
if (plyr.alch>39)
{
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Create Stat Potion","Moss");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Create Stat Potion",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
}
if (plyr.alch>64)
{
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Create Battle Potion","Nightshade");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Create Battle Potion",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
}
if (plyr.alch>89)
{
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Create Elixir","Moss+Pearl+NShade");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Create Elixir",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
}
if (plyr.alch==99)
{
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Create Giant's Potion","Moss+Pearl+NShade");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Create Giant's Potion",BROWN,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
}
}
++totalopt; ++row; if (row>5) {row=0; ++col;}
blitbutton(2,13,(col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),0);
addtoinfo((col*(320))+drawx,drawy+text_height(smallfont)*3+(row*buttonbord->h),(col*(320))+drawx+buttonbord->w,drawy+text_height(smallfont)*3+((row+1)*buttonbord->h),"Return to Town","");
smalltext((col*(320))+drawx+(buttonbord->w),drawy+text_height(smallfont)*3+(row*buttonbord->h),"Exit",GOLD,SCREEN,LEFT);
clickx[totalopt-1][0]=(col*(320))+drawx; clickx[totalopt-1][1]=(col*(320))+drawx+buttonbord->w;
clicky[totalopt-1][0]=drawy+text_height(smallfont)*3+(row*buttonbord->h); clicky[totalopt-1][1]=drawy+text_height(smallfont)*3+((row+1)*buttonbord->h);
drawlog(1); drawinfopane(-1);
blitscreen();
while ((!(mouse_b&1))&&(!(mouse_b&2)))
{
if ((fetchmousex()!=lastmousex)||(fetchmousey()!=lastmousey))
{
for (x=0; x<infoct; ++x)
{
if ((fetchmousex()>=infox[x][0])&&(fetchmousex()<=infox[x][1])&&(fetchmousey()>=infoy[x][0])&&(fetchmousey()<=infoy[x][1])&&(lastinfo!=x))
{
lastinfo=x;
show_mouse(NULL);
drawinfopane(x);
blit(screentemp,screen,guilocations.infox[0],guilocations.infoy[0],guilocations.infox[0],guilocations.infoy[0],
     (guilocations.infox[1]-guilocations.infox[0]),(guilocations.infoy[1]-guilocations.infoy[0]));
show_mouse(screen);
}
}
lastmousex=fetchmousex(); lastmousey=fetchmousey();
}
}
if (mouse_b&2)
{
while (mouse_b&2)
{
}
done=1;
}
for (x=0; x<totalopt; ++x)
{
if ((mouse_b&1)&&(fetchmousex()>=clickx[x][0])&&(fetchmousex()<=clickx[x][1])&&(fetchmousey()>=clicky[x][0])&&(fetchmousey()<=clicky[x][1]))
{
if (x==0)
blitinversebutton(5,11,drawx,drawy+text_height(smallfont)*3,0);
else if (x==(totalopt-1))
blitinversebutton(2,13,clickx[x][0],clicky[x][0],0);
else if (plyr.trade==2)
{
if (x>5)
blitinversebutton(9,4,(col*(320))+drawx,drawy+text_height(smallfont)*3+((x-6)*buttonbord->h),0);
else
blitinversebutton(9,4,drawx,drawy+text_height(smallfont)*3+(x*buttonbord->h),0);
}
else if (plyr.trade==0)
{
blitinversebutton(0,12,drawx,drawy+text_height(smallfont)*3+(x*buttonbord->h),0);
}
blitscreen();
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
if (x==0)
{
strcpy(menuopt[0], "Archaeologist");
strcpy(menuopt[1], "Scholar");
strcpy(menuopt[2], "Alchemist");
strcpy(menuopt[3], "None");
//drawborder(0,0,screentemp->w,bknd->h,"Trade Guild  ");
y = draw_menu(CENTER, TOP, 4, 2,"Choose Trade",3);
if (y!=-2)
{
if (plyr.gold<500)
{
addlog("You don't have enough gold",1);
}
else
{
if (y==4) y=-1;
if (plyr.trade==y)
{
addlog("That's already your current trade.",1);
}
else
{
plyr.gold=plyr.gold-500;
plyr.trade=y;
if ((y==0)&&(plyr.arch==0)) plyr.arch=1;
if ((y==1)&&(plyr.scholar==0)) plyr.scholar=1;
if ((y==2)&&(plyr.alch==0)) plyr.alch=1;
addlog("Learned a trade at the Trade Guild.",0);
if ((plyr.trade!=0)&&(plyr.invmagic[plyr.equipped[8]]==3)) plyr.equipped[8]=-1;
if ((plyr.trade!=0)&&(plyr.invmagic[plyr.equipped[9]]==3)) plyr.equipped[9]=-1;
}
}
}
}
if ((x==1)&&(plyr.trade==0))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=4009)); ++y);
if (y==80)
addlog("You don't have any scraps.",1);
else
{
if (plyr.invnum[y]<10)
addlog("You need at least 10 scrap to turn in.",1);
else
{
d=(plyr.invnum[y]/10);
plyr.invnum[y]=plyr.invnum[y]-(d*10);
if (plyr.invnum[y]==0) lostitem(y);
c=0;
for (y=0; y<d; ++y)
{
if (plyr.arch<60) e=0;
else if (plyr.arch<75) e=randomnum(0,1);
else if (plyr.arch<90) e=randomnum(0,2);
else e=randomnum(0,3);
if (e==0) ++c;
}
plyr.arch=plyr.arch+c;
if (plyr.arch>99) plyr.arch=99;
sprintf(temp,"Turned in %d scrap and gained %d skill.",d*10,c);
addlog(temp,0);
playsound("loot2.wav");
}
}
}
if ((x==2)&&(plyr.trade==0))
{
z=0;
for (y=0; (y<100); ++y)
{
if (plyr.invcls[y]==4010) z=z+plyr.invnum[y];
}
if (z==0)
addlog("You don't have any sigils.",1);
else
{
y=0;
while (y!=100)
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=4010)); ++y);
if (y!=100) lostitem(y);
}
c=0;
for (y=0; y<z; ++y)
{
if (plyr.arch<80) e=0;
else if (plyr.arch<90) e=randomnum(0,1);
else e=randomnum(0,2);
if (e==0) ++c;
}
plyr.arch=plyr.arch+c;
if (plyr.arch>99) plyr.arch=99;
sprintf(temp,"Turned in %d sigils and gained %d skill.",z,c);
addlog(temp,0);
playsound("loot2.wav");
}
}
if ((x==3)&&(plyr.trade==0))
{
z=0;
for (y=0; (y<100); ++y)
{
if (plyr.invcls[y]==4011) z=z+plyr.invnum[y];
}
if (z==0)
addlog("You don't have any parchments.",1);
else
{
y=0;
while (y!=100)
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=4011)); ++y);
if (y!=100) lostitem(y);
}
c=0;
for (y=0; y<z; ++y)
{
if (plyr.arch<94) e=0;
else e=randomnum(0,1);
if (e==0) ++c;
}
plyr.arch=plyr.arch+c;
if (plyr.arch>99) plyr.arch=99;
sprintf(temp,"Turned in %d parchment and gained %d skill.",z,c);
addlog(temp,0);
playsound("loot2.wav");
}
}
if ((x==4)&&(plyr.trade==0))
{
if (plyr.arch<99)
addlog("This turn in requires 99 skill.",1);
else if (plyr.invtype[99]!=-1)
addlog("Your inventory is full.",1);
else
{
z=0;
for (y=0; (y<100); ++y)
{
if (plyr.invcls[y]==4012) z=z+plyr.invnum[y];
}
if (z<10)
addlog("You don't enough fragments.",1);
else
{
strcpy(menuopt[0], "Dagger");
strcpy(menuopt[1], "1H Sword");
strcpy(menuopt[2], "1H Mace");
strcpy(menuopt[3], "2H Sword");
strcpy(menuopt[3], "Nunchuk");
c = draw_menu(CENTER, CENTER, 5, 3,"Choose Reward",0);
if (c!=-2)
{
if (c==0) {strcpy(temp,"You chose Dagger."); y=strlen(temp); strcat(temp,"Are you sure?");}
if (c==1) {strcpy(temp,"You chose one-handed"); y=strlen(temp); strcat(temp,"sword.  Are you sure?");}
if (c==2) {strcpy(temp,"You chose one-handed"); y=strlen(temp); strcat(temp,"mace.  Are you sure?");}
if (c==3) {strcpy(temp,"You chose two-handed"); y=strlen(temp); strcat(temp,"sword.  Are you sure?");}
if (c==4) {strcpy(temp,"You chose Nunchuk."); y=strlen(temp); strcat(temp,"Are you sure?");}
e=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (e==0)
{
y=0; z=c; c=0;
while (c!=10)
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=4012)); ++y);
if (y!=100)
{
if ((c+plyr.invnum[y])>10) {plyr.invnum[y]=plyr.invnum[y]-(10-c); c=10;}
else {c=c+plyr.invnum[y]; lostitem(y);}
}
}
c=0;
if (z==0) {cls=0; h=0;}
if (z==1) {cls=2; h=0;}
if (z==2) {cls=1; h=0;}
if (z==3) {cls=2; h=1;}
if (z==4) {cls=6; h=0;}
for (x=total.mons; x<total.mons+total.melee; ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl>=60)&&(tiledata[tlu[x].x][tlu[x].y].cls==cls)&&
    (tiledata[tlu[x].x][tlu[x].y].treasure==h)&&(tiledata[tlu[x].x][tlu[x].y].magic==1)) {pickt[c]=x; ++c;}
}
y=randomnum(0,c-1);
y=pickt[y];
makeweapon(-1,-1,-1,plyr.lvl+5,tlu[y].x,tlu[y].y,1);
if (z==0) strcpy(tempitem.name,"Finger of Circe");
if (z==1) strcpy(tempitem.name,"Tongue of Circe");
if (z==2) strcpy(tempitem.name,"Hand of Circe");
if (z==3) strcpy(tempitem.name,"Fist of Circe");
if (z==4) strcpy(tempitem.name,"Touch of Circe");
tempitem.magic=3;
for (y=0; y<5; ++y)
tempitem.add[y]=-2;
gotitem(-1,0);
}
}
}
}
}
if (x==(totalopt-1))
done=1;
else if (plyr.invtype[99]!=-1)
{
addlog("Your inventory is full.",1);
done=1;
}
else if ((x==1)&&(plyr.trade==2))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=5000)); ++y);
if (y==100)
addlog("You don't have any deep moss.",1);
else
{
--plyr.invnum[y]; if (plyr.invnum[y]==0) lostitem(y);
makepotion(plyr.lvl,1,-1,-1,1);
tempitem.treasure=2; tempitem.elem=0; tempitem.lvl=1;
getpotionname();
gotitem(-2,0);
alchskillup(20);
}
}
else if ((x==2)&&(plyr.trade==2))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=5002)); ++y);
if (y==100)
addlog("You don't have any pearls.",1);
else
{
strcpy(menuopt[0], "Wind");
strcpy(menuopt[1], "Fire");
strcpy(menuopt[2], "Water");
strcpy(menuopt[3], "Earth");
strcpy(menuopt[4], "Shadow");
strcpy(menuopt[5], "Light");
strcpy(menuopt[6], "Poison");
z = draw_menu(CENTER, CENTER, 7, 4,"Choose Element",0);
if (z!=-2)
{
--plyr.invnum[y]; if (plyr.invnum[y]==0) lostitem(y);
makepotion(plyr.lvl,1,-1,-1,1);
tempitem.treasure=5; tempitem.elem=z;
getpotionname();
gotitem(-2,0);
alchskillup(20);
}
}
}
else if ((x==3)&&(plyr.trade==2))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=5000)); ++y);
if (y==100)
addlog("You don't have any deep moss.",1);
else
{
--plyr.invnum[y]; if (plyr.invnum[y]==0) lostitem(y);
makepotion(plyr.lvl,1,-1,-1,1);
tempitem.treasure=1; tempitem.elem=0;
tempitem.l=20+(plyr.lvl*5);
tempitem.h=25+(plyr.lvl*5);
getpotionname();
gotitem(-2,0);
alchskillup(30);
}
}
else if ((x==4)&&(plyr.trade==2))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=5001)); ++y);
if (y==100)
addlog("You don't have any nightshade.",1);
else
{
--plyr.invnum[y]; if (plyr.invnum[y]==0) lostitem(y);
makepotion(plyr.lvl,1,-1,-1,1);
tempitem.treasure=1; tempitem.elem=1;
tempitem.l=20+(plyr.lvl*5);
tempitem.h=25+(plyr.lvl*5);
getpotionname();
gotitem(-2,0);
alchskillup(30);
}
}
else if ((x==5)&&(plyr.trade==2))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=5002)); ++y);
if (y==100)
addlog("You don't have any pearls.",1);
else
{
--plyr.invnum[y]; if (plyr.invnum[y]==0) lostitem(y);
makepotion(plyr.lvl,1,-1,-1,1);
tempitem.treasure=2; tempitem.elem=1;
getpotionname();
gotitem(-2,0);
alchskillup(40);
}
}
else if ((x==6)&&(plyr.trade==2))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=5000)); ++y);
if (y==100)
addlog("You don't have any deep moss.",1);
else
{
strcpy(menuopt[0], "Mighty Potion");
strcpy(menuopt[1], "Agile Potion");
strcpy(menuopt[2], "Sage's Potion");
strcpy(menuopt[3], "Leadership Potion");
z = draw_menu(CENTER, CENTER, 4, 2,"Choose Potion",0);
if (z!=-2)
{
--plyr.invnum[y]; if (plyr.invnum[y]==0) lostitem(y);
makepotion(plyr.lvl,1,-1,-1,1);
tempitem.treasure=3; tempitem.elem=z;
getpotionname();
gotitem(-2,0);
alchskillup(55);
}
}
}
else if ((x==7)&&(plyr.trade==2))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=5001)); ++y);
if (y==100)
addlog("You don't have any nightshade.",1);
else
{
strcpy(menuopt[0], "Frothy Potion");
strcpy(menuopt[1], "Kinetic Potion");
strcpy(menuopt[2], "Lucky Potion");
strcpy(menuopt[3], "Celerity Potion");
z = draw_menu(CENTER, CENTER, 4, 2,"Choose Potion",0);
if (z!=-2)
{
--plyr.invnum[y]; if (plyr.invnum[y]==0) lostitem(y);
makepotion(plyr.lvl,1,-1,-1,1);
tempitem.treasure=4; tempitem.elem=z;
getpotionname();
gotitem(-2,0);
alchskillup(80);
}
}
}
else if ((x==8)&&(plyr.trade==2))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=5000)); ++y);
if (y==100)
addlog("You don't have any deep moss.",1);
else
{
for (c=0; ((c<100)&&(plyr.invcls[c]!=5001)); ++c);
if (c==100)
addlog("You don't have any nightshade.",1);
else
{
for (d=0; ((d<100)&&(plyr.invcls[d]!=5002)); ++d);
if (d==100)
addlog("You don't have any pearls.",1);
else
{
for (e=0; ((e<100)&&(!((plyr.invmtype[e]==4)&&(plyr.invcls[e]==1)&&(plyr.invtreasure[e]==7)&&(plyr.invelem[e]==0)))); ++e);
if (e!=100)
addlog("You can only have one of this item at a time.",1);
else
{
--plyr.invnum[y]; if (plyr.invnum[y]==0) lostitem(y);
--plyr.invnum[c]; if (plyr.invnum[c]==0) lostitem(c);
--plyr.invnum[d]; if (plyr.invnum[d]==0) lostitem(d);
makepotion(plyr.lvl,1,-1,-1,1);
tempitem.treasure=7; tempitem.elem=0;
getpotionname();
gotitem(-2,0);
alchskillup(100);
}
}
}
}
}
else if ((x==9)&&(plyr.trade==2))
{
for (y=0; ((y<100)&&(plyr.invcls[y]!=5000)); ++y);
if (y==100)
addlog("You don't have any deep moss.",1);
else
{
for (c=0; ((c<100)&&(plyr.invcls[c]!=5001)); ++c);
if (c==100)
addlog("You don't have any nightshade.",1);
else
{
for (d=0; ((d<100)&&(plyr.invcls[d]!=5002)); ++d);
if (d==100)
addlog("You don't have any pearls.",1);
else
{
for (e=0; ((e<100)&&(!((plyr.invmtype[e]==4)&&(plyr.invcls[e]==1)&&(plyr.invtreasure[e]==7)&&(plyr.invelem[e]==1)))); ++e);
if (e!=100)
addlog("You can only have one of this item at a time.",1);
else
{
--plyr.invnum[y]; if (plyr.invnum[y]==0) lostitem(y);
--plyr.invnum[c]; if (plyr.invnum[c]==0) lostitem(c);
--plyr.invnum[d]; if (plyr.invnum[d]==0) lostitem(d);
makepotion(plyr.lvl,1,-1,-1,1);
tempitem.treasure=7; tempitem.elem=1;
getpotionname();
gotitem(-1,0);
}
}
}
}
}
}
}
}
return;
}


void doxantosintro()
{
playmusic("xantosspeech.mid",1);
clear_bitmap(screentemp);
masked_blit(maskedtiles, screentemp, (xantosx*64), (xantosy*64), (screentemp->w/2)-(64/2), (screentemp->h/3)-64, 64, 64);
regulartext((screentemp->w/2),(screentemp->h/3)+64,"Welcome, adventurer.  So pleased that you could join me.",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+text_height(largefont),"(Press Any Key)",WHITE,SCREEN,CENTER);
blitscreen();
clear_keybuf();
while (!keypressed())
{
}
clear_bitmap(screentemp);
masked_blit(maskedtiles, screentemp, (xantosx*64), (xantosy*64), (screentemp->w/2)-(64/2), (screentemp->h/3)-64, 64, 64);
regulartext((screentemp->w/2),(screentemp->h/3)+64,                         "I was once like you.  In a previous life, I was the mage Timberlain.",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+text_height(largefont),    "I entered the Endless Dungeons in an effort to find a way to seal it",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+(text_height(largefont)*2),"forever.  Little did I know that I would become trapped here.",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+(text_height(largefont)*3),"(Press Any Key)",WHITE,SCREEN,CENTER);
blitscreen();
clear_keybuf();
while (!keypressed())
{
}
clear_bitmap(screentemp);
masked_blit(maskedtiles, screentemp, (xantosx*64), (xantosy*64), (screentemp->w/2)-(64/2), (screentemp->h/3)-64, 64, 64);
regulartext((screentemp->w/2),(screentemp->h/3)+64,                         "As the months grew to years I began to change.  A power from",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+text_height(largefont),    "within this place filled me, and I was transformed into the",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+(text_height(largefont)*2),"magnificent creature you see before you.",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+(text_height(largefont)*3),"(Press Any Key)",WHITE,SCREEN,CENTER);
blitscreen();
clear_keybuf();
while (!keypressed())
{
}
clear_bitmap(screentemp);
masked_blit(maskedtiles, screentemp, (xantosx*64), (xantosy*64), (screentemp->w/2)-(64/2), (screentemp->h/3)-64, 64, 64);
regulartext((screentemp->w/2),(screentemp->h/3)+64,                         "I became the leader of sorts for this rag-tag bunch of minions.",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+text_height(largefont),    "I shall lead them upwards, outwards, into the realm of the light",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+(text_height(largefont)*2),"which was so greviously taken from me.  You shall all pay for",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+(text_height(largefont)*3),"your arrogance in sending me here!",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+(text_height(largefont)*4),"(Press Any Key)",WHITE,SCREEN,CENTER);
blitscreen();
clear_keybuf();
while (!keypressed())
{
}
clear_bitmap(screentemp);
masked_blit(maskedtiles, screentemp, (xantosx*64), (xantosy*64), (screentemp->w/2)-(64/2), (screentemp->h/3)-64, 64, 64);
regulartext((screentemp->w/2),(screentemp->h/3)+64,                         "But first, I must deal with you!  Let us do battle, someplace",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+text_height(largefont),    "more ... imaginative.",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+(text_height(largefont)*2),"(Press Any Key)",WHITE,SCREEN,CENTER);
blitscreen();
clear_keybuf();
while (!keypressed())
{
}
playsound("Portalstone.wav");
}


void drawxantostiles()
{
int x, y, z, drawx, drawy;
clear(xantosbuffer);
drawx=tiledraw.sx;
drawy=tiledraw.sy;
if (tileres==64)
{
for (x=0; x<tiledraw.x; ++x)
{
for (y=0; y<tiledraw.y; ++y)
{
if ((x==tiledraw.cx)&&(y==tiledraw.cy))
{
blit(tiles,xantosbuffer,dtiles[dgn.tset].floorx[0]*tileres,dtiles[dgn.tset].floory[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
masked_blit(maskedtiles,xantosbuffer,plyr.icnx*tileres,plyr.icny*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
}
else if ((x==tiledraw.cx)&&(y==tiledraw.cy-1))
{
blit(tiles,xantosbuffer,dtiles[dgn.tset].floorx[0]*tileres,dtiles[dgn.tset].floory[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
masked_blit(maskedtiles,xantosbuffer,xantosx*tileres,xantosy*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
}
else if ((((x>(tiledraw.cx-2))&&(x<(tiledraw.cx+2)))&&(y==tiledraw.cy-2))
    ||((x==tiledraw.cx-1)&&(y==tiledraw.cy-1))||((x==tiledraw.cx+1)&&(y==tiledraw.cy-1)))
blit(tiles,xantosbuffer,dtiles[dgn.tset].wallx[0]*tileres,dtiles[dgn.tset].wally[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
else if (((x==tiledraw.cx-1)||(x==tiledraw.cx+1))&&(y==tiledraw.cy))
blit(tiles,xantosbuffer,dtiles[dgn.tset].floorx[0]*tileres,dtiles[dgn.tset].floory[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
else
{
z=randomnum(1,10);
if (z==1) masked_blit(maskedtiles,xantosbuffer,sfieldx[1]*tileres,sfieldy[1]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
else if (z<8) masked_blit(maskedtiles,xantosbuffer,sfieldx[0]*tileres,sfieldy[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
}
}
}
}
else
{
for (x=0; x<tiledraw.x; ++x)
{
for (y=0; y<tiledraw.y; ++y)
{
if ((x==tiledraw.cx)&&(y==tiledraw.cy))
{
blit(smalltiles,xantosbuffer,dtiles[dgn.tset].floorx[0]*tileres,dtiles[dgn.tset].floory[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
masked_blit(maskedsmalltiles,xantosbuffer,plyr.icnx*tileres,plyr.icny*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
}
else if ((x==tiledraw.cx)&&(y==tiledraw.cy-1))
{
blit(smalltiles,xantosbuffer,dtiles[dgn.tset].floorx[0]*tileres,dtiles[dgn.tset].floory[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
masked_blit(maskedsmalltiles,xantosbuffer,xantosx*tileres,xantosy*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
}
else if ((((x>(tiledraw.cx-2))&&(x<(tiledraw.cx+2)))&&(y==tiledraw.cy-2))
    ||((x==tiledraw.cx-1)&&(y==tiledraw.cy-1))||((x==tiledraw.cx+1)&&(y==tiledraw.cy-1)))
blit(smalltiles,xantosbuffer,dtiles[dgn.tset].wallx[0]*tileres,dtiles[dgn.tset].wally[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
else if (((x==tiledraw.cx-1)||(x==tiledraw.cx+1))&&(y==tiledraw.cy))
blit(smalltiles,xantosbuffer,dtiles[dgn.tset].floorx[0]*tileres,dtiles[dgn.tset].floory[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
else
{
z=randomnum(1,10);
if (z==1) masked_blit(maskedsmalltiles,xantosbuffer,sfieldx[1]*tileres,sfieldy[1]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
else if (z<8) masked_blit(maskedsmalltiles,xantosbuffer,sfieldx[0]*tileres,sfieldy[0]*tileres,drawx+(x*tileres),drawy+(y*tileres),tileres,tileres);
}
}
}
}
}

void gamecredits(int flag)
{
int x, y, z, c, row, itemheight[400], itemx[400][3], itemy[400][3], totaldraw;
char temp[40],temp2[10];
BITMAP *creditsbmp;
for (x=0; x<400; ++x)
{
itemheight[x]=-1;
for (y=0; y<3; ++y)
itemx[x][y]=-1;
}
if (flag!=-1)
{
sprintf(temp,"Xantos defeated after %d turns.",plyr.combatturn);
playmusic("credits.mid",-1);
clear_bitmap(screentemp);
masked_blit(maskedtiles, screentemp, (xantosx*64), (xantosy*64), (screentemp->w/2)-(64/2), (screentemp->h/3)-64, 64, 64);
regulartext((screentemp->w/2),(screentemp->h/3)+64,temp,WHITE,SCREEN,CENTER);
if ((plyr.bestxantos==0)||(plyr.bestxantos==-1)||(plyr.bestxantos>plyr.combatturn))
{
regulartext((screentemp->w/2),(screentemp->h/3)+64+text_height(largefont),"This is a new record.",WHITE,SCREEN,CENTER);
regulartext((screentemp->w/2),(screentemp->h/3)+64+(text_height(largefont)*2),"(Press Any Key)",WHITE,SCREEN,CENTER);
plyr.bestxantos=plyr.combatturn;
}
else regulartext((screentemp->w/2),(screentemp->h/3)+64+text_height(largefont),"(Press Any Key)",WHITE,SCREEN,CENTER);
blitscreen();
clear_keybuf();
while (!keypressed())
{
}
}
else
clear_bitmap(screentemp);
clear_keybuf();
show_mouse(NULL);
regulartext(0,0,"Press Escape to Exit or P to pause",WHITE,SCREEN,LEFT);
blitscreen();
creditsbmp=create_bitmap(screentemp->w,screentemp->h-(text_height(largefont)));
row=0;
totaldraw=creditsbmp->h;
itemheight[row]=totaldraw;
itemx[row][0]=200;
totaldraw=totaldraw+text_height(largefont);
++row;
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
itemheight[row]=totaldraw;
itemx[row][0]=202;
totaldraw=totaldraw+text_height(largefont);
++row;
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
for (z=1; z<100; z=z+10)
{
if (z==11) z=10;
itemx[row][0]=300+z;
itemheight[row]=totaldraw;
totaldraw=totaldraw+text_height(largefont);
++row;
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
c=0;
for (x=0; x<64; ++x)
{
for (y=0; y<28; ++y)
{
if ((plyr.metsqr[x][y]!=-1)&&(tiledata[x][y].lvl==z))
{
itemx[row][c]=x; itemy[row][c]=y;
itemheight[row]=totaldraw;
++c; if (c>2) {c=0; ++row; totaldraw=totaldraw+64+(64/2);}
}
}
}
if (c!=0)
{
++row;
totaldraw=totaldraw+64+(64/2);
}
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
itemx[row][0]=400+z;
itemheight[row]=totaldraw;
totaldraw=totaldraw+text_height(largefont);
++row;
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
c=0;
for (x=0; x<64; ++x)
{
for (y=0; y<28; ++y)
{
if ((plyr.metsqr[x][y]==-1)&&(tiledata[x][y].lvl==z)&&(tiledata[x][y].type==1))
{
itemx[row][c]=x; itemy[row][c]=y;
itemheight[row]=totaldraw;
++c; if (c>2) {c=0; ++row; totaldraw=totaldraw+64+(64/2);}
}
}
}
if (c!=0)
{
++row;
totaldraw=totaldraw+64+(64/2);
}
itemheight[row]=totaldraw;
itemx[row][0]=201;
totaldraw=totaldraw+text_height(largefont);
++row;
}
itemheight[row]=totaldraw;
itemx[row][0]=203;
totaldraw=totaldraw+text_height(largefont);
++row;
z=totaldraw;
totaldraw=0;
for (x=0; ((x<400)&&(itemx[x][0]!=-1)); ++x);
c=x;
while ((!(key[KEY_ESC]))&&(totaldraw!=z+(creditsbmp->h)))
{
if (key[KEY_P])
{
while (key[KEY_P])
{
}
while (!(key[KEY_P]))
{
}
while (key[KEY_P])
{
}
}
clear_bitmap(creditsbmp);
for (x=0; x<c; ++x)
{
--itemheight[x];
if ((itemheight[x]>0)&&(itemheight[x]<creditsbmp->h))
{
if (itemx[x][0]==200)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], WHITE,  -1, "The Endless Dungeons");
if (itemx[x][0]==201)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], WHITE,  -1, "");
if (itemx[x][0]==202)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], WHITE,  -1, "Cast of Creatures");
if (itemx[x][0]==203)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], WHITE,  -1, "Thank you for playing!");
if (itemx[x][0]==301)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 1-9");
if (itemx[x][0]==310)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 10-19");
if (itemx[x][0]==320)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 20-29");
if (itemx[x][0]==330)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 30-39");
if (itemx[x][0]==340)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 40-49");
if (itemx[x][0]==350)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 50-59");
if (itemx[x][0]==360)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 60-69");
if (itemx[x][0]==370)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 70-79");
if (itemx[x][0]==380)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 80-89");
if (itemx[x][0]==390)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Encountered, Levels 90-99");
if (itemx[x][0]==401)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 1-9");
if (itemx[x][0]==410)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 10-19");
if (itemx[x][0]==420)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 20-29");
if (itemx[x][0]==430)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 30-39");
if (itemx[x][0]==440)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 40-49");
if (itemx[x][0]==450)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 50-59");
if (itemx[x][0]==460)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 60-69");
if (itemx[x][0]==470)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 70-79");
if (itemx[x][0]==480)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 80-89");
if (itemx[x][0]==490)
textprintf_centre_ex(creditsbmp, largefont, (creditsbmp->w/2), itemheight[x], GOLD,  -1, "Creatures Not Yet Encountered, Levels 90-99");
if ((itemx[x][0]<200)&&(itemx[x][0]!=-1))
{
masked_blit(maskedtiles,creditsbmp,itemx[x][0]*64,itemy[x][0]*64,64,itemheight[x],64,64);
textprintf_ex(creditsbmp, largefont, 64+64+(64/2), itemheight[x], WHITE,  -1, tiledata[itemx[x][0]][itemy[x][0]].name);
}
if ((itemx[x][1]<200)&&(itemx[x][1]!=-1))
{
masked_blit(maskedtiles,creditsbmp,itemx[x][1]*64,itemy[x][1]*64,(creditsbmp->w/2)-(text_length(largefont,"0123456789012345")),itemheight[x],64,64);
textprintf_ex(creditsbmp, largefont, ((creditsbmp->w/2)-(text_length(largefont,"0123456789012345")))+64+(64/2), itemheight[x], WHITE,  -1, tiledata[itemx[x][1]][itemy[x][1]].name);
}
if ((itemx[x][2]<200)&&(itemx[x][2]!=-1))
{
masked_blit(maskedtiles,creditsbmp,itemx[x][2]*64,itemy[x][2]*64,(creditsbmp->w)-(text_length(largefont,"0123456789012345678901234567890123")),itemheight[x],64,64);
textprintf_ex(creditsbmp, largefont, ((creditsbmp->w)-(text_length(largefont,"0123456789012345678901234567890123")))+64+(64/2), itemheight[x], WHITE,  -1, tiledata[itemx[x][2]][itemy[x][2]].name);
}
}
}
++totaldraw;
blit(creditsbmp,screentemp,0,0,0,0+text_height(largefont),creditsbmp->w,creditsbmp->h);
blitscreen();
}

clear_bitmap(screentemp);
masked_blit(maskedtiles, screentemp, (xantosx*64), (xantosy*64), (screentemp->w/2)-(text_length(largefont,"0123456"))-64, (screenresy/2)-(64/2), 64, 64);
masked_blit(maskedtiles, screentemp, (plyr.icnx*64), (plyr.icny*64), (screentemp->w/2)+(text_length(largefont,"0123456")), (screenresy/2)-(64/2), 64, 64);
regulartext((screentemp->w/2),(screenresy/2)-(text_height(largefont)/2),"THE END",WHITE,SCREEN,CENTER);
blitscreen();
clear_keybuf();
while (!keypressed())
{
}
destroy_bitmap(creditsbmp);
show_mouse(screen);
}


void hallofheroes()
{
static int lastsquarex = -1;
static int lastsquarey = -1;
int done, done2, done3, x, y, z, lastinfo, lastmousex, lastmousey, gemmax[5], gempay[5], c, d, e, mx, my, gx, gy, gw, gh, a, drawx, drawy;
char temp[80], temp2[20];
done=-1; lastinfo=-1; lastmousex=-1; lastmousey=-1;
while (done==-1)
{
infoct=0;
drawx=drawalignborder(CENTER,CENTER,640,400,"Hall of Heroes  ");
drawx=drawx+offsetx;
drawy=(screen->h/2)-200+offsety;
shadowtext((screenresx/2),drawy,"HALL OF HEROES",GOLD,SCREEN,CENTER);
smallshadowtext(drawx,drawy+text_height(smallfont)*2,"Quests",WHITE,SCREEN,LEFT);
smallshadowtext((screenresx/2),drawy+text_height(smallfont)*2,"Upgrades",WHITE,SCREEN,LEFT);
if (plyr.llevel<5)
{
blitbutton(6,12,drawx,drawy+text_height(smallfont)*3,0);
addtoinfo(drawx,drawy+text_height(smallfont)*3,drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+buttonbord->h,"Come back after reaching","this objective.");
}
else if ((plyr.llevel>4)&&(plyr.levelquest[0]==-1))
{
blitbutton(9,6,drawx,drawy+text_height(smallfont)*3,0);
addtoinfo(drawx,drawy+text_height(smallfont)*3,drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+buttonbord->h,"Click to claim your reward!","");
}
else if ((plyr.llevel>4)&&(plyr.levelquest[0]==0))
{
blitbutton(9,14,drawx,drawy+text_height(smallfont)*3,0);
addtoinfo(drawx,drawy+text_height(smallfont)*3,drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+buttonbord->h,"Reward already claimed","");
}
smalltext(drawx+buttonbord->w,drawy+text_height(smallfont)*3,"Reach dungeon level 5",BROWN,SCREEN,LEFT);
clickx[0][0]=drawx; clickx[0][1]=drawx+buttonbord->w; clicky[0][0]=drawy+text_height(smallfont)*3; clicky[0][1]=drawy+(text_height(smallfont)*3)+buttonbord->h;
if (plyr.kills<500)
{
blitbutton(6,12,drawx,drawy+(text_height(smallfont)*3)+buttonbord->h,0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+buttonbord->h,drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),"Come back after reaching","this objective.");
}
else if ((plyr.kills>499)&&(plyr.killquest[0]==-1))
{
blitbutton(9,6,drawx,drawy+(text_height(smallfont)*3)+buttonbord->h,0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+buttonbord->h,drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),"Click to claim your reward!","");
}
else if ((plyr.kills>499)&&(plyr.killquest[0]==0))
{
blitbutton(9,14,drawx,drawy+(text_height(smallfont)*3)+buttonbord->h,0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+buttonbord->h,drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),"Reward already claimed","");
}
smalltext(drawx+buttonbord->w,drawy+text_height(smallfont)*3+buttonbord->h,"Defeat 500 monsters",BROWN,SCREEN,LEFT);
clickx[1][0]=drawx; clickx[1][1]=drawx+buttonbord->w; clicky[1][0]=drawy+text_height(smallfont)*3+buttonbord->h; clicky[1][1]=drawy+text_height(smallfont)*3+(buttonbord->h*2);
if (plyr.llevel<25)
{
blitbutton(6,12,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),"Come back after reaching","this objective.");
}
else if ((plyr.llevel>24)&&(plyr.levelquest[1]==-1))
{
blitbutton(9,6,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),"Click to claim your reward!","");
}
else if ((plyr.llevel>24)&&(plyr.levelquest[1]==0))
{
blitbutton(9,14,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),"Reward already claimed","");
}
smalltext(drawx+buttonbord->w,drawy+text_height(smallfont)*3+(buttonbord->h*2),"Reach Dungeon Level 25",BROWN,SCREEN,LEFT);
clickx[2][0]=drawx; clickx[2][1]=drawx+buttonbord->w; clicky[2][0]=drawy+text_height(smallfont)*3+(buttonbord->h*2); clicky[2][1]=drawy+text_height(smallfont)*3+(buttonbord->h*3);
if (plyr.kills<1200)
{
blitbutton(6,12,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),"Come back after reaching","this objective.");
}
else if ((plyr.kills>1199)&&(plyr.killquest[1]==-1))
{
blitbutton(9,6,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),"Click to claim your reward!","");
}
else if ((plyr.kills>1199)&&(plyr.killquest[1]==0))
{
blitbutton(9,14,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),"Reward already claimed","");
}
smalltext(drawx+buttonbord->w,drawy+text_height(smallfont)*3+(buttonbord->h*3),"Defeat 1200 monsters",BROWN,SCREEN,LEFT);
clickx[3][0]=drawx; clickx[3][1]=drawx+buttonbord->w; clicky[3][0]=drawy+text_height(smallfont)*3+(buttonbord->h*3); clicky[3][1]=drawy+text_height(smallfont)*3+(buttonbord->h*4);
if (plyr.llevel<60)
{
blitbutton(6,12,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),"Come back after reaching","this objective.");
}
else if ((plyr.llevel>59)&&(plyr.levelquest[2]==-1))
{
blitbutton(9,6,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),"Click to claim your reward!","");
}
else if ((plyr.llevel>59)&&(plyr.levelquest[2]==0))
{
blitbutton(9,14,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),"Reward already claimed","");
}
smalltext(drawx+buttonbord->w,drawy+text_height(smallfont)*3+(buttonbord->h*4),"Reach Dungeon Level 60",BROWN,SCREEN,LEFT);
clickx[4][0]=drawx; clickx[4][1]=drawx+buttonbord->w; clicky[4][0]=drawy+text_height(smallfont)*3+(buttonbord->h*4); clicky[4][1]=drawy+text_height(smallfont)*3+(buttonbord->h*5);
if (plyr.llevel<99)
{
blitbutton(6,12,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*6),"Come back after reaching","this objective.");
}
else if ((plyr.llevel>98)&&(plyr.levelquest[3]==-1))
{
blitbutton(9,6,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*6),"Click to claim your reward!","");
}
else if ((plyr.llevel>98)&&(plyr.levelquest[3]==0))
{
blitbutton(9,14,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),0);
addtoinfo(drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),drawx+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*6),"Reward already claimed","");
}
smalltext(drawx+buttonbord->w,drawy+text_height(smallfont)*3+(buttonbord->h*5),"Reach Dungeon Level 99",BROWN,SCREEN,LEFT);
clickx[5][0]=drawx; clickx[5][1]=drawx+buttonbord->w; clicky[5][0]=drawy+text_height(smallfont)*3+(buttonbord->h*5); clicky[5][1]=drawy+text_height(smallfont)*3+(buttonbord->h*6);
blitbutton(0,12,(screenresx/2),drawy+text_height(smallfont)*3,0);
addtoinfo((screenresx/2),drawy+text_height(smallfont)*3,(screenresx/2)+buttonbord->w,drawy+(text_height(smallfont)*3)+buttonbord->h,"Exchange 10 scrolls for 5 gems of","your choice.");
smalltext((screenresx/2)+buttonbord->w,drawy+text_height(smallfont)*3,"Exchange scrolls for gems",BROWN,SCREEN,LEFT);
clickx[6][0]=(screenresx/2); clickx[6][1]=(screenresx/2)+buttonbord->w; clicky[6][0]=drawy+text_height(smallfont)*3; clicky[6][1]=drawy+(text_height(smallfont)*3)+buttonbord->h;
blitbutton(0,12,(screenresx/2),drawy+text_height(smallfont)*3+buttonbord->h,0);
addtoinfo((screenresx/2),drawy+text_height(smallfont)*3+buttonbord->h,(screenresx/2)+buttonbord->w,drawy+(text_height(smallfont)*3+buttonbord->h)+buttonbord->h,"Exchange 10 gems for 10 scrolls of","your choice.");
smalltext((screenresx/2)+buttonbord->w,drawy+text_height(smallfont)*3+buttonbord->h,"Exchange gems for scrolls",BROWN,SCREEN,LEFT);
clickx[7][0]=(screenresx/2); clickx[7][1]=(screenresx/2)+buttonbord->w; clicky[7][0]=drawy+text_height(smallfont)*3+(buttonbord->h); clicky[7][1]=drawy+(text_height(smallfont)*3+buttonbord->h)+buttonbord->h;
blitbutton(9,5,(screenresx/2),drawy+text_height(smallfont)*3+buttonbord->h*2,0);
addtoinfo((screenresx/2),drawy+text_height(smallfont)*3+buttonbord->h*2,(screenresx/2)+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),"Pay 10 gems to add/replace an","element on a item.");
smalltext((screenresx/2)+buttonbord->w,drawy+text_height(smallfont)*3+buttonbord->h*2,"Add/replace element",BROWN,SCREEN,LEFT);
clickx[8][0]=(screenresx/2); clickx[8][1]=(screenresx/2)+buttonbord->w; clicky[8][0]=drawy+text_height(smallfont)*3+(buttonbord->h*2); clicky[8][1]=drawy+(text_height(smallfont)*3)+(buttonbord->h*3);
blitbutton(9,5,(screenresx/2),drawy+text_height(smallfont)*3+(buttonbord->h*3),0);
addtoinfo((screenresx/2),drawy+text_height(smallfont)*3+(buttonbord->h*3),(screenresx/2)+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),"Pay 15 gems to reroll stats on an","item.  Results vary.");
smalltext((screenresx/2)+buttonbord->w,drawy+text_height(smallfont)*3+(buttonbord->h*3),"Re-roll item stats",BROWN,SCREEN,LEFT);
clickx[9][0]=(screenresx/2); clickx[9][1]=(screenresx/2)+(buttonbord->w); clicky[9][0]=drawy+text_height(smallfont)*3+(buttonbord->h*3); clicky[9][1]=drawy+(text_height(smallfont)*3)+(buttonbord->h*4);
blitbutton(9,5,(screenresx/2),drawy+text_height(smallfont)*3+(buttonbord->h*4),0);
addtoinfo((screenresx/2),drawy+text_height(smallfont)*3+(buttonbord->h*4),(screenresx/2)+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),"Pay 20 gems to add a stat bonus","to an item.");
smalltext((screenresx/2)+buttonbord->w,drawy+text_height(smallfont)*3+(buttonbord->h*4),"Add a stat bonus to an item",BROWN,SCREEN,LEFT);
clickx[10][0]=(screenresx/2); clickx[10][1]=(screenresx/2)+(buttonbord->w); clicky[10][0]=drawy+text_height(smallfont)*3+(buttonbord->h*4); clicky[10][1]=drawy+(text_height(smallfont)*3)+(buttonbord->h*5);
shadowtext((screenresx/2),drawy+(text_height(smallfont)*2)+(buttonbord->h*6),"Exit",GOLD,SCREEN,LEFT);
blitbutton(2,13,(screenresx/2),drawy+(text_height(smallfont)*3)+(buttonbord->h*6),0);
addtoinfo((screenresx/2),drawy+text_height(smallfont)*3+(buttonbord->h*6),(screenresx/2)+buttonbord->w,drawy+(text_height(smallfont)*3)+(buttonbord->h*7),"Return to town","");
smalltext((screenresx/2)+buttonbord->w,drawy+text_height(smallfont)*3+(buttonbord->h*6),"Return to town",BROWN,SCREEN,LEFT);
clickx[11][0]=(screenresx/2); clickx[11][1]=(screenresx/2)+(buttonbord->w); clicky[11][0]=drawy+text_height(smallfont)*3+(buttonbord->h*6); clicky[11][1]=drawy+(text_height(smallfont)*3)+(buttonbord->h*7);
drawlog(1); drawinfopane(-1);
blitscreen();
while ((!(mouse_b&1))&&(!(mouse_b&2)))
{
if ((fetchmousex()!=lastmousex)||(fetchmousey()!=lastmousey))
{
for (x=0; x<infoct; ++x)
{
if ((fetchmousex()>=infox[x][0])&&(fetchmousex()<=infox[x][1])&&(fetchmousey()>=infoy[x][0])&&(fetchmousey()<=infoy[x][1])&&(lastinfo!=x))
{
lastinfo=x;
show_mouse(NULL);
drawinfopane(x);
blit(screentemp,screen,guilocations.infox[0],guilocations.infoy[0],guilocations.infox[0],guilocations.infoy[0],
     (guilocations.infox[1]-guilocations.infox[0]),(guilocations.infoy[1]-guilocations.infoy[0]));
show_mouse(screen);
}
}
lastmousex=fetchmousex(); lastmousey=fetchmousey();
}
}
if (mouse_b&2)
{
while (mouse_b&2)
{
}
done=1;
}
for (x=0; x<12; ++x)
{
if ((mouse_b&1)&&(fetchmousex()>=clickx[x][0])&&(fetchmousex()<=clickx[x][1])&&(fetchmousey()>=clicky[x][0])&&(fetchmousey()<=clicky[x][1]))
{
if ((plyr.llevel<5)&&(x==0))
blitinversebutton(6,12,drawx,drawy+text_height(smallfont)*3,0);
else if ((plyr.llevel>4)&&(plyr.levelquest[0]==-1)&&(x==0))
blitinversebutton(9,6,drawx,drawy+text_height(smallfont)*3,0);
else if ((plyr.llevel>4)&&(plyr.levelquest[0]==0)&&(x==0))
blitinversebutton(9,14,drawx,drawy+text_height(smallfont)*3,0);
if ((plyr.kills<500)&&(x==1))
blitinversebutton(6,12,drawx,drawy+(text_height(smallfont)*3)+buttonbord->h,0);
else if ((plyr.kills>499)&&(plyr.killquest[0]==-1)&&(x==1))
blitinversebutton(9,6,drawx,drawy+(text_height(smallfont)*3)+buttonbord->h,0);
else if ((plyr.kills>499)&&(plyr.killquest[0]==0)&&(x==1))
blitinversebutton(9,14,drawx,drawy+(text_height(smallfont)*3)+buttonbord->h,0);
if ((plyr.llevel<25)&&(x==2))
blitinversebutton(6,12,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),0);
else if ((plyr.llevel>24)&&(plyr.levelquest[1]==-1)&&(x==2))
blitinversebutton(9,6,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),0);
else if ((plyr.llevel>24)&&(plyr.levelquest[1]==0)&&(x==2))
blitinversebutton(9,14,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*2),0);
if ((plyr.kills<1200)&&(x==3))
blitinversebutton(6,12,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),0);
else if ((plyr.kills>1199)&&(plyr.killquest[1]==-1)&&(x==3))
blitinversebutton(9,6,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),0);
else if ((plyr.kills>1199)&&(plyr.killquest[1]==0)&&(x==3))
blitinversebutton(9,14,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*3),0);
if ((plyr.llevel<60)&&(x==4))
blitinversebutton(6,12,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),0);
else if ((plyr.llevel>59)&&(plyr.levelquest[2]==-1)&&(x==4))
blitinversebutton(9,6,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),0);
else if ((plyr.llevel>59)&&(plyr.levelquest[2]==0)&&(x==4))
blitinversebutton(9,14,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*4),0);
if ((plyr.llevel<99)&&(x==5))
blitinversebutton(6,12,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),0);
else if ((plyr.llevel>98)&&(plyr.levelquest[3]==-1)&&(x==5))
blitinversebutton(9,6,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),0);
else if ((plyr.llevel>98)&&(plyr.levelquest[3]==0)&&(x==5))
blitinversebutton(9,14,drawx,drawy+(text_height(smallfont)*3)+(buttonbord->h*5),0);
if (x==6)
blitinversebutton(0,12,(screenresx/2),drawy+text_height(smallfont)*3,0);
if (x==7)
blitinversebutton(0,12,(screenresx/2),drawy+text_height(smallfont)*3+buttonbord->h,0);
if (x==8)
blitinversebutton(9,5,(screenresx/2),drawy+text_height(smallfont)*3+buttonbord->h*2,0);
if (x==9)
blitinversebutton(9,5,(screenresx/2),drawy+text_height(smallfont)*3+(buttonbord->h*3),0);
if (x==10)
blitinversebutton(9,5,(screenresx/2),drawy+text_height(smallfont)*3+(buttonbord->h*4),0);
if (x==11)
blitinversebutton(2,13,(screenresx/2),drawy+(text_height(smallfont)*3)+(buttonbord->h*6),0);
blitscreen();
while (mouse_b&1)
{
}
if ((x==0)&&(plyr.llevel<5))
addlog("You can't claim this reward yet.",1);
else if ((x==0)&&(plyr.levelquest[0]==0))
addlog("You already claimed this reward.",1);
else if ((x==0)&&(plyr.invtype[99]!=-1))
addlog("Your inventory is full.",1);
else if (x==0)
{
for (y=0; y<6; ++y)
{
for (z=0; z<10; ++z)
{
makescroll(-1,-1);
tempitem.elem=y;
namescroll();
gotitem(-2,0);
}
}
plyr.levelquest[0]=0;
addlog("Recieved 10 of each scroll.",0);
savechar();
}
else if ((x==1)&&(plyr.kills<500))
addlog("You can't claim this reward yet.",1);
else if ((x==1)&&(plyr.killquest[0]==0))
addlog("You already claimed this reward.",1);
else if (x==1)
{
for (y=0; y<6; ++y)
{
for (z=0; z<10; ++z)
{
if ((y<5)&&(z<5))
{
makegem(-1,-1);
tempitem.icnx=tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+y].x;
tempitem.icny=tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+y].y;
tempitem.cls=tiledata[tempitem.icnx][tempitem.icny].cls;
gotitem(-2,0);
}
makescroll(-1,-1);
tempitem.elem=y;
namescroll();
gotitem(-2,0);
}
}
plyr.killquest[0]=0;
addlog("Recieved 10 of each scroll.",0);
addlog("Recieved 5 of each gem.",0);
savechar();
}
else if ((x==2)&&(plyr.llevel<25))
addlog("You can't claim this reward yet.",1);
else if ((x==2)&&(plyr.levelquest[1]==0))
addlog("You already claimed this reward.",1);
else if ((x==2)&&(plyr.invtype[99]!=-1))
addlog("You inventory is full.",1);
else if (x==2)
{
for (y=0; y<5; ++y)
{
for (z=0; z<10; ++z)
{
makegem(-1,-1);
tempitem.icnx=tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+y].x;
tempitem.icny=tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+y].y;
tempitem.cls=tiledata[tempitem.icnx][tempitem.icny].cls;
gotitem(-2,0);
}
}
plyr.levelquest[1]=0;
addlog("Recieved 10 of each gem.",0);
savechar();
}
else if ((x==3)&&(plyr.kills<1200))
addlog("You can't claim this reward yet.",1);
else if ((x==3)&&(plyr.killquest[1]==0))
addlog("You already claimed this reward.",1);
else if ((x==3)&&(plyr.invtype[99]!=-1))
addlog("Your inventory is full.",1);
else if (x==3)
{
rewarditem(1,0);
if ((plyr.str>=plyr.dex)&&(plyr.str>=plyr.itl)) y=0;
else if ((plyr.dex>=plyr.str)&&(plyr.dex>=plyr.itl)) y=1;
else y=2;
if (tempitem.add[0]==-1) {addstat(-1); addstat(y);}
else addstat(y);
gotitem(-1,0);
for (y=0; y<6; ++y)
{
for (z=0; z<10; ++z)
{
makescroll(-1,-1);
tempitem.elem=y;
namescroll();
gotitem(-2,0);
}
}
plyr.killquest[1]=0;
addlog("Recieved 10 of each scroll.",0);
savechar();
}
else if ((x==4)&&(plyr.llevel<60))
addlog("You can't claim this reward yet.",1);
else if ((x==4)&&(plyr.levelquest[2]==0))
addlog("You already claimed this reward.",1);
else if ((x==4)&&(plyr.invtype[99]!=-1))
addlog("Your inventory is full.",1);
else if (x==4)
{
rewarditem(2,0);
if ((plyr.str>=plyr.dex)&&(plyr.str>=plyr.itl)) y=0;
else if ((plyr.dex>=plyr.str)&&(plyr.dex>=plyr.itl)) y=1;
else y=2;
if (tempitem.add[0]==-1) {addstat(-1); addstat(y);}
else addstat(y);
gotitem(-1,0);
for (y=0; y<5; ++y)
{
for (z=0; z<15; ++z)
{
makegem(-1,-1);
tempitem.icnx=tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+y].x;
tempitem.icny=tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+y].y;
tempitem.cls=tiledata[tempitem.icnx][tempitem.icny].cls;
gotitem(-2,0);
}
}
plyr.levelquest[2]=0;
addlog("Recieved 15 of each gem.",0);
savechar();
}
else if ((x==5)&&(plyr.llevel<99))
addlog("You can't claim this reward yet.",1);
else if ((x==5)&&(plyr.levelquest[3]==0))
addlog("You already claimed this reward.",1);
else if ((x==5)&&(plyr.invtype[99]!=-1))
addlog("Your inventory is full.",1);
else if (x==5)
{
makearmor(0,5,1,-1,-1);
strcpy(tempitem.name,"Eye of Eluria");
tempitem.lvl=99;
tempitem.magic=2;
for (y=0; y<4; ++y)
{
tempitem.add[y]=y;
tempitem.addamt[y]=30;
}
tempitem.elem=7;
gotitem(-1,0);
plyr.levelquest[3]=0;
savechar();
}
else if (x==11)
done=1;
else if (x==6)
{
strcpy(menuopt[0], "Wind");
strcpy(menuopt[1], "Fire");
strcpy(menuopt[2], "Water");
strcpy(menuopt[3], "Earth");
strcpy(menuopt[4], "Shadow");
strcpy(menuopt[5], "Light");
d = draw_menu(CENTER, CENTER, 6, 4,"Choose Scroll",0);
if (d>-1)
{
gemmax[0]=0;
for (z=0; z<100; ++z)
{
if ((plyr.invmtype[z]==4)&&(plyr.invcls[z]==2)&&(plyr.invelem[z]==d))
{
y=z;
gemmax[0]=plyr.invnum[y];
}
}
if (gemmax[0]<10)
{
addlog("Scroll to gem exchange requires at least 10 scrolls.",1);
}
else
{
strcpy(menuopt[0], "Amethyst");
strcpy(menuopt[1], "Sapphire");
strcpy(menuopt[2], "Ruby");
strcpy(menuopt[3], "Quartz");
strcpy(menuopt[4], "Emerald");
d = draw_menu(CENTER, CENTER, 5, 4,"Choose Gem",0);
if (d>-1)
{
for (a=total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll;
     a<total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+total.gem; ++a)
{
if (tiledata[tlu[a].x][tlu[a].y].cls==d)
{
for (z=0; z<5; ++z)
{
makegem(tlu[a].x,tlu[a].y);
gotitem(-2,0);
}
plyr.invnum[y]=plyr.invnum[y]-10;
if (plyr.invnum[y]==0) lostitem(y);
addlog("Exchanged scrolls for gems at the Hall of Heroes.",0);
}
}
}
}
}
}
else if ((x==8)&&(plyr.lvl<25))
addlog("Requires level 25.",1);
else if ((x==9)&&(plyr.lvl<50))
addlog("Requires level 50.",1);
else if ((x==10)&&(plyr.lvl<70))
addlog("Requires level 70.",1);
else
{
// gem payment
// first, count gem totals
for (y=0; y<5; ++y)
{
gemmax[y]=0; gempay[y]=0;
}
for (y=0; y<100; ++y)
{
if (plyr.invmtype[y]==5)
gemmax[plyr.invcls[y]]=plyr.invnum[y];
}
done2=-1;
e=0;
while (done2==-1)
{
//blit the background
gw=32*16;
gh=32*10;
gx=(screenresx/2)-(gw/2);
gy=(screen->h/2)-(gh/2);
drawborder(gx,gy,gx+gw,gy+gh,"Gem Payment  ");
if (x==7) c=10;
if (x==8) c=10;
if (x==9) c=15;
if (x==10) c=20;
sprintf(temp,"Choose how to pay %d gems.",c);
smallshadowtext((screenresx/2), gy+text_height(smallfont), temp, GOLD, SCREEN, CENTER);
d=gx+32;
for (y=0; y<5; ++y)
{
for (z=0; ((z<5)&&(tiledata[tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+z].x]
                   [tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+z].y].cls!=y)); ++z);
if (y==0)
boldsmalltext(d, gy+text_height(smallfont)*2, "Amethyst", BROWN, SCREEN, LEFT);
if (y==1)
boldsmalltext(d, gy+text_height(smallfont)*2, "Sapphire", BROWN, SCREEN, LEFT);
if (y==2)
boldsmalltext(d, gy+text_height(smallfont)*2, "Ruby", BROWN, SCREEN, LEFT);
if (y==3)
boldsmalltext(d, gy+text_height(smallfont)*2, "Quartz", BROWN, SCREEN, LEFT);
if (y==4)
boldsmalltext(d, gy+text_height(smallfont)*2, "Emerald", BROWN, SCREEN, LEFT);
textprintf_ex(screentemp, smallfont, d, gy+text_height(smallfont)*3, BROWN, -1, "%i",gemmax[y]);
blit(smalltiles,screentemp,tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+z].x*32,
     tlu[total.mons+total.melee+total.ranged+total.armor+total.ammo+total.food+total.potion+total.scroll+z].y*32,d,gy+text_height(smallfont)*4,32,32);
draw_trans_sprite(screentemp,plusminus,d-6,gy+text_height(smallfont)*5+32);
clickx[y][0]=d-6;
clickx[y][1]=(d-6)+plusminus->w;
clicky[y][0]=gy+text_height(smallfont)*5+32;
clicky[y][1]=gy+text_height(smallfont)*5+32+plusminus->h;
textprintf_ex(screentemp, smallfont, d, gy+text_height(smallfont)*4+32, BLACK, -1, "%i",gempay[y]);
d=d+32*3;
}
draw_button("Confirm", (gx+(gw/2))-((buttons[0]->w/2)+(offsetx/2)), (gy+gh)-(buttons[0]->h+text_height(smallfont)), 0, 0);
clickx[5][0]=(gx+(gw/2))-((buttons[0]->w)+(offsetx/2));
clickx[5][1]=(gx+(gw/2))-(offsetx/2);
clicky[5][0]=(gy+gh)-(buttons[0]->h+text_height(smallfont));
clicky[5][1]=(gy+gh)-text_height(smallfont);
draw_button("Cancel", (gx+(gw/2))+((buttons[0]->w/2)+(offsetx/2)), (gy+gh)-(buttons[0]->h+text_height(smallfont)), 0, 0);
clickx[6][0]=(gx+(gw/2))+(offsetx/2);
clickx[6][1]=(gx+(gw/2))+((buttons[0]->w)+(offsetx/2));
clicky[6][0]=(gy+gh)-(buttons[0]->h+text_height(smallfont));
clicky[6][1]=(gy+gh)-text_height(smallfont);
drawlog(1);
blitscreen();
while ((!(mouse_b&1))&&(!(mouse_b&2)))
{
}
if (mouse_b&2)
{
while (mouse_b&2)
{
}
done2=0;
}
for (z=0; z<10; ++z)
{
if ((mouse_b&1)&&(fetchmousex()>=clickx[z][0])&&(fetchmousex()<=clickx[z][1])&&(fetchmousey()>=clicky[z][0])&&(fetchmousey()<=clicky[z][1]))
{
if (z==5)
draw_button("Confirm", (gx+(gw/2))-((buttons[0]->w/2)+(offsetx/2)), (gy+gh)-(buttons[0]->h+text_height(smallfont)), 1, 0);
if (z==6)
draw_button("Cancel", (gx+(gw/2))+((buttons[0]->w/2)+(offsetx/2)), (gy+gh)-(buttons[0]->h+text_height(smallfont)), 1, 0);
blitscreen();
while (mouse_b&1)
{
}
d=0;
for (y=0; y<5; ++y)
d=d+gempay[y];
if (z<5)
{
if (fetchmousex()<=(clickx[z][0]+((clickx[z][1]-clickx[z][0])/2)))
if (gempay[z]>0) --gempay[z];
if (fetchmousex()>(clickx[z][0]+((clickx[z][1]-clickx[z][0])/2)))
if ((gempay[z]<gemmax[z])&&(d<c)) ++gempay[z];
playsound("confirm.wav");
}
if ((z==5)&&(c!=d))
addlog("That's not enough gems.",1);
if ((z==5)&&(c==d))
{
if (x==7)
{
strcpy(menuopt[0], "Wind");
strcpy(menuopt[1], "Fire");
strcpy(menuopt[2], "Water");
strcpy(menuopt[3], "Earth");
strcpy(menuopt[4], "Shadow");
strcpy(menuopt[5], "Light");
d = draw_menu(CENTER, CENTER, 6, 4,"Choose Scroll",0);
if (d==-2) done2=0;
else
{
for (y=0; y<10; ++y)
{
makescroll(-1,-1);
tempitem.elem=d;
namescroll();
gotitem(-2,0);
}
namescroll();
strcpy(temp,"Received ");
strcat(temp,tempitem.name);
strcat(temp," x 10.");
addlog(temp,0);
e=1;
}
}
else
{
done3=-1;
bagpage=0;
while (done3==-1)
{
drawbags(bagpage,3);
drawlog(1);
blitscreen();
lastmousex=-1; lastmousey=-1;
while ((!(mouse_b&1))&&(!(mouse_b&2)))
{
if ((lastmousex!=fetchmousex())||(lastmousey!=fetchmousey()))
{
lastmousex=fetchmousex(); lastmousey=fetchmousey();
lastinfo=-1;
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if ((((mx==0)||(mx==6))&&(my>0)&&(my<11))&&((lastsquarex!=mx)||(lastsquarey!=my)))
{
lastsquarex=mx;
lastsquarey=my;
dobaginfo(mx,my);
}
}
}
if (((mouse_b&1)||(mouse_b&2))&&(fetchmousex()<guilocations.bagx[1]))
{

mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if ((mx==3)&&(my==12)&&(bagpage!=4)&&(plyr.invicnx[(bagpage*20)+20]!=-1))
{
++bagpage;
blitinversebutton(3,13,offsetx+32*3,offsety+32*12,0);
blitscreen();
while (mouse_b&1)
{
}
}
else if ((mx==2)&&(my==12)&&(bagpage>0))
{
--bagpage;
blitinversebutton(3,13,offsetx+32*2,offsety+32*12,1);
blitscreen();
while (mouse_b&1)
{
}
}
else if ((mx==0)&&(my==12))
{
blitinversebutton(0,12,offsetx,offsety+32*12,0);
//else blitinversebutton(0,12,offsetx+tileres*10-4,offsety+tileres*1-4,1);
blitscreen();
while (mouse_b&1)
{
}
sortbags();
}

else if (mouse_b&2)
{
while (mouse_b&2)
{
}
done3=0; done2=0;
}
else if ((mouse_b&1)&&((mx==0)||(mx==6))&&(my>0))
{
while (mouse_b&1)
{
}
d=my-1;  if (mx==6) d=d+10;
d=d+(bagpage*20);
if (plyr.invicnx[d]!=-1)
{
if (plyr.invmtype[d]>3)
addlog("Can't modify this item.",1);
else
{
strcpy(temp,"Really modify ");
strcat(temp,plyr.invname[d]);
strcat(temp,"?");
y=strlen(temp);
strcat(temp,"(This can't be undone)");
z=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (z==0) done3=1;
else {done3=0; done2=0;}
}
}
}
}
}
if (done3==1)
{
if (x==8)
{
strcpy(menuopt[0], "Wind");
strcpy(menuopt[1], "Fire");
strcpy(menuopt[2], "Water");
strcpy(menuopt[3], "Earth");
strcpy(menuopt[4], "Shadow");
strcpy(menuopt[5], "Light");
strcpy(menuopt[6], "Poison");
z = draw_menu(CENTER, CENTER, 7, 4,"Choose Element",0);
if (z==-2) done2=0;
else
{
if (plyr.invelem[d]==-1)
addlog("Modified an item at the Heroes Hall.",0);
else
addlog("Modified an item at the Heroes Hall.",0);
plyr.invelem[d]=z;
if (plyr.invmtype[d]==2)
{
if (plyr.invelem[d]==0) z=100;
else if (plyr.invelem[d]==1) z=200;
else if (plyr.invelem[d]==2) z=300;
else if (plyr.invelem[d]==3) z=400;
else if (plyr.invelem[d]==4) z=500;
else if (plyr.invelem[d]==5) z=600;
else if (plyr.invelem[d]==6) z=700;
else if (plyr.invelem[d]==7) z=800;
y=randomnum(25+(plyr.invlvl[d]/4),50+(plyr.invlvl[d]/2));
if (y>99) y=99;
plyr.invelem[d]=z+y;
}
equiprecalc();
e=1;
}
}
if (x==9)
{
if (plyr.invmtype[d]!=2)
makeweapon(0,0,0,plyr.lvl+randomnum(1,2),plyr.invicnx[d],plyr.invicny[d],-1);
else
makearmor(0,0,plyr.lvl+randomnum(1,2),plyr.invicnx[d],plyr.invicny[d]);
if (tempitem.add[0]==-1) addstat(-1);
gotitem(d,1);
addlog("Modified an item at the Heroes Hall.",0);
e=1;
}
if ((x==9)&&(plyr.invadd[d][4]!=-1))
addlog("Can't add any more stats to this item.",1);
else if (x==10)
{
invtotemp(d);
addstat(-1);
gotitem(d,1);
addlog("Improved an item at the Heroes Hall.",0);
e=1;
}
}
}

}
if (e==1)
{
//deduct selected gems
for (y=0; y<5; ++y)
{
for (d=0; ((d<100)&&(!((plyr.invmtype[d]==5)&&(plyr.invcls[d]==y)))); ++d);
plyr.invnum[d]=plyr.invnum[d]-gempay[y];
}
done2=1;
}
if (z==6)
done2=0;
}
}
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
}
}
}
}
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
}
return;
}

void addstat(int which)
{
int x;
for (x=0; ((x<5)&&(tempitem.add[x]!=-1)); ++x);
if (x!=5)
{
if (which==-1) which=randomnum(0,3);
tempitem.add[x]=which;
tempitem.addamt[x]=randomnum((tempitem.lvl/8),(tempitem.lvl/4));
tempitem.addamt[x]=randomnum((tempitem.lvl/8)+4,(tempitem.lvl/4)+4);
}
}

int systemmenu()
{
int done, x, y, extraopt, drawx, drawy;
char temp[80], temp2[10];
done=-1;
while (done==-1)
{
extraopt=0;
drawx=drawalignborder(CENTER,CENTER,500,400,"System Menu ");
drawx=drawx+offsetx;
drawy=(screen->h/2)-200+offsety;
drawmaindock(0,-1); drawinfopane(-1);
blitbutton(4,0,drawx,drawy,0);
smalltext(drawx+buttonbord->w,drawy,"Reset Dungeons",BROWN,SCREEN,LEFT);
clickx[0][0]=drawx; clickx[0][1]=drawx+buttonbord->w; clicky[0][0]=drawy; clicky[0][1]=drawy+buttonbord->h;
blitbutton(4,0,drawx,drawy+buttonbord->h,0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h,"Randomize Shops",BROWN,SCREEN,LEFT);
clickx[1][0]=drawx; clickx[1][1]=drawx+buttonbord->w; clicky[1][0]=drawy+buttonbord->h; clicky[1][1]=drawy+buttonbord->h*2;
blitbutton(4,0,drawx,drawy+buttonbord->h*2,0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h*2,"Delete Character",BROWN,SCREEN,LEFT);
clickx[2][0]=drawx; clickx[2][1]=drawx+buttonbord->w; clicky[2][0]=drawy+buttonbord->h*2; clicky[2][1]=drawy+buttonbord->h*3;
strcpy(temp,"Difficulty: ");
if (difficulty==2)
strcat(temp,"Hard");
else if (difficulty==1)
strcat(temp,"Normal");
else
strcat(temp,"Easy");
blitbutton(4,0,drawx,drawy+buttonbord->h*(3),0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h*(3),temp,BROWN,SCREEN,LEFT);
clickx[3][0]=drawx; clickx[3][1]=drawx+buttonbord->w; clicky[3][0]=drawy+buttonbord->h*(3); clicky[3][1]=drawy+buttonbord->h*(4);
strcpy(temp,"Map Size: ");
if (mapsize==2)
strcat(temp,"Large");
else if (mapsize==1)
strcat(temp,"Normal");
else
strcat(temp,"Small");
blitbutton(4,0,drawx,drawy+buttonbord->h*(4),0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h*(4),temp,BROWN,SCREEN,LEFT);
clickx[4][0]=drawx; clickx[4][1]=drawx+buttonbord->w; clicky[4][0]=drawy+buttonbord->h*(4); clicky[4][1]=drawy+(buttonbord->h*(5));
strcpy(temp,"Random Bosses: ");
if (bossflag==1)
strcat(temp,"Enabled");
else
strcat(temp,"Disabled");
blitbutton(4,0,drawx,drawy+buttonbord->h*(5),0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h*(5),temp,BROWN,SCREEN,LEFT);
clickx[5][0]=drawx; clickx[5][1]=drawx+buttonbord->w; clicky[5][0]=drawy+buttonbord->h*(5); clicky[5][1]=drawy+(buttonbord->h*(6));
if (plyr.bestxantos>0)
{
blitbutton(4,0,drawx,drawy+buttonbord->h*(6),0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h*6,"Reply Credits",BROWN,SCREEN,LEFT);
clickx[6][0]=drawx; clickx[6][1]=drawx+buttonbord->w; clicky[6][0]=drawy+buttonbord->h*6; clicky[6][1]=drawy+buttonbord->h*7;
blitbutton(4,0,drawx,drawy+buttonbord->h*(7),0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h*7,"Teleport to Dungeon Level",BROWN,SCREEN,LEFT);
clickx[7][0]=drawx; clickx[7][1]=drawx+buttonbord->w; clicky[7][0]=drawy+buttonbord->h*7; clicky[7][1]=drawy+(buttonbord->h*8);
extraopt=extraopt+2;
}
/*
if (cheat==999)
{
blitbutton(4,0,drawx,drawy+buttonbord->h*(3+extraopt),0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h*(3+extraopt),"Gain Level",BROWN,SCREEN,LEFT);
clickx[3+extraopt][0]=drawx; clickx[3+extraopt][1]=drawx+buttonbord->w; clicky[3+extraopt][0]=drawy+buttonbord->h*(3+extraopt); clicky[3+extraopt][1]=drawy+buttonbord->h*(4+extraopt);
blitbutton(4,0,drawx,drawy+buttonbord->h*(4+extraopt),0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h*(4+extraopt),"Generate Item",BROWN,SCREEN,LEFT);
clickx[4+extraopt][0]=drawx; clickx[4+extraopt][1]=drawx+buttonbord->w; clicky[4+extraopt][0]=drawy+buttonbord->h*(4+extraopt); clicky[4+extraopt][1]=drawy+(buttonbord->h*(5+extraopt));
extraopt=extraopt+2;
}
*/
blitbutton(4,0,drawx,drawy+buttonbord->h*(6+extraopt),0);
smalltext(drawx+buttonbord->w,drawy+buttonbord->h*(6+extraopt),"Exit Menu",BROWN,SCREEN,LEFT);
clickx[6+extraopt][0]=drawx; clickx[6+extraopt][1]=drawx+buttonbord->w; clicky[6+extraopt][0]=drawy+buttonbord->h*(6+extraopt); clicky[6+extraopt][1]=drawy+(buttonbord->h*(7+extraopt));
drawlog(1); drawstatpane(0);
blitscreen();
while ((!(mouse_b&1))&&(!(mouse_b&2))&&(!key[KEY_ESC]))
{
}
if (mouse_b&2)
{
while (mouse_b&2)
{
}
done=1;
}
if (key[KEY_ESC])
done=1;
for (x=0; x<(7+extraopt); ++x)
{
if ((mouse_b&1)&&(fetchmousex()>=clickx[x][0])&&(fetchmousex()<=clickx[x][1])&&(fetchmousey()>=clicky[x][0])&&(fetchmousey()<=clicky[x][1]))
{
blitinversebutton(4,0,drawx,drawy+buttonbord->h*x,0);
blitscreen();
while (mouse_b&1)
{
}
if (x==0)
{
strcpy(temp,"Reset all dungeon maps?  This will");
y=strlen(temp);
strcat(temp,"also repopulate monsters and loot.");
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if ((x==0)&&(gamescr==4))
addlog("Can't reset dungeon maps while in the dungeon.",1);
else if (x==0)
{
for (x=0; x<99; ++x)
plyr.savedlvl[x]=-1;
addlog("Dungeon maps reset.",1);
}
}
else if (x==1)
{
strcpy(temp,"Randomize shops?");
y=strlen(temp);
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (x==0)
{
randshops();
addlog("Shops randomized.",1);
}
}
else if (x==(6+extraopt))
{
done=1;
}
else if (x==2)
{
strcpy(temp,"Really delete this character and");
y=strlen(temp);
strcat(temp,"all associated savegame files?");
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (x==0)
{
strcpy(temp,"Delete Character");
y=strlen(temp);
strcat(temp,"Are you sure?");
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (x==0)
{
deletechar();
return 1;
}
}
}
else if (x==3)
{
++difficulty;
if (difficulty == 3) difficulty = 0;
}
else if (x==4)
{
++mapsize;
if (mapsize == 3) mapsize = 0;
}
else if (x==5)
{
++bossflag;
if (bossflag == 2) bossflag = 0;
}
else if ((x==6)&&(plyr.bestxantos>0))
{
playmusic("credits.mid",-1);
gamecredits(-1);
}
else if ((x==7)&&(plyr.bestxantos>0))
{
strcpy(menuopt[0], "10");
strcpy(menuopt[1], "20");
strcpy(menuopt[2], "30");
strcpy(menuopt[3], "40");
strcpy(menuopt[4], "50");
strcpy(menuopt[5], "60");
strcpy(menuopt[6], "70");
strcpy(menuopt[7], "80");
strcpy(menuopt[8], "90");
y = draw_menu(CENTER, CENTER, 9, 5, "Choose Level", 0);
if (y!=-2)
{
plyr.dlvl=((y+1)*10);
savechar();
if (plyr.savedlvl[plyr.dlvl-1]==-1)
{
randdungeon();
plyr.savedlvl[plyr.dlvl-1]=0;
savedlvl();
}
else
loaddlvl();
plyr.x=dgn.supx;
plyr.y=dgn.supy;
playsound("Portalstone.wav");
return 2;
}
}
else if (((x==5)&&(plyr.bestxantos>0))||(x==3))
{
if (plyr.lvl<99)
{
gainedlevel();
plyr.llevel=plyr.lvl;
for (y=0; y<6; ++y)
if ((plyr.magic[y]>0)&&(plyr.magic[y]<99)) ++plyr.magic[y];
}
}
else if (((x==6)&&(plyr.bestxantos>0))||(x==4))
{
y=randomnum(0,1);
if (y==0) rewarditem(1,0);
else rewarditem(3,0);
gotitem(-1,0);
}
}
}
}
return 0;
}

void sortbags()
{
int x, y, z, c, d;
c=0;
//equipped items first
for (x=10; x>-1; --x)
{
if (plyr.equipped[x]!=-1)
{
if (plyr.equipped[x]!=c)
{
d=plyr.equipped[x];
for (y=0; (y<11)&&(plyr.equipped[y]!=c); ++y);
if (y!=11) plyr.equipped[y]=d;
plyr.equipped[x]=c;
invswap(c,d);
}
++c;
}
}
//then gems
for (y=0; y<5; ++y)
{
d=-1;
while ((d!=0)&&(c<100))
{
for (x=c; (x<100)&&(!((plyr.invmtype[x]==5)&&(plyr.invcls[x]==y))); ++x);
if (x!=100)
{
invswap(c,x);
++c;
}
else d=0;
}
}
//then portal stones
d=-1;
while ((d!=0)&&(c<100))
{
for (x=c; (x<100)&&(!((plyr.invmtype[x]==4)&&(plyr.invcls[x]==3))); ++x);
if (x!=100)
{
invswap(c,x);
++c;
}
else d=0;
}
//then skeleton keys
d=-1;
while ((d!=0)&&(c<100))
{
for (x=c; (x<100)&&(!((plyr.invmtype[x]==4)&&(plyr.invcls[x]==4))); ++x);
if (x!=100)
{
invswap(c,x);
++c;
}
else d=0;
}
//then scrolls
for (y=0; y<7; ++y)
{
d=-1;
while ((d!=0)&&(c<100))
{
for (x=c; (x<100)&&(!((plyr.invmtype[x]==4)&&(plyr.invcls[x]==2)&&(plyr.invelem[x]==y))); ++x);
if (x!=100)
{
invswap(c,x);
++c;
}
else d=0;
}
}
//then potions, identified first
for (y=0; y<8; ++y)
{
for (z=0; z<8; ++z)
{
d=-1;
while ((d!=0)&&(c<100))
{
for (x=c; (x<100)&&(!((plyr.invmtype[x]==4)&&(plyr.invcls[x]==1)
                     &&(plyr.invtreasure[x]==y)&&(plyr.invelem[x]==z)
                     &&(plyr.invbonus[x]==0))); ++x);
if (x!=100)
{
invswap(c,x);
++c;
}
else d=0;
}
}
}
//then unidentified
d=-1;
while ((d!=0)&&(c<100))
{
for (x=c; (x<100)&&(!((plyr.invmtype[x]==4)&&(plyr.invcls[x]==1)
                     &&(plyr.invbonus[x]==-1))); ++x);
if (x!=100)
{
invswap(c,x);
++c;
}
else d=0;
}
//then food
d=-1;
while ((d!=0)&&(c<100))
{
for (x=c; (x<100)&&(!((plyr.invmtype[x]==4)&&(plyr.invcls[x]==0))); ++x);
if (x!=100)
{
invswap(c,x);
++c;
}
else d=0;
}
//then trade items
d=-1;
while ((d!=0)&&(c<100))
{
for (x=c; (x<100)&&(!(plyr.invcls[x]>2999)); ++x);
if (x!=100)
{
invswap(c,x);
++c;
}
else d=0;
}
playsound("loot1.wav");
addlog("Bags sorted.",1);
}

int combatevent(int which, int boss)
{
int x, y, done, bx, by, lastmousex, lastmousey, lastinfo, z, q, mx, my,
lastsquarex, lastsquarey, stun, a, b, c, d, e, f, g, h, pickt[30], rspeed, redrawright, redraw,
drawx, drawy, monsx, monsy;
char temp[60], temp2[40];
if ((combatzoom==1)&&(tilepref!=64))
{
zoomin();
tileres=64;
recalcdraw();
}
if (plyr.berserking>0) plyr.berserking=0;
if (fastmode==0) rspeed=750;
else rspeed=325;
redrawright=-1;
pausedungeonmusic();
clear_keybuf();
if (which!=9999)
{
x=randomnum(1,7);
sprintf(temp,"Battle%d.mid",x);
playmusic(temp,0);
}
else playmusic("xantos.mid",0);
done=-1; bagsopen=0; stun=0; iscombat=1; mons.stun=0;
mons.lvl=dgn.doormnlvl[which];
if (boss==1) mons.lvl=plyr.lvl+3;
if (which!=9999) makemonster(dgn.doormx[which],dgn.doormy[which],plyr.dlvl,boss);
else makemonster(xantosx,xantosy,plyr.dlvl,boss);
plyr.combatturn=0;
monsx=plyr.x; monsy=plyr.y;
if (which!=9999)
{
plyr.metsqr[mons.icnx][mons.icny]=0;
bx=tiledraw.cx; by=tiledraw.cy;
if (plyr.lastx<plyr.x) {++bx; ++monsx;}
else if (plyr.lastx>plyr.x) {--bx; --monsx;}
else if (plyr.lasty>plyr.y) {--by; --monsy;}
else {++by; ++monsy;}
}
else
{
bx=tiledraw.cx; by=tiledraw.cy-1;
--monsy;
}
strcpy(temp, "Encountered ");
strcat(temp,mons.name);
strcat(temp,".");
addlog(temp,0);
while (done==-1)
{
infoct=0;
if (which!=9999) redrawdungeontiles();
else blit(xantosbuffer,screentemp,0,0,0,0,screen->w,screen->h);
//blit monster graphic onto map
if (which!=9999)
{
if (tileres==64) masked_blit(maskedtiles,screentemp,mons.icnx*tileres,mons.icny*tileres,tiledraw.sx+(bx*tileres),tiledraw.sy+(by*tileres),tileres,tileres);
else masked_blit(maskedsmalltiles,screentemp,mons.icnx*tileres,mons.icny*tileres,tiledraw.sx+(bx*tileres),tiledraw.sy+(by*tileres),tileres,tileres);
blit(tiles,monsterbuffer,dgn.icnx[monsx][monsy]*64,dgn.icny[monsx][monsy]*64,0,0,64,64);
masked_blit(maskedtiles,monsterbuffer,mons.icnx*64,mons.icny*64,0,0,64,64);
}
else
{
blit(tiles,monsterbuffer,dtiles[dgn.tset].floorx[0]*64,dtiles[dgn.tset].floory[0]*64,0,0,64,64);
masked_blit(maskedtiles,monsterbuffer,xantosx*64,xantosy*64,0,0,64,64);
}
drawcombatborder(bx, by);
drawlog(2);
drawmaindock(1,-1);
for (x=0; x<3; ++x)
{
if (x==0)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Attack","Take the initiative");
if (x==1)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Speak","Hope for peace");
if (x==2)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Flee","Run away");
}
blitscreen();
lastmousex=-1; lastmousey=-1; lastinfo=-1;
while ((!(mouse_b&1))&&(!(mouse_b&2))&&(checknumkeys()==-1)
        &&(!(((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(key[KEY_F])))&&(!(key[KEY_SLASH])))
{
if ((fetchmousex()!=lastmousex)||(fetchmousey()!=lastmousey))
{
for (x=0; x<infoct; ++x)
{
if ((fetchmousex()>=infox[x][0])&&(fetchmousex()<=infox[x][1])&&(fetchmousey()>=infoy[x][0])&&(fetchmousey()<=infoy[x][1])&&(lastinfo!=x))
{
lastinfo=x;
show_mouse(NULL);
drawinfopane(x);
blit(screentemp,screen,guilocations.infox[0],guilocations.infoy[0],guilocations.infox[0],guilocations.infoy[0],
     (guilocations.infox[1]-guilocations.infox[0]),(guilocations.infoy[1]-guilocations.infoy[0]));
show_mouse(screen);
}
}
lastmousex=fetchmousex(); lastmousey=fetchmousey();
}
}

if (((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(checknumkeys()!=-1))
{
e=checknumkeys();
usehotkey(e);
}

if (key[KEY_SLASH])
{
if (help==0) help=1;
else help=0;
}

if (((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(key[KEY_F]))
{
while (key[KEY_F])
{
}
if (fastmode==1)
{
fastmode=0;
rspeed=750;
addlog("Fast combat mode disabled.",1);
}
else
{
fastmode=1;
rspeed=325;
addlog("Fast combat mode enabled.",1);
}
}


for (x=0; x<3; ++x)
{
if (((mouse_b&1)&&(fetchmousex()>=clickx[x][0])&&(fetchmousex()<=clickx[x][1])&&(fetchmousey()>=clicky[x][0])&&(fetchmousey()<=clicky[x][1]))||
    ((checknumkeys()!=-1)&&(checknumkeys()!=3)))
{
if (checknumkeys()!=-1) x=checknumkeys();
drawmaindock(1, x);
blitscreen();
while ((checknumkeys()!=-1)||(mouse_b&1))
{
}
if (x==0) done=0;
if (x==1)
{
d=0;
if (mons.align==1)
{
if ((mons.mtype==7)||(mons.mtype==1))
{
z=randomnum(((plyr.chr+plyr.adjchr)/4)*3,(plyr.chr+plyr.adjchr)+((plyr.chr+plyr.adjchr)/4));
if (z>mons.itl) d=1;
}
}
if (mons.align==2) d=1;
if (d==0) {addlog("You try to negotiate, but it doesn't work.",0); done=0;}
else
{
// Negotiation acheived, proc interaction
z=randomnum(1,100);
if (z<60)
{
// General advice
strcpy(temp,"The creature offers the following advice:");
addlog(temp,0);
z=randomnum(1,23);
if (z==1)
addlog("Beware of boss monsters.",0);
if (z==2)
addlog("Undead are weak to fire but strong to water.",0);
if (z==3)
addlog("Skeleton keys can open any door or chest.",0);
if (z==4)
addlog("Save your skeleton keys until you need them.",0);
if (z==5)
addlog("Some doors take several trys to force open.",0);
if (z==6)
addlog("Golem type creatures are weak against wind.",0);
if (z==7)
addlog("Scrolls can increase a skill or grant a boon.",0);
if (z==8)
addlog("Boons are important.",0);
if (z==9)
addlog("The martial arts weapons are strong vs humanoids.",0);
if (z==10)
addlog("The sage in town can identify potions.",0);
if (z==11)
addlog("Unidentified potions are dangerous.",0);
if (z==12)
addlog("Insects are weak against fire.",0);
if (z==13)
addlog("Reptiles are weak against water.",0);
if (z==14)
addlog("Dragons are more powerful than other creatures.",0);
if (z==15)
addlog("Oozes and undead are vulnerable to maces.",0);
if (z==16)
addlog("Swords deal bonus damage to dragons.",0);
if (z==17)
addlog("Gems are needed for powerful spells.",0);
if (z==18)
addlog("A ranged weapon makes you harder to hit.",0);
if (z==19)
addlog("If your bags are full, sell the extra.",0);
if (z==20)
addlog("Demons are weak against light and water.",0);
if (z==21)
addlog("Learning a Trade can benefit you.",0);
if (z==22)
addlog("Wands are found past dungeon level 20.",0);
if (z==23)
addlog("Creatures with colossal strength are powerful.",0);
done=4;
}
else if (z<75)
{
//Location of stairs
strcpy(temp,"The creature offers the following advice:");
addlog(temp,0);
if (plyr.x>dgn.sdnx) x=plyr.x-dgn.sdnx;
else if (plyr.x<dgn.sdnx) x=dgn.sdnx-plyr.x;
else x=0;
if (plyr.y>dgn.sdny) y=plyr.y-dgn.sdny;
else if (plyr.y<dgn.sdny) y=dgn.sdny-plyr.y;
else y=0;
if (plyr.dlvl!=99)
{
if (x>y)
{
if (plyr.x>=dgn.sdnx)
addlog("The stairs down are west of here.",0);
else
addlog("The stairs down are east of here.",0);
}
else
{
if (plyr.y>=dgn.sdny)
addlog("The stairs down are north of here.",0);
else
addlog("The stairs down are south of here.",0);
}
}
else
{
if (x>y)
{
if (plyr.x>=dgn.sdnx)
addlog("Xantos is west of here.",0);
else
addlog("Xantos is east of here.",0);
}
else
{
if (plyr.y>=dgn.sdny)
addlog("Xantos is north of here.",0);
else
addlog("Xantos is south of here.",0);
}
}
done=4;
}
else if (z<95)
{
//Business transaction
strcpy(temp,"The creature offered a business transaction.");
addlog(temp,0);
z=randomnum(1,10);
if (z<6)
makefood(plyr.dlvl,-1,-1);
else if (z<8)
makepotion(plyr.dlvl,1,-1,-1,-1);
else if (z==8)
rewarditem(1,-1);
else if (z==9)
rewarditem(2,-1);
else
makegem(-1,-1);
if (z>5)
{
strcpy(temp, "I'll sell you a ");
strcat(temp,tempitem.name);
}
else
{
strcpy(temp,"I'll sell you ");
sprintf(temp2,"%d",tempitem.num);
strcat(temp, temp2);
strcat(temp, " ");
strcat(temp, tempitem.name);
}
y=strlen(temp);
z=getprice();
z=z/2;
sprintf(temp2, "for %d gold.  Deal?",z);
strcat(temp,temp2);
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
if (x==0)
{
if (plyr.gold<z)
{
addlog("You don't have enough gold",1);
}
else
{
plyr.gold=plyr.gold-z;
gotitem(-1,0);
}
}
done=4;
}
else
{
// A gift!
strcpy(temp,"The creature says: Please accept this gift.");
addlog(temp,0);
z=randomnum(1,10);
if (z<5)
{
x=10+randomnum((plyr.dlvl/2),plyr.dlvl+5);
sprintf(temp,"Received %d gold.",x);
addlog(temp,0);
}
else
{
if (z<7)
makefood(plyr.dlvl,-1,-1);
if (z<9)
makepotion(plyr.dlvl,1,-1,-1,-1);
if (z==9)
makescroll(-1,-1);
else
makegem(-1,-1);
gotitem(-1,0);
}
}
done=4;
}
}
else if (x==2)
{
z=randomnum(1,100);
z=z+((plyr.dex+plyr.adjdex)/10);
h=0;
for (a=0; a<9; ++a)
{
if (plyr.equipped[a]!=-1)
{
for (x=0; x<5; ++x)
if (plyr.invadd[plyr.equipped[a]][x]==7)
h=1;
}
}
if (h==1) z=z+20;
if (which==9999)
{
addlog("It's impossible to flee here.",0); done=0;
}
else if (z>50)
{
addlog("You flee.",0);
playsound("flee.wav");
done=3;
}
else
{
addlog("You attempted to flee, but failed.",0); done=0;
}
}
}
}
}
lastinfo=-1; lastsquarex=-1; lastsquarey=-1;
while (done==0)
{
clear_keybuf();
infoct=0;
rightpane=0;
if (which!=9999)
{
redrawdungeontiles();
if (tileres==64) masked_blit(maskedtiles,screentemp,mons.icnx*tileres,mons.icny*tileres,tiledraw.sx+(bx*tileres),tiledraw.sy+(by*tileres),tileres,tileres);
else masked_blit(maskedsmalltiles,screentemp,mons.icnx*tileres,mons.icny*tileres,tiledraw.sx+(bx*tileres),tiledraw.sy+(by*tileres),tileres,tileres);
}
else blit(xantosbuffer,screentemp,0,0,0,0,screen->w,screen->h);
drawcombatborder(bx,by);
drawlog(2);
for (x=0; x<4; ++x)
{
if (x==0)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Attack","Attack with weapon");
if (x==1)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Item","Use or equip item");
if (x==2)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Magic","Cast a spell");
if (x==3)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Flee","Attempt to escape");
}
blitscreen();
if (stun==0)
{
z=-1;
while (z==-1)
{
redraw=1;
lastmousex=-1; lastmousey=-1; lastinfo=-1;
clear_keybuf();
while ((!(mouse_b&1))&&(!(mouse_b&2))&&(checknumkeys()==-1)&&(checkfunckeys()==0)
       &&(!(((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(key[KEY_F]))&&(!(key[KEY_SLASH])))&&(stun==0))
{
if (redraw==1)
{
infoct=0;
for (x=0; x<4; ++x)
{
if (x==0)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Attack","Attack with weapon");
if (x==1)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Item","Use or equip item");
if (x==2)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Magic","Cast a spell");
if (x==3)
addtoinfo(clickx[x][0],clicky[x][0],clickx[x][1],clicky[x][1],"Flee","Attempt to escape");
}
if (which!=9999)
{
redrawdungeontiles();
if (tileres==64) masked_blit(maskedtiles,screentemp,mons.icnx*tileres,mons.icny*tileres,tiledraw.sx+(bx*tileres),tiledraw.sy+(by*tileres),tileres,tileres);
else masked_blit(maskedsmalltiles,screentemp,mons.icnx*tileres,mons.icny*tileres,tiledraw.sx+(bx*tileres),tiledraw.sy+(by*tileres),tileres,tileres);
}
else blit(xantosbuffer,screentemp,0,0,0,0,screen->w,screen->h);
if (bagsopen==1)
{
drawbags(bagpage,0);
drawlog(1);
}
else
{
drawlog(2);
}
drawcombatborder(bx,by);
if (rightpane==1)
drawspellbook(-1,-1);
drawmaindock(2,-1);
drawinfopane(-1);
blitscreen();
redraw=0;
}
if ((fetchmousex()!=lastmousex)||(fetchmousey()!=lastmousey))
{
for (x=0; x<infoct; ++x)
{
if ((fetchmousex()>=infox[x][0])&&(fetchmousex()<=infox[x][1])&&(fetchmousey()>=infoy[x][0])&&(fetchmousey()<=infoy[x][1])&&(lastinfo!=x))
{
lastinfo=x;
show_mouse(NULL);
drawinfopane(x);
blit(screentemp,screen,guilocations.infox[0],guilocations.infoy[0],guilocations.infox[0],guilocations.infoy[0],
     (guilocations.infox[1]-guilocations.infox[0]),(guilocations.infoy[1]-guilocations.infoy[0]));
show_mouse(screen);
}
}
lastmousex=fetchmousex(); lastmousey=fetchmousey();
}
if ((bagsopen==1)&&(fetchmousex()<guilocations.bagx[1])&&(fetchmousey()<guilocations.bagy[1]))
{
lastinfo=-1;
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if ((((mx==0)||(mx==6))&&(my>0)&&(my<11))&&((lastsquarex!=mx)||(lastsquarey!=my)))
{
lastsquarex=mx;
lastsquarey=my;
dobaginfo(mx,my);
}
}
}
if (((mouse_b&1)||(mouse_b&2))&&(bagsopen==1)&&(fetchmousex()<guilocations.bagx[1]))
{
mx = (fetchmousex()-offsetx)/32;
my = (fetchmousey()-offsety)/32;
if ((mouse_b&1)&&(mx==3)&&(my==12)&&(bagpage!=4)&&(plyr.invicnx[(bagpage*20)+20]!=-1))
{
++bagpage;
blitinversebutton(3,13,offsetx+32*3,offsety+32*12,0);
blitscreen();
while (mouse_b&1)
{
}
}
else if ((mouse_b&1)&&(mx==2)&&(my==12)&&(bagpage>0))
{
--bagpage;
blitinversebutton(3,13,offsetx+32*2,offsety+32*12,1);
blitscreen();
while (mouse_b&1)
{
}
}
else if ((mx==0)&&(my==12))
{
blitinversebutton(0,12,offsetx+32*0,offsety+32*12,0);
blitscreen();
while (mouse_b&1)
{
}
sortbags();
}
else if ((mouse_b&2)&&((mx==0)||(mx==6))&&(my>0))
{
while (mouse_b&2)
{
}
f=my-1;  if (mx==6) f=f+10;
f=f+(bagpage*20);
if (plyr.invicnx[f]!=-1)
{
if ((plyr.invmtype[f]==0)||(plyr.invmtype[f]==1))
{
equipuse(f);
}
else if ((plyr.invmtype[f]==4)||(plyr.invcls[f]<3)) {bagsopen=0; z=1;}
else
addlog("Can only use items or equip weapons during combat.",1);
}
}
}

y=-1;

if (mouse_b&1)
{
for (x=0; x<4; ++x)
{
if ((fetchmousex()>=clickx[x][0])&&(fetchmousex()<=clickx[x][1])&&(fetchmousey()>=clicky[x][0])&&(fetchmousey()<=clicky[x][1]))
{
drawmaindock(2,x);
blitscreen();
while (mouse_b&1)
{
}
y=x;
}
}
}

else if (((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(key[KEY_F]))
{
while (key[KEY_F])
{
}
if (fastmode==1)
{
fastmode=0;
rspeed=750;
addlog("Fast combat mode disabled.",1);
}
else
{
fastmode=1;
rspeed=325;
addlog("Fast combat mode enabled.",1);
}
}

else if (key[KEY_SLASH])
{
if (help==0) help=1;
else help=0;
}

else if ((checknumkeys()!=-1)&&(!((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))))
{
x=checknumkeys();
drawmaindock(2,x);
y=x;
blitscreen();
while (checknumkeys()!=-1)
{
}
}
else if (((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(checknumkeys()!=-1))
{
e=checknumkeys();
if (bagsopen==1)
{
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
}
if ((bagsopen==1)&&((mx==0)||(mx==6))&&(my>0)&&(my<11))
hotkey(mx,my,e);
else
usehotkey(e);
}

if (checkfunckeys()==1)
    {
    g=-1;
    if ((bagsopen==1)&&(fetchmousex()<guilocations.bagx[1]))
    {
    mx = ((fetchmousex()-offsetx)/32);
    my = ((fetchmousey()-offsety)/32);
    e=my-1;  if (mx==6) e=e+10;
    e=e+(bagpage*20);
    if (plyr.invicnx[e]!=-1)
    {
    if ((plyr.invtype[e]==2)&&(plyr.invmtype[e]==4)
        &&((plyr.invcls[e]==1)||(plyr.invcls[e]==2)))
    {
    b=fetchfkeynum();
    if (b!=-1)
    {
    assignfuncmacro(b, e,-1);
    }
    }
    else
    {
    addlog("F-key macros require potion, scroll, or spell.",1);
    }
    g=0;
    }
    }
    else if ((rightpane==1)&&(fetchmousex()>guilocations.statpanex[0]))
    {
    e=32;
    c=32;
    mx=((fetchmousex()-((screen->w-312)+offsetx))/c);
    my=(fetchmousey()-(offsety))/e;
    if ((my==1)||(my==3)||(my==5)||(my==7))
    {
    --my;
    my=my/2;
    if ((my<4)&&(mx<6)&&(spellbutton[mx][my]!=-1))
    {
    drawspellbook(mx,my);
    blitscreen();
    b=fetchfkeynum();
    if (b!=-1)
    {
    assignfuncmacro(b,my,mx);
    g=0;
    }
    }
    }
    }
    if (g!=0)
    {
    e=fetchfkeynum();
    if (plyr.funcmacrox[e]!=-1)
    {
    if (plyr.funcmacroy[e]==-1)
    {
    f=plyr.funcmacrox[e]; z=1;
    }
    else
    {
    my=plyr.funcmacrox[e];
    mx=plyr.funcmacroy[e];
    z=2;
    }
    }
    else
    {
    sprintf(temp,"No current macro for F%d.",e+1);
    addlog(temp,1);
    }
    }
    while (checkfunckeys()==1)
          {
          }
    }



if (y!=-1)
{
if (y==0) {rightpane=0; bagsopen=0; z=0;}
else if ((y==1)&&(bagsopen==0)) {bagsopen=1; rightpane=0; redraw=1;}
else if ((y==1)&&(bagsopen==1)) {bagsopen=0; redraw=1;}
else if ((y==2)&&(rightpane==1)) {rightpane=0; bagsopen=0; redraw=1;}
else if (y==2) {rightpane=1; bagsopen=0; redraw=1;}
else if (y==3)
{
x=randomnum(1,100);
x=x+((plyr.dex+plyr.adjdex)/10);
h=0;
for (a=0; a<9; ++a)
{
if (plyr.equipped[a]!=-1)
{
for (g=0; g<5; ++g)
if (plyr.invadd[plyr.equipped[a]][g]==7)
h=1;
}
}
if (h==1) x=x+20;
if (which==9999)
{
addlog("It's impossible to flee here.",0); z=4;
}
else if (x>50)
{
addlog("You flee.",0); playsound("flee.wav"); resumedungeonmusic();
bagsopen=0; return 1;
}
else
{
addlog("You attempted to flee, but failed.",0); z=4;
}
}
}

if ((rightpane==1)&&(mouse_b&1))
{
x=32;
c=32;
mx=((fetchmousex()-((screen->w-312)+offsetx))/c);
my=(fetchmousey()-(offsety))/x;
if ((my==1)||(my==3)||(my==5)||(my==7))
{
--my;
my=my/2;
if (spellbutton[mx][my]!=-1)
{
drawspellbook(mx,my);
blitscreen();
while (mouse_b&1)
{
}
z=2;
rightpane=0;
}
}
}
}
}
// player has chosen an action, pass to the combat engine
// determine initiative
++plyr.combatturn;
d=-1; c=-1;
for (x=0; x<10; ++x)
{
if ((plyr.buffx[x]==6)&&(plyr.buffy[x]==7)&&(plyr.buffmod[x]!=0)) d=0;
else if ((plyr.buffx[x]==8)&&(plyr.buffy[x]==5)) d=0;
}
if (strcmp(plyr.clname,"Monk")==0) d=0;
if ((mons.buffx[0]==6)&&(mons.buffy[0]==7)&&(mons.magic[0]>=40)) c=0;
if ((mons.buffx[1]==6)&&(mons.buffy[1]==7)&&(mons.magic[0]>=40)) c=0;
if (((d==0)&&(mons.bonus==6))||(c==0)) {if ((plyr.dex+plyr.adjdex)>mons.dex) q=1; else q=0;}
else if (d==0) q=1;
else if ((mons.bonus==6)||(c==0)) q=0;
else {if ((plyr.dex+plyr.adjdex)>mons.dex) q=1; else q=0;}
for (d=0; d<2; ++d)
{
if (((q==0)&&(d==0))||(q==1)&&(d==1))
{
//monster turn
if (mons.stun>0) --mons.stun;
else
{
//proc damage every turn effects
if (((mons.buffx[0]==1)&&(mons.buffy[0]==9))||((mons.buffx[1]==1)&&(mons.buffy[1]==9)))
{
strcpy(temp,"Enemy's Crown of the Dead deals ");
y=10+randomnum(mons.itl/12,mons.itl/8);
if (plyr.resist[4]>0) y=y-((plyr.resist[4]*y)/132);
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
mons.hp=mons.hp+y/4; if (mons.hp>mons.hpm) mons.hp=mons.hpm;
playsound("Spell-Shadow-Boon.wav");
}
if ((((mons.buffx[0]==0)&&(mons.buffy[0]==5))||((mons.buffx[1]==0)&&(mons.buffy[1]==5)))&&(mons.magic[1]>=99))
{
strcpy(temp,"Enemy's Fire Weapon deals ");
y=15+randomnum(mons.itl/20,mons.itl/12);
if (plyr.resist[1]>0) y=y-((plyr.resist[1]*y)/132);
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage5.wav");
}
if ((((mons.buffx[0]==7)&&(mons.buffy[0]==9))||((mons.buffx[1]==7)&&(mons.buffy[1]==9)))&&(mons.magic[1]>=40))
{
strcpy(temp,"Enemy's Icy Shield heals ");
y=10+randomnum(mons.itl/20,mons.itl/12);
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp," HP.");
addlog(temp,0);
mons.hp=mons.hp+y; if (mons.hp>mons.hpm) mons.hp=mons.hpm;
playsound("Spell-Water-Heal1.wav");
}
//chance for spellcast
x=randomnum(1,100);
if (which==9999)
{
y=randomnum(0,2);
if (y==0) mons.cls=1;
else mons.cls=2;
}
if ((mons.cls==6)&&((mons.buffx[0]==2)&&(mons.buffy[0]==5))&&(mons.magic[5]<20)) x=0;
if ((mons.cls==7)||(mons.cls==1)||(mons.cls==2)) x=x+(mons.lvl/4);
if ((mons.cls==5)||(mons.cls==6)||(mons.cls==9)) x=x-20;
if (mons.lvl<20) x=x-10;
if ((which==9999)&&(x>105))
{
y=randomnum(250, 350);
strcpy(temp,"Xantos cast Anti-matter for ");
sprintf(temp2,"%d",y);
strcat(temp,temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("antimatter.wav");
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
if (plyr.hp<1) {return 3;}
}
else if ((mons.mtype==7)&&(mons.lvl>39)&&(x>70))
{
//chance for breath weapon
if (mons.elem!=-1) e=mons.elem;
else e=1;
y=randomnum((mons.lvl*2), (mons.lvl*4));
if (mons.lvl>=60) y=y*1.5;
if (plyr.resist[e]>0) y=y-((plyr.resist[e]*y)/132);
if (e==0) strcpy(temp,"Enemy breathed lightning for ");
if (e==1) strcpy(temp,"Enemy breathed fire for ");
if (e==2) strcpy(temp,"Enemy breathed ice for ");
if (e==3) strcpy(temp,"Enemy breathed sand for ");
if (e==4) strcpy(temp,"Enemy breathed a dark cloud for ");
if (e==5) strcpy(temp,"Enemy breathed holy beams for ");
if (e==6) {strcpy(temp,"Enemy breathed poison for "); gotdebuff(0);}
sprintf(temp2,"%d",y);
strcat(temp,temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage4.wav");
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
if (plyr.hp<1) {plyr.hp=0; x=playerdied(-1); if (x==0) return 3; else return 2;}
}
else if ((mons.mtype==8)&&(mons.lvl>39)&&(mons.elem!=-1)&&(x>69))
{
//chance for elemental attack
y=randomnum((mons.lvl*2), ((mons.lvl*2)+4));
if (plyr.resist[mons.elem]>0) y=y-((plyr.resist[mons.elem]*y)/132);
if (mons.elem==0) strcpy(temp,"Enemy used charged air effect for ");
if (mons.elem==1) strcpy(temp,"Enemy used scorch for ");
if (mons.elem==2) strcpy(temp,"Enemy used ice shards for ");
if (mons.elem==3) strcpy(temp,"Enemy used sand storm for ");
if (mons.elem==4) strcpy(temp,"Enemy used power of darkness for ");
if (mons.elem==5) strcpy(temp,"Enemy used divine wrath for ");
if (mons.elem==6) {strcpy(temp,"Enemy used poison cloud for "); gotdebuff(0);}
sprintf(temp2,"%d",y);
strcat(temp,temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage6.wav");
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
if (plyr.hp<1) {plyr.hp=0; x=playerdied(-1); if (x==0) return 3; else return 2;}
}
else if ((mons.cls==2)&&(mons.lvl>59)&&(x>100)&&(which!=9999))
{
//chance for polymorph spell
y=randomnum(0,2);
if (y!=0)
{
addlog("Enemy casts polymorph, but it wasn't effective.",0);
playsound("failure.wav");
}
else
{
addlog("Enemy casts polymorph.  You were cursed.",0);
gotdebuff(2);
d=0;
for (y=0; ((y<total.mons)&&(d<30)); ++y)
if (tiledata[tlu[y].x][tlu[y].y].mtype==2) {pickt[d]=y; ++d;}
y=randomnum(0,d-1);
plyr.pmorx=tlu[pickt[y]].x;
plyr.pmory=tlu[pickt[y]].y;
playsound("Spell-Cure.wav");
}
drawlog(2);
}
else if ((mons.cls!=0)&&(mons.cls!=3)&&(mons.cls!=8)&&(mons.cls!=4)&&(x>=40))
{
//monster casts a spell
x=randomnum(1,12);
if (((mons.cls==1)&&(((mons.buffx[0]==2)&&(mons.buffy[0]==5))||(mons.buffx[1]!=-1))&&(mons.magic[5]<20))) x=0;
if ((mons.cls==2)&&(((mons.buffx[0]==0)&&(mons.buffy[0]==8)&&(mons.buffx[1]!=-1))
                    ||((mons.buffx[0]==1)&&(mons.buffy[0]==5)&&(mons.buffx[1]!=-1))
                    ||((mons.buffx[0]!=-1)&&(mons.buffx[1]!=-1)))&&(mons.magic[3]<20)&&(x>9)) x=4;
if ((mons.cls==7)&&(x>4)&&(x<9)&&(mons.magic[3]<20)) {a=randomnum(0,1); if (a==0) x=0; else x=12;}
if (((mons.cls==2)&&(x<4))||((mons.cls==9)&&(x<7)))
{
//wind spell
y=1; c=-1;
if (mons.magic[0]>=30) ++y;
if (mons.magic[0]>=55) ++y;
if (mons.magic[0]>=75) ++y;
while (c==-1)
{
if (which==9999) e=randomnum(3,4);
else e=randomnum(0,y);
if (((e==0)&&((mons.buffx[0]==6)&&(mons.buffy[0]==7)))||((e==0)&&(mons.buffx[1]!=-1))) c=-1;
else c=0;
}
if (e==0)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=6; mons.buffy[0]=7;}
else {mons.buffx[1]=6; mons.buffy[1]=7;}
if (mons.magic[0]>=99) {mons.itl=mons.itl+(mons.itl/10); mons.dex=mons.dex+(mons.dex/10);}
strcpy(temp,"Enemy casts Zephyr's Boon.");
addlog(temp,0);
playsound("Spell-Wind-Boon.wav");
}
if (e==1)
{
strcpy(temp,"Enemy casts Charged Strike for ");
y=10+randomnum(mons.itl/16,mons.itl/8);
if (plyr.resist[0]>0) y=y-((plyr.resist[0]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
c=randomnum(mons.l,mons.h);
c=c+mons.str/10;
c=c-(plyr.deflect/2);  if (c<0) c=0;
y=y+c;
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage6.wav");
}
if (e==2)
{
strcpy(temp,"Enemy casts Lightning Bolt for ");
y=70+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[0]>0) y=y-((plyr.resist[0]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage6.wav");
}
if (e==3)
{
strcpy(temp,"Enemy casts Cyclone Strike for ");
a=randomnum(0,1);
if (a==0)
y=130+randomnum(mons.itl/6,mons.itl/3);
else
y=10+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[0]>0) y=y-((plyr.resist[0]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage6.wav");
}
if (e==4)
{
c=randomnum(2,4);
strcpy(temp,"Enemy's Storm ");
sprintf(temp2,"%d",c);
strcat(temp,temp2);
strcat(temp," bolts hit ");
a=0;
for (y=0; y<c; ++y) {b=randomnum(1,10); if (b<8) ++a;}
sprintf(temp2,"%d",a);
strcat(temp,temp2);
strcat(temp," times for ");
y=70+randomnum(mons.itl/6,mons.itl/3);
y=y*a;
if (plyr.resist[0]>0) y=y-((plyr.resist[0]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage1.wav");
}
}
if (((mons.cls==2)&&(x>3)&&(x<7))||((mons.cls==5)&&(x<7))||((mons.cls==7)&&(x<5)))
{
//Fire spell
y=1; c=-1;
if (mons.magic[1]>=20) ++y;
if (mons.magic[1]>=30) ++y;
if (mons.magic[1]>=55) ++y;
if (mons.magic[1]>=75) ++y;
while (c==-1)
{
if (which==9999) e=randomnum(4,5);
else e=randomnum(0,y);
if (((e==0)&&((mons.buffx[0]==0)&&(mons.buffy[0]==5)))||((e==0)&&(mons.buffx[1]!=-1))) c=-1;
else c=0;
}
if (e==0)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=0; mons.buffy[0]=5;}
else {mons.buffx[1]=0; mons.buffy[1]=5;}
strcpy(temp,"Enemy casts Fire Weapon.");
addlog(temp,0);
playsound("Spell-Fire-Boon.wav");
}
if (e==1)
{
strcpy(temp,"Enemy casts Firebolt for ");
y=14+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[1]>0) y=y-((plyr.resist[1]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage5.wav");
}
if (e==2)
{
strcpy(temp,"Enemy casts Concussive Blast for ");
y=36+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[1]>0) y=y-((plyr.resist[1]*y)/132);
if ((strcmp(plyr.clname,"Nomad")==0)&&(plyr.scholar>49)) y=0;
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
a=randomnum(1,10);
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
if (a==1)
{
addlog("You were stunned.",0);
stun=randomnum(1,3);
addlog(temp,0);
}
plyrdamagecheck(y);
playsound("Spell-Damage5.wav");
}
if (e==3)
{
strcpy(temp,"Enemy casts Inferno for ");
y=80+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[1]>0) y=y-((plyr.resist[1]*y)/132);
if ((strcmp(plyr.clname,"Nomad")==0)&&(plyr.scholar>49)) y=0;
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage2.wav");
}
if (e==4)
{
strcpy(temp,"Enemy casts Charged Blast for ");
y=200+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[1]>0) y=y-((plyr.resist[1]*y)/132);
if ((strcmp(plyr.clname,"Nomad")==0)&&(plyr.scholar>49)) y=0;
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage2.wav");
}
if (e==5)
{
strcpy(temp,"Enemy casts Meteor for ");
y=350+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[1]>0) y=y-((plyr.resist[1]*y)/132);
if ((strcmp(plyr.clname,"Nomad")==0)&&(plyr.scholar>49)) y=0;
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage4.wav");
}
}
if (((mons.cls==2)&&(x>6)&&(x<10))||((mons.cls==5)&&(x>6)))
{
//Water spell
y=1; c=-1;
if (mons.magic[2]>=20) ++y;
if (mons.magic[2]>=30) ++y;
if (mons.magic[2]>=55) ++y;
if (mons.magic[2]>=75) ++y;
while (c==-1)
{
if (which==9999) e=randomnum(4,5);
else e=randomnum(0,y);
if (((e==0)&&((mons.buffx[0]==7)&&(mons.buffy[0]==9)))||((e==0)&&(mons.buffx[1]!=-1))) c=-1;
else if (((e==5)&&((mons.buffx[0]==0)&&(mons.buffy[0]==10)))||((e==5)&&(mons.buffx[1]!=-1))) c=-1;
else c=0;
}
if (e==0)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=7; mons.buffy[0]=9;}
else {mons.buffx[1]=7; mons.buffy[1]=9;}
if (mons.magic[2]>=99) mons.deflect=mons.deflect+(mons.deflect/5);
else mons.deflect=mons.deflect+(mons.deflect/10);
strcpy(temp,"Enemy casts Icy Shield.");
addlog(temp,0);
playsound("Spell-Ice.wav");
}
if (e==1)
{
strcpy(temp,"Enemy casts Lesser Healing Waters for ");
y=20+randomnum(mons.itl/6,mons.itl/3);
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
mons.hp=mons.hp+y; if (mons.hp>mons.hpm) mons.hp=mons.hpm;
playsound("Spell-Water-Heal1.wav");
}
if (e==2)
{
strcpy(temp,"Enemy casts Wave for ");
y=32+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[2]>0) y=y-((plyr.resist[2]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp," damage and ");
a=10+randomnum(mons.itl/16,mons.itl/8);
sprintf(temp2,"%d",a);
strcat(temp, temp2);
strcat(temp," healing.");
addlog(temp,0);
plyrdamagecheck(y);
mons.hp=mons.hp+a; if (mons.hp>mons.hpm) mons.hp=mons.hpm;
playsound("Spell-Water-Damage.wav");
}
if (e==3)
{
strcpy(temp,"Enemy casts Kraken Strike for ");
y=80+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[2]>0) y=y-((plyr.resist[2]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Water-Damage.wav");
}
if (e==4)
{
strcpy(temp,"Enemy casts Greater Healing Waters for ");
y=80+randomnum(mons.itl/6,mons.itl/3);
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp," HP.");
addlog(temp,0);
mons.hp=mons.hp+y; if (mons.hp>mons.hpm) mons.hp=mons.hpm;
playsound("Spell-Water-Heal2.wav");
}
if (e==5)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=0; mons.buffy[0]=10;}
else {mons.buffx[1]=0; mons.buffy[1]=10;}
mons.itl=mons.itl+(mons.itl/5);
strcpy(temp,"Enemy casts Mind of the Kraken.");
addlog(temp,0);
playsound("Spell-Water-Heal1.wav");
}
}
if (((mons.cls==2)&&(x>9))||((mons.cls==7)&&(x>4)&&(x<9))||((mons.cls==9)&&(x>6)))
{
//Earth spell
y=1; c=-1;
if (mons.magic[3]>=20) ++y;
if (mons.magic[3]>=30) ++y;
if (mons.magic[3]>=55) ++y;
while (c==-1)
{
if (which==9999) e=4;
else e=randomnum(0,y);
if (((e==0)&&((mons.buffx[0]==0)&&(mons.buffy[0]==8)))||((e==0)&&(mons.buffx[1]!=-1))) c=-1;
else if (((e==1)&&((mons.buffx[0]==1)&&(mons.buffy[0]==5)))||((e==1)&&(mons.buffx[1]!=-1))) c=-1;
else c=0;
}
if (e==0)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=0; mons.buffy[0]=8;}
else {mons.buffx[1]=0; mons.buffy[1]=8;}
if (mons.magic[4]>=75) mons.str=mons.str+(mons.str/5);
else mons.str=mons.str+(mons.str/10);
strcpy(temp,"Enemy casts Mountain's Might.");
addlog(temp,0);
playsound("Spell-Earth-Boon.wav");
}
if (e==1)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=1; mons.buffy[0]=5;}
else {mons.buffx[1]=1; mons.buffy[1]=5;}
mons.deflect=mons.deflect+(mons.deflect/10);
strcpy(temp,"Enemy casts Fortification.");
addlog(temp,0);
playsound("Spell-Earth-Boon.wav");
}
if (e==2)
{
a=randomnum(1,3);
if (a!=1) strcpy(temp,"Enemy casts Tremor, but you weren't damaged.");
else
{
strcpy(temp,"Enemy casts Tremor, hitting you for ");
y=120+randomnum(mons.itl/6,mons.itl/3);
if ((strcmp(plyr.clname,"Nomad")==0)&&(plyr.scholar>49)) y=0;
if (plyr.resist[3]>0) y=y-((plyr.resist[3]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
plyrdamagecheck(y);
playsound("Spell-Damage1.wav");
}
addlog(temp,0);
}
if (e==3)
{
strcpy(temp,"Enemy casts Rock Volley for ");
y=82+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[3]>0) y=y-((plyr.resist[3]*y)/132);
if ((strcmp(plyr.clname,"Nomad")==0)&&(plyr.scholar>49)) y=0;
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage3.wav");
}
if (e==4)
{
strcpy(temp,"Enemy casts Smash for ");
if (mons.magic[3]<56) y=120+randomnum(mons.itl/6,mons.itl/3);
else y=220+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[3]>0) y=y-((plyr.resist[3]*y)/132);
if ((strcmp(plyr.clname,"Nomad")==0)&&(plyr.scholar>49)) y=0;
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage3.wav");
}
}
if (((mons.cls==1)&&(x<7))||((mons.cls==7)&&(x>8)))
{
//shadow spell
y=1; c=-1;
if (mons.magic[4]>=20) ++y;
if (mons.magic[4]>=30) ++y;
if (mons.magic[4]>=55) ++y;
if (mons.magic[4]>=75) ++y;
while (c==-1)
{
if (which==9999) e=randomnum(3,5);
else e=randomnum(0,y);
if (((e==0)&&((mons.buffx[0]==2)&&(mons.buffy[0]==9)))||((e==0)&&(mons.buffx[1]!=-1))) c=-1;
else if (((e==4)&&((mons.buffx[0]==1)&&(mons.buffy[0]==9)))||((e==4)&&(mons.buffx[1]!=-1))) c=-1;
else c=0;
}
if (e==0)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=2; mons.buffy[0]=9;}
else {mons.buffx[1]=2; mons.buffy[1]=9;}
if (mons.magic[4]>=99) mons.itl=mons.itl+(mons.itl/10);
strcpy(temp,"Enemy casts Laughing Shadow.");
addlog(temp,0);
playsound("Spell-Shadow-Boon.wav");
}
if (e==1)
{
strcpy(temp,"Enemy casts Touch of Shadow for ");
y=14+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[4]>0) y=y-((plyr.resist[4]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Shadow-Boon.wav");
}
if (e==2)
{
y=randomnum(1,100);
y=y-((plyr.dex+plyr.adjdex)/20);
y=y-(plyr.resist[4]/8);
if (y<51)
{
strcpy(temp,"Enemy casts Dark Strike, but missed.");
addlog(temp,0);
playsound("failure.wav");
}
else
{
strcpy(temp,"Enemy cast Dark Strike.  You are stunned.");
addlog(temp,0);
stun=randomnum(1,3);
playsound("Spell-Damage5.wav");
}
}
if (e==3)
{
strcpy(temp,"Enemy casts Drain Life for ");
y=75+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[4]>0) y=y-((plyr.resist[4]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
mons.hp=mons.hp+y/3; if (mons.hp>mons.hpm) mons.hp=mons.hpm;
playsound("Spell-Shadow-Cloud.wav");
}
if (e==4)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=1; mons.buffy[0]=9;}
else {mons.buffx[1]=1; mons.buffy[1]=9;}
strcpy(temp,"Enemy casts Crown of the Dead.");
addlog(temp,0);
playsound("Spell-Shadow-Boon.wav");
}
if (e==5)
{
strcpy(temp,"Enemy casts Dark Cloud for ");
y=350+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[4]>0) y=y-((plyr.resist[4]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Shadow-Cloud.wav");
}
}
if (((mons.cls==1)&&(x>6))||(mons.cls==6))
{
//light spell
y=0; c=-1;
if (mons.magic[5]>=20) ++y;
if (mons.magic[5]>=30) ++y;
if (mons.magic[5]>=40) ++y;
if (mons.magic[5]>=75) ++y;
while (c==-1)
{
if (which==9999) e=4;
else e=randomnum(0,y);
if (((e==0)&&((mons.buffx[0]==2)&&(mons.buffy[0]==5)))||((e==0)&&(mons.buffx[1]!=-1))) c=-1;
else if (((e==2)&&((mons.buffx[0]==6)&&(mons.buffy[0]==6)))||((e==2)&&(mons.buffx[1]!=-1))) c=-1;
else c=0;
}
if (e==0)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=2; mons.buffy[0]=5;}
else {mons.buffx[1]=2; mons.buffy[1]=5;}
mons.l=mons.l+(mons.l/10); mons.h=mons.h+(mons.h/10);
if (mons.magic[5]>=55) {mons.l=mons.l+(mons.l/20); mons.h=mons.h+(mons.h/20);}
strcpy(temp,"Enemy casts Holy Warrior.");
addlog(temp,0);
playsound("Spell-Light-Boon.wav");
}
if (e==1)
{
strcpy(temp,"Enemy casts Light's Infusion for ");
y=50+randomnum(mons.itl/6,mons.itl/3);
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp," HP.");
addlog(temp,0);
mons.hp=mons.hp+y; if (mons.hp>mons.hpm) mons.hp=mons.hpm;
playsound("Spell-Light-Heal.wav");
}
if (e==2)
{
if (mons.buffx[0]==-1) {mons.buffx[0]=6; mons.buffy[0]=6;}
else {mons.buffx[1]=6; mons.buffy[1]=6;}
mons.str=mons.str+(mons.str/10); mons.itl=mons.itl+(mons.itl/10);
if (mons.magic[5]>=99) {mons.str=mons.str+(mons.str/20); mons.itl=mons.itl+(mons.itl/20);}
strcpy(temp,"Enemy casts Holy Boon.");
addlog(temp,0);
playsound("Spell-Light-Boon.wav");
}
if (e==3)
{
strcpy(temp,"Enemy casts Sunstrike for ");
y=75+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[5]>0) y=y-((plyr.resist[5]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
playsound("Spell-Damage6.wav");
}
if (e==4)
{
y=225+randomnum(mons.itl/6,mons.itl/3);
if (plyr.resist[5]>0) y=y-((plyr.resist[5]*y)/132);
if ((((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
     &&(mons.magic[4]>39))
{
d=5+randomnum((mons.itl/30),(mons.itl/15));
if (plyr.resist[4]>0) d=d-((plyr.resist[4]*d)/132);
y=y+d;
}
strcpy(temp,"Enemy casts Call the Light for ");
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
plyrdamagecheck(y);
mons.hp=mons.hp+(y/2); if (mons.hp>mons.hpm) mons.hp=mons.hp;
playsound("Spell-Light-Heal.wav");
playsound("Spell-Damage6.wav");
}
}
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
if ((plyr.hp<1)&&(which!=9999)) {plyr.hp=0; x=playerdied(-1); if (x==0) return 3; else return 2;}
else if (plyr.hp<1) {return 3;}
}
else
{
//monster attacks
//chance to dodge
a=randomnum(1,100);
if ((strcmp(plyr.clname,"Warrior")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) a=a+10;
if ((z==2)||((z==0)&&(plyr.invmtype[plyr.equipped[9]]==1)))
{
a=a+40;
if ((plyr.dex+plyr.adjdex)<401) a=a+((plyr.dex+plyr.adjdex)/20);
else if ((plyr.dex+plyr.adjdex)<1001) {a=a+20; a=a+(((plyr.dex+plyr.adjdex)-400)/60);}
else {a=a+30; a=a+(((plyr.dex+plyr.adjdex)-1000)/250);}
}
else
{
if ((plyr.dex+plyr.adjdex)<21) a=a+(plyr.dex+plyr.adjdex);
else if ((plyr.dex+plyr.adjdex)<81) {a=a+20+(((plyr.dex+plyr.adjdex)-20)/6);}
else if ((plyr.dex+plyr.adjdex)<201) {a=a+30+(((plyr.dex+plyr.adjdex)-80)/12);}
else if ((plyr.dex+plyr.adjdex)<401) {a=a+40+(((plyr.dex+plyr.adjdex)-200)/20);}
else if ((plyr.dex+plyr.adjdex)<1001) {a=a+55+(((plyr.dex+plyr.adjdex)-400)/40);}
else {a=a+70+(((plyr.dex+plyr.adjdex)-1000)/250);}
}
if (plyr.lvl>mons.lvl) a=a+((plyr.lvl-mons.lvl)*5);
if (plyr.lvl<mons.lvl) a=a-((mons.lvl-plyr.lvl)*5);
for (x=0; x<10; ++x)
{
if ((plyr.buffx[x]==6)&&(plyr.buffy[x]==7)) a=a+10;
}
h=0;
for (c=0; c<9; ++c)
{
if (plyr.equipped[c]!=-1)
{
for (x=0; x<5; ++x)
if (plyr.invadd[plyr.equipped[c]][x]==6)
h=1;
}
}
if (h==1) a=a-15;
if (stun>0) a=0;
if (a>89)
{
addlog("You skillfully avoided the monster's attack",0);
a=randomnum(1,3);
strcpy(temp,"swing");
sprintf(temp2,"%d",a);
strcat(temp,temp2);
strcat(temp,".wav");
playsound(temp);
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
}
else
{
c=randomnum(mons.l,mons.h);
if (mons.elem!=-1)
{
y=(c/3);
y=y+c/5;
c=c-(c/3);
if (plyr.resist[mons.elem]>0) y=y-((plyr.resist[mons.elem]*y)/132);
if (mons.elem==6) {a=randomnum(1,10); if (a==1) gotdebuff(0);}
}
else y=0;
b=0;
if (((mons.buffx[0]==0)&&(mons.buffy[0]==5))||((mons.buffx[1]==0)&&(mons.buffy[1]==5)))
{
b=10+randomnum(mons.itl/20,mons.itl/10);
if (mons.magic[1]==99) b=b*2;
if (plyr.resist[1]>0) b=b-((plyr.resist[1]*b)/132);
}
c=c+mons.str/10;
if (mons.mtype==8)
{if (plyr.resist[mons.elem]>0) c=c-((plyr.resist[mons.elem]*c)/132);}
else
c=c-(plyr.deflect/2);
if (c<0) c=0;
c=c+y+b;
if (c<1) c=1;
e=0;
for (x=0; x<10; ++x)
{
if ((plyr.buffx[x]==2)&&(plyr.buffy[x]==9)) e=randomnum(1,10);
}
if (e==1)
{
sprintf(temp,"The monster's attack is diverted for %d.",c/2);
addlog(temp,0);
mons.hp=mons.hp-c/2;
if (mons.hp<1) {done=2; d=2;}
playsound("failure.wav");
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
}
else
{
a=-1; e=-1;
if (mons.bonus==2) a=randomnum(1,10);
if (mons.mtype==4) e=randomnum(1,10);
if ((mons.mtype==6)&&(mons.lvl>59)) e=randomnum(1,10);
if (mons.bonus==5)
{
if (which==9999) sprintf(temp,"Enemy attacks for %d and heals %d hitpoints.",c,c);
else sprintf(temp,"Enemy attacks for %d and heals %d hitpoints.",c,c/3);
addlog(temp,0);
if (which==9999) mons.hp=mons.hp+c;
else mons.hp=mons.hp+c/3;
if (mons.hp>mons.hpm) mons.hp=mons.hpm;
}
else if (a==1)
{
sprintf(temp,"Enemy attacks for %d.  You were paralyzed.",c);
addlog(temp,0);
stun=randomnum(1,3);
}
else if (e==1)
{
sprintf(temp,"Enemy attacks for %d.  You were cursed.",c);
addlog(temp,0);
if (mons.mtype==4) gotdebuff(1);
else gotdebuff(3);
}
else
{
b=0;
if ((mons.bonus==1)&&(mons.lvl>59))
b=randomnum(1,5);
if (b==1)
{
c=c*2;
sprintf(temp,"Enemy got crushing blow for %d.",c);
}
else
sprintf(temp,"Enemy attacks for %d.",c);
addlog(temp,0);
}
plyrdamagecheck(c);
if (mons.mtype==0) playsound("Monster-Beast.wav");
if (mons.mtype==1)
{
a=randomnum(1,4);
sprintf(temp,"hit_%d.wav",a);
playsound(temp);
}
if (mons.mtype==2) playsound("Monster-Reptile.wav");
if (mons.mtype==3) playsound("Monster-Insect.wav");
if (mons.mtype==4) playsound("Monster-Undead.wav");
if (mons.mtype==5) playsound("Monster-Apparition.wav");
if (mons.mtype==6) playsound("Monster-Golem.wav");
if (mons.mtype==7) playsound("Monster-Dragon.wav");
if (mons.mtype==8) playsound("Monster-Elemental.wav");
if (mons.mtype==9) playsound("Monster-Demon.wav");
if (mons.mtype==10) playsound("Monster-Ooze.wav");
if (mons.mtype==11) playsound("Monster-Plant.wav");
if (mons.mtype==12) playsound("xantos.wav");
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
}
}
if ((plyr.hp<1)&&(which!=9999)) {plyr.hp=0; x=playerdied(-1); if (x==0) return 3; else return 2;}
else if (plyr.hp<1) {return 3;}
}
}
}
else if (((q==1)&&(d==0))||(q==0)&&(d==1))
{
//player turn
if (stun>0) --stun;
else
{
// New grouped by turn regeneration effects
h=0;
for (a=0; a<9; ++a)
{
if (plyr.equipped[a]!=-1)
{
for (x=0; x<5; ++x)
if (plyr.invadd[plyr.equipped[a]][x]==5)
{
if (plyr.invaddamt[plyr.equipped[a]][x]>h) h=plyr.invaddamt[plyr.equipped[a]][x];
}
}
}
//proc damage every turn effects
for (x=0; x<10; ++x)
{
if ((plyr.buffx[x]==1)&&(plyr.buffy[x]==9))
{
strcpy(temp,"Crown of the Dead deals ");
y=10+randomnum((plyr.itl+plyr.adjitl)/20,(plyr.itl+plyr.adjitl)/12);
if (mons.resist[4]>0) y=y-((mons.resist[4]*y)/132);
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
monsdamagecheck(y);
playerheal(y/4);
playsound("Spell-Shadow-Boon.wav");
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
}
if ((plyr.buffx[x]==0)&&(plyr.buffy[x]==5)&&(plyr.buffmod[x]==2))
{
strcpy(temp,"Fire Weapon deals ");
y=15+randomnum((plyr.itl+plyr.adjitl)/20,(plyr.itl+plyr.adjitl)/12);
if (mons.resist[1]>0) y=y-((mons.resist[1]*y)/132);
sprintf(temp2,"%d",y);
strcat(temp, temp2);
strcat(temp,".");
addlog(temp,0);
monsdamagecheck(y);
playsound("Spell-Damage5.wav");
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
}
if ((plyr.buffx[x]==7)&&(plyr.buffy[x]==9)&&(plyr.buffmod[x]>0))
h=h+10+randomnum((plyr.itl+plyr.adjitl)/20,(plyr.itl+plyr.adjitl)/12);
if (mons.hp<1) {done=2; d=2;}
}
if (h>0)
{
sprintf(temp,"Regenerated %d HP.",h);
addlog(temp,0);
playerheal(h);
playsound("Spell-Water-Heal1.wav");
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
}
if (z==0)
{
//player attacks
//check for ammo in case of ranged weapon
if ((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]<3)&&((plyr.equipped[10]==-1)||
    (plyr.invcls[plyr.equipped[10]]!=plyr.invcls[plyr.equipped[9]])))
{
addlog("You tried to attack, but are out of ammo.",0);
}
else
{
//chance to miss
a=randomnum(1,100);
if (mons.lvl>plyr.lvl) a=a-((mons.lvl-plyr.lvl)*5);
if (mons.lvl<plyr.lvl) a=a+((plyr.lvl-mons.lvl)*5);
a=a-(mons.dex/10);
if (plyr.dex<20) a=a+plyr.dex;
else if (plyr.dex<345) a=a+(plyr.dex/5)+20;
else a=a+((plyr.dex-345)/50)+85;
if (mons.stun>0) a=a+100;
if (((mons.buffx[0]==6)&&(mons.buffy[0]==7))||((mons.buffx[1]==6)&&(mons.buffy[1]==7))) a=a-10;
if (mons.mtype==5) a=a-40;
if ((plyr.equipped[9]!=-1)&&(!((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4))))
{
for (x=0; x<5; ++x)
if (plyr.invadd[plyr.equipped[9]][x]==7)
a=a+15;
}
if (a<21)
{
strcpy(temp,"The enemy avoided your attack");
addlog(temp,0);
if ((plyr.invmtype[plyr.equipped[9]]==0)||((plyr.invmtype[plyr.equipped[9]]==1)&&
                                           ((plyr.invcls[plyr.equipped[9]]>1)&&(plyr.invcls[plyr.equipped[9]]!=4))))
{
a=randomnum(1,3);
sprintf(temp,"swing%d.wav",a);
playsound(temp);
}
else
{
if (plyr.invcls[plyr.equipped[9]]<2)
playsound("bowmiss.wav");
if (plyr.invcls[plyr.equipped[9]]==4)
playsound("wandshot.wav");
}
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
}
else
{
c=randomnum(plyr.dmgl,plyr.dmgh);
if ((strcmp(plyr.clname,"Monk")==0)&&(plyr.scholar>49)) c=c+(c/5);
if ((plyr.equipped[9]!=-1)&&(!((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4))))
{
for (x=0; x<5; ++x)
if (plyr.invadd[plyr.equipped[9]][x]==4)
c=c+((c*plyr.invaddamt[plyr.equipped[9]][x])/100);
}
if ((strcmp(plyr.clname,"Warrior")==0)&&(plyr.trade==1)&&(plyr.scholar==99)
    &&(plyr.equipped[9]!=-1)&&(plyr.invtreasure[plyr.equipped[9]]==0)) c=c*1.3;
if (plyr.invmtype[plyr.equipped[9]]==1) c=c+(((plyr.rwskill[plyr.invcls[plyr.equipped[9]]]/3)/100)*c);
if (plyr.invmtype[plyr.equipped[9]]==0) c=c+(((plyr.wskill[plyr.invcls[plyr.equipped[9]]]/3)/100)*c);
if (((plyr.invmtype[plyr.equipped[9]]==0)&&(plyr.invcls[plyr.equipped[9]]==6)&&(plyr.itl>plyr.str))||
    ((plyr.invmtype[plyr.equipped[9]]==0)&&(plyr.invcls[plyr.equipped[9]]==5)&&(plyr.invbonus[plyr.equipped[9]]==1)&&(plyr.itl>plyr.str)))
{
c=c+(plyr.itl+plyr.adjitl)/10;
}
else if (plyr.invmtype[plyr.equipped[9]]==0)
{
c=c+(plyr.str+plyr.adjstr)/10;
if (plyr.dextodmg!=-1) c=c+(plyr.dex+plyr.adjdex)/10;
}
else if ((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4))
c=c+(plyr.itl+plyr.adjitl)/10;
else
{
c=c+(plyr.str+plyr.adjstr)/20;
c=c+(plyr.dex+plyr.adjdex)/10;
}
for (x=0; x<10; ++x)
{
if ((plyr.buffx[x]==2)&&(plyr.buffy[x]==5)&&(plyr.buffmod[x]==1)&&(mons.mtype==4)) c=c+(c/5)+(c/10);
else if ((plyr.buffx[x]==2)&&(plyr.buffy[x]==5)&&(plyr.buffmod[x]==0)&&(mons.mtype==4)) c=c+(c/5);
else if ((plyr.buffx[x]==2)&&(plyr.buffy[x]==5)&&(plyr.buffmod[x]==1)) c=c+(c/10)+(c/20);
else if ((plyr.buffx[x]==2)&&(plyr.buffy[x]==5)) c=c+(c/10);
}
if ((plyr.crusader!=-1)&&(mons.mtype==4)) c=c+(c/10);
if (plyr.invelem[plyr.equipped[9]]!=-1)
{
c=c*1.3;
y=(c/2);
c=(c/2);
if ((plyr.invelem[plyr.equipped[9]]==6)&&(plyr.poisonexpert!=-1)) y=y+(y/3);
if (mons.resist[plyr.invelem[plyr.equipped[9]]]>0) y=y-((mons.resist[plyr.invelem[plyr.equipped[9]]]*y)/132);
}
else y=0;
if ((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]<3)&&(plyr.invelem[plyr.equipped[10]]!=-1))
{
e=c/10;
if ((plyr.invelem[plyr.equipped[10]]==6)&&(plyr.poisonexpert!=-1)) e=e*1.5;
if (mons.resist[plyr.invelem[plyr.equipped[10]]]>0) e=e-((mons.resist[plyr.invelem[plyr.equipped[10]]]*y)/132);
y=y+e;
}
a=-1; b=0;
for (x=0; x<10; ++x) if ((plyr.buffx[x]==0)&&(plyr.buffy[x]==5)) a=x;
if (a!=-1)
{
b=10+randomnum((plyr.itl+plyr.adjitl)/20,(plyr.itl+plyr.adjitl)/10);
if (plyr.buffmod[a]==2) b=b*2;
if ((strcmp(plyr.clname,"Spellblade")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) b=b+(b/5);
if (mons.resist[1]>0) b=b-((mons.resist[1]*b)/132);
}
if (plyr.berserking!=-1) c=c+((c/10)*plyr.berserking);
if ((strcmp(plyr.clname,"Paladin")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) c=c+(c/10);
if (((mons.mtype==4)||(mons.mtype==10))&&((plyr.invmtype[plyr.equipped[9]]==0)&&(plyr.invcls[plyr.equipped[9]]==1))) c=c+(c/2);
if ((mons.mtype==11)&&(plyr.invmtype[plyr.equipped[9]]==0)&&((plyr.invcls[plyr.equipped[9]]==5)||(plyr.invcls[plyr.equipped[9]]==3))) c=c+(c/2);
if ((mons.mtype==7)&&(plyr.invmtype[plyr.equipped[9]]==0)&&(plyr.invcls[plyr.equipped[9]]==2)) c=c+(c/2);
if ((mons.mtype==1)&&(plyr.invmtype[plyr.equipped[9]]==0)&&(plyr.invcls[plyr.equipped[9]]==6)) c=c+(c/2);
if ((mons.mtype==3)&&(plyr.invmtype[plyr.equipped[9]]==0)
    &&((plyr.invcls[plyr.equipped[9]]==4)&&(plyr.invbonus[plyr.equipped[9]]==0))
    ||((plyr.invcls[plyr.equipped[9]]==2)&&(plyr.invbonus[plyr.equipped[9]]==1))
    ||((plyr.invcls[plyr.equipped[9]]==0)&&(plyr.invbonus[plyr.equipped[9]]==1))) c=c+(c/2);

c=c-(mons.deflect/2);  if (c<0) c=0;
c=c+y+b;
e=0;
if (((mons.buffx[0]==2)&&(mons.buffy[0]==9))||((mons.buffx[1]==2)&&(mons.buffy[1]==9)))
e=randomnum(1,10);
if (e==1)
{
sprintf(temp,"Your attack was diverted.  You take %d.",c/2);
addlog(temp,0);
plyr.hp=plyr.hp-(c/2);
playsound("failure.wav");
if (plyr.hp<1) {plyr.hp=0; x=playerdied(-1); if (x==0) return 3; else return 2;}
}
else
{
if ((plyr.equipped[9]!=-1)&&(plyr.invmtype[plyr.equipped[9]]==0))
{
h=randomnum(1,4);
if (plyr.sex==0)
sprintf(temp,"meleem%d.wav",h);
else
sprintf(temp,"meleef%d.wav",h);
playsound(temp);
}
if (c<1) c=1;
a=0;
if ((strcmp(plyr.clname,"Monk")==0)&&(plyr.scholar>49)) a=randomnum(1,4);
else if (strcmp(plyr.clname,"Monk")==0) a=randomnum(1,5);
for (x=0; x<10; ++x)
{
if ((plyr.buffx[x]==6)&&(plyr.buffy[x]==7)&&(plyr.buffmod[x]>0)&&(a!=1)) a=randomnum(1,5);
if ((plyr.buffx[x]==8)&&(plyr.buffy[x]==5)&&(a!=1)) a=randomnum(1,5);
if ((plyr.buffx[x]==3)&&(plyr.buffy[x]==6)&&(a!=1)) a=randomnum(1,3);
}
e=randomnum(1,100);
if ((plyr.equipped[9]!=-1)&&(!((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4))))
{
for (x=0; x<5; ++x)
if (plyr.invadd[plyr.equipped[9]][x]==5)
e=e+15;
}
if ((plyr.lucky!=-1)) e=e+12;
if ((strcmp(plyr.clname,"Thief")==0)&&(plyr.trade==1)&&(plyr.scholar==99)) e=e+12;
if ((strcmp(plyr.clname,"Warrior")==0)&&(plyr.trade==1)&&(plyr.scholar==99)
    &&(plyr.equipped[9]!=-1)&&(plyr.invtreasure[plyr.equipped[9]]==1)) e=e+12;
if ((strcmp(plyr.clname,"Barbarian")==0)&&(plyr.trade==1)&&(plyr.scholar==99)) e=e+(5*plyr.berserking);
for (x=0; x<10; ++x)
if ((plyr.buffx[x]==8)&&(plyr.buffy[x]==8)) e=e+12;
if ((a==1)&&(e>97))
{
c=c*2;
c=c*1.5;
sprintf(temp,"What luck!  Lucky strike+double strike for %d.",c);
addlog(temp,0);
if (plyr.invmtype[plyr.equipped[9]]==1) x=weaponskillupchance(plyr.invcls[plyr.equipped[9]]+6);
else x=weaponskillupchance(plyr.invcls[plyr.equipped[9]]);
if ((x==1)&&(plyr.invmtype[plyr.equipped[9]]==1)) ++plyr.rwskill[plyr.invcls[plyr.equipped[9]]];
else if (x==1) ++plyr.wskill[plyr.invcls[plyr.equipped[9]]];
}
else if (e>97)
{
c=c*1.5;
sprintf(temp, "What luck!  Landed a lucky strike for %d.",c);
addlog(temp,0);
if (plyr.invmtype[plyr.equipped[9]]==1) x=weaponskillupchance(plyr.invcls[plyr.equipped[9]]+6);
else x=weaponskillupchance(plyr.invcls[plyr.equipped[9]]);
if ((x==1)&&(plyr.invmtype[plyr.equipped[9]]==1)) ++plyr.rwskill[plyr.invcls[plyr.equipped[9]]];
else if (x==1) ++plyr.wskill[plyr.invcls[plyr.equipped[9]]];
}
else if (a==1)
{
c=c*2;
sprintf(temp,"Landed a double strike for %d.",c);
addlog(temp,0);
if (plyr.invmtype[plyr.equipped[9]]==1) x=weaponskillupchance(plyr.invcls[plyr.equipped[9]]+6);
else x=weaponskillupchance(plyr.invcls[plyr.equipped[9]]);
if ((x==1)&&(plyr.invmtype[plyr.equipped[9]]==1)) ++plyr.rwskill[plyr.invcls[plyr.equipped[9]]];
else if (x==1) ++plyr.wskill[plyr.invcls[plyr.equipped[9]]];
}
else
{
if (plyr.invmtype[plyr.equipped[9]]==1) x=weaponskillupchance(plyr.invcls[plyr.equipped[9]]+6);
else x=weaponskillupchance(plyr.invcls[plyr.equipped[9]]);
if ((x==1)&&(plyr.invmtype[plyr.equipped[9]]==1)) ++plyr.rwskill[plyr.invcls[plyr.equipped[9]]];
else if (x==1) ++plyr.wskill[plyr.invcls[plyr.equipped[9]]];
strcpy(temp,"Attacked for ");
sprintf(temp2,"%d",c);
strcat(temp, temp2);
if (x==0)
strcat(temp," damage.");
else
strcat(temp," damage and gained a skillpoint.");
addlog(temp,0);
}
monsdamagecheck(c);
// Check for slumber weapon, put monster to sleep
if ((plyr.equipped[9]!=-1)&&(!((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4))))
{
// Elemental, plant, Xantos, undead, apparition don't sleep
if (!((mons.mtype==8)||(mons.mtype>10)||(mons.mtype==4)||(mons.mtype==5)))
{
for (x=0; x<5; ++x)
if (plyr.invadd[plyr.equipped[9]][x]==8)
{
h=randomnum(1,100);
if (h<11)
{
addlog("Slumber triggers and the enemy falls asleep.",0);
mons.stun=randomnum(2,4);
}
}
}
}
// New combined attack heal section
h=0;
if ((plyr.equipped[9]!=-1)&&(!((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4))))
{
for (x=0; x<5; ++x)
if (plyr.invadd[plyr.equipped[9]][x]==6)
{
h=1;
playerheal(c/10);
}
}
if ((strcmp(plyr.clname,"Paladin")==0)&&(plyr.trade==1)&&(plyr.scholar==99))
{
playerheal(c/5);
if (h==0) sprintf(temp,"Lightblade heals you for %d HP.",c/5);
else sprintf(temp,"Lightblade+Lifesteal heals you for %d HP.",(c/5)+(c/10));
addlog(temp,0);
}
else if (h==1)
{
sprintf(temp,"Lifesteal heals you for %d HP.",(c/10));
addlog(temp,0);
}
if ((plyr.invmtype[plyr.equipped[9]]==1)&&((plyr.invcls[plyr.equipped[9]]==3)||(plyr.invcls[plyr.equipped[9]]==5)))
{
--plyr.invnum[plyr.equipped[9]];
if (plyr.invnum[plyr.equipped[9]]==0) lostitem(plyr.equipped[9]);
}
else if ((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]<3))
{
--plyr.invnum[plyr.equipped[10]];
if (plyr.invnum[plyr.equipped[10]]==0) lostitem(plyr.equipped[10]);
}
if ((plyr.invmtype[plyr.equipped[9]]==0)||((plyr.invmtype[plyr.equipped[9]]==1)&&
                                           ((plyr.invcls[plyr.equipped[9]]>1)&&(plyr.invcls[plyr.equipped[9]]!=4))))
{
a=randomnum(1,4);
sprintf(temp,"hit_%d.wav",a);
playsound(temp);
}
else
{
if (plyr.invcls[plyr.equipped[9]]<2)
playsound("bowhit.wav");
if (plyr.invcls[plyr.equipped[9]]==4)
{playsound("wandshot.wav"); rest(50); playsound("wandhit.wav");}
}
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
}
}
}
}
if (z==1)
{
equipuse(f);
h=0;
drawlog(2);
}
if (z==2)
{
x=castspell(spellbutton[mx][my],mx);
monsdamagecheck(x);
drawlog(2); blitscreen();
if (sound==1) rest(rspeed);
}
}
if (mons.hp<1) {done=2; d=2;}
if ((plyr.berserking!=-1)&&(plyr.berserking<4)) ++plyr.berserking;
if (((plyr.invmagic[plyr.equipped[9]]==3)||((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4)
    &&(plyr.invtreasure[plyr.equipped[9]]==1)))&&(plyr.mpm>0))
{plyr.mp=plyr.mp+10; if (plyr.mp>(plyr.mpm+plyr.adjmp)) plyr.mp=(plyr.mpm+plyr.adjmp);}
endofturn();
}
}
}
if (done==3) {bagsopen=0; resumedungeonmusic(); return 1;}
else if (done==2)
{
++plyr.kills;
sprintf(temp,"Enemy defeated in %d turns. +",plyr.combatturn);
if ((plyr.lvl<mons.lvl+4)&&(plyr.lvl<99))
{
sprintf(temp2,"%d",mons.xp);
strcat(temp,temp2);
strcat(temp,"XP +");
plyr.xp=plyr.xp+mons.xp;
}
sprintf(temp2,"%d",mons.gold);
strcat(temp,temp2);
strcat(temp,"Gold.");
addlog(temp,0);
plyr.gold=plyr.gold+mons.gold;
playsound("victory.wav");
if (dgn.doormn[which]==1)
{
//loot roll, for boss monsters only
++plyr.bkills;
x=randomnum(1,10);
if (x<6)
makegem(-1,-1);
else if (x<9)
makescroll(-1,-1);
else if (x==9)
rewarditem(1,0);
else if (x==10)
rewarditem(2,0);
if ((x>5)&&(x<9))
{
for (y=0; y<3; ++y)
gotitem(-2,0);
strcpy(temp,"Recived 3x ");
strcat(temp,tempitem.name);
strcat(temp,".");
addlog(temp,0);
}
else
gotitem(-1,0);
}
if ((mission==0)&&(missiontyp<11)&&(mons.mtype==missiontyp)) missionprogress();
else if ((mission==0)&&(missiontyp==11)&&(dgn.doormn[which]==1)) missioncomplete();
dgn.doormn[which]=-1;
if (plyr.siphon!=-1)
{
x=(plyr.hpm+plyr.adjhp)/12;
y=(plyr.mpm+plyr.adjmp)/12;
sprintf(temp,"Siphoned %d HP %d MP.",x,y);
addlog(temp,0);
plyr.hp=plyr.hp+x; plyr.mp=plyr.mp+y;
if (plyr.hp>(plyr.hpm+plyr.adjhp)) plyr.hp=(plyr.hpm+plyr.adjhp);
if (plyr.mp>(plyr.mpm+plyr.adjmp)) plyr.mp=(plyr.mpm+plyr.adjmp);
}
if ((plyr.xp>=plyr.xpn)&&(plyr.lvl<99)) gainedlevel();
savedlvl();
savechar();
if ((plyr.kills==500)||(plyr.kills==1200))
{
if (plyr.kills==500) addlog("That was your 500th kill.",0);
if (plyr.kills==1200) addlog("That was your 1200th kill.",0);
addlog("Return to the Hall of Heroes for a reward.",0);
}
resumedungeonmusic();
bagsopen=0;
if (plyr.berserking!=-1) plyr.berserking=0;
return 0;
}
else
{
dgn.doormn[which]=-1;
savedlvl();
savechar();
resumedungeonmusic();
bagsopen=0;
return 0;
}
}

void gainedlevel()
{
int x;
char temp[45], temp2[20];
plyr.xpn=plyr.xpn+plyr.xpn*0.10;
plyr.xp=0;
plyr.lvl=plyr.lvl+1;
x=plyr.spl+randomnum(-1,1);
if (x<0) x=0;
if ((plyr.str+x)>700) x=700-plyr.str;
plyr.str=plyr.str+x;
strcpy(temp,"Reached level ");
sprintf(temp2,"%d",plyr.lvl);
strcat(temp,temp2);
strcat(temp,".");
addlog(temp,0);
strcpy(temp,"+");
sprintf(temp2,"%d",x);
strcat(temp,temp2);
strcat(temp,"STR +");
x=plyr.dpl+randomnum(-1,1);
if (x<0) x=0;
if ((plyr.dex+x)>700) x=700-plyr.dex;
plyr.dex=plyr.dex+x;
sprintf(temp2,"%d",x);
strcat(temp,temp2);
strcat(temp,"DEX +");
x=plyr.ipl+randomnum(-1,1);
if (x<0) x=0;
if ((plyr.itl+x)>700) x=700-plyr.itl;
plyr.itl=plyr.itl+x;
sprintf(temp2,"%d",x);
strcat(temp,temp2);
strcat(temp,"INT +");
x=plyr.cpl+randomnum(-1,1);
if (x<0) x=0;
if ((plyr.chr+x)>700) x=700-plyr.chr;
plyr.chr=plyr.chr+x;
sprintf(temp2,"%d",x);
strcat(temp,temp2);
strcat(temp,"CHR +");
x=plyr.hpl+randomnum(-1,2);
if (x<0) x=0;
plyr.hpm=plyr.hpm+x;
plyr.hp=(plyr.hpm+plyr.adjhp);
sprintf(temp2,"%d",x);
strcat(temp,temp2);
strcat(temp,"HP ");
if (plyr.mpm>0)
{
strcat(temp,"+");
x=plyr.mpl+randomnum(-1,2);
if (x<0) x=0;
plyr.mpm=plyr.mpm+x;
plyr.mp=(plyr.mpm+plyr.adjmp);
sprintf(temp2,"%d",x);
strcat(temp,temp2);
strcat(temp,"MP");
}
playsound("levelup.wav");
addlog(temp,0);
}

int lockeddoorevent(int which)
{
int x, y, z, sel, d, mx, my, drawx;
clear_keybuf();
drawx=drawalignborder(RIGHT,TOP,312,scaley(632),"Locked Door ");
z=buttonbord->w;
y=0;
for (x=0; x<6; ++x)
{
if (plyr.magic[x]>=20) y=1;
}
if (dgn.door[which]==2)
regulartext(drawx+offsetx,offsety,"The door is locked.",BROWN,SCREEN,LEFT);
if (dgn.door[which]==3)
regulartext(drawx+offsetx,offsety,"The door is magically locked.",BROWN,SCREEN,LEFT);
regulartext(drawx+offsetx,offsety+text_height(largefont),"What will you do?",BROWN,SCREEN,LEFT);
sel=-2;
while (sel==-2)
{
if (plyr.lockpick==-1)
{
blitbutton(5,6,drawx+offsetx,offsety+(text_height(largefont)*2),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2),"Bash",BROWN,SCREEN,LEFT);
}
else
{
blitbutton(7,8,drawx+offsetx,offsety+(text_height(largefont)*2),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2),"Pick Lock",BROWN,SCREEN,LEFT);
}
blitbutton(8,11,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*1),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2)+(z*1),"Use Skeleton Key",BROWN,SCREEN,LEFT);
if (y==1)
{
blitbutton(9,13,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*2),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2)+(z*2),"Unlock with Magic",BROWN,SCREEN,LEFT);
blitbutton(9,14,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*3),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2)+(z*3),"Cancel",BROWN,SCREEN,LEFT);
}
else
{
blitbutton(9,14,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*2),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2)+(z*2),"Cancel",BROWN,SCREEN,LEFT);
}
blitscreen();
while ((checknumkeys()==-1)&&(!key[KEY_ESC])&&(!(mouse_b&1))&&(!(mouse_b&2)))
{
}
mx=((fetchmousex()-(drawx+offsetx))/z);
my=(fetchmousey()-((text_height(largefont)*2)+offsety))/z;
if (checknumkeys()!=-1)
{
mx=0;
my=checknumkeys();
}
if ((mouse_b&2)||(key[KEY_ESC]))
{
if (mouse_b&2)
{
while (mouse_b&2)
{
}
}
sel=-1;
}
if ((mx==0)&&(my==0))
{
if ((plyr.lockpick!=-1)&&(dgn.door[which]==2))
{
blitinversebutton(7,8,drawx+offsetx,offsety+(text_height(largefont)*2),0);
blitscreen();
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
x=randomnum(0,2);
d=randomnum(0,plyr.lockpick);
if ((x==0)||(d>30))
{
x=lpskillup();
if (x==0) addlog("Picked a lock.",0);
if (x==1) addlog("Picked a lock and gained a skillpoint.",0);
playsound("lpsucceed.wav");
sel=0;
}
else {addlog("Attempted to pick a lock but failed.",0); playsound("lpfail.wav"); sel=-1;}
}
else if ((plyr.lockpick!=-1)&&(dgn.door[which]==3))
{
addlog("This door resists all attempts at lockpicking.",0); sel=-1;
}
else if ((plyr.lockpick==-1)&&(dgn.door[which]==2))
{
blitinversebutton(5,6,drawx+offsetx,offsety+(text_height(largefont)*2),0);
blitscreen();
while (mouse_b&1)
{
}
x=randomnum(0,2);
if (plyr.dextodmg!=-1)
d=randomnum(0,plyr.dex);
else
d=randomnum(0,plyr.str);
if ((x==0)||(d>(10+(plyr.dlvl*3))))
{
addlog("Broke a door down by force.",0); sel=1;
}
else
{
addlog("Attempted to break down a door but failed.",0); sel=-1;
}
}
else if ((plyr.lockpick==-1)&&(dgn.door[which]==3))
{
addlog("This door can't be forced open.",0); sel=-1;
}
}
if ((mx==0)&&(my==1))
{
blitinversebutton(8,11,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*1),0);
blitscreen();
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
d=-1;
for (x=0; x<100; ++x)
{
if ((plyr.invmtype[x]==4)&&(plyr.invcls[x]==3)) d=x;
}
if (d!=-1)
{
--plyr.invnum[d]; if (plyr.invnum[d]==0) lostitem(d);
addlog("Used a skeleton key and unlocked a door.",0); sel=0;
}
else {addlog("You don't have any skeleton keys at the moment.",1); sel=-1;}
}
if ((mx==0)&&(my==2)&&(y!=1))
{
blitinversebutton(9,14,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*2),0);
blitscreen();
while (mouse_b&1)
{
}
sel=-1;
}
else if ((mx==0)&&(my==2)&&(y==1))
{
blitinversebutton(9,13,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*2),0);
blitscreen();
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
if ((plyr.magic[0]>=20)||(plyr.magic[3]>=20)) d=10;
else if ((plyr.magic[1]>=20)||(plyr.magic[4]>=20)) d=12;
else if (plyr.magic[2]>=20) d=15;
else if (plyr.magic[5]>=20) d=20;
if (plyr.mp<d) {addlog("Not enough MP.",1); sel=-1;}
else
{
addlog("Unlocked a door using magic.",0);
plyr.mp=plyr.mp-d;
sel=0;
}
}
else if ((mx==0)&&(my==3)&&(y==1))
{
blitinversebutton(9,14,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*3),0);
blitscreen();
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
sel=-1;
}
}
while (checknumkeys()!=-1)
{
}
if (sel==-1)
{
playsound("failure.wav");
return 0;
}
else if (sel==0)
{
playsound("dooropen.wav");
dgn.door[which]=4; ++dgn.icnx[dgn.doorx[which]][dgn.doory[which]]; savedlvl(); return 1;
}
else if (sel==1)
{
playsound("doorbreak.wav");
dgn.door[which]=1; dgn.icnx[dgn.doorx[which]][dgn.doory[which]]=dgn.icnx[dgn.doorx[which]][dgn.doory[which]]+2; savedlvl(); return 1;
}
return 0;
}

int chestevent(int which)
{
int x, y, z, sel, d, mx, my, drawx;
clear_keybuf();
drawx=drawalignborder(RIGHT,TOP,312,scaley(632),"Treasure Chest ");
z=buttonbord->w;
y=0;
if (plyr.lockpick!=-1) y=1;
regulartext(drawx+offsetx,offsety,"There is a treasure chest here.",BROWN,SCREEN,LEFT);
regulartext(drawx+offsetx,offsety+text_height(largefont),"It's locked.  What will you do?",BROWN,SCREEN,LEFT);
sel=-2;
while (sel==-2)
{
if (y==1)
{
blitbutton(7,8,drawx+offsetx,offsety+(text_height(largefont)*2),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2),"Pick Lock",BROWN,SCREEN,LEFT);
blitbutton(8,11,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*1),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2)+(z*1),"Use Skeleton Key",BROWN,SCREEN,LEFT);
blitbutton(9,14,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*2),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2)+(z*2),"Cancel",BROWN,SCREEN,LEFT);
}
else
{
blitbutton(8,11,drawx+offsetx,offsety+(text_height(largefont)*2),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2),"Use Skeleton Key",BROWN,SCREEN,LEFT);
blitbutton(9,14,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*1),0);
regulartext(drawx+offsetx+z,offsety+(text_height(largefont)*2)+(z*1),"Cancel",BROWN,SCREEN,LEFT);
}
drawlog(1); blitscreen();
while ((!key[KEY_1])&&(!key[KEY_2])&&(!key[KEY_3])&&(!key[KEY_ESC])
       &&(!(mouse_b&1))&&(!(mouse_b&2)))
{
}
mx=((fetchmousex()-(drawx+offsetx))/z);
my=(fetchmousey()-((text_height(largefont)*2)+offsety))/z;
if (key[KEY_1]) {mx=0; my=0;}
if (key[KEY_2]) {mx=0; my=1;}
if (key[KEY_3]) {mx=0; my=2;}
if ((mouse_b&2)||(key[KEY_ESC]))
{
if (mouse_b&2)
{
while (mouse_b&2)
{
}
}
sel=-1;
}
if ((mx==0)&&(my==0)&&(y==1))
{
blitinversebutton(7,8,drawx+offsetx,offsety+(text_height(largefont)*2),0);
blitscreen();
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
x=randomnum(0,3);
d=randomnum(0,plyr.lockpick);
if ((x==0)||(d>40))
{
x=lpskillup();
if (x==0) addlog("Unlocked a chest.",0);
if (x==1) addlog("Unlocked a chest and gained a skillpoint.",0);
playsound("lpsucceed.wav"); rest(100);
sel=0;
}
else {addlog("Attempted to unlock a chest but failed.",0); playsound("lpfail.wav"); rest(100);}
}
if (((mx==0)&&(my==1)&&(y==1))||((mx==0)&&(my==0)&&(y!=1)))
{
blitinversebutton(8,11,drawx+offsetx,offsety+(text_height(largefont)*2)+(y*1),0);
blitscreen();
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
d=-1;
for (x=0; x<100; ++x)
{
if ((plyr.invmtype[x]==4)&&(plyr.invcls[x]==3)) d=x;
}
if (d!=-1)
{
--plyr.invnum[d]; if (plyr.invnum[d]==0) lostitem(d);
addlog("Used a skeleton key and opened a chest.",0); sel=0;
}
else {addlog("You don't have any skeleton keys at the moment.",1); sel=-1;}
}
if (((mx==0)&&(my==2)&&(y==1))||((mx==0)&&(my==1)&&(y!=1)))
{
if (y==1) blitinversebutton(9,14,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*2),0);
else blitinversebutton(9,14,drawx+offsetx,offsety+(text_height(largefont)*2)+(z*1),0);
blitscreen();
while (mouse_b&1)
{
}
sel=-1;
}
while (checknumkeys()!=-1)
{
}
}
if (sel==-1)
{
return 0;
}
else if (sel==0)
{
//number of items in the chest
//small chests have 2-3 items, large chests have 3-5
playsound("chestopen.wav");
if ((dgn.item[which]==2003)||(dgn.item[which]==2005)||(dgn.item[which]==2007)) z=randomnum(2,3);
else z=randomnum(3,5);
//first item is always a rewarditem()
y=randomnum(1,3);
rewarditem(y,0);
gotitem(-1,0);
for (x=1; x<z; ++x)
{
y=randomnum(0,4);
if (y==0)
{
makegem(-1,-1);
gotitem(-1,0);
z=randomnum(0,1);
if (z==0)
{
makegem(-1,-1);
gotitem(-1,0);
}
}
if (y==1)
{
makescroll(-1,-1);
gotitem(-1,0);
makescroll(-1,-1);
gotitem(-1,0);
}
if (y==2)
{
makepotion(plyr.dlvl,0,-1,-1,-1);
gotitem(-1,0);
if (z==0)
{
makepotion(plyr.dlvl,0,-1,-1,-1);
gotitem(-1,0);
}
}
if (y==3)
{
z=randomnum(0,12);
if ((z==11)&&(plyr.dlvl<20)) z=12;
if ((z==0)||(z==5)) d=randomnum(0,1);
else if ((z==3)||(z>6)) d=0;
else d=randomnum(0,2);
if (z<7) sel=randomnum(0,1);
else sel=0;
makeweapon(z,d,sel,plyr.dlvl,-1,-1,-1);
gotitem(-1,0);
}
if (y==4)
{
z=randomnum(0,7);
d=randomnum(0,4);
makearmor(d,z,plyr.dlvl,-1,-1);
gotitem(-1,0);
}
}
if (mission==1) missioncomplete();
return 1;
}
return 0;
}



void alchskillup(int breakpoint)
{
int c, e;
char temp[50];
c=0;
if (plyr.alch<breakpoint) c=1;
else if (plyr.alch<(breakpoint+15)) {e=randomnum(0,2); if (e==0) c=1;}
if ((c==1)&&(plyr.alch<99))
{
++plyr.alch;
strcpy(temp,"Created ");
strcat(temp,tempitem.name);
strcat(temp, " and gained skill.");
}
else
{
strcpy(temp,"Created ");
strcat(temp,tempitem.name);
strcat(temp, ".");
}
addlog(temp,0);
playsound("potion.wav");
}

int lpskillup()
{
int x;
if (plyr.lockpick<40) {++plyr.lockpick; return 1;}
else if (plyr.lockpick<65) {x=randomnum(0,1); if (x==0) {++plyr.lockpick; return 1;} else return 0;}
else if (plyr.lockpick<85) {x=randomnum(0,2); if (x==0) {++plyr.lockpick; return 1;} else return 0;}
else if (plyr.lockpick<99) {x=randomnum(0,3); if (x==0) {++plyr.lockpick; return 1;} else return 0;}
return 0;
}


int drawplayerbuffs(int dx, int offset)
{
int x, c, cc, rc, rowinc;
char temp[40], temp2[40], temp3[20];
cc=dx; rc=0;
c=0;
rowinc=32;
//else rowinc=22;
for (x=1; x<5; ++x)
{
if (plyr.equipa[x]!=-1) c=x;
}
halficon(5+c,0,cc,offset+(rowinc*rc),0);
if (c==0) strcpy(temp,"Equips up to Cloth armor");
if (c==1) strcpy(temp,"Equips up to Leather armor");
if (c==2) strcpy(temp,"Equips up to Chain armor");
if (c==3) strcpy(temp,"Equips up to Half-plate armor");
if (c==4) strcpy(temp,"Equips up to Full-plate armor");
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),temp,"");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
if (plyr.equipa[5]!=-1)
{
halficon(0,1,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Equips Shields","");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
if (plyr.twoh==1)
{
halficon(5,6,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Equips Two-handed weapons","");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
if (plyr.dextodmg!=-1)
{
halficon(9,7,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Dexterity to damage","Dexterity stat boosts melee damage");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
if (plyr.crusader!=-1)
{
halficon(8,7,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Undead slayer","Bonus damage versus the undead");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
if (plyr.berserking!=-1)
{
halficon(7,7,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Berserking","Gain rage and damage during battle");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
if (strcmp(plyr.clname,"Monk")==0)
{
halficon(3,5,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Swiftness","Haste + chance for double strikes");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
halficon(1,5,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Chi","Sometimes prevents damage");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
if (plyr.lucky!=-1)
{
halficon(8,8,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Lucky","Increased lucky strike chance");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
if (plyr.poisonexpert!=-1)
{
halficon(9,8,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Poison expert","Bonus damage with poison weapons");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
if (plyr.siphon!=-1)
{
halficon(3,9,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Life stealer","Steals HP/MP after battle");
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
if ((plyr.trade==1)&&(plyr.scholar>49))
{
// determine class
x=-1;
if (strcmp(plyr.clname,"Warrior")==0) x=0;
if (strcmp(plyr.clname,"Cleric")==0) x=1;
if (strcmp(plyr.clname,"Mage")==0) x=2;
if (strcmp(plyr.clname,"Thief")==0) x=3;
if (strcmp(plyr.clname,"Barbarian")==0) x=4;
if (strcmp(plyr.clname,"Spellblade")==0) x=5;
if (strcmp(plyr.clname,"Paladin")==0) x=6;
if (strcmp(plyr.clname,"Warlock")==0) x=7;
if (strcmp(plyr.clname,"Monk")==0) x=8;
if (strcmp(plyr.clname,"Nomad")==0) x=9;
if (x==-1) allegro_message("Error");
if (x==0)
{
halficon(4,3,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Art of Evasion","Bonus dodge chance");
}
if (x==1)
{
halficon(4,5,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Benediction","+20%% to Healing");
}
if (x==2)
{
halficon(3,11,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Arcane Mind","Spells sometimes cost you no MP");
}
if (x==3)
{
halficon(5,12,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Agile Mastery","+10%% Dexterity");
}
if (x==4)
{
halficon(9,10,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Endurance","Deflect increases with rage");
}
if (x==5)
{
halficon(2,8,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Fire Mind","+20%% Fire Dmg");
}
if (x==6)
{
halficon(7,13,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Vendetta","+10%% Damage");
}
if (x==7)
{
halficon(9,12,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Dark Secret","-33%% Shadow spell MP Cost");
}
if (x==8)
{
halficon(5,6,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Fists of Fury","+20%% dmg, +double strikes");
}
if (x==9)
{
halficon(4,2,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Hardiness","Immune to Fire/Earth spells");
}
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
if (plyr.scholar==99)
{
if (x==0)
{
halficon(6,11,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Art of War","+30%% 1H dmg; +2H lucky strikes");
}
if (x==1)
{
halficon(2,6,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Holy Shield","+100%% Shield Deflect");
}
if (x==2)
{
halficon(8,14,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Spellblast","Spells sometimes deal 2x damage");
}
if (x==3)
{
halficon(2,7,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Lucky Mastery","Lucky strike chance improved");
}
if (x==4)
{
halficon(2,12,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Frenzy","Rage boosts lucky strikes");
}
if (x==5)
{
halficon(6,9,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Cool Mind","-50%% Water MP Cost");
}
if (x==6)
{
halficon(0,7,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Lightblade","Your attacks heal you");
}
if (x==7)
{
halficon(7,12,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Fiery Secret","Fire/Earth spells sometimes stun");
}
if (x==8)
{
halficon(1,3,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Iron Will","+30%% deflection");
}
if (x==9)
{
halficon(0,13,cc,offset+(rowinc*rc),0);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Aura of Magic","Magic items more common");
}
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
}
for (x=0; x<10; ++x)
{
if (plyr.buffx[x]!=-1)
{
halficon(plyr.buffx[x],plyr.buffy[x],cc,offset+(rowinc*rc),0);
fetchbuffinfo(plyr.buffx[x],plyr.buffy[x],0,plyr.buffmod[x]);
strcpy(temp, passtemp);
fetchbuffinfo(plyr.buffx[x],plyr.buffy[x],1,plyr.buffmod[x]);
strcpy(temp2,passtemp);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),temp,temp2);
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
}
if ((plyr.berserking>0)&&(iscombat!=-1))
{
halficon(7,7,cc,offset+(rowinc*rc),0);
strcpy(temp,"Berserker Rage");
sprintf(temp2,"+%i0%%%% damage",plyr.berserking);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),temp,temp2);
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
for (x=0; x<4; ++x)
{
if (plyr.curse[x]!=-1)
{
if (plyr.curse[x]==0)
{
halficon(1,11,cc,offset+(rowinc*rc),2);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Poisoned","Taking damage every turn");
}
if (plyr.curse[x]==1)
{
halficon(6,10,cc,offset+(rowinc*rc),2);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Weakness","Stats are reduced");
}
if (plyr.curse[x]==2)
{
halficon(8,10,cc,offset+(rowinc*rc),2);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Polymorph","Changed to animal");
}
if (plyr.curse[x]==3)
{
halficon(1,10,cc,offset+(rowinc*rc),2);
addtoinfo(cc,offset+(rowinc*rc),cc+(32),offset+(rowinc*rc)+(32),"Softness","Armor is weakened");
}
cc=cc+(32)+(0); if (cc>(screentemp->w-offsety-32)) {cc=dx; ++rc;}
}
}
if (cc!=dx) ++rc;
return rc;
}


void drawmaindock(int type, int sel)
{
int x, butx[6], buty[6], buttons, drawx, cc;
char caption[30];
cc=(buttonbord->w*.95);
if (type==0)
{
// Usual main dock
butx[0]=8; buty[0]=12;
butx[1]=0; buty[1]=0;
butx[2]=1; buty[2]=0;
butx[3]=2; buty[3]=0;
butx[4]=3; buty[4]=0;
butx[5]=4; buty[5]=0;
buttons=6;
strcpy(caption,"Main Dock ");
}
if (type==1)
{
// Creature encounter pre-combat
butx[0]=5; buty[0]=4;
butx[1]=6; buty[1]=4;
butx[2]=7; buty[2]=4;
buttons=3;
strcpy(caption,"Creature Encounter ");
}
if (type==2)
{
// Combat
butx[0]=8; buty[0]=4;
butx[1]=0; buty[1]=0;
butx[2]=2; buty[2]=0;
butx[3]=7; buty[3]=4;
buttons=4;
strcpy(caption,"Combat! ");
}
drawx=(screen->w/2)-(cc*(buttons/2));
if (sel==0) blitinversebutton(butx[0],buty[0],drawx,(screen->h-buttonbord->h),0);
else blitbutton(butx[0],buty[0],drawx,(screen->h-buttonbord->h),0);
if (sel==1) blitinversebutton(butx[1],buty[1],drawx+(cc*1),(screen->h-buttonbord->h),0);
else blitbutton(butx[1],buty[1],drawx+(cc*1),(screen->h-buttonbord->h),0);
if (sel==2) blitinversebutton(butx[2],buty[2],drawx+(cc*2),(screen->h-buttonbord->h),0);
else blitbutton(butx[2],buty[2],drawx+(cc*2),(screen->h-buttonbord->h),0);
if (buttons>3)
{
if (sel==3) blitinversebutton(butx[3],buty[3],drawx+(cc*3),(screen->h-buttonbord->h),0);
else blitbutton(butx[3],buty[3],drawx+(cc*3),(screen->h-buttonbord->h),0);
if (buttons>4)
{
if (sel==4) blitinversebutton(butx[4],buty[4],drawx+(cc*4),(screen->h-buttonbord->h),0);
else blitbutton(butx[4],buty[4],drawx+(cc*4),(screen->h-buttonbord->h),0);
if (buttons>5)
{
if (sel==5) blitinversebutton(butx[5],buty[5],drawx+(cc*5),(screen->h-buttonbord->h),0);
else blitbutton(butx[5],buty[5],drawx+(cc*5),(screen->h-buttonbord->h),0);
}
}
}
for (x=0; x<6; ++x)
{
clickx[x][0]=drawx+(cc*x); clickx[x][1]=drawx+(cc*x)+cc; clicky[x][0]=(screen->h-buttonbord->h); clicky[x][1]=screen->h;
// draw keyboard shortcuts if help enabled
if ((x<buttons)&&(type!=0)&&(help==1))
textprintf_centre_ex(screentemp, smallfont, drawx-4+(cc*x)+(buttonbord->w/2), (screen->h-buttonbord->h)-text_height(smallfont), DARKGOLD,  -1, "%i",x+1);
if ((x<buttons)&&(type==0)&&(help==1)&&(x==0))
textprintf_centre_ex(screentemp, smallfont, drawx-4+(cc*x)+(buttonbord->w/2), (screen->h-buttonbord->h)-text_height(smallfont), DARKGOLD,  -1, "M");
if ((x<buttons)&&(type==0)&&(help==1)&&(x==1))
textprintf_centre_ex(screentemp, smallfont, drawx-4+(cc*x)+(buttonbord->w/2), (screen->h-buttonbord->h)-text_height(smallfont), DARKGOLD,  -1, "I");
if ((x<buttons)&&(type==0)&&(help==1)&&(x==2))
textprintf_centre_ex(screentemp, smallfont, drawx-4+(cc*x)+(buttonbord->w/2), (screen->h-buttonbord->h)-text_height(smallfont), DARKGOLD,  -1, "E");
if ((x<buttons)&&(type==0)&&(help==1)&&(x==3))
textprintf_centre_ex(screentemp, smallfont, drawx-4+(cc*x)+(buttonbord->w/2), (screen->h-buttonbord->h)-text_height(smallfont), DARKGOLD,  -1, "B");
if ((x<buttons)&&(type==0)&&(help==1)&&(x==4))
textprintf_centre_ex(screentemp, smallfont, drawx-4+(cc*x)+(buttonbord->w/2), (screen->h-buttonbord->h)-text_height(smallfont), DARKGOLD,  -1, "S");
if ((x<buttons)&&(type==0)&&(help==1)&&(x==5))
textprintf_centre_ex(screentemp, smallfont, drawx-4+(cc*x)+(buttonbord->w/2), (screen->h-buttonbord->h)-text_height(smallfont), DARKGOLD,  -1, "Y");
}
if (sel!=-1) playsound("confirm.wav");
}


void blitbutton(int bx, int by, int x, int y, int flip)
{
if (flip!=1)
{
blit(icons,screentemp,(bx*buttonsize),(by*buttonsize),x,y,buttonsize,buttonsize);
draw_trans_sprite(screentemp,buttonbord,x-4,y-4);
}
else
{
blit(icons,flipbuttontemp,buttonsize*3,buttonsize*13,0,0,buttonsize,buttonsize);
draw_sprite_v_flip(screentemp,flipbuttontemp,x,y);
draw_trans_sprite(screentemp,buttonbord,x-4,y-4);
}
}

void blitinversebutton(int bx, int by, int x, int y, int flip)
{
if (flip!=1)
{
masked_blit(iconsi,screentemp,(bx*buttonsize),(by*buttonsize),x,y,buttonsize,buttonsize);
draw_trans_sprite(screentemp,buttonbord,x-4,y-4);
}
else
{
blit(iconsi,flipbuttontemp,buttonsize*3,buttonsize*13,0,0,buttonsize,buttonsize);
draw_sprite_v_flip(screentemp,flipbuttontemp,x,y);
draw_trans_sprite(screentemp,buttonbord,x-4,y-4);
}
playsound("confirm.wav");
}


void drawbags(int page, int shop)
{
int offset, cc, rc, invct, x, y, maxpage, width;
char temp[40], temp2[10], temp3[5];
width=486;
drawalignborder(LEFT,TOP,width,offsety+(14*32),"Bags ");
guilocations.bagx[0]=0;
guilocations.bagx[1]=width;
guilocations.bagy[0]=0;
guilocations.bagy[1]=offsety+(14*32);
sprintf(temp,"Gold: %d",plyr.gold);
cc=width-offsetx-text_length(smallfont, temp);
textprintf_ex(screentemp, smallfont, cc, offsety, BROWN, -1, "%s", temp);
for (x=0; ((x<100)&&(plyr.invicnx[x]!=-1)); ++x);
invct=x+1;
if (((page*20)+20)<=invct) maxpage=(page*20)+20;
else maxpage=invct;
cc=offsetx; rc=1;
for (x=(page*20); x<maxpage; ++x)
{
for (y=0; y<11; ++y)
if (plyr.equipped[y]==x) masked_blit(maskedsmalltiles,screentemp,62*32,27*32,cc,offsety+(rc*32),32,32);
masked_blit(maskedsmalltiles,screentemp,plyr.invicnx[x]*32,plyr.invicny[x]*32,cc,offsety+(rc*32),32,32);
strcpy(temp,plyr.invname[x]);
if (plyr.invnum[x]>1) textprintf_ex(screentemp, smallfont, (cc)+32, offsety+(rc*32), BROWN, -1, "%s (%i)", temp,plyr.invnum[x]);
else textprintf_ex(screentemp, smallfont, (cc)+32, offsety+(rc*32), BROWN, -1, "%s", temp);
strcpy(temp2,"");
for (y=0; y<12; ++y)
{
if (plyr.invmtype[x]!=4)
{
if ((y<4)&&((plyr.wswap[y][0]==x)||(plyr.wswap[y][1]==x)))
{
sprintf(temp3,"%d",y+1);
strcat(temp2,temp3);
textprintf_ex(screentemp, smallfont, (cc)+32+text_length(smallfont,temp), offsety+(rc*32), RED, -1, " %s", temp2);
}
}
else if ((plyr.funcmacrox[y]==x)&&(plyr.funcmacroy[y]==-1))
{
if (y<9) rectfill(screentemp,(cc),offsety+(rc*32),(cc)+text_length(smallfont,"F5"),offsety+(rc*32)+14,BLACK);
else rectfill(screentemp,(cc),offsety+(rc*32),(cc)+text_length(smallfont,"F11"),offsety+(rc*32)+14,BLACK);
textprintf_ex(screentemp, smallfont, (cc), offsety+(rc*32)-4, RED, -1, "F%i",y+1);
}
}
++rc; if (rc==11) {cc=offsetx+(32*6); rc=1;}
}
if (shop==3) boldsmalltext(offsetx,offsety,"Left-click: select item Right-click: cancel.",BROWN,SCREEN,LEFT);
else if (shop==2) boldsmalltext(offsetx,offsety,"Left-click: drop item Right-click: cancel.",BROWN,SCREEN,LEFT);
else if ((shop==1)&&(saleoption==0)) boldsmalltext(offsetx,offsety,"Right-click to sell an item.",BROWN,SCREEN,LEFT);
else if ((shop==1)&&(saleoption==1)) boldsmalltext(offsetx,offsety,"Left-click to sell an item.",BROWN,SCREEN,LEFT);
else boldsmalltext(offsetx,offsety,"Right-click to use or equip item.",BROWN,SCREEN,LEFT);
blitbutton(0,12,offsetx,offsety+32*12,0);
if (page>0)
blitbutton(3,13,offsetx+32*2,offsety+32*12,1);
if ((page!=4)&&(plyr.invicnx[(page*20)+20]!=-1))
blitbutton(3,13,offsetx+32*3,offsety+32*12,0);
blitbutton(3,13,offsetx+32*3,offsety+32*12,0);
y=0;
for (x=0; x<100; ++x)
if (plyr.invicnx[x]!=-1) ++y;
--y;
textprintf_ex(screentemp, smallfont, offsetx+32*5, offsety+32*12, BROWN, -1, "Page %d of %d",bagpage+1,(y/20)+1);
}

void purchaseitem(int where, int slot)
{
int x, y;
char tempstr[50], strint[10];
if (plyr.gold<shop[where].price[slot])
{
addlog("You don't have enough gold.",1);
drawlog(1);
blitscreen();
}
else
{
strcpy(tempstr,"Purchase ");
strcat(tempstr,shop[where].name[slot]);
y=strlen(tempstr);
strcat(tempstr,"for ");
sprintf(strint,"%d",shop[where].price[slot]);
strcat(tempstr,strint);
strcat(tempstr," gold?");
x=draw_menu(CENTER, CENTER, y, 1, tempstr, 2);
if (x==0)
{
plyr.gold=plyr.gold-shop[where].price[slot];
shoptotemp(where, slot);
gotitem(-1,0);
shoplostitem(where, slot);
playsound("sell_buy_item.wav");
}
}
}


void sellitem(int slot)
{
int x, y, z;
char temp[50], temp2[40];
invtotemp(slot);
z=getprice();
z=z/4;

if (plyr.invicnx[slot]!=-1)
{
if (((plyr.invmtype[slot]==4)&&(plyr.invcls[slot]>1))||(plyr.invmtype[slot]==5)
     ||(plyr.invcls[slot]==4012))
{
addlog("The merchant doesn't want this item.",1);
x=1;
}
else if (((plyr.invmtype[slot]==0)&&(plyr.equipmw[plyr.invcls[slot]][plyr.invbonus[slot]]==1)&&
          ((plyr.invtreasure[slot]==0)||((plyr.invtreasure[slot]==1)&&(plyr.twoh!=-1))))||
    ((plyr.invmtype[slot]==1)&&(plyr.equipr[plyr.invcls[slot]]==1))||
    ((plyr.invmtype[slot]==2)&&(((plyr.invcls[slot]<4)&&(plyr.equipa[plyr.invtreasure[slot]]==1))||
    ((plyr.invcls[slot]==7)&&(plyr.equipa[5]==1))
    ||(((plyr.invcls[slot]==5)||(plyr.invcls[slot]==6)||(plyr.invcls[slot]==4))&&((plyr.invelem[slot]!=-1)||(plyr.invadd[slot][0]!=-1)))))
         ||((plyr.invcls[slot]==1)&&(plyr.invmtype[slot]==4)))
{
/*
strcpy(temp,"Sell ");
strcat(temp,plyr.invname[slot]);
y=strlen(temp);
strcat(temp,"for ");
sprintf(temp2,"%d",z);
strcat(temp,temp2);
strcat(temp," gold?");
*/
if (plyr.invnum[slot]>1) sprintf(temp2,"Sell %d %s", plyr.invnum[slot], plyr.invname[slot]);
else sprintf(temp2,"Sell %s", plyr.invname[slot]);
y=strlen(temp2);
sprintf(temp,"%s for %d gold?", temp2, z);
x=draw_menu(CENTER, CENTER, y, 1, temp, 2);
}
else x=0;
if (x==0)
{
plyr.gold=plyr.gold+z;
sprintf(temp,"Sold %s for %d gold.",plyr.invname[slot],z);
addlog(temp,0);
lostitem(slot);
playsound("sell_buy_item.wav");
}
}
}


void dgnlostitem(int slot)
{
int x, y;
dmap[plyr.x][plyr.y].trki=-1;
for (x=slot; x<255; ++x)
{
dgn.item[x]=dgn.item[x+1];
dgn.itemx[x]=dgn.itemx[x+1];
dgn.itemy[x]=dgn.itemy[x+1];
}
dgn.item[255]=-1;
for (x=0; x<128; ++x)
{
for (y=0; y<128; ++y)
{
if (dmap[x][y].trki>slot) --dmap[x][y].trki;
}
}
}


void lostitem(int slot)
{
int x,c;
for (x=0; x<11; ++x)
{
if (plyr.equipped[x]==slot) plyr.equipped[x]=-1;
else if (plyr.equipped[x]>slot) --plyr.equipped[x];
}
for (x=0; x<4; ++x)
{
if (plyr.wswap[x][0]==slot) plyr.wswap[x][0]=-1;
else if (plyr.wswap[x][0]>slot) --plyr.wswap[x][0];
if (plyr.wswap[x][1]==slot) plyr.wswap[x][1]=-1;
else if (plyr.wswap[x][1]>slot) --plyr.wswap[x][1];
}
for (x=0; x<12; ++x)
{
if (plyr.funcmacroy[x]==-1)
{
if (plyr.funcmacrox[x]==slot) plyr.funcmacrox[x]=-1;
else if (plyr.funcmacrox[x]>slot) --plyr.funcmacrox[x];
}
}
for (x=slot; x<99; ++x)
{
invtotemp(x+1);
gotitem(x,1);
}
strcpy(tempitem.name,"");
tempitem.type=-1;
tempitem.mtype=-1;
tempitem.cls=-1;
tempitem.elem=-1;
tempitem.treasure=-1;
tempitem.magic=-1;
tempitem.bonus=-1;
tempitem.icnx=-1;
tempitem.icny=-1;
tempitem.num=-1;
tempitem.lvl=-1;
tempitem.h=-1;
tempitem.l=-1;
if (tempitem.mtype<4)
{
for (c=0; c<5; ++c)
{
tempitem.add[c]=-1;
tempitem.addamt[c]=-1;
}
}
gotitem(99,1);
}


void shoplostitem(int where, int slot)
{
int x;
for (x=slot; x<7; ++x)
{
shoptotemp(where, x+1);
shopgot(where, x);
}
shop[where].icnx[7]=-1;
}



void drawshop(int where)
{
int x, cc;
char temp[50];
if (where==0) strcpy(temp,"Weapon Shop ");
if (where==1) strcpy(temp,"Armor Shop ");
if (where==2) strcpy(temp,"Item Shop ");
drawborder(guilocations.bagx[1],0,guilocations.bagx[1]+300,guilocations.bagy[1],temp);
if (saleoption==0)
boldsmalltext(guilocations.bagx[1]+offsetx,offsety,"Right-click to buy an item.",BROWN,SCREEN,LEFT);
else
boldsmalltext(guilocations.bagx[1]+offsetx,offsety,"Left-click to buy an item.",BROWN,SCREEN,LEFT);
for (x=0; x<8; ++x)
{
if (shop[where].icnx[x]!=-1)
{
masked_blit(maskedsmalltiles,screentemp,shop[where].icnx[x]*32,shop[where].icny[x]*32,guilocations.bagx[1]+offsetx,offsety+32+(x*32),32,32);
if (shop[where].num[x]>1) textprintf_ex(screentemp, smallfont, guilocations.bagx[1]+offsetx+32, offsety+32+(x*32), BROWN, -1, "%s (%i)", shop[where].name[x], shop[where].num[x]);
else if (strlen(shop[where].name[x])>20)
{
strcpy(temp,"");
strncat(temp,shop[where].name[x],20);
textprintf_ex(screentemp, smallfont, guilocations.bagx[1]+offsetx+32, offsety+32+(x*32), BROWN, -1, "%s", temp);
}
else textprintf_ex(screentemp, smallfont, guilocations.bagx[1]+offsetx+32, offsety+32+(x*32), BROWN, -1, "%s", shop[where].name[x]);
sprintf(temp,"%d",shop[where].price[x]);
cc=guilocations.bagx[1]+265-text_length(smallfont, temp);
textprintf_ex(screentemp, smallfont, cc, offsety+32+(x*32), BROWN, -1, "%i",shop[where].price[x]);
}
}
draw_button("EXIT", guilocations.bagx[1]+150, guilocations.bagy[1]-buttonl[0]->h-32, 0, 1);
clickx[0][0]=guilocations.bagx[1]+150-(buttonl[0]->w/2);
clickx[0][1]=guilocations.bagx[1]+150+(buttonl[0]->w/2);
clicky[0][0]=guilocations.bagy[1]-buttonl[0]->h-32;
clicky[0][1]=guilocations.bagy[1]-32;
}

void drawpaperdoll()
{
int dollx, dolly, equipx[11], equipy[11], x, drawx;
/*
plyr.equpped

   0 = head
   1 = chest
   2 = hands
   3 = feet
   4 = back
   5 = ring
   6 = ring2
   7 = necklace
   8 = shield
   9 = weapon
   10 = ammo
*/

drawx=drawalignborder(RIGHT,TOP,312,scaley(632),"Equipped Items ");
dollx=drawx+(156-(paperdoll->w/2)); dolly=(scaley(632)/2)-(paperdoll->h/2);
equipx[0]=66; equipy[0]=5;
equipx[1]=67; equipy[1]=64;
equipx[2]=6; equipy[2]=89;
equipx[3]=67; equipy[3]=202;
equipx[4]=121; equipy[4]=22;
equipx[5]=15; equipy[5]=201;
equipx[6]=124; equipy[6]=195;
equipx[7]=10; equipy[7]=17;
equipx[8]=125; equipy[8]=151;
equipx[9]=10; equipy[9]=152;
equipx[10]=127; equipy[10]=74;
draw_trans_sprite(screentemp,paperdoll,dollx,dolly);
for (x=0; x<11; ++x)
{
if (plyr.equipped[x]!=-1)
{
blit(paper,screentemp,0,0,dollx+equipx[x],dolly+equipy[x],32,32);
masked_blit(maskedsmalltiles,screentemp,plyr.invicnx[plyr.equipped[x]]*32,plyr.invicny[plyr.equipped[x]]*32,dollx+equipx[x],dolly+equipy[x],32,32);
}
}
infoct=0;
for (x=0; x<11; ++x)
{
addtoinfo(dollx+equipx[x],dolly+equipy[x],dollx+equipx[x]+32,dolly+equipy[x]+32,"","");
}
}


int castspell(int school, int button)
{
int cost, costg, x, y, c, a, b;
char temp[40], strint[10];
b=-1;
for (a=0; a<4; ++a)
if (plyr.curse[a]==2) b=0;
if (b==0)
{
addlog("Can't cast spells while polymorphed.",1);
}
else
{
costg=-1; cost=-2;
if (((school==0)&&(button==0))||((school==1)&&(button==0))||((school==2)&&(button==0))||((school==4)&&(button==0)))
{
if (plyr.magic[school]==99) cost=30;
else if (plyr.magic[school]>39) cost=15;
else cost=5;
}
else if ((school==0)&&(button==2)) cost=10;
else if ((school==0)&&(iscombat==-1)) cost=-1;
else if ((school==0)&&(button==1)) cost=10;
else if ((school==0)&&(button==3)) cost=30;
else if ((school==0)&&(button==4)) cost=40;
else if ((school==0)&&(button==5)) {cost=65; costg=4;}
else if ((school==1)&&(button!=0)&&(iscombat==-1)) cost=-1;
else if ((school==1)&&(button==1)) cost=10;
else if ((school==1)&&(button==2)) cost=12;
else if ((school==1)&&(button==3)) cost=25;
else if ((school==1)&&(button==4)) cost=80;
else if ((school==1)&&(button==5)) {cost=65; costg=2;}
else if ((school==2)&&(button==1)) cost=10;
else if ((school==2)&&(button==4)) cost=40;
else if ((school==2)&&(button==5)) {cost=60; costg=1;}
else if ((school==2)&&(iscombat==-1)) cost=-1;
else if ((school==2)&&(button==2)) cost=15;
else if ((school==2)&&(button==3)) cost=30;
else if ((school==3)&&(button==0))
{
if (plyr.magic[school]>=75) cost=30;
else cost=5;
}
else if ((school==3)&&(button==1))
{
if (plyr.magic[school]>=40) cost=20;
else cost=10;
}
else if ((school==3)&&(iscombat==-1)) cost=-1;
else if ((school==3)&&(button==2)) cost=10;
else if ((school==3)&&(button==3)) cost=30;
else if ((school==3)&&(button==4)&&(plyr.magic[3]!=99)) cost=40;
else if ((school==3)&&(button==4)&&(plyr.magic[3]==99)) cost=65;
else if ((school==4)&&(button==4)) cost=40;
else if ((school==4)&&(iscombat==-1)) cost=-1;
else if ((school==4)&&(button==1)) cost=8;
else if ((school==4)&&(button==2)) cost=12;
else if ((school==4)&&(button==3)) cost=25;
else if ((school==4)&&(button==5)) {cost=70; costg=0;}
else if ((school==5)&&(button==0))
{
if (plyr.magic[school]>=55) cost=20;
else cost=5;
}
else if ((school==5)&&(button==3))
{
if (plyr.magic[school]==99) cost=40;
else cost=20;
}
else if ((school==5)&&(button==1)) cost=5;
else if ((school==5)&&(button==2)) cost=20;
else if ((school==5)&&(iscombat==-1)) cost=-1;
else if ((school==5)&&(button==4)) cost=30;
else if ((school==5)&&(button==5)) {cost=60; costg=3;}
if ((strcmp(plyr.clname,"Spellblade")==0)&&(plyr.trade==1)&&(plyr.scholar==99)&&(school==2)) cost=cost/2;
if ((strcmp(plyr.clname,"Warlock")==0)&&(plyr.trade==1)&&(plyr.scholar>49)&&(school==4)) cost=cost-(cost/3);
if (cost==-1) addlog("You can only cast that spell in combat.",1);
else if (plyr.mp<cost) {addlog("You don't have enough MP to cast that.",1);}
else if (cost==-2) {addlog("Cost not assigned properly (error).",1);}
else
{
if (costg!=-1)
{
c=0;
for (x=0; x<100; ++x)
if ((plyr.invmtype[x]==5)&&(plyr.invcls[x]==costg)) c=x;
if (c==0)
{
addlog("You don't have the necessary gem to cast that.",1);
return 0;
}
else
{
--plyr.invnum[c]; if (plyr.invnum[c]==0) lostitem(c);
}
}
if ((strcmp(plyr.clname,"Mage")==0)&&(plyr.trade==1)&&(plyr.scholar>49))
{
c=randomnum(1,5);
if (c==1) {cost=0; addlog("Arcane Mind triggers and the spell costs 0 MP.",0);}
}
if ((plyr.equipped[9]!=-1)&&((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4)))
{
for (a=0; a<5; ++a)
if (plyr.invadd[plyr.equipped[9]][a]==6)
cost=cost-((cost*plyr.invaddamt[plyr.equipped[9]][a])/100);
}
plyr.mp=plyr.mp-cost;
if ((school==0)&&(button==0))
{
if (plyr.magic[school]==99) {addlog("Cast Zephyr's Boon Level 3.",0); gotbuff(6,7,2,plyr.magic[school]);}
else if (plyr.magic[school]>=40) {addlog("Cast Zephyr's Boon Level 2.",0); gotbuff(6,7,1,plyr.magic[school]);}
else {addlog("Cast Zephyr's Boon Level 1.",0); gotbuff(6,7,0,plyr.magic[school]);}
playsound("Spell-Wind-Boon.wav");
}
if ((school==0)&&(button==1))
{
strcpy(temp, "Cast Charged Strike for ");
y=10+randomnum((plyr.itl+plyr.adjitl)/16,(plyr.itl+plyr.adjitl)/8);
if (mons.resist[0]>0) y=y-((mons.resist[0]*y)/132);
y=spelldamage(y);
c=randomnum(plyr.dmgl,plyr.dmgh);
c=c+(plyr.str+plyr.adjstr)/10;
c=c-(mons.deflect/2);  if (c<0) c=0;
y=y+c;
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage6.wav");
return y;
}
if ((school==0)&&(button==2))
{
addlog("Cast Cleanse and removed curses.",0); curedebuff(-1);
playsound("Spell-Cure.wav");
}
if ((school==0)&&(button==3))
{
strcpy(temp,"Cast Lightning Bolt for ");
y=70+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[0]>0) y=y-((mons.resist[0]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage6.wav");
return y;
}
if ((school==0)&&(button==4))
{
strcpy(temp,"Cast Cyclone Strike for ");
a=randomnum(0,1);
if (a==0)
y=120+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
else
y=10+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[0]>0) y=y-((mons.resist[0]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage6.wav");
return y;
}
if ((school==0)&&(button==5))
{
c=randomnum(2,4);
strcpy(temp,"Storm summons ");
sprintf(strint,"%d",c);
strcat(temp,strint);
strcat(temp," bolts, hits ");
a=0;
for (y=0; y<c; ++y) {b=randomnum(1,10); if (b<8) ++a;}
sprintf(strint,"%d",a);
strcat(temp,strint);
strcat(temp," times for ");
y=70+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
y=y*a;
if (mons.resist[0]>0) y=y-((mons.resist[0]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage1.wav");
return y;
}
if ((school==1)&&(button==0))
{
if (plyr.magic[school]==99) {addlog("Cast Fire Weapon Level 3.",0); gotbuff(0,5,2,plyr.magic[school]);}
else if (plyr.magic[school]>=40) {addlog("Cast Fire Weapon Level 2.",0); gotbuff(0,5,1,plyr.magic[school]);}
else {addlog("Cast Fire Weapon Level 1.",0); gotbuff(0,5,0,plyr.magic[school]);}
playsound("Spell-Fire-Boon.wav");
}
if ((school==1)&&(button==1))
{
strcpy(temp,"Cast Firebolt for ");
y=14+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if ((strcmp(plyr.clname,"Spellblade")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) y=y+(y/5);
if (mons.resist[1]>0) y=y-((mons.resist[1]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
warlockstunchance();
playsound("Spell-Damage5.wav");
return y;
}
if ((school==1)&&(button==2))
{
strcpy(temp,"Cast Concussive Blast for ");
y=36+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if ((strcmp(plyr.clname,"Spellblade")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) y=y+(y/5);
if (mons.resist[1]>0) y=y-((mons.resist[1]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
a=randomnum(1,10);
strcat(temp,".");
addlog(temp,0);
if (a==1)
{
addlog("Target is stunned.",0);
mons.stun=randomnum(1,3);
}
warlockstunchance();
playsound("Spell-Damage5.wav");
return y;
}
if ((school==1)&&(button==3))
{
strcpy(temp,"Cast Inferno for ");
y=80+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if ((strcmp(plyr.clname,"Spellblade")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) y=y+(y/5);
if (mons.resist[1]>0) y=y-((mons.resist[1]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage2.wav");
warlockstunchance();
return y;
}
if ((school==1)&&(button==4))
{
strcpy(temp,"Cast Charged Blast for ");
y=200+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if ((strcmp(plyr.clname,"Spellblade")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) y=y+(y/5);
if (mons.resist[1]>0) y=y-((mons.resist[1]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage2.wav");
warlockstunchance();
return y;
}
if ((school==1)&&(button==5))
{
strcpy(temp,"Cast Meteor for ");
y=350+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if ((strcmp(plyr.clname,"Spellblade")==0)&&(plyr.trade==1)&&(plyr.scholar>49)) y=y+(y/5);
if (mons.resist[1]>0) y=y-((mons.resist[1]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage4.wav");
warlockstunchance();
return y;
}
if ((school==2)&&(button==0))
{
if (plyr.magic[school]==99) {addlog("Cast Icy Shield Level 3.",0); gotbuff(7,9,2,plyr.magic[school]);}
else if (plyr.magic[school]>=40) {addlog("Cast Icy Shield Level 2.",0); gotbuff(7,9,1,plyr.magic[school]);}
else {addlog("Cast Icy Shield Level 1.",0); gotbuff(7,9,0,plyr.magic[school]);}
playsound("Spell-Ice.wav");
}
if ((school==2)&&(button==1))
{
y=20+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
sprintf(strint,"%d",y);
strcpy(temp, "Cast Lesser Healing Waters and healed ");
strcat(temp, strint);
strcat(temp," HP.");
addlog(temp,0);
playerheal(y);
playsound("Spell-Water-Heal1.wav");
}
if ((school==2)&&(button==2))
{
strcpy(temp,"Cast Wave for ");
y=32+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[2]>0) y=y-((mons.resist[2]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp," damage and ");
a=10+randomnum((plyr.itl+plyr.adjitl)/16,(plyr.itl+plyr.adjitl)/8);
sprintf(strint,"%d",a);
strcat(temp, strint);
strcat(temp," healing.");
playerheal(a);
addlog(temp,0);
playsound("Spell-Water-Damage.wav");
return y;
}
if ((school==2)&&(button==3))
{
strcpy(temp,"Cast Kraken Strike for ");
y=80+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[2]>0) y=y-((mons.resist[2]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Water-Damage.wav");
return y;
}
if ((school==2)&&(button==4))
{
y=80+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
sprintf(strint,"%d",y);
strcpy(temp, "Cast Greater Healing Waters and healed ");
strcat(temp, strint);
strcat(temp," HP.");
addlog(temp,0);
playerheal(y);
playsound("Spell-Water-Heal2.wav");
}
if ((school==2)&&(button==5))
{
addlog("Cast Mind of the Kraken.",0); gotbuff(0,10,0,plyr.magic[school]);
playsound("Spell-Water-Heal1.wav");
}
if ((school==3)&&(button==0))
{
if (plyr.magic[school]>74)  {addlog("Cast Mountain's Might Level 2.",0); gotbuff(0,8,1,plyr.magic[school]);}
else {addlog("Cast Mountain's Might Level 1.",0); gotbuff(0,8,0,plyr.magic[school]);}
playsound("Spell-Earth-Boon.wav");
}
if ((school==3)&&(button==1))
{
if (plyr.magic[school]>39)  {addlog("Cast Fortification Level 2.",0); gotbuff(1,5,1,plyr.magic[school]);}
else {addlog("Cast Fortification Level 1.",0); gotbuff(1,5,0,plyr.magic[school]);}
playsound("Spell-Earth-Boon.wav");
}
if ((school==3)&&(button==2))
{
a=randomnum(1,3);
y=0;
if (a!=1)
strcpy(temp,"Cast Tremor, but the spell missed the target.");
else
{
strcpy(temp,"Cast Tremor for ");
y=120+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[3]>0) y=y-((mons.resist[3]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
}
addlog(temp,0);
playsound("Spell-Damage1.wav");
warlockstunchance();
return y;
}
if ((school==3)&&(button==3))
{
strcpy(temp,"Cast Rock Volley for ");
y=82+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[3]>0) y=y-((mons.resist[3]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage3.wav");
warlockstunchance();
return y;
}
if ((school==3)&&(button==4))
{
strcpy(temp,"Cast Smash for ");
if (plyr.magic[3]!=99)
y=120+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
else
y=220+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if ((mons.mtype==3)||(mons.mtype==10)||(mons.mtype==2)) y=y*2;
if (mons.resist[3]>0) y=y-((mons.resist[3]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage3.wav");
warlockstunchance();
return y;
}
if ((school==4)&&(button==0))
{
if (plyr.magic[school]==99) {addlog("Cast Laughing Shadow Level 3.",0); gotbuff(2,9,2,plyr.magic[school]);}
else if (plyr.magic[school]>=40) {addlog("Cast Laughing Shadow Level 2.",0); gotbuff(2,9,1,plyr.magic[school]);}
else {addlog("Cast Laughing Shadow Level 1.",0); gotbuff(2,9,0,plyr.magic[school]);}
playsound("Spell-Shadow-Boon.wav");
}
if ((school==4)&&(button==1))
{
strcpy(temp,"Cast Touch of Shadow for ");
y=12+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[4]>0) y=y-((mons.resist[4]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Shadow-Boon.wav");
return y;
}
if ((school==4)&&(button==2))
{
a=randomnum(1,100);
a=a+(plyr.magic[4]/7);
a=a-(mons.resist[4]/8);
if (a<51)
{
strcpy(temp,"Cast Dark Strike, but it had no effect.");
playsound("failure.wav");
}
else
{
strcpy(temp,"Cast Dark Strike and the target was stunned.");
mons.stun=randomnum(1,3);
playsound("Spell-Damage5.wav");
}
addlog(temp,0);
}
if ((school==4)&&(button==3))
{
strcpy(temp,"Cast Drain Life for ");
y=75+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[4]>0) y=y-((mons.resist[4]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp," damage and regained HP.");
addlog(temp,0);
playerheal(y/3);
playsound("Spell-Shadow-Cloud.wav");
return y;
}
if ((school==4)&&(button==4))
{
addlog("Cast Crown of the Dead.",0); gotbuff(1,9,0,plyr.magic[school]);
playsound("Spell-Shadow-Boon.wav");
}
if ((school==4)&&(button==5))
{
strcpy(temp,"Cast Dark Cloud for ");
y=350+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[4]>0) y=y-((mons.resist[4]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Shadow-Cloud.wav");
return y;
}
if ((school==5)&&(button==0))
{
if (plyr.magic[school]>54)  {addlog("Cast Holy Warrior Level 2.",0); gotbuff(2,5,1,plyr.magic[school]);}
else {addlog("Cast Holy Warrior Level 1.",0); gotbuff(2,5,0,plyr.magic[school]);}
playsound("Spell-Light-Boon.wav");
}
if ((school==5)&&(button==1))
{
addlog("Cast Cure Poison and was cured of poison.",0);
curedebuff(0);
playsound("Spell-Cure.wav");
}
if ((school==5)&&(button==2))
{
y=50+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
sprintf(strint,"%d",y);
strcpy(temp, "Cast Light's Infusion and healed ");
strcat(temp, strint);
strcat(temp," HP.");
addlog(temp,0);
playerheal(y);
playsound("Spell-Light-Heal.wav");
}
if ((school==5)&&(button==3))
{
if (plyr.magic[school]==99)  {addlog("Cast Holy Boon Level 2.",0); gotbuff(6,6,1,plyr.magic[school]);}
else {addlog("Cast Holy Boon Level 1.",0); gotbuff(6,6,0,plyr.magic[school]);}
playsound("Spell-Light-Boon.wav");
}
if ((school==5)&&(button==4))
{
strcpy(temp,"Cast Sunstrike for ");
y=75+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[5]>0) y=y-((mons.resist[5]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp,".");
addlog(temp,0);
playsound("Spell-Damage6.wav");
return y;
}
if ((school==5)&&(button==5))
{
strcpy(temp,"Cast Call the Light for ");
y=225+randomnum((plyr.itl+plyr.adjitl)/6,(plyr.itl+plyr.adjitl)/3);
if (mons.resist[5]>0) y=y-((mons.resist[5]*y)/132);
y=spelldamage(y);
sprintf(strint,"%d",y);
strcat(temp, strint);
strcat(temp," damage and regained HP.");
addlog(temp,0);
playerheal(y/2);
playsound("Spell-Light-Heal.wav");
playsound("Spell-Damage6.wav");
return y;
}
}
}
return 0;
}


void drawspellbook(int row, int button)
{
int rowinc, x, rowx[6][6], rowy[6][6], rowid[6], rows, y, yy, xx, z, drawx;
for (x=0; x<6; ++x)
{
for (y=0; y<5; ++y)
{
spellbutton[x][y]=-1;
}
}
infoct=0;
rowinc=64;
drawx=drawalignborder(RIGHT,TOP,312,scaley(632),"Spellbook ");
xx=32;
yy=32;
for (x=0; x<6; ++x)
{
for (y=0; y<6; ++y)
{
rowx[x][y]=-1; rowy[x][y]=-1; rowid[x]=-1;
}
}
rows=0;
for (x=0; x<6; ++x)
{
if (plyr.magic[x]!=-1)
{
if (x==0)
{rowx[0][0]=6; rowy[0][0]=7;}
if ((x==0)&&(plyr.magic[x]>9))
{rowx[0][1]=4; rowy[0][1]=7;}
if ((x==0)&&(plyr.magic[x]>19))
{rowx[0][2]=3; rowy[0][2]=7;}
if ((x==0)&&(plyr.magic[x]>29))
{rowx[0][3]=0; rowy[0][3]=14;}
if ((x==0)&&(plyr.magic[x]>54))
{rowx[0][4]=5; rowy[0][4]=7;}
if ((x==0)&&(plyr.magic[x]>74))
{rowx[0][5]=2; rowy[0][5]=14;}
if (x==1)
{rowx[rows][0]=0; rowy[rows][0]=5;}
if ((x==1)&&(plyr.magic[x]>9))
{rowx[rows][1]=5; rowy[rows][1]=13;}
if ((x==1)&&(plyr.magic[x]>19))
{rowx[rows][2]=9; rowy[rows][2]=13;}
if ((x==1)&&(plyr.magic[x]>29))
{rowx[rows][3]=8; rowy[rows][3]=13;}
if ((x==1)&&(plyr.magic[x]>54))
{rowx[rows][4]=3; rowy[rows][4]=8;}
if ((x==1)&&(plyr.magic[x]>74))
{rowx[rows][5]=6; rowy[rows][5]=13;}
if (x==2)
{rowx[rows][0]=7; rowy[rows][0]=9;}
if ((x==2)&&(plyr.magic[x]>9))
{rowx[rows][1]=8; rowy[rows][1]=9;}
if ((x==2)&&(plyr.magic[x]>19))
{rowx[rows][2]=5; rowy[rows][2]=9;}
if ((x==2)&&(plyr.magic[x]>29))
{rowx[rows][3]=8; rowy[rows][3]=10;}
if ((x==2)&&(plyr.magic[x]>54))
{rowx[rows][4]=9; rowy[rows][4]=9;}
if ((x==2)&&(plyr.magic[x]>74))
{rowx[rows][5]=0; rowy[rows][5]=10;}
if (x==3)
{rowx[rows][0]=0; rowy[rows][0]=8;}
if ((x==3)&&(plyr.magic[x]>9))
{rowx[rows][1]=1; rowy[rows][1]=5;}
if ((x==3)&&(plyr.magic[x]>19))
{rowx[rows][2]=1; rowy[rows][2]=8;}
if ((x==3)&&(plyr.magic[x]>29))
{rowx[rows][3]=4; rowy[rows][3]=14;}
if ((x==3)&&(plyr.magic[x]>54))
{rowx[rows][4]=3; rowy[rows][4]=14;}
if (x==4)
{rowx[rows][0]=2; rowy[rows][0]=9;}
if ((x==4)&&(plyr.magic[x]>9))
{rowx[rows][1]=6; rowy[rows][1]=14;}
if ((x==4)&&(plyr.magic[x]>19))
{rowx[rows][2]=1; rowy[rows][2]=14;}
if ((x==4)&&(plyr.magic[x]>29))
{rowx[rows][3]=4; rowy[rows][3]=9;}
if ((x==4)&&(plyr.magic[x]>54))
{rowx[rows][4]=1; rowy[rows][4]=9;}
if ((x==4)&&(plyr.magic[x]>74))
{rowx[rows][5]=7; rowy[rows][5]=14;}
if (x==5)
{rowx[rows][0]=2; rowy[rows][0]=5;}
if ((x==5)&&(plyr.magic[x]>9))
{rowx[rows][1]=5; rowy[rows][1]=8;}
if ((x==5)&&(plyr.magic[x]>19))
{rowx[rows][2]=6; rowy[rows][2]=8;}
if ((x==5)&&(plyr.magic[x]>29))
{rowx[rows][3]=6; rowy[rows][3]=6;}
if ((x==5)&&(plyr.magic[x]>39))
{rowx[rows][4]=4; rowy[rows][4]=8;}
if ((x==5)&&(plyr.magic[x]>74))
{rowx[rows][5]=5; rowy[rows][5]=14;}
rowid[rows]=x; ++rows;
}
}
for (x=0; x<rows; ++x)
{
if (rowid[x]==0) boldtext(drawx+offsetx,offsety+(x*rowinc),"Wind",BROWN,SCREEN,LEFT);
else if (rowid[x]==1) boldtext(drawx+offsetx,offsety+(x*rowinc),"Fire",BROWN,SCREEN,LEFT);
else if (rowid[x]==2) boldtext(drawx+offsetx,offsety+(x*rowinc),"Water",BROWN,SCREEN,LEFT);
else if (rowid[x]==3) boldtext(drawx+offsetx,offsety+(x*rowinc),"Earth",BROWN,SCREEN,LEFT);
else if (rowid[x]==4) boldtext(drawx+offsetx,offsety+(x*rowinc),"Shadow",BROWN,SCREEN,LEFT);
else if (rowid[x]==5) boldtext(drawx+offsetx,offsety+(x*rowinc),"Light",BROWN,SCREEN,LEFT);
for (y=0; y<6; ++y)
{
if (rowx[x][y]!=-1)
rectfill(screentemp,drawx+offsetx+(xx*y)-2,offsety+yy+(rowinc*x)-1,drawx+offsetx+(xx*y)+32+1,offsety+yy+(rowinc*x)+32+1,BLACK);
if ((row!=-1)&&(row==y)&&(button==x)) blit(iconsi,screentemp,rowx[x][y]*32,rowy[x][y]*32,drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),32,32);
else blit(icons,screentemp,rowx[x][y]*32,rowy[x][y]*32,drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),32,32);
for (z=0; z<12; ++z)
if ((plyr.funcmacrox[z]==x)&&(plyr.funcmacroy[z]==y))
{
if (z<9) rectfill(screentemp,drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+text_length(smallfont,"F5"),offsety+yy+(rowinc*x)+14,BLACK);
else rectfill(screentemp,drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+text_length(smallfont,"F11"),offsety+yy+(rowinc*x)+14,BLACK);
textprintf_ex(screentemp, smallfont, drawx+offsetx+(xx*y), offsety+yy+(rowinc*x)-4, RED, -1, "F%i",z+1);
}
if (rowx[x][y]!=-1)
{
spellbutton[y][x]=rowid[x];
}
if ((rowid[x]==0)&&(y==0)&&(plyr.magic[0]<40)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Zephyr's Boon Level 1, Cost 5","Increases dodge chance.");
else if ((rowid[x]==0)&&(y==0)&&(plyr.magic[0]<99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Zephyr's Boon Level 2, Cost 15","Grants +Dodge and Haste.");
else if ((rowid[x]==0)&&(y==0)&&(plyr.magic[0]==99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Zephyr's Boon Level 3, Cost 30","+Dodge +Haste +%%10 Int/Dex");
if ((rowid[x]==0)&&(y==1)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Charged Strike, Cost 10","Melee+Lightning Attack");
if ((rowid[x]==0)&&(y==2)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Cleanse, Cost 10","Remove Curses or open door");
if ((rowid[x]==0)&&(y==3)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Lightning Bolt, Cost 30","Medium damage spell.");
if ((rowid[x]==0)&&(y==4)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Cyclone Strike, Cost 40","High or low damage.");
if ((rowid[x]==0)&&(y==5)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Storm, Cost 65+Emerald","High damage");
if ((rowid[x]==1)&&(y==0)&&(plyr.magic[1]<40)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Fire Weapon Level 1, Cost 5","Fire damage to attacks");
else if ((rowid[x]==1)&&(y==0)&&(plyr.magic[1]<99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Fire Weapon Level 2, Cost 15","Greater fire damage/attacks");
else if ((rowid[x]==1)&&(y==0)&&(plyr.magic[1]==99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Fire Weapon Level 3, Cost 30","+Fire dmg +Free dmg every turn");
if ((rowid[x]==1)&&(y==1)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Firebolt, Cost 10","Low Damage Spell");
if ((rowid[x]==1)&&(y==2)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Concussive Blast, Cost 12","Dmg+Stun or Open Door");
if ((rowid[x]==1)&&(y==3)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Inferno, Cost 25","Medium damage");
if ((rowid[x]==1)&&(y==4)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Charged Blast, Cost 80","High damage but expensive");
if ((rowid[x]==1)&&(y==5)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Meteor, Cost 65+Ruby","High damage");
if ((strcmp(plyr.clname,"Spellblade")==0)&&(plyr.trade==1)&&(plyr.scholar==99))
{
if ((rowid[x]==2)&&(y==0)&&(plyr.magic[2]<40)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Icy Shield Level 1, Cost 2","Deflection +%%10");
else if ((rowid[x]==2)&&(y==0)&&(plyr.magic[2]<99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Icy Shield Level 2, Cost 7","+%%10 Deflect, heals HP/turn");
else if ((rowid[x]==2)&&(y==0)&&(plyr.magic[2]==99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Icy Shield Level 3, Cost 15","+%%20 Deflect, heals HP/turn");
if ((rowid[x]==2)&&(y==1)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Healing Waters, Cost 5","Heals a small amount");
if ((rowid[x]==2)&&(y==2)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Wave Strike, Cost 7","Dmg+Heal or open door");
if ((rowid[x]==2)&&(y==3)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Kraken Strike, Cost 15","Medium damage");
if ((rowid[x]==2)&&(y==4)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Greater Healing Waters, Cost 20","Heals a large amount");
if ((rowid[x]==2)&&(y==5)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Mind of the Kraken, C30+Sapphire","+%%20 base Int");
}
else
{
if ((rowid[x]==2)&&(y==0)&&(plyr.magic[2]<40)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Icy Shield Level 1, Cost 5","Deflection +%%10");
else if ((rowid[x]==2)&&(y==0)&&(plyr.magic[2]<99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Icy Shield Level 2, Cost 15","+%%10 Deflect, heals HP/turn");
else if ((rowid[x]==2)&&(y==0)&&(plyr.magic[2]==99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Icy Shield Level 3, Cost 30","+%%20 Deflect, heals HP/turn");
if ((rowid[x]==2)&&(y==1)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Healing Waters, Cost 10","Heals a small amount");
if ((rowid[x]==2)&&(y==2)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Wave Strike, Cost 15","Dmg+Heal or open door");
if ((rowid[x]==2)&&(y==3)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Kraken Strike, Cost 30","Medium damage");
if ((rowid[x]==2)&&(y==4)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Greater Healing Waters, Cost 40","Heals a large amount");
if ((rowid[x]==2)&&(y==5)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Mind of the Kraken, C60+Sapphire","+%%20 base Int");
}
if ((rowid[x]==3)&&(y==0)&&(plyr.magic[3]<75)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Mountain's Might Level 1, Cost 5","+%%10 base Strength");
else if ((rowid[x]==3)&&(y==0)&&(plyr.magic[3]>74)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Mountain's Might Level 2, Cost 30","+%%20 base Strength");
if ((rowid[x]==3)&&(y==1)&&(plyr.magic[3]<40)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Fortification Level 1, Cost 10","Deflection +%%10");
else if ((rowid[x]==3)&&(y==1)&&(plyr.magic[3]>39)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Fortification Level 2, Cost 20","+%%10Deflect +Chance to avoid dmg");
if ((rowid[x]==3)&&(y==2)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Tremor, Cost 10","Chance for high dmg, or open door");
if ((rowid[x]==3)&&(y==3)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Rock Volley, Cost 30","Medium damage");
if ((rowid[x]==3)&&(y==4)&&(plyr.magic[3]<99)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Smash Level 1, Cost 40","Extra dmg vs insect, reptile, ooze");
if ((rowid[x]==3)&&(y==4)&&(plyr.magic[3]==99)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Smash Level 2, Cost 65","Extra dmg vs insect, reptile, ooze");
if ((strcmp(plyr.clname,"Warlock")==0)&&(plyr.trade==1)&&(plyr.scholar>49))
{
if ((rowid[x]==4)&&(y==0)&&(plyr.magic[4]<40)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Laughing Shadow Level 1, Cost 4","Enemies sometimes harm selves");
else if ((rowid[x]==4)&&(y==0)&&(plyr.magic[4]<99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Laughing Shadow Level 2, Cost 10","+Divert, +Shadow dmg");
else if ((rowid[x]==4)&&(y==0)&&(plyr.magic[4]==99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Laughing Shadow Level 3, Cost 20","+Divert +Dmg +%%10 Int");
if ((rowid[x]==4)&&(y==1)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Touch of Darkness, Cost 6","Low Damage");
if ((rowid[x]==4)&&(y==2)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Shadow Strike, Cost 9","Stun enemy or open door.");
if ((rowid[x]==4)&&(y==3)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Drain Life, Cost 17","Medium dmg +lifesteal");
if ((rowid[x]==4)&&(y==4)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Crown of the Dead, Cost 27","Dmg and lifesteal/turn");
if ((rowid[x]==4)&&(y==5)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Dark Cloud, Cost 47+Amethyst","High damage");
}
else
{
if ((rowid[x]==4)&&(y==0)&&(plyr.magic[4]<40)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Laughing Shadow Level 1, Cost 5","Enemies sometimes harm selves");
else if ((rowid[x]==4)&&(y==0)&&(plyr.magic[4]<99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Laughing Shadow Level 2, Cost 15","+Divert, +Shadow dmg");
else if ((rowid[x]==4)&&(y==0)&&(plyr.magic[4]==99)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Laughing Shadow Level 3, Cost 30","+Divert +Dmg +%%10 Int");
if ((rowid[x]==4)&&(y==1)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Touch of Darkness, Cost 8","Low Damage");
if ((rowid[x]==4)&&(y==2)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Shadow Strike, Cost 12","Stun enemy or open door.");
if ((rowid[x]==4)&&(y==3)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Drain Life, Cost 25","Medium dmg +lifesteal");
if ((rowid[x]==4)&&(y==4)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Crown of the Dead, Cost 40","Dmg and lifesteal/turn");
if ((rowid[x]==4)&&(y==5)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Dark Cloud, Cost 70+Amethyst","High damage");
}
if ((rowid[x]==5)&&(y==0)&&(plyr.magic[5]<55)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Holy Warrior Level 1, Cost 5","+Dmg and +Dmg vs undead");
else if ((rowid[x]==5)&&(y==0)&&(plyr.magic[5]>54)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Holy Warrior Level 2, cost 20","+Dmg and +Dmg vs undead");
if ((rowid[x]==5)&&(y==1)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Cure Poison, Cost 5","Cures Poison");
if ((rowid[x]==5)&&(y==2)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Light's Infusion, Cost 20","Heal a medium amount or open door");
if ((rowid[x]==5)&&(y==3)&&(plyr.magic[5]<99)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Holy Boon Level 1, Cost 20","+%%10 base Str/Int");
if ((rowid[x]==5)&&(y==3)&&(plyr.magic[5]==99)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Holy Boon Level 2, Cost 40","+%%15 base Str/Int");
if ((rowid[x]==5)&&(y==4)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Sunstrike, Cost 30","Medium damage");
if ((rowid[x]==5)&&(y==5)&&(rowx[x][y]!=-1)) addtoinfo(drawx+offsetx+(xx*y),offsety+yy+(rowinc*x),drawx+offsetx+(xx*y)+32,offsety+yy+(rowinc*x)+32,"Call the Light, Cost 60+Quartz","High damage +heal");
}
}
}

void fetchbuffinfo(int bx, int by, int which, int mod)
{
char temp[50];
if ((bx==4)&&(by==2)&&(which==0)) strcpy(temp, "Bonus to Strength");
if ((bx==5)&&(by==2)&&(which==0)) strcpy(temp, "Bonus to Dexterity");
if ((bx==6)&&(by==2)&&(which==0)) strcpy(temp, "Bonus to Intelligence");
if ((bx==7)&&(by==2)&&(which==0)) strcpy(temp, "Bonus to Charisma");
if (((bx>=4)&&(bx<8))&&(by==2)&&(which==1)) strcpy(temp, "+%%10 of base value");
if ((bx==4)&&(by==13)&&(which==0)) strcpy(temp, "Recklessness");
if ((bx==4)&&(by==13)&&(which==1)) strcpy(temp, "Attack damage up / deflect down");
if ((bx==7)&&(by==5)&&(which==0)) strcpy(temp, "Spellstrike");
if ((bx==7)&&(by==5)&&(which==1)) strcpy(temp, "Spells deal %%50 more damage");
if ((bx==8)&&(by==8)&&(which==0)) strcpy(temp, "Lucky Potion");
if ((bx==8)&&(by==8)&&(which==1)) strcpy(temp, "Increased chance for lucky strikes");
if ((bx==8)&&(by==5)&&(which==0)) strcpy(temp, "Haste");
if ((bx==8)&&(by==5)&&(which==1)) strcpy(temp, "Speed plus chance for double strike");
if ((bx==9)&&(by==3)&&(which==0)) strcpy(temp, "+%%20 Wind Resist");
if ((bx==9)&&(by==3)&&(which==1)) strcpy(temp, "");
if ((bx==0)&&(by==4)&&(which==0)) strcpy(temp, "+%%20 Fire Resist");
if ((bx==1)&&(by==4)&&(which==0)) strcpy(temp, "+%%20 Water Resist");
if ((bx==2)&&(by==4)&&(which==0)) strcpy(temp, "+%%20 Earth Resist");
if ((bx==3)&&(by==4)&&(which==0)) strcpy(temp, "+%%20 Shadow Resist");
if ((bx==4)&&(by==4)&&(which==0)) strcpy(temp, "+%%20 Light Resist");
if ((bx<5)&&(by==4)&&(which==1)) strcpy(temp, "");
if ((bx==1)&&(by==11)&&(which==0)) strcpy(temp, "+%%20 Poison Resist");
if ((bx==1)&&(by==11)&&(which==1)) strcpy(temp, "");
if ((bx==6)&&(by==7)&&(which==0)&&(mod==0)) strcpy(temp, "Zephyr's Boon Level 1");
if ((bx==6)&&(by==7)&&(which==0)&&(mod==1)) strcpy(temp, "Zephyr's Boon Level 2");
if ((bx==6)&&(by==7)&&(which==0)&&(mod==2)) strcpy(temp, "Zephyr's Boon Level 3");
if ((bx==6)&&(by==7)&&(which==1)&&(mod==0)) strcpy(temp, "Increases dodge chance");
if ((bx==6)&&(by==7)&&(which==1)&&(mod==1)) strcpy(temp, "+Dodge +Haste");
if ((bx==6)&&(by==7)&&(which==1)&&(mod==2)) strcpy(temp, "+Dodge +Haste +%%10 base Dex/Int");
if ((bx==0)&&(by==5)&&(which==0)&&(mod==0)) strcpy(temp, "Fire Weapon Level 1");
if ((bx==0)&&(by==5)&&(which==0)&&(mod==1)) strcpy(temp, "Fire Weapon Level 2");
if ((bx==0)&&(by==5)&&(which==0)&&(mod==2)) strcpy(temp, "Fire Weapon Level 3");
if ((bx==0)&&(by==5)&&(which==1)&&((mod==0)||(mod==1))) strcpy(temp, "+Fire damage/attack");
if ((bx==0)&&(by==5)&&(which==1)&&(mod==2)) strcpy(temp, "+Fire dmg  +Damage/turn");
if ((bx==7)&&(by==9)&&(which==0)&&(mod==0)) strcpy(temp, "Icy Shield Level 1");
if ((bx==7)&&(by==9)&&(which==0)&&(mod==1)) strcpy(temp, "Icy Shield Level 2");
if ((bx==7)&&(by==9)&&(which==0)&&(mod==2)) strcpy(temp, "Icy Shield Level 3");
if ((bx==7)&&(by==9)&&(which==1)&&(mod==0)) strcpy(temp, "Deflection +%%10");
if ((bx==7)&&(by==9)&&(which==1)&&(mod==1)) strcpy(temp, "+%%10 Deflect, heals HP/turn");
if ((bx==7)&&(by==9)&&(which==1)&&(mod==2)) strcpy(temp, "+%%20 Deflect, heals HP/turn");
if ((bx==0)&&(by==10)&&(which==0)) strcpy(temp,"Mind of the Kraken");
if ((bx==0)&&(by==10)&&(which==1)) strcpy(temp,"+%%20 base Int");
if ((bx==0)&&(by==8)&&(which==0)&&(mod==0)) strcpy(temp, "Mountain's Might Level 1");
if ((bx==0)&&(by==8)&&(which==0)&&(mod==1)) strcpy(temp, "Mountain's Might Level 2");
if ((bx==0)&&(by==8)&&(which==1)&&(mod==0)) strcpy(temp, "+%%10 base Strength");
if ((bx==0)&&(by==8)&&(which==1)&&(mod==1)) strcpy(temp, "+%%20 base Strength");
if ((bx==1)&&(by==5)&&(which==0)&&(mod==0)) strcpy(temp, "Fortification Level 1");
if ((bx==1)&&(by==5)&&(which==0)&&(mod==1)) strcpy(temp, "Fortification Level 2");
if ((bx==1)&&(by==5)&&(which==1)&&(mod==0)) strcpy(temp, "+%%10 Deflection");
if ((bx==1)&&(by==5)&&(which==1)&&(mod==1)) strcpy(temp, "+Deflect +Chance to avoid dmg");
if ((bx==2)&&(by==9)&&(which==0)&&(mod==0)) strcpy(temp, "Laughing Shadow Level 1");
if ((bx==2)&&(by==9)&&(which==0)&&(mod==1)) strcpy(temp, "Laughing Shadow Level 2");
if ((bx==2)&&(by==9)&&(which==0)&&(mod==2)) strcpy(temp, "Laughing Shadow Level 3");
if ((bx==2)&&(by==9)&&(which==1)&&(mod==0)) strcpy(temp, "Enemys sometimes harm selves");
if ((bx==2)&&(by==9)&&(which==1)&&(mod==1)) strcpy(temp, "+Divert +Shadow dmg");
if ((bx==2)&&(by==9)&&(which==1)&&(mod==2)) strcpy(temp, "+Divert +Shadow dmg +Int");
if ((bx==1)&&(by==9)&&(which==0)) strcpy(temp,"Crown of the Dead");
if ((bx==1)&&(by==9)&&(which==1)) strcpy(temp,"Damage+life steal/turn");
if ((bx==2)&&(by==5)&&(which==0)&&(mod==0)) strcpy(temp, "Holy Warrior Level 1");
if ((bx==2)&&(by==5)&&(which==0)&&(mod==1)) strcpy(temp, "Holy Warrior Level 2");
if ((bx==2)&&(by==5)&&(which==1)) strcpy(temp, "+Dmg and +Dmg vs. Undead");
if ((bx==6)&&(by==6)&&(which==0)&&(mod==0)) strcpy(temp, "Holy Boon Level 1");
if ((bx==6)&&(by==6)&&(which==0)&&(mod==1)) strcpy(temp, "Holy Boon Level 2");
if ((bx==6)&&(by==6)&&(which==1)&&(mod==0)) strcpy(temp, "+%%10 base Str and Int");
if ((bx==6)&&(by==6)&&(which==1)&&(mod==1)) strcpy(temp, "+%%15 base Str and Int");
if ((bx==3)&&(by==6)&&(which==0)&&(mod==0)) strcpy(temp, "Double Dragon");
if ((bx==3)&&(by==6)&&(which==1)&&(mod==0)) strcpy(temp, "Frequent Double Strikes");
if ((bx==9)&&(by==4)&&(which==0)&&(mod==0)) strcpy(temp, "Giant Strength");
if ((bx==9)&&(by==4)&&(which==1)&&(mod==0)) strcpy(temp, "+%%100 all stats 10 turns");
strcpy(passtemp,temp);
}

void drawminimap(int scale)
{
int x, y, xx, yy, maxx;
char temp[20];
sprintf(temp,"Dungeon Level %d ",plyr.dlvl);
drawalignborder(LEFT,TOP,352+(offsetx*2),352+(offsety*2),temp);
guilocations.mapx[0]=0; guilocations.mapx[1]=352+(offsetx*2);
guilocations.mapy[0]=0; guilocations.mapy[1]=352+(offsety*2);
if (scale==8) maxx=44;
else maxx=22;
for (x=0; x<maxx; ++x)
{
xx=(plyr.x-(maxx/2))+x;
for (y=0; y<maxx; ++y)
{
yy=(plyr.y-(maxx/2))+y;
if ((xx>-1)&&(yy>-1)&&(xx<128)&&(yy<128))
{
if (dgn.explore[xx][yy]!=-1)
{
if (dmap[xx][yy].automap==0)
stretch_blit(smalltiles,screentemp,25*32,27*32,32,32,(offsetx+(x*scale)),(offsety+(y*scale))-2,scale,scale);
else if (dmap[xx][yy].automap==1)
stretch_blit(smalltiles,screentemp,26*32,27*32,32,32,(offsetx+(x*scale)),(offsety+(y*scale))-2,scale,scale);
else if (dmap[xx][yy].automap==2)
stretch_blit(smalltiles,screentemp,35*32,26*32,32,32,(offsetx+(x*scale)),(offsety+(y*scale))-2,scale,scale);
else if ((dmap[xx][yy].automap==3)&&(dgn.supx==xx)&&(dgn.supy==yy))
stretch_blit(smalltiles,screentemp,24*32,26*32,32,32,(offsetx+(x*scale)),(offsety+(y*scale))-2,scale,scale);
else if ((dmap[xx][yy].automap==3)&&(dgn.sdnx==xx)&&(dgn.sdny==yy))
stretch_blit(smalltiles,screentemp,25*32,26*32,32,32,(offsetx+(x*scale)),(offsety+(y*scale))-2,scale,scale);
if ((plyr.x==xx)&&(plyr.y==yy))
masked_stretch_blit(maskedsmalltiles,screentemp,plyr.icnx*32,plyr.icny*32,32,32,(offsetx+(x*scale)),(offsety+(y*scale))-2,scale,scale);
}
}
}
}
}

void drawfullscreenmap()
{
int x, y, xx, yy, maxx, maxy, scale, drawx, drawy;
clear_bitmap(screentemp);
scale=8;
maxx=screen->w/scale;
maxy=screen->h/scale;
drawx=(screen->w-(maxx*scale))/2;
drawy=(screen->h-(maxy*scale))/2;
for (x=0; x<maxx; ++x)
{
xx=(plyr.x-(maxx/2))+x;
for (y=0; y<maxy; ++y)
{
yy=(plyr.y-(maxy/2))+y;
if ((xx>-1)&&(yy>-1)&&(xx<128)&&(yy<128))
{
if (dgn.explore[xx][yy]!=-1)
{
if (dmap[xx][yy].automap==0)
stretch_blit(smalltiles,screentemp,25*32,27*32,32,32,(drawx+(x*scale)),(drawy+(y*scale)),scale,scale);
else if (dmap[xx][yy].automap==1)
stretch_blit(smalltiles,screentemp,26*32,27*32,32,32,(drawx+(x*scale)),(drawy+(y*scale)),scale,scale);
else if (dmap[xx][yy].automap==2)
stretch_blit(smalltiles,screentemp,35*32,26*32,32,32,(drawx+(x*scale)),(drawy+(y*scale)),scale,scale);
else if ((dmap[xx][yy].automap==3)&&(dgn.supx==xx)&&(dgn.supy==yy))
stretch_blit(smalltiles,screentemp,24*32,26*32,32,32,(drawx+(x*scale)),(drawy+(y*scale)),scale,scale);
else if ((dmap[xx][yy].automap==3)&&(dgn.sdnx==xx)&&(dgn.sdny==yy))
stretch_blit(smalltiles,screentemp,25*32,26*32,32,32,(drawx+(x*scale)),(drawy+(y*scale)),scale,scale);
if ((plyr.x==xx)&&(plyr.y==yy))
masked_stretch_blit(maskedsmalltiles,screentemp,plyr.icnx*32,plyr.icny*32,32,32,(drawx+(x*scale)),(drawy+(y*scale)),scale,scale);
}
}
}
}
textprintf_centre_ex(screentemp, largefont, (screen->w/2), 0, WHITE,  BLACK, "Dungeon Level %i",plyr.dlvl);
blitscreen();
clear_keybuf();
while ((!keypressed())&&(!(mouse_b&1))&&(!(mouse_b&2)))
{
}
if ((mouse_b&1)||(mouse_b&2))
{
while ((mouse_b&1)||(mouse_b&2))
{
}
}
}

void drawstatpane(int notused)
{
int x, y, c, z, col[4], rc, cc, rowinc, fontcolor, bgcolor, size;
char temp[40], temp2[40], strint[10];
FONT *tempfont;
infoct=0;
if (fontsize==0) {rowinc=text_height(smallfont)*0.7; tempfont=smallfont;}
else {rowinc=text_height(largefont)*0.7; tempfont=largefont;}
size=text_length(tempfont,"                                ");
if (guiborders==1)
{
fontcolor=BROWN;
size=size+(offsetx*2);
x=drawalignborder(RIGHT,TOP,size,scaley(632),"Stats ");
col[0]=x+offsetx; col[1]=x+(size/4); col[2]=x+(size/2); col[3]=col[2]+text_length(largefont,"   ");
bgcolor=-1;
}
else
{
x=screen->w-size;
fontcolor=WHITE;
bgcolor=-1;
col[0]=x; col[1]=x+(size/4); col[2]=x+(size/2); col[3]=(size/4)*3;
rectfill(screentemp,x-offsetx,0,screen->w,rowinc*24,BLUE);
}
guilocations.statpanex[0]=x;
guilocations.statpanex[1]=screen->w;
guilocations.statpaney[0]=0;
guilocations.statpaney[1]=scaley(632);
textprintf_ex(screentemp, tempfont, col[0], offsety, fontcolor, bgcolor, "Name: %s  Cl: %s",plyr.name, plyr.clname);
//Text-based stats, easier to understand I think
rowinc=text_height(tempfont);
if (rightpane==0)
{
if (plyr.lvl<99)
{
if (plyr.lvl<10) textprintf_ex(screentemp, tempfont, col[0], offsety+rowinc, fontcolor, bgcolor, "Lvl: 0%i   XP: %i (%i)", plyr.lvl, plyr.xp, plyr.xpn);
else textprintf_ex(screentemp, tempfont, col[0], offsety+rowinc, fontcolor, bgcolor, "Lvl: %i   XP: %i (%i)", plyr.lvl, plyr.xp, plyr.xpn);
}
else
textprintf_ex(screentemp, tempfont, col[0], offsety+rowinc, fontcolor, bgcolor, "Lvl: %i", plyr.lvl);
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*2), fontcolor, bgcolor, "HP: %i/%i",plyr.hp,(plyr.hpm+plyr.adjhp));
if (plyr.mpm>0) textprintf_ex(screentemp, tempfont, col[2], offsety+(rowinc*1), fontcolor, bgcolor, "MP: %i/%i",plyr.mp,(plyr.mpm+plyr.adjmp));
//textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*2), fontcolor, bgcolor, "Attributes:");
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*3), fontcolor, bgcolor, "Strength: %i",plyr.str+plyr.adjstr);
textprintf_ex(screentemp, tempfont, col[2], offsety+(rowinc*3), fontcolor, bgcolor, "Dexterity: %i",plyr.dex+plyr.adjdex);
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*4), fontcolor, bgcolor, "Intelligen: %i",plyr.itl+plyr.adjitl);
textprintf_ex(screentemp, tempfont, col[2], offsety+(rowinc*4), fontcolor, bgcolor, "Charisma: %i",plyr.chr+plyr.adjchr);
x=plyr.dmgl; y=plyr.dmgh;
if ((plyr.equipped[9]!=-1)&&(!((plyr.invmtype[plyr.equipped[9]]==1)&&(plyr.invcls[plyr.equipped[9]]==4))))
{
for (z=0; z<5; ++z)
if (plyr.invadd[plyr.equipped[9]][z]==4)
{
x=x+((x*plyr.invaddamt[plyr.equipped[9]][z])/100);
y=y+((y*plyr.invaddamt[plyr.equipped[9]][z])/100);
}
}
if ((plyr.equipped[9]!=-1)&&(plyr.invelem[plyr.equipped[9]]!=-1))
{
x=x*1.3; y=y*1.3;
}
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*5), fontcolor, bgcolor, "Dmg: %i-%i",x,y);
textprintf_ex(screentemp, tempfont, col[2], offsety+(rowinc*5), fontcolor, bgcolor, "Def: %i",plyr.deflect);
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*6), fontcolor, bgcolor, "Gold: %i",plyr.gold);
if (plyr.lockpick!=-1)
textprintf_ex(screentemp, tempfont, col[2], offsety+(rowinc*6), fontcolor, bgcolor, "Lockpicking: %i",plyr.lockpick);
rc=7;
if (plyr.trade==0)
{
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Profession- Archaeology %i",plyr.arch);
++rc;
}
if (plyr.trade==1)
{
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Profession- Scholar %i",plyr.scholar);
++rc;
}
if (plyr.trade==2)
{
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Profession- Alchemy %i",plyr.alch);
++rc;
}
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Resistances-");
++rc;
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Wind  %i   Fire  %i   Water %i",plyr.resist[0],plyr.resist[1],plyr.resist[2]);
++rc;
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Earth  %i   Shdw %i   Light %i",plyr.resist[3],plyr.resist[4],plyr.resist[5]);
++rc;
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Poison %i",plyr.resist[6]);
++rc;
c=0;
cc=0;
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Boons:");
++rc;
x=drawplayerbuffs(col[0], offsety+(rowinc*rc));
if (x!=1) rc=rc+2;
else ++rc;
textprintf_ex(screentemp, tempfont, screen->w-text_length(tempfont,"2 / 2")-offsety, scaley(572), fontcolor, bgcolor, "1 / 2");
}
if (rightpane==4)
{
if (bossflag==1) textprintf_ex(screentemp, tempfont, col[0], offsety+rowinc, fontcolor, bgcolor, "Kills: %i (%i)",plyr.kills, plyr.bkills);
else textprintf_ex(screentemp, tempfont, col[0], offsety+rowinc, fontcolor, bgcolor, "Kills: %i",plyr.kills);
textprintf_ex(screentemp, tempfont, col[2], offsety+rowinc, fontcolor, bgcolor, "Deaths: %i",plyr.deaths);
for (y=0; y<total.mons; ++y)
{
if (plyr.metsqr[tlu[y].x][tlu[y].y]==0) ++c;
}
if (c>0) z=(c*100)/total.mons;
else z=0;
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*2), fontcolor, bgcolor, "Encountered: %i/%i (%i%%)",c,total.mons,z);
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*3), fontcolor, bgcolor, "Weapon Skills-");
rc=4; cc=0;
for (x=0; x<7; ++x)
{
c=0;
for (y=0; y<3; ++y)
{
if (((x==0)||(x==5))&&(y<2))
{
if (plyr.equipmw[x][y]!=-1) c=1;
}
else if ((x==1)||(x==2)||(x==4)||(x==6))
{
if (plyr.equipmw[x][y]!=-1) c=1;
}
else if ((x==3)&&(y==0))
{
if (plyr.equipmw[x][y]!=-1) c=1;
}
}
if (c==1)
{
if (x==0) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Dagger: %i",plyr.wskill[x]);
if (x==1) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Mace: %i",plyr.wskill[x]);
if (x==2) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Sword: %i",plyr.wskill[x]);
if (x==3) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Axe: %i",plyr.wskill[x]);
if (x==4) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Spear: %i",plyr.wskill[x]);
if (x==5) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "PoleArm: %i",plyr.wskill[x]);
if (x==6) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "MartialArts: %i",plyr.wskill[x]);
cc=cc+2; if (cc==4) {cc=0; ++rc;}
}
}
for (x=0; x<6; ++x)
{
if (plyr.equipr[x]!=-1)
{
if (cc==4) {cc=0; ++rc;}
if (x==0) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Bow: %i",plyr.rwskill[x]);
if (x==1) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Crossbow: %i",plyr.rwskill[x]);
if (x==2) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Sling: %i",plyr.rwskill[x]);
if (x==3) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Darts: %i",plyr.rwskill[x]);
if (x==4) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Wand: %i",plyr.rwskill[x]);
if (x==5) textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "Shruiken: %i",plyr.rwskill[x]);
cc=cc+2;
}
}
y=0;
for (x=0; x<6; ++x)
if (plyr.magic[x]!=-1) y=1;
if (y==1)
{
++rc;
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Magic Skills-");
++rc; cc=0;
for (x=0; x<6; ++x)
{
if (plyr.magic[x]!=-1)
{
if (cc==4) {cc=0; ++rc;}
if (x==0) strcpy(temp,"Wind: ");
if (x==1) strcpy(temp,"Fire: ");
if (x==2) strcpy(temp,"Water: ");
if (x==3) strcpy(temp,"Earth: ");
if (x==4) strcpy(temp,"Shadow: ");
if (x==5) strcpy(temp,"Light: ");
textprintf_ex(screentemp, tempfont, col[cc], offsety+(rowinc*rc), fontcolor, bgcolor, "%s%i",temp,plyr.magic[x]);
cc=cc+2;
}
}
}
++rc;
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Current Mission-"); ++rc;
if (mission==0)
{
if (missiontyp==0)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d Beasts.",missioncount);
if (missiontyp==1)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d humanoids.",missioncount);
if (missiontyp==2)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d reptiles.",missioncount);
if (missiontyp==3)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d insects.",missioncount);
if (missiontyp==4)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d undead.",missioncount);
if (missiontyp==5)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d apparitions.",missioncount);
if (missiontyp==6)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d golems.",missioncount);
if (missiontyp==7)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d dragons.",missioncount);
if (missiontyp==8)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d elementals.",missioncount);
if (missiontyp==9)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d demons.",missioncount);
if (missiontyp==10)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat %d ooze.",missioncount);
if (missiontyp==11)
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Defeat a random boss monster.");
}
else if (mission==1)
{
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Open a treasure chest."); ++rc;
}
else if (mission==2)
{
textprintf_ex(screentemp, tempfont, col[0], offsety+(rowinc*rc), fontcolor, bgcolor, "Reach dungeon level %d.",plyr.llevel+1); ++rc;
}
textprintf_ex(screentemp, tempfont, screen->w-text_length(tempfont,"2 / 2")-offsety, scaley(572), fontcolor, bgcolor, "2 / 2");
}
}


void makebook(int which)
{
if (dgn.item[which]<3004)
makecommon(trade.bookx[0]+(dgn.item[which]-3000), trade.booky[0],1,-1);
else if (dgn.item[which]<3008)
makecommon(trade.bookx[1]+(dgn.item[which]-3004), trade.booky[1],1,-1);
else if (dgn.item[which]<3012)
makecommon(trade.bookx[2]+(dgn.item[which]-3008), trade.booky[2],1,-1);
else if (dgn.item[which]<3016)
makecommon(trade.bookx[3]+(dgn.item[which]-3012), trade.booky[3],1,-1);
else if (dgn.item[which]<3020)
makecommon(trade.bookx[4]+(dgn.item[which]-3016), trade.booky[4],1,-1);
else if (dgn.item[which]<3024)
makecommon(trade.bookx[5]+(dgn.item[which]-3020), trade.booky[5],1,-1);
else if (dgn.item[which]<3028)
makecommon(trade.bookx[6]+(dgn.item[which]-3024), trade.booky[6],1,-1);
else if (dgn.item[which]<3035)
makecommon((trade.bookx[0]+4), trade.booky[0]+(dgn.item[which]-3028),1,-1);
else if (dgn.item[which]<3042)
makecommon((trade.bookx[0]+5), trade.booky[0]+(dgn.item[which]-3035),1,-1);
else if (dgn.item[which]<3049)
makecommon((trade.bookx[0]+6), trade.booky[0]+(dgn.item[which]-3042),1,-1);
else if (dgn.item[which]<4000)
makecommon((trade.bookx[0]+7), trade.booky[0]+(dgn.item[which]-3049),1,-1);
tempitem.type=5;
tempitem.mtype=4;
tempitem.cls=dgn.item[which];
}

// common item copy routine for other make routines
void makecommon(int x, int y, int level, int flag)
{
int c,z;
strcpy(tempitem.name,tiledata[x][y].name);
tempitem.type=tiledata[x][y].type;
tempitem.mtype=tiledata[x][y].mtype;
tempitem.cls=tiledata[x][y].cls;
tempitem.elem=tiledata[x][y].elem;
--tempitem.elem;
tempitem.treasure=tiledata[x][y].treasure;
tempitem.magic=tiledata[x][y].magic;
tempitem.bonus=tiledata[x][y].bonus;
tempitem.icnx=x;
tempitem.icny=y;
tempitem.num=1;
if (tempitem.mtype==3) tempitem.lvl=level;
else if (flag==1) tempitem.lvl=level;
else
{
if (level < 5) tempitem.lvl=randomnum(1,5);
else if (level < 10) tempitem.lvl=randomnum(5,12);
else tempitem.lvl=(level-5)+randomnum(1,10);
}
// bonuns stats on eligible items
for (c=0; c<5; ++c)
{
tempitem.add[c]=-1;
tempitem.addamt[c]=0;
}
if (tempitem.mtype<4)
{
// chance for elemental bonus
if (tempitem.elem==-1)
{
z=randomnum(1,100);
if (((tempitem.mtype==2)&&(tempitem.cls>4))||(tempitem.mtype<2)) z=z+(tempitem.lvl*1.15);
else z=z+(tempitem.lvl*0.65);
if (z>97)
{
if ((tempitem.mtype==2)&&((tempitem.cls==1)||(tempitem.cls==7)))
tempitem.elem=randomnum(0,7);
else
tempitem.elem=randomnum(0,6);
}
}
// chance for rare quality item
if (tempitem.mtype<3)
{
z=randomnum(1,100);
if ((tempitem.mtype==2)&&(tempitem.cls>4)) z=z+(tempitem.lvl*0.75);
else z=z+(tempitem.lvl*0.40);
if ((strcmp(plyr.clname,"Nomad")==0)&&(plyr.scholar==99)) z=z+25;
if (z>105)
{
z=randomnum(1,((tempitem.lvl/24)+1));
for (c=0; c<z; ++c)
{
// Chance per property for something other than stats
// 60% chance for stats
// Only chests, shields, and weapons can spawn special properties
y=randomnum(1,10);
if ((y>6)&&((tempitem.mtype<2)||((tempitem.mtype==2)&&((tempitem.cls==1)||(tempitem.cls==7)))))
{
if ((tempitem.mtype==0)||((tempitem.mtype==1)&&(tempitem.cls!=4)))
tempitem.add[c]=randomnum(4,8);
else if ((tempitem.mtype==1)&&(tempitem.cls==4))
tempitem.add[c]=randomnum(4,6);
else
tempitem.add[c]=randomnum(4,7);
if (tempitem.add[c]==4) tempitem.addamt[c]=randomnum(5,15);
else tempitem.addamt[c]=0;
if ((tempitem.add[c]==5)&&(tempitem.mtype==2)) tempitem.addamt[c]=randomnum(5,10)+(tempitem.lvl/10);
if ((tempitem.mtype==1)&&(tempitem.cls==4)&&(tempitem.add[c]==6)) tempitem.addamt[c]=randomnum(10,20);
// Check for duplicate property, if so redo the roll
for (level=0; level<5; ++level)
if (level!=c)
{
if (tempitem.add[level]==tempitem.add[c])
{
// Duplicate found, redo this attempt
tempitem.add[c]=-1;
tempitem.addamt[c]=-1;
level=5;
--c;
}
}
}
else
{
tempitem.add[c]=randomnum(0,3);
tempitem.addamt[c]=randomnum((tempitem.lvl/8)+4,(tempitem.lvl/4)+4);
}
}
}
}
}
if ((tempitem.mtype==2)&&(tempitem.elem!=-1))
{
if (tempitem.elem==0) x=100;
else if (tempitem.elem==1) x=200;
else if (tempitem.elem==2) x=300;
else if (tempitem.elem==3) x=400;
else if (tempitem.elem==4) x=500;
else if (tempitem.elem==5) x=600;
else if (tempitem.elem==6) x=700;
else if (tempitem.elem==7) x=800;
if (tempitem.elem==7)
y=randomnum(12+(tempitem.lvl/8),22+(tempitem.lvl/8));
else
y=randomnum(25+(tempitem.lvl/4),50+(tempitem.lvl/2));
if (y>99) y=99;
tempitem.elem=x+y;
}
}


// copy shop item to temp for info panel routine
void shoptotemp(int where, int slot)
{
int c;
strcpy(tempitem.name,shop[where].name[slot]);
tempitem.type=shop[where].type[slot];
tempitem.mtype=shop[where].mtype[slot];
tempitem.cls=shop[where].cls[slot];
tempitem.elem=shop[where].elem[slot];
tempitem.treasure=shop[where].treasure[slot];
tempitem.magic=shop[where].magic[slot];
tempitem.bonus=shop[where].bonus[slot];
tempitem.icnx=shop[where].icnx[slot];
tempitem.icny=shop[where].icny[slot];
tempitem.num=shop[where].num[slot];
tempitem.lvl=shop[where].lvl[slot];
tempitem.h=shop[where].h[slot];
tempitem.l=shop[where].l[slot];
if (tempitem.mtype<4)
{
for (c=0; c<5; ++c)
{
tempitem.add[c]=shop[where].add[slot][c];
tempitem.addamt[c]=shop[where].addamt[slot][c];
}
}
}


// copy inventory item to temp for info panel routine
void invtotemp(int slot)
{
int c;
strcpy(tempitem.name,plyr.invname[slot]);
tempitem.type=plyr.invtype[slot];
tempitem.mtype=plyr.invmtype[slot];
tempitem.cls=plyr.invcls[slot];
tempitem.elem=plyr.invelem[slot];
tempitem.treasure=plyr.invtreasure[slot];
tempitem.magic=plyr.invmagic[slot];
tempitem.bonus=plyr.invbonus[slot];
tempitem.icnx=plyr.invicnx[slot];
tempitem.icny=plyr.invicny[slot];
tempitem.num=plyr.invnum[slot];
tempitem.lvl=plyr.invlvl[slot];
tempitem.h=plyr.invh[slot];
tempitem.l=plyr.invl[slot];
for (c=0; c<5; ++c)
{
tempitem.add[c]=plyr.invadd[slot][c];
tempitem.addamt[c]=plyr.invaddamt[slot][c];
}
}

// swap two inventory slots
void invswap(int target, int source)
{
int c, x;
for (x=0; x<4; ++x)
{
if (plyr.wswap[x][0]==target) plyr.wswap[x][0]=source;
else if (plyr.wswap[x][0]==source) plyr.wswap[x][0]=target;
else if (plyr.wswap[x][1]==target) plyr.wswap[x][1]=source;
else if (plyr.wswap[x][1]==source) plyr.wswap[x][1]=target;
}
for (x=0; x<12; ++x)
{
if (plyr.funcmacroy[x]==-1)
{
if (plyr.funcmacrox[x]==target) plyr.funcmacrox[x]=source;
else if (plyr.funcmacrox[x]==source) plyr.funcmacrox[x]=target;
}
}
invtotemp(target);
strcpy(plyr.invname[target],plyr.invname[source]);
plyr.invtype[target]=plyr.invtype[source];
plyr.invmtype[target]=plyr.invmtype[source];
plyr.invcls[target]=plyr.invcls[source];
plyr.invelem[target]=plyr.invelem[source];
plyr.invtreasure[target]=plyr.invtreasure[source];
plyr.invmagic[target]=plyr.invmagic[source];
plyr.invbonus[target]=plyr.invbonus[source];
plyr.invicnx[target]=plyr.invicnx[source];
plyr.invicny[target]=plyr.invicny[source];
plyr.invnum[target]=plyr.invnum[source];
plyr.invlvl[target]=plyr.invlvl[source];
plyr.invh[target]=plyr.invh[source];
plyr.invl[target]=plyr.invl[source];
for (c=0; c<5; ++c)
{
plyr.invadd[target][c]=plyr.invadd[source][c];
plyr.invaddamt[target][c]=plyr.invaddamt[source][c];
}
gotitem(source,1);
}

int makearmor(int kind, int slot, int level, int tx, int ty)
{
int x, y, z, c, pickt[50], magicseek;

// correct mistake
if (slot==4) kind=0;
if (tx==-1)
{
c=0;
for (x=(total.mons+total.melee+total.ranged); x<(total.mons+total.melee+total.ranged+total.armor); ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=level)&&(tiledata[tlu[x].x][tlu[x].y].cls==slot)
    &&(!((tiledata[tlu[x].x][tlu[x].y].cls==7)&&(tiledata[tlu[x].x][tlu[x].y].treasure==1))))
{
if ((slot<5)&&(tiledata[tlu[x].x][tlu[x].y].treasure==kind)) {pickt[c]=x; ++c;}
else if (slot>4){pickt[c]=x; ++c;}
}
}
if (c==0) {allegro_message("Make armor broken- no suitable armor found.  Exiting.  Slot %i Kind %i",slot,kind); return 1;}
y=randomnum(0,c-1);
y=pickt[y];
}
else
{
y=-1;
for (x=(total.mons+total.melee+total.ranged); x<(total.mons+total.melee+total.ranged+total.armor); ++x)
{
if ((tlu[x].x==tx)&&(tlu[x].y==ty)) y=x;
}
if (y==-1) {allegro_message("Error in passed tx/ty values to makearmor.  %i %i",tx,ty); return 1;}
}
// copy item variables into temp item
makecommon(tlu[y].x, tlu[y].y, level,-1);
if (tx==-1)
{
// check for magic properties and reassign graphic if possible
z=0;
if (slot<4)
{
if ((tempitem.elem==-1)&&(tempitem.add[0]==-1))
{
// item is not magical
if (tempitem.magic==1)
{
// but graphic is magical
magicseek=0;
z=1;
}
}
else
{
// item is magical
// if elemental doesn't match property, or if item is magical and not flagged magic, search
if (((tempitem.elem!=-1)&&((tiledata[tempitem.icnx][tempitem.icny].elem-1)!=tempitem.elem))||
    ((tempitem.elem==-1)&&(tempitem.magic==0)))
{
magicseek=1;
z=1;
}
}
}
if (z!=0)
{
c=0;
for (x=(total.mons+total.melee+total.ranged); x<(total.mons+total.melee+total.ranged+total.armor); ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=level)&&(tiledata[tlu[x].x][tlu[x].y].cls==slot)
    &&(!((tiledata[tlu[x].x][tlu[x].y].cls==7)&&(tiledata[tlu[x].x][tlu[x].y].treasure==1)))
    &&(tiledata[tlu[x].x][tlu[x].y].treasure==kind)
    &&(tempitem.elem==(tiledata[tlu[x].x][tlu[x].y].elem-1))
    &&(tiledata[tlu[x].x][tlu[x].y].magic==magicseek))
{
pickt[c]=x; ++c;
}
}
if (c==0)
{
// make a second pass, disregarding the element
c=0;
for (x=(total.mons+total.melee+total.ranged); x<(total.mons+total.melee+total.ranged+total.armor); ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=level)&&(tiledata[tlu[x].x][tlu[x].y].cls==slot)
    &&(!((tiledata[tlu[x].x][tlu[x].y].cls==7)&&(tiledata[tlu[x].x][tlu[x].y].treasure==1)))
    &&(tiledata[tlu[x].x][tlu[x].y].treasure==kind)
    &&(tiledata[tlu[x].x][tlu[x].y].magic==magicseek))
{
pickt[c]=x; ++c;
}
}
}
if (c!=0)
{
//match or matches found
y=randomnum(0,c-1);
y=pickt[y];
tempitem.icnx=tlu[y].x;
tempitem.icny=tlu[y].y;
strcpy(tempitem.name,tiledata[tempitem.icnx][tempitem.icny].name);
}
}
}
// deflect value
if ((tempitem.cls==1)&&(tempitem.treasure==0)) tempitem.h=1+(tempitem.lvl*0.20);
else if ((tempitem.cls==1)&&(tempitem.treasure==1)) tempitem.h=1+(tempitem.lvl*0.35);
else if ((tempitem.cls==1)&&(tempitem.treasure==2)) tempitem.h=1+(tempitem.lvl*0.45);
else if ((tempitem.cls==1)&&(tempitem.treasure==3)) tempitem.h=1+(tempitem.lvl*0.55);
else if ((tempitem.cls==1)&&(tempitem.treasure==4)) tempitem.h=1+(tempitem.lvl*0.65);
else if (((tempitem.cls==0)||(tempitem.cls<4))&&(tempitem.treasure==0)) tempitem.h=1+(tempitem.lvl*0.10);
else if (((tempitem.cls==0)||(tempitem.cls<4))&&(tempitem.treasure==1)) tempitem.h=1+(tempitem.lvl*0.13);
else if (((tempitem.cls==0)||(tempitem.cls<4))&&(tempitem.treasure==2)) tempitem.h=1+(tempitem.lvl*0.16);
else if (((tempitem.cls==0)||(tempitem.cls<4))&&(tempitem.treasure==3)) tempitem.h=1+(tempitem.lvl*0.19);
else if (((tempitem.cls==0)||(tempitem.cls<4))&&(tempitem.treasure==4)) tempitem.h=1+(tempitem.lvl*0.22);
else if (tempitem.cls==4) tempitem.h=1+(tempitem.lvl*0.10);
else if (tempitem.cls==7) tempitem.h=1+(tempitem.lvl*0.55);
else tempitem.h = 0;
// Name rings and necklaces
if ((tempitem.cls==5)||(tempitem.cls==6))
namejewelry();
return 0;
}

int makeweapon(int cls, int bonus, int hand, int level, int tx, int ty, int flag)
{
int x, y, z, c, pickt[30], magicseek;
// Correct errors first
// Rapier type swords are always 1 handed, and Katana type swords are always 2 handed
// Only hammer type 2H maces have matching graphics in the current tileset
// Daggers and martial arts weapons are never two handed
if ((cls==0)||(cls==6)) hand=0;
else if ((cls==4)||(cls==5)||((cls==2)&&(bonus==2))) hand=1;
else if ((cls==1)&&(hand==1)) bonus=1;
else if ((cls==2)&&(bonus==1)) hand=0;
if (tx==-1)
{
c=0;
if (cls<7)
{
for (x=total.mons; x<total.mons+total.melee; ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=level)&&(tiledata[tlu[x].x][tlu[x].y].cls==cls)&&
    (tiledata[tlu[x].x][tlu[x].y].bonus==bonus)&&(tiledata[tlu[x].x][tlu[x].y].treasure==hand)) {pickt[c]=x; ++c;}
}
}
else
{
for (x=total.mons+total.melee; x<total.mons+total.melee+total.ranged; ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=plyr.lvl)&&(tiledata[tlu[x].x][tlu[x].y].cls==cls-7)) {pickt[c]=x; ++c;}
}
}
if (c==0) {allegro_message("Make weapon broken- no suitable weapons found.  Exiting.  cls %i  bonus %i", cls, bonus); return 1;}
y=randomnum(0,c-1);
y=pickt[y];
}
else
{
y=-1;
for (x=total.mons; x<total.mons+total.melee+total.ranged; ++x)
{
if ((tlu[x].x==tx)&&(tlu[x].y==ty)) y=x;
}
if (y==-1) {allegro_message("Error in passed tx/ty values to makeweapon.  %i %i",tx,ty); return 1;}
}
// copy item variables into temp item
makecommon(tlu[y].x, tlu[y].y, level, flag);
if (tx==-1)
{
// check for magic properties and reassign graphic if possible
z=0;
if ((tempitem.elem==-1)&&(tempitem.add[0]==-1))
{
// item is not magical
if (tempitem.magic==1)
{
// but graphic is magical
magicseek=0;
z=1;
}
}
else
{
// item is magical
// if elemental doesn't match property, or if item is magical and not flagged magic, search
if (((tempitem.elem!=-1)&&((tiledata[tempitem.icnx][tempitem.icny].elem-1)!=tempitem.elem))||
    ((tempitem.elem==-1)&&(tempitem.magic==0)))
{
magicseek=1;
z=1;
}
}
if (z!=0)
{
c=0;
if (cls<7)
{
for (x=total.mons; x<total.mons+total.melee; ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=level)&&(tiledata[tlu[x].x][tlu[x].y].cls==cls)&&
    (tiledata[tlu[x].x][tlu[x].y].bonus==bonus)&&(tiledata[tlu[x].x][tlu[x].y].treasure==hand)
    &&(tempitem.elem==(tiledata[tlu[x].x][tlu[x].y].elem-1))
    &&(tiledata[tlu[x].x][tlu[x].y].magic==magicseek)) {pickt[c]=x; ++c;}
}
}
else
{
for (x=total.mons+total.melee; x<total.mons+total.melee+total.ranged; ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=plyr.lvl)&&(tiledata[tlu[x].x][tlu[x].y].cls==cls-7)
    &&(tempitem.elem==(tiledata[tlu[x].x][tlu[x].y].elem-1))
    &&(tiledata[tlu[x].x][tlu[x].y].magic==magicseek)) {pickt[c]=x; ++c;}
}
}
if ((c==0)&&(tempitem.elem==-1))
{
// make a second pass, disregarding the element, IF item has no element
if (cls<7)
{
for (x=total.mons; x<total.mons+total.melee; ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=level)&&(tiledata[tlu[x].x][tlu[x].y].cls==cls)&&
    (tiledata[tlu[x].x][tlu[x].y].bonus==bonus)&&(tiledata[tlu[x].x][tlu[x].y].treasure==hand)
    &&(tiledata[tlu[x].x][tlu[x].y].magic==magicseek)) {pickt[c]=x; ++c;}
}
}
else
{
for (x=total.mons+total.melee; x<total.mons+total.melee+total.ranged; ++x)
{
if ((tiledata[tlu[x].x][tlu[x].y].lvl<=plyr.lvl)&&(tiledata[tlu[x].x][tlu[x].y].cls==cls-7)
    &&(tiledata[tlu[x].x][tlu[x].y].magic==magicseek)) {pickt[c]=x; ++c;}
}
}
}
if (c!=0)
{
//match or matches found
y=randomnum(0,c-1);
y=pickt[y];
tempitem.icnx=tlu[y].x;
tempitem.icny=tlu[y].y;
tempitem.elem=tiledata[tempitem.icnx][tempitem.icny].elem-1;
strcpy(tempitem.name,tiledata[tempitem.icnx][tempitem.icny].name);
}
}
}
// min / max damage
if (tempitem.mtype==0)
{
if (tempitem.cls==0) {tempitem.l=2+(tempitem.lvl*0.65); tempitem.h=((tempitem.l*3)-2)+randomnum(1,4);}
else if ((tempitem.cls==1)||(tempitem.cls==6))
    {tempitem.l=3+(tempitem.lvl*0.9); tempitem.h=((tempitem.l*1.9)-1)+randomnum(1,2);
    if (tempitem.treasure==1) {tempitem.l=tempitem.l*1.4; tempitem.h=tempitem.h*1.4;}}
else if ((tempitem.cls==2)||(tempitem.cls==3))
    {tempitem.l=4+(tempitem.lvl); tempitem.h=((tempitem.l*1.8)-2)+randomnum(1,4);
    if (tempitem.treasure==1) {tempitem.l=tempitem.l*1.35; tempitem.h=tempitem.h*1.35;}}
else {tempitem.l=6+(tempitem.lvl*1.20); tempitem.h=((tempitem.l*2)-4)+randomnum(2,8);}
}
else
{
if ((tempitem.cls==0)||(tempitem.cls==1)||(tempitem.cls==4))
    {tempitem.l=4+(tempitem.lvl); tempitem.h=((tempitem.l*1.8)-2)+randomnum(1,5);}
else if (tempitem.cls==2)
    {tempitem.l=3+(tempitem.lvl*0.75); tempitem.h=((tempitem.l*2)-1)+randomnum(1,2);}
else
{
tempitem.l=2+(tempitem.lvl*0.65); tempitem.h=((tempitem.l*3)-2)+randomnum(1,4);
tempitem.num=randomnum(40,255);
}
}
return 0;
}


int rewarditem(int which, int modifier)
{
int x, y, c, z, pickx[30], picky[30];
c=0;
// Weapon that the char can use.  Always melee if below level 10.
if (which==1)
{
for (x=0; x<13; ++x)
{
for (y=0; y<3; ++y)
{
if ((x<7)&&(plyr.equipmw[x][y]!=-1)) {pickx[c]=x; picky[c]=y; ++c;}
}
if ((plyr.lvl>9)&&(x>7)&&(plyr.equipr[x-7]!=-1)) {pickx[c]=x; ++c;}
}
y=randomnum(0,c-1);
z=0;
// Assign 2h possibility where appropriate
// Rapier swords are always 1 handed, and Katana swords are always 2 handed
// Only hammer 2H maces have matching graphics in the current tileset
if ((plyr.twoh==1)&&(pickx[y]!=0)&&(pickx[y]!=6)&&(pickx[y]<4)&&
    !((pickx[y]==2)&&(picky[y]==1))) z=randomnum(0,1);
else if ((pickx[y]==4)||(pickx[y]==5)||((pickx[y]==2)&&(picky[y]==2))) z=1;
if ((z==1)&&(pickx[y]==1)) picky[y]=1;
x=makeweapon(pickx[y],picky[y],z,plyr.lvl,-1,-1,-1);
if (x==1) return 1;
}
// Chest armor
if (which==2)
{
c=0;
for (x=0; x<5; ++x)
{
if (plyr.equipa[x]!=-1) ++c;
}
if (c==1) z=0;
else z=randomnum((c-1)+modifier,c-1);
x=makearmor(z, 1, plyr.lvl,-1,-1);
if (x==1) return 1;
}
// Ring or necklace
if (which==3)
{
z=randomnum(5,6);
makearmor(0, z, plyr.lvl,-1,-1);
if (tempitem.elem==-1) tempitem.elem=randomnum(0,6);
if (tempitem.add[0]==-1) addstat(-1);
namejewelry();
if (x==1) return 1;
}
return 0;
}

void archfind()
{
int x, y, q, scrap;
char temp[40], temp2[10];
x=randomnum(0,2+(plyr.arch/49));
if (x==0)
{
addlog("You examine the area, but find nothing of note.",0);
playsound("loot2.wav");
}
else
{
x=randomnum(1,100)+(plyr.arch/10);
if (((plyr.arch<99)&&(x<(4+(plyr.arch/10))))||((plyr.arch==99)&&(x<(7+(plyr.arch/10)))))
{
//%3 chance for a equipment drop per arch loot, or %6 at 99 skill
//Equipment is either a lantern or a necklace
y=randomnum(1,2);
if (y==1)
{
y=randomnum(1,10);
if ((plyr.dlvl>79)&&(y==1))
{
makearmor(-1,-1,1,trade.lantx[1],trade.lanty[1]);
tempitem.magic=3;
tempitem.bonus=1;
}
else
{
makearmor(-1,-1,1,trade.lantx[0],trade.lanty[0]);
tempitem.magic=3;
tempitem.bonus=0;
}
tempitem.h=0;
tempitem.lvl=plyr.lvl;
for (y=0; y<5; ++y)
tempitem.add[y]=-2;
gotitem(-1,0);
}
else
{
y=1;
if (plyr.dlvl>79) y=randomnum(1,10);
makearmor(0,6,plyr.dlvl,-1,-1);
for (q=0; q<4; ++q)
{
tempitem.add[q]=q;
if (y==10) {tempitem.addamt[q]=35; strcpy(tempitem.name,"Cosmic Amulet");}
else if (plyr.dlvl>29) {tempitem.addamt[q]=25; strcpy(tempitem.name,"Stellar Amulet");}
else {tempitem.addamt[q]=12; strcpy(tempitem.name,"Starry Amulet");}
}
tempitem.add[4]=-2;
gotitem(-1,0);
}
}
else if (x<70)
{
scrap=randomnum(1,4)+(plyr.arch/20);
sprintf(temp,"Found %d scrap.",scrap);
playsound("loot2.wav");
for (y=0; y<scrap; ++y)
{
makecommon(trade.scrapx,trade.scrapy,1,-1);
tempitem.cls=4009;
gotitem(-2,0);
}
addlog(temp,0);
}
else if (x<87)
{
y=randomnum(0,2);
makecommon(trade.sigilx[y], trade.sigily[y],1,-1);
tempitem.cls=4010;
gotitem(-1,0);
}
else if (x<98)
{
y=randomnum(0,1);
makecommon(trade.docx[y], trade.docy[y],1,-1);
tempitem.cls=4011;
gotitem(-1,0);
}
else
{
y=randomnum(0,2);
makecommon(trade.fragx[y], trade.fragy[y],1,-1);
tempitem.cls=4012;
gotitem(-1,0);
}
}
}

// move temp item to player inventory
// slot puts the item in a specific slot (used by drop routine)
// slot = -1 for first available slot (or -2 for no log output)
int gotitem(int slot, int silent)
{
int x, c, z, y, got;
char message[80];
if ((plyr.invtype[99]==-1)||(slot>-1))
{
got=0;
// first check for an identical item in the case of ammo, food, gems, etc
if ((slot==-1)||(slot==-2))
{
c=0;
for (x=0; x<100; ++x)
{
if (tempitem.mtype==4)
{
if ((tempitem.mtype==plyr.invmtype[x])&&(tempitem.cls==0)&&(plyr.invcls[x]==0)
    &&(plyr.invicnx[x]==tempitem.icnx)&&(plyr.invicny[x]==tempitem.icny)
    &&(plyr.invl[x]==tempitem.l)&&(plyr.invh[x]==tempitem.h)&&(plyr.invnum[x]<999))
    c=1;
else if ((tempitem.mtype==plyr.invmtype[x])&&(tempitem.cls==2)&&(plyr.invcls[x]==2)
    &&(plyr.invelem[x]==tempitem.elem)&&(plyr.invnum[x]<999))
    c=1;
else if ((tempitem.mtype==plyr.invmtype[x])&&(tempitem.cls==3)&&(plyr.invcls[x]==3))
    c=1;
else if ((tempitem.mtype==plyr.invmtype[x])&&(tempitem.cls==4)&&(plyr.invcls[x]==4))
    c=1;
}
else if ((tempitem.mtype==3)&&(plyr.invmtype[x]==3)&&(tempitem.cls==plyr.invcls[x])&&
    (tempitem.lvl==plyr.invlvl[x])&&(tempitem.elem==plyr.invelem[x]))
         c=1;
else if ((tempitem.mtype==5)&&(plyr.invmtype[x]==5)&&(tempitem.cls==plyr.invcls[x])) c=1;
else if ((tempitem.type==5)&&(plyr.invtype[x]==5)&&(tempitem.icnx==plyr.invicnx[x])
         &&(tempitem.icny==plyr.invicny[x])) c=1;
if (c==1)
{
plyr.invnum[x]=plyr.invnum[x]+tempitem.num;
if (plyr.invnum[x]>999) plyr.invnum[x]=999;
if (slot==-1)
{
strcpy(message,"Received ");
strcat(message,tempitem.name);
strcat(message,".");
addlog(message,0);
if ((tempitem.mtype<2)||(tempitem.mtype==3)) playsound("loot1.wav");
else if ((tempitem.mtype==2)&&((tempitem.cls!=5)&&(tempitem.cls!=6))) playsound("loot1.wav");
else if ((tempitem.cls<3000)&&((tempitem.mtype==2)||(tempitem.mtype==5)||((tempitem.mtype==4)&&(tempitem.cls>2)))) playsound("lootring.wav");
else playsound("loot2.wav");
}
return 0;
}
}
}
if (got==0)
{
if (slot<0)
{
y=-1;
for (x=0; ((x<100)&&(y==-1)); ++x)
{
if (plyr.invtype[x]==-1) y=x;
}
}
else
{
y=slot;
}
// copy temp item variables into inventory array
strcpy(plyr.invname[y],tempitem.name);
plyr.invtype[y]=tempitem.type;
plyr.invmtype[y]=tempitem.mtype;
plyr.invcls[y]=tempitem.cls;
plyr.invelem[y]=tempitem.elem;
plyr.invtreasure[y]=tempitem.treasure;
plyr.invmagic[y]=tempitem.magic;
plyr.invbonus[y]=tempitem.bonus;
plyr.invicnx[y]=tempitem.icnx;
plyr.invicny[y]=tempitem.icny;
plyr.invnum[y]=tempitem.num;
plyr.invlvl[y]=tempitem.lvl;
plyr.invl[y]=tempitem.l;
plyr.invh[y]=tempitem.h;
for (c=0; c<5; ++c)
{
plyr.invadd[y][c]=tempitem.add[c];
plyr.invaddamt[y][c]=tempitem.addamt[c];
}
if (slot==-1)
{
strcpy(message,"Received ");
strcat(message,tempitem.name);
strcat(message,".");
addlog(message,0);
if ((tempitem.mtype<2)||(tempitem.mtype==3)) playsound("loot1.wav");
else if ((tempitem.mtype==2)&&((tempitem.cls!=5)&&(tempitem.cls!=6))) playsound("loot1.wav");
else if ((tempitem.cls<3000)&&((tempitem.mtype==2)||(tempitem.mtype==5)||((tempitem.mtype==4)&&(tempitem.cls>2)))) playsound("lootring.wav");
else playsound("loot2.wav");
}
}
if ((plyr.invtype[90]!=-1)&&(silent!=1)) addlog("Warning: you are running out of bag space.",1);
}
return 0;
}


// move temp item to shop inventory
void shopgot(int s, int slot)
{
int c, x;
x = getprice();
shop[s].price[slot]=x;
strcpy(shop[s].name[slot],tempitem.name);
shop[s].type[slot]=tempitem.type;
shop[s].mtype[slot]=tempitem.mtype;
shop[s].cls[slot]=tempitem.cls;
shop[s].elem[slot]=tempitem.elem;
shop[s].treasure[slot]=tempitem.treasure;
shop[s].magic[slot]=tempitem.magic;
shop[s].bonus[slot]=tempitem.bonus;
shop[s].icnx[slot]=tempitem.icnx;
shop[s].icny[slot]=tempitem.icny;
shop[s].num[slot]=tempitem.num;
shop[s].lvl[slot]=tempitem.lvl;
shop[s].l[slot]=tempitem.l;
shop[s].h[slot]=tempitem.h;
for (c=0; c<5; ++c)
{
shop[s].add[slot][c]=tempitem.add[c];
shop[s].addamt[slot][c]=tempitem.addamt[c];
}
return;
}

int getprice()
{
int x, price;
price=0;
if (tempitem.mtype==0)
{
price=30+tempitem.lvl*20;
if ((tempitem.cls>0)&&(tempitem.cls<6)) price=price+(price/3);
if (tempitem.treasure==1) price=price+100;
}
if (tempitem.mtype==1)
{
price=50+tempitem.lvl*20;
if (tempitem.cls<2) price=price+(price/2);
else if (tempitem.cls==4) price=price*2;
else price=price+(tempitem.num/2);
}
if (tempitem.mtype==2)
{
if (tempitem.treasure==0) price=30+tempitem.lvl*15;
else if (tempitem.treasure==1) price=40+tempitem.lvl*15;
else if (tempitem.treasure==2) price=45+tempitem.lvl*15;
else if (tempitem.treasure==3) price=50+tempitem.lvl*15;
else if (tempitem.treasure==4) price=60+tempitem.lvl*15;
if (tempitem.cls==1) price=price*2;
if (tempitem.cls==7) price=price*2;
}
if ((tempitem.mtype<4)&&(tempitem.elem!=-1)) price=price*2;
if (tempitem.mtype==3)
price=tempitem.num;
if ((tempitem.mtype==4)&&(tempitem.cls==0))
price=(5*((tempitem.lvl/10)+1))*tempitem.num;
if ((tempitem.mtype==4)&&(tempitem.cls==1))
price=100+(tempitem.lvl*2)*tempitem.num;
if ((tempitem.mtype==4)&&(tempitem.cls==1)&&(tempitem.treasure==2)&&(tempitem.elem==1))
price=100*tempitem.num;
if ((tempitem.mtype==4)&&(tempitem.cls==1)&&(tempitem.treasure==2)&&(tempitem.elem==0))
price=50*tempitem.num;
if ((tempitem.mtype==4)&&(tempitem.cls==1)&&(tempitem.treasure==6))
price=20000*tempitem.num;
if (tempitem.mtype<3)
{
for (x=0; x<5; ++x)
{
if (tempitem.add[x]!=-1) price=price+tempitem.addamt[x]*200;
}
}
if ((tempitem.mtype<4)&&(tempitem.elem!=-1)) price=price+(tempitem.lvl*10);
if (tempitem.mtype==5) price=120;
if (tempitem.cls>2999) price=250;
return price;
}


int loadchar()
{
char fname[24] = "Saves//"; int z, a;
z=strlen(plyr.name);
if (z>8) z=8;
strncat(fname, plyr.name, z);
strcat(fname, ".dat");
inputfile = fopen(fname,"r");
if (!inputfile) return 1;
fscanf(inputfile, "%[^\n]\n", &plyr.name);
fscanf(inputfile, "%[^\n]\n", &plyr.clname);
fscanf(inputfile, "%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i\n",
        &plyr.lvl, &plyr.xp, &plyr.xpn, &plyr.str, &plyr.dex, &plyr.itl, &plyr.chr, &plyr.spl, &plyr.dpl, &plyr.ipl, &plyr.cpl,
        &plyr.xpl, &plyr.hpl, &plyr.mpl, &plyr.hp, &plyr.mp, &plyr.hpm, &plyr.mpm, &plyr.lockpick, &plyr.kills, &plyr.deaths, &plyr.hdamage,
        &plyr.turns, &plyr.llevel, &plyr.twoh, &plyr.dextodmg, &plyr.lucky, &plyr.berserking, &plyr.crusader, &plyr.siphon, &plyr.poisonexpert);
for (z=0; z<7; ++z)
{
fscanf(inputfile,"%i,%i,",&plyr.resist[z],&plyr.wskill[z]);
}
for (z=0; z<6; ++z)
{
fscanf(inputfile,"%i,%i,%i,%i,",&plyr.magic[z],&plyr.equipa[z],&plyr.equipr[z],&plyr.rwskill[z]);
}
for (z=0; z<7; ++z)
{
for (a=0; a<3; ++a)
{
fscanf(inputfile,"%i,",&plyr.equipmw[z][a]);
}
}
fscanf(inputfile,"%i\n",&plyr.gold);
for (z=0; z<100; ++z)
{
fscanf(inputfile, "%[^\n]\n", &plyr.invname[z]);
fscanf(inputfile, "%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i\n", &plyr.invh[z], &plyr.invl[z], &plyr.invicnx[z],
        &plyr.invicny[z], &plyr.invnum[z], &plyr.invlvl[z],  &plyr.invtype[z], &plyr.invmtype[z], &plyr.invcls[z], &plyr.invelem[z],
        &plyr.invtreasure[z], &plyr.invmagic[z], &plyr.invbonus[z]);
for (a=0; a<5; ++a)
{
fscanf(inputfile,"%i,%i,",&plyr.invadd[z][a],&plyr.invaddamt[z][a]);
}
}
fscanf(inputfile, "%i,",&plyr.bkills);
for (z=0; z<5; ++z)
{
if (z<4) fscanf(inputfile, "%i,%i,%i,", &plyr.levelquest[z], &plyr.killquest[z], &plyr.questmodb[z]);
else fscanf(inputfile, "%i,%i,%i,", &plyr.levelquest[z], &plyr.killquest[z], &plyr.bestxantos);
}
for (z=0; z<64; ++z)
{
for (a=0; a<28; ++a)
{
fscanf(inputfile, "%i,", &plyr.metsqr[z][a]);
}
}
fscanf(inputfile,"%i,%i,%i,%i,%i\n",&plyr.deflect,&plyr.dmgl,&plyr.dmgh,&plyr.icnx,&plyr.icny);
for (z=0; z<11; ++z)
{
fscanf(inputfile,"%i,", &plyr.equipped[z]);
}
for (z=0; z<99; ++z)
{
fscanf(inputfile,"%i,", &plyr.savedlvl[z]);
}
fscanf(inputfile,"%i,%i,%i,%i,%i\n", &plyr.arch, &plyr.alch, &plyr.scholar, &plyr.trade, &plyr.sex);
for (z=0; z<4; ++z)
fscanf(inputfile,"%i,%i\n", &plyr.wswap[z][0], &plyr.wswap[z][1]);
for (z=0; z<12; ++z)
fscanf(inputfile,"%i,%i\n", &plyr.funcmacrox[z], &plyr.funcmacroy[z]);
fscanf(inputfile,"%i,%i,%i\n", &difficulty, &mapsize, &bossflag);
//difficulty=1; mapsize=1; bossflag=1;
fclose(inputfile);
for (z=0; z<10; ++z)
{
plyr.buffx[z]=-1; plyr.buffdur[z]=0; plyr.buffmod[z]=0;
}
equiprecalc();
// Call drawspellbook once, to populate spellbutton array.
// Otherwise the macros for spells don't work.
drawspellbook(-1,-1);
if (plyr.rwskill[0]>99) plyr.rwskill[0]=99;
if (plyr.wskill[6]>99) plyr.wskill[6]=99;
return 0;
}

int savechar()
{
char fname[20] = "Saves//"; int z, a;
z=strlen(plyr.name);
if (z>8) z=8;
strncat(fname, plyr.name, z);
strcat(fname, ".dat");
inputfile = fopen(fname,"w");
if (!inputfile) return 1;
fprintf(inputfile, "%s\n%s\n%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i\n", plyr.name,
        plyr.clname, plyr.lvl, plyr.xp, plyr.xpn, plyr.str, plyr.dex, plyr.itl, plyr.chr, plyr.spl, plyr.dpl, plyr.ipl, plyr.cpl,
        plyr.xpl, plyr.hpl, plyr.mpl, plyr.hp, plyr.mp, plyr.hpm, plyr.mpm, plyr.lockpick, plyr.kills, plyr.deaths, plyr.hdamage,
        plyr.turns, plyr.llevel, plyr.twoh, plyr.dextodmg, plyr.lucky, plyr.berserking, plyr.crusader, plyr.siphon, plyr.poisonexpert);
for (z=0; z<7; ++z)
{
fprintf(inputfile,"%i,%i,",plyr.resist[z],plyr.wskill[z]);
}
for (z=0; z<6; ++z)
{
fprintf(inputfile,"%i,%i,%i,%i,",plyr.magic[z],plyr.equipa[z],plyr.equipr[z],plyr.rwskill[z]);
}
for (z=0; z<7; ++z)
{
for (a=0; a<3; ++a)
{
fprintf(inputfile,"%i,",plyr.equipmw[z][a]);
}
}
fprintf(inputfile,"%i\n",plyr.gold);
for (z=0; z<100; ++z)
{
fprintf(inputfile, "%s\n%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i\n", plyr.invname[z], plyr.invh[z], plyr.invl[z], plyr.invicnx[z],
        plyr.invicny[z], plyr.invnum[z], plyr.invlvl[z],  plyr.invtype[z], plyr.invmtype[z], plyr.invcls[z], plyr.invelem[z],
        plyr.invtreasure[z], plyr.invmagic[z], plyr.invbonus[z]);
for (a=0; a<5; ++a)
{
fprintf(inputfile,"%i,%i,",plyr.invadd[z][a],plyr.invaddamt[z][a]);
}
}
fprintf(inputfile, "%i,",plyr.bkills);
for (z=0; z<5; ++z)
{
if (z<4) fprintf(inputfile, "%i,%i,%i,", plyr.levelquest[z], plyr.killquest[z], plyr.questmodb[z]);
else fprintf(inputfile, "%i,%i,%i,", plyr.levelquest[z], plyr.killquest[z], plyr.bestxantos);
}
for (z=0; z<64; ++z)
{
for (a=0; a<28; ++a)
{
fprintf(inputfile, "%i,", plyr.metsqr[z][a]);
}
}
fprintf(inputfile,"%i,%i,%i,%i,%i\n",plyr.deflect,plyr.dmgl,plyr.dmgh,plyr.icnx,plyr.icny);
for (z=0; z<11; ++z)
{
fprintf(inputfile,"%i,", plyr.equipped[z]);
}
for (z=0; z<99; ++z)
{
fprintf(inputfile,"%i,", plyr.savedlvl[z]);
}
fprintf(inputfile,"%i,%i,%i,%i,%i\n", plyr.arch, plyr.alch, plyr.scholar, plyr.trade, plyr.sex);
for (z=0; z<4; ++z)
fprintf(inputfile,"%i,%i\n", plyr.wswap[z][0], plyr.wswap[z][1]);
for (z=0; z<12; ++z)
fprintf(inputfile,"%i,%i\n", plyr.funcmacrox[z], plyr.funcmacroy[z]);
fprintf(inputfile,"%i,%i,%i\n", difficulty, mapsize, bossflag);
fclose(inputfile);
return 0;
}

int savedlvl()
{
char fname[20] = "Saves//"; int x, y, z;
char strint[8];
z=strlen(plyr.name);
if (z>8) z=8;
strncat(fname, plyr.name, z);
sprintf(strint,".L%d",plyr.dlvl);
strcat(fname, strint);
inputfile = fopen(fname,"w");
if (!inputfile) return 1;
for (x=0; x<128; ++x)
{
fprintf(inputfile, "%i,%i,%i,%i,%i,%i,%i\n", dgn.door[x], dgn.doorx[x], dgn.doory[x], dgn.doormn[x], dgn.doormnlvl[x], dgn.doormx[x], dgn.doormy[x]);
for (y=0; y<128; ++y)
{
fprintf(inputfile, "%i,%i,%i,%i,%i\n", dgn.icnx[x][y], dgn.icny[x][y], dgn.explore[x][y], dgn.open[x][y], dmap[x][y].automap);
}
}
for (x=0; x<256; ++x)
{
fprintf(inputfile, "%i,%i,%i\n", dgn.item[x], dgn.itemx[x], dgn.itemy[x]);
}
fprintf(inputfile, "%i,%i,%i,%i,%i\n", dgn.tset, dgn.supx, dgn.supy, dgn.sdnx, dgn.sdny);
fclose(inputfile);
return 0;
}


int loaddlvl()
{
char fname[20] = "Saves//"; int x, y, z;
char strint[5];
z=strlen(plyr.name);
if (z>8) z=8;
strncat(fname, plyr.name, z);
sprintf(strint,".L%d",plyr.dlvl);
strcat(fname, strint);
inputfile = fopen(fname,"r");
if (!inputfile) return 1;
for (x=0; x<128; ++x)
{
fscanf(inputfile, "%i,%i,%i,%i,%i,%i,%i\n", &dgn.door[x], &dgn.doorx[x], &dgn.doory[x], &dgn.doormn[x], &dgn.doormnlvl[x], &dgn.doormx[x], &dgn.doormy[x]);
for (y=0; y<128; ++y)
{
fscanf(inputfile, "%i,%i,%i,%i,%i\n", &dgn.icnx[x][y], &dgn.icny[x][y], &dgn.explore[x][y], &dgn.open[x][y], &dmap[x][y].automap);
}
}
for (x=0; x<256; ++x)
{
fscanf(inputfile, "%i,%i,%i\n", &dgn.item[x], &dgn.itemx[x], &dgn.itemy[x]);
}
fscanf(inputfile, "%i,%i,%i,%i,%i\n", &dgn.tset, &dgn.supx, &dgn.supy, &dgn.sdnx, &dgn.sdny);
fclose(inputfile);
for (x=0; x<128; ++x)
{
for (y=0; y<128; ++y)
{
dmap[x][y].trki=-1;
}
}
for (x=0; x<256; ++x)
{
if (dgn.item[x]!=-1)
dmap[dgn.itemx[x]][dgn.itemy[x]].trki=x;
}
return 0;
}


int randomnum( const int min, const int max )
{

    int range = max - min;
    if (min>max) allegro_message("Error in call to randomnum(): Min %i greater than max %i.", min, max);
    if (range==0) return min;
    else {++range; return rand() % range + min;}

}


// menu routine.  flag 1 = char creation, flag 2 = simple confirm prompt
int draw_menu(int wherex, int wherey, int options, int rows, char caption[40], int flag)
{
int sel, l, r, c, confirmed, redraw, z, width, m, bx, by;
char line1[80];
BITMAP *temp, *bordtemp;
/*
wherex
0 = left
1 = center
2 = right
wherey
0 = top
1 = center
2 = bottom
*/
if (wherex==0) bx=offsetx;
if (wherex==1) bx = (screenresx/2)-(menubord->w/2);
if (wherex==2) bx = screenresx-menubord->w;
if (wherey==0) by=offsety;
if (wherey==1) by = (screenresy/2)-(menubord->h/2);
if (wherey==2) by = screenresy - menubord->h;
if ((wherex>2)||(wherey>2))
{
allegro_message("Error: Invalid wherex or wherey passed to draw_menu. Wherex %i wherey %i",wherex,wherey);
return -1;
}
temp=create_bitmap(screenresx, screenresy);
blit(screentemp,temp,0,0,0,0,screentemp->w,screentemp->h);
sel=0; confirmed=-1; redraw=1;
width=options/rows;  if (width<2) width=2;  if (flag==2) width=2;
bordtemp = create_bitmap(((menubord->w/2)*width)+(offsetx/2), (menutemp->h*rows)+(text_height(largefont)+(offsety*7)));
stretch_blit(menubord,bordtemp,0,0,menubord->w,menubord->h,0,0,bordtemp->w,bordtemp->h);
if (flag==3) by=(screen->h/2)-bordtemp->h;
while (confirmed==-1)
{
if (redraw==1)
{
blit(temp,screentemp,0,0,0,0,screentemp->w,screentemp->h);
r=0; c=0;
show_mouse(NULL);
set_alpha_blender();
draw_trans_sprite(screentemp,bordtemp,bx,by);
if (flag==3)
{
draw_trans_sprite(screentemp,bordtemp,bx,by+bordtemp->h);
shadowtext(screen->w/2,(screenresy/2)+(text_height(largefont)),"Info:",GOLD, SCREEN, CENTER);
//shadowtext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*2),"Info:",GOLD, SCREEN, CENTER);
if (sel==0)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*2),
           "Archaeologist: Collect artifact",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "fragments from within the dungeon,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "eventually gaining access to",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "powerful and unique pieces of",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "ancient equipment.",BROWN, SCREEN, CENTER);
}
if (sel==1)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*2),
           "Scholar: Collect ancient tomes of",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "knowledge, eventally unlocking two",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "additional innate abilites specific",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "to your class.",BROWN, SCREEN, CENTER);
}
if (sel==2)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*2),
           "Alchemist: Create your own potions,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "including powerful",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "alchemist-specific potions.",BROWN, SCREEN, CENTER);
}
if (sel==3)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*2),
           "None: If you select no trade, no",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "trade items will drop in the",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "dungeon, and you'll receive more",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "normal loot.",BROWN, SCREEN, CENTER);
}
}
if (flag==1)
{
draw_trans_sprite(screentemp,bordtemp,offsetx,(screenresy/2));
shadowtext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*2),"Info:",GOLD, SCREEN, CENTER);
if (sel==0)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "Masters of physical combat, the",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "Warrior has high hitpoints (endurance)",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "and can equip any kind of weapon or",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "armor.",BROWN, SCREEN, CENTER);
}
if (sel==1)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "Holy warriors commanding the power",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "of both light and shadow, the Cleric",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "has powerful healing and damage",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "spells, and can equip blunt weapons",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*7),
           "such as maces, and wear chainmail",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*8),
           "armor.",BROWN, SCREEN, CENTER);
}
if (sel==2)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "The Mage is a master of arcane magics,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "able to wield the powers of the Fire,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "Earth, Air, and Water schools of",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "magic. The Mage relys strongly on his",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*7),
           "wits, and can only equip a few kinds",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*8),
           "of weapons and cloth armor.",BROWN, SCREEN, CENTER);
}
if (sel==3)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "The Thief relys on dexterity and",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "guile. Though he lacks the physical",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "strength of classes such as the warrior,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "the thief can land lucky strikes, has",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*7),
           "expertise in lockpicking and poison,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*8),
           "and requires less XP per level than",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*9),
           "most classes.",BROWN, SCREEN, CENTER);
}
if (sel==4)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "A warrior of the wild, the Barbarian",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "commands great physical strength and",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "a Beserker rage which increases his",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "damage in battle.  He can equip",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*7),
           "leather armor and a wide variety of",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*8),
           "weapons.",BROWN, SCREEN, CENTER);
}
if (sel==5)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "A mysterious cabal of warriors",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "from the east, the Spellblades make",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "use of both might and magic. They are",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "able to cast Fire and Water spells,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*7),
           "as well as equipping half-plate armor,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*8),
           "swords, bows, and most other kinds of",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*9),
           "weapons.",BROWN, SCREEN, CENTER);
}
if (sel==6)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "The Paladin is the ultimate holy",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "warrior. A crusader of western",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "campaigns, he is able to equip almost",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           " all types of weapons and armor,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*7),
           "receives bonus damage versus undead,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*8),
           "and can cast Light type magic spells.",BROWN, SCREEN, CENTER);
}
if (sel==7)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "The Warlock is a dark and mysterious",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "magician.  Able to command the",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "powers of the Fire, Earth, and Shadow",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "schools of magic, he can also drain",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*7),
           "the life from his enemies, and equip",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*8),
           "Scythes in addition to a few other", BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*9),
           "weapon types.", BROWN, SCREEN, CENTER);
}
if (sel==8)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "The Monk is a master of the martial",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "arts. Though they can only equip",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "cloth armor, they can often avoid",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "or absorb attacks through their",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*7),
           "powerful Chi ability. Monks are",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*8),
           "fleet of foot, and have a haste", BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*9),
           "type effect in battle.", BROWN, SCREEN, CENTER);
}
if (sel==9)
{
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*3),
           "The Nomads are desert mercenaries.",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*4),
           "Their clan passes down knowledge of",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*5),
           "the Wind and Earth magics, and are",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*6),
           "powerful fighters as well, able to",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*7),
           "equip Polearms, Spears, Swords,",BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*8),
           "Shields, and Bows, as well as Chain", BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),(screenresy/2)+(text_height(largefont)*9),
           "quality armor.", BROWN, SCREEN, CENTER);
}

}
//}
if (flag==2)
{
playsound("dialog.wav");
strcpy(line1,"");
strncat(line1, caption, options);
regulartext((bx+(menubord->w/2)),by+(offsety*1.5),line1, BROWN, SCREEN, CENTER);
regulartext((bx+(menubord->w/2)),by+(offsety*1.5)+text_height(largefont),&caption[options], BROWN, SCREEN, CENTER);
}
else regulartext((bx+(menubord->w/2)),by+(offsety*1.7),caption, BROWN, SCREEN, CENTER);
//else regulartext(bx+(((menutemp->w*width)+(offsetx*3))/2),by+(offsety*2),caption, BROWN, SCREEN, CENTER);
if (flag!=2)
{
for (l=0; l<options; ++l)
{
blit(screentemp,menutemp,bx+(c*(menutemp->w-(offsetx*2)))+(offsetx*2),by+(r*menutemp->h)+(offsety*2.3)+text_height(largefont),0,0,menutemp->w,menutemp->h);
shadowtext(menu[0]->w+(menu[0]->w/3),-2,menuopt[l],GOLD, MENU, LEFT);
set_alpha_blender();
if (sel==l) draw_trans_sprite(menutemp,menu[1],0,4);
else draw_trans_sprite(menutemp,menu[0],0,4);
blit(menutemp,screentemp,0,0,bx+(c*(menutemp->w-(offsetx*2)))+(offsetx*2),by+(r*menutemp->h)+(offsety*2.3)+text_height(largefont),menutemp->w,menutemp->h);
clickx[l][0]=bx+(c*(menutemp->w-(offsetx*2)));
clickx[l][1]=bx+(c*(menutemp->w-(offsetx*2)))+menutemp->w;
clicky[l][0]=by+(r*menutemp->h)+(offsety*2.3)+text_height(largefont);
clicky[l][1]=by+(r*menutemp->h)+(offsety*2.3)+menutemp->h+text_height(largefont);
++c; if (c==width) {c=0; ++r;}
}
}
if (flag==2) l=0;
c=offsety*2.5;
draw_button("Confirm", bx+(offsetx/2)+(buttons[0]->w)-(offsetx/8), by+(rows*menutemp->h)+menutemp->h+c+text_height(largefont), 0, 0);
clickx[l][0]=bx+(offsetx/2)+(buttons[0]->w)-(offsetx/8)-(buttons[0]->w/2);
clickx[l][1]=bx+(offsetx/2)+(buttons[0]->w)-(offsetx/8)+(buttons[0]->w/2);
clicky[l][0]=by+(rows*menutemp->h)+menutemp->h+(offsety*2.5)+text_height(largefont);
clicky[l][1]=by+(rows*menutemp->h)+menutemp->h+(offsety*2.5)+text_height(largefont)+buttons[0]->h;
draw_button("Cancel", bx+(offsetx/2)+(buttons[0]->w*2), by+(rows*menutemp->h)+menutemp->h+c+text_height(largefont), 0, 0);
clickx[l+1][0]=bx+(offsetx/2)+(buttons[0]->w*2)-(buttons[0]->w/2);
clickx[l+1][1]=bx+(offsetx/2)+(buttons[0]->w*2)+(buttons[0]->w/2);
clicky[l+1][0]=by+(rows*menutemp->h)+menutemp->h+(offsety*2.5)+text_height(largefont);
clicky[l+1][1]=by+(rows*menutemp->h)+menutemp->h+(offsety*2.5)+text_height(largefont)+buttons[0]->h;
blitscreen();
show_mouse(screen);
redraw=0;
while ((key[KEY_ENTER])||(key[KEY_ESC]))
{
}
}
if ((mouse_b&1)||((key[KEY_ENTER])||(key[KEY_ESC])))
{
z=-1;
if (flag!=2) m=options+2;
else m=2;
for (l=0; l<m; ++l)
{
if ((fetchmousex()>=clickx[l][0])&&(fetchmousex()<clickx[l][1])&&(fetchmousey()>=clicky[l][0])&&(fetchmousey()<clicky[l][1]))
{
z=l; l=m; playsound("confirm.wav");
}
}
if ((flag!=2)&&(z<options)&&(z!=-1)&&(!(key[KEY_ENTER])&&!(key[KEY_ESC])))
{
sel=z; redraw=1;
while (mouse_b&1)
{
}
}
else if ((z==m-2)||(key[KEY_ENTER]))
{
show_mouse(NULL);
draw_button("Confirm", bx+(offsetx/2)+(buttons[0]->w)-(offsetx/8), by+(rows*menutemp->h)+menutemp->h+c+text_height(largefont), 1, 0);
blitscreen();
show_mouse(screen);
while (mouse_b&1)
{
}
if (flag!=2) confirmed=sel;
else confirmed=0;
}
else if ((z==m-1)||(key[KEY_ESC]))
{
show_mouse(NULL);
draw_button("Cancel", bx+(offsetx/2)+(buttons[0]->w*2), by+(rows*menutemp->h)+menutemp->h+c+text_height(largefont), 1, 0);
blitscreen();
show_mouse(screen);
while (mouse_b&1)
{
}
if (flag!=2) confirmed=-2;
else confirmed=1;
}
}
}
destroy_bitmap(temp); destroy_bitmap(bordtemp);
return confirmed;
}

void draw_button(char *bstr, int bx, int by, int bs, int ls)
{
// ls = button size 0 = small 1 = large
// bx and by are considered to be the center point of the button
// bs is button state 0 = not pressed 1 = pressed
if (ls==1) blit(buttonl[bs], screentemp, 0, 0, bx-(buttonl[bs]->w/2), by, buttonl[bs]->w, buttonl[bs]->h);
else blit(buttons[bs], screentemp, 0, 0, bx-(buttons[bs]->w/2), by, buttons[bs]->w, buttons[bs]->h);
if (bs==1) playsound("confirm.wav");
shadowtext(bx, (by+(buttonl[bs]->h/2))-text_height(largefont)/2, bstr, GOLD, 0, 1);
}


int main(int argc, char *argv[])
{
BITMAP *title, *ccreation;
PALETTE the_palette, fontpalette;
char quit, tempstr[80], strint[10];
int x, y, c, d, e, depth, fullscreen, mx, my, z, q, automove, movedx[60], movedy[60], movedxx[60], movedyy[60],
redraw, tileset, npcx[12], npcy[12], state, dx, dy, lastmousex, lastmousey, lastinfo, lastsquarex, lastsquarey;

// define basic varaibles

quit = 0; movex = -1; movey = -1; automove=-1; redraw=1; infoct=0; bagsopen=0; iscombat=-1; cheat = -1;
lastmousex=-1; lastmousey=-1; lastinfo=-1; mx=-1; guiborders=1; fontsize=0;

rightpane=0;  dmusicpos=0; dmusictrack=randomnum(1,8); fastmode=0; minimap=0; mapscale=8;

for (x=0; x<8; ++x)
{
trade.bonesx[x]=-1; trade.bonesy[x]=-1;
if (x<7) {trade.bookx[x]=-1; trade.booky[x]=-1;}
if (x<3) {trade.sigilx[x]=-1; trade.sigily[x]=-1; trade.fragx[x]=-1; trade.fragy[x]=-1;
          runex[x]=-1; runey[x]=-1;}
if (x<2) {trade.docx[x]=-1; trade.docy[x]=-1;}
if (x<6) {chestx[x]=-1; chesty[x]=-1;}
}

trade.mossx=-1; trade.mossy=-1; trade.pearlx=-1; trade.pearly=-1; trade.nshadex=-1; trade.nshadey=-1;
trade.scrapx=-1; trade.scrapy=-1;

/*
gamescr tracks which screen is displayed
0 = title screen
1 = create new character
2 = town
3 = shop
4 = dungeon
*/

gamescr=0;

//for (x=0; x<24; ++x) strcpy(charlist[x],"none");

for (x=0; x<60; ++x)
{
for (y=0; y<60; ++y)
{
moveflag[x][y]=-1;
}
}

if (allegro_init() != 0)
  return 1;

//parse cfg
set_config_file("dungeon.cfg");
screenresx = get_config_int("screen","xres", 0);
screenresy = get_config_int("screen","yres", 0);
depth = get_config_int("screen","colordepth", 0);
fullscreen = get_config_int("screen","fullscreen", 0);
tileset = get_config_int("screen", "tileset", 0);
tilepref = get_config_int("screen", "tilerender", 0);
if ((tilepref!=32)&&(tilepref!=64)) tilepref=32;
tileres=tilepref;
playerlog = get_config_int("game", "playerlogs", 0);
saleoption = get_config_int("game", "saleoption", 0);
midi = get_config_int("sound", "music", 0);
sound = get_config_int("sound", "sound", 0);
cheat = get_config_int("cheat", "unlock", 0);
statstyle = get_config_int("game", "statpane", 0);
fastmode = get_config_int("game","fastcombat", 0);
help = get_config_int("game","help", 0);
combatzoom = get_config_int("game","combatzoom", 0);

buttonsize = 32;

if ((fastmode!=0)&&(fastmode!=1)) fastmode=0;

install_keyboard();

srand( time( NULL ) );

set_color_depth(depth);

if (fullscreen==1)
{
if (set_gfx_mode(GFX_AUTODETECT_FULLSCREEN, screenresx, screenresy, 0, 0) != 0) {
 allegro_message("Unable to set graphic mode %dx%d fullscreen.\n%s\nTry changing or reducing the resolution value in dungeon.cfg.\n", screenresx,screenresy,allegro_error);
 return 1;
}
}
else
{
if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, screenresx, screenresy, 0, 0) != 0) {
 allegro_message("Unable to set graphic mode %dx%d windowed.\n%s\nTry changing or reducing the resolution value in dungeon.cfg.\n", screenresx,screenresy,allegro_error);
 return 1;
}
}

/* install a MIDI sound driver */

if (install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, NULL) != 0)
  midi=0;

set_color_conversion(COLORCONV_KEEP_ALPHA);

paper = load_bitmap("paper.bmp", the_palette);
if (!paper) {allegro_message("Failed loading menu border bmp"); return 1;}
smalltiles=load_bitmap("tiles.bmp", the_palette);
if (tileset==1) tiles = load_bitmap("tiles64.bmp", the_palette);
else
{
tiles = create_bitmap(4096,1792);
stretch_blit(smalltiles,tiles,0,0,smalltiles->w,smalltiles->h,0,0,tiles->w,tiles->h);
}

if (screen->h>=900) bknd = load_bitmap("1280bknd.bmp", the_palette);
else
{
screentemp = load_bitmap("1280bknd.bmp", NULL);
x=scalex(screentemp->w);
y=scaley(screentemp->h);
bknd=create_bitmap(x,y);
stretch_blit(screentemp,bknd,0,0,screentemp->w,screentemp->h,0,0,x,y);
destroy_bitmap(screentemp);
}

screentemp = load_bitmap("buttonbordh.tga", NULL);
if (!screentemp) {allegro_message("Failed loading button border tga"); return 1;}
buttonbord=create_bitmap(39,39);
stretch_blit(screentemp,buttonbord,0,0,screentemp->w,screentemp->h,0,0,39,39);
destroy_bitmap(screentemp);
screentemp = load_bitmap("iconsl.bmp", the_palette);
if (!screentemp) {allegro_message("Failed loading icons bmp"); return 1;}
icons = create_bitmap(10*buttonsize,15*buttonsize);
stretch_blit(screentemp,icons,0,0,screentemp->w,screentemp->h,0,0,10*buttonsize,15*buttonsize);
destroy_bitmap(screentemp);
screentemp = load_bitmap("iconsli.bmp", the_palette);
if (!screentemp) {allegro_message("Failed loading inverse icons bmp"); return 1;}
iconsi = create_bitmap(10*buttonsize,15*buttonsize);
stretch_blit(screentemp,iconsi,0,0,screentemp->w,screentemp->h,0,0,10*buttonsize,15*buttonsize);
destroy_bitmap(screentemp);


if (!bknd) {allegro_message("Failed loading background bmp"); return 1;}
title = load_bitmap("1280title.bmp", NULL);
if (!title) {allegro_message("Failed loading title bmp"); return 1;}
ccreation = load_bitmap("1280charcreation.bmp", NULL);
if (!ccreation) {allegro_message("Failed loading char creation bmp"); return 1;}

screentemp = load_bitmap("paperdolll.tga", the_palette);
if (!screentemp) {allegro_message("Failed loading paper doll tga"); return 1;}
paperdoll = create_bitmap(screentemp->w/2,screentemp->h/2);
stretch_blit(screentemp,paperdoll,0,0,screentemp->w,screentemp->h,0,0,paperdoll->w,paperdoll->h);
destroy_bitmap(screentemp);

darken = load_bitmap("darkentile.tga", the_palette);
if (!darken) {allegro_message("Failed loading darktile tga"); return 1;}

screentemp = load_bitmap("plusminusl.tga", the_palette);
if (!screentemp) {allegro_message("Failed loading plus minus tga"); return 1;}
plusminus = create_bitmap(screentemp->w/2,screentemp->h/2);
stretch_blit(screentemp,plusminus,0,0,screentemp->w,screentemp->h,0,0,plusminus->w,plusminus->h);
destroy_bitmap(screentemp);

if (screen->h>=900)
{
menu[0] = load_bitmap("menus0.tga", NULL);
menu[1] = load_bitmap("menus1.tga", NULL);
menubord = load_bitmap("menubordl.tga", the_palette);
if (!menubord) {allegro_message("Failed loading menu border bmp"); return 1;}
}
else
{
// create scaled version of menu elements
screentemp = load_bitmap("menus0.tga", NULL);
x=scalex(screentemp->w);
y=scaley(screentemp->h);
menu[0]=create_bitmap(x,y);
stretch_blit(screentemp,menu[0],0,0,screentemp->w,screentemp->h,0,0,x,y);
destroy_bitmap(screentemp);
screentemp = load_bitmap("menus1.tga", NULL);
x=scalex(screentemp->w);
y=scaley(screentemp->h);
menu[1]=create_bitmap(x,y);
stretch_blit(screentemp,menu[1],0,0,screentemp->w,screentemp->h,0,0,x,y);
destroy_bitmap(screentemp);
screentemp = load_bitmap("menubordl.tga", NULL);
if (!screentemp) {allegro_message("Failed loading menu border bmp"); return 1;}
x=scalex(screentemp->w);
y=scaley(screentemp->h);
menubord=create_bitmap(x,y);
stretch_blit(screentemp,menubord,0,0,screentemp->w,screentemp->h,0,0,x,y);
destroy_bitmap(screentemp);
}

menutemp = create_bitmap((menubord->w/2),scaley(32));

if (screen->h>=900)
{
buttonl[1] = load_bitmap("buttonhld.bmp", NULL);
buttonl[0] = load_bitmap("buttonhlu.bmp", NULL);
buttons[1] = load_bitmap("buttonhsd.bmp", NULL);
buttons[0] = load_bitmap("buttonhsu.bmp", NULL);
}
else
{
//create scaled versions of the buttons
screentemp = load_bitmap("buttonhld.bmp", NULL);
x=scalex(screentemp->w);
y=scaley(screentemp->h);
buttonl[1]=create_bitmap(x,y);
stretch_blit(screentemp,buttonl[1],0,0,screentemp->w,screentemp->h,0,0,x,y);
destroy_bitmap(screentemp);
screentemp = load_bitmap("buttonhlu.bmp", NULL);
x=scalex(screentemp->w);
y=scaley(screentemp->h);
buttonl[0]=create_bitmap(x,y);
stretch_blit(screentemp,buttonl[0],0,0,screentemp->w,screentemp->h,0,0,x,y);
destroy_bitmap(screentemp);
screentemp = load_bitmap("buttonhsd.bmp", NULL);
x=scalex(screentemp->w);
y=scaley(screentemp->h);
buttons[1]=create_bitmap(x,y);
stretch_blit(screentemp,buttons[1],0,0,screentemp->w,screentemp->h,0,0,x,y);
destroy_bitmap(screentemp);
screentemp = load_bitmap("buttonhsu.bmp", NULL);
x=scalex(screentemp->w);
y=scaley(screentemp->h);
buttons[0]=create_bitmap(x,y);
stretch_blit(screentemp,buttons[0],0,0,screentemp->w,screentemp->h,0,0,x,y);
destroy_bitmap(screentemp);
}
for (z=0; z<2; ++z)
{
if ((!menu[z])||(!buttonl[z])||(!buttons[z]))
{allegro_message("Failed loading button element."); return 1;}
}
if (screen->h>=900) {offsetx = 38; offsety = 36;}
else {offsetx = scalex(38); offsety=scaley(36);}
tiletemp = create_bitmap(64,64);
monsterbuffer = create_bitmap(64,64);
flipbuttontemp = create_bitmap(buttonsize,buttonsize);
screentemp = create_bitmap(screenresx,screenresy);
xantosbuffer = create_bitmap(screenresx,screenresy);

if (!tiles) {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Error reading bitmap file");
  return 1;
}

x = get_config_int("screen", "font", 0);
if (x!=1)
{
largefont = load_font("font18.pcx", fontpalette, NULL);
if (largefont == NULL) {allegro_message("Unable to load font."); return 1;}
smallfont = load_font("font14.pcx", fontpalette, NULL);
if (smallfont == NULL) {allegro_message("Unable to load font."); return 1;}
}
else
{
largefont = load_font("font14.pcx", fontpalette, NULL);
if (largefont == NULL) {allegro_message("Unable to load font."); return 1;}
smallfont = load_font("font12.pcx", fontpalette, NULL);
if (smallfont == NULL) {allegro_message("Unable to load font."); return 1;}
}

// Create a copy of the tileset for masked blitting
// Substitute 255,255,255 (white)  255,0,255 in the tile file,  masked blitting
maskedtiles = create_bitmap(4096,1792);
blit(tiles, maskedtiles, 0, 0, 0, 0, 4096, 1792);
for (y = 0;y < maskedtiles->h;++y)
{
for (x = 0;x < maskedtiles->w;++x)
{
c = getpixel(maskedtiles, x, y);
if(c == makecol(255, 0, 255))
  putpixel(maskedtiles, x, y, makecol(255, 0, 255)-1);
else if(c == WHITE)
  putpixel(maskedtiles, x, y, makecol(255, 0, 255));
}
}

maskedsmalltiles = create_bitmap(2048,896);
blit(smalltiles, maskedsmalltiles, 0, 0, 0, 0, 2048,896);
for (y = 0;y < maskedsmalltiles->h;++y)
{
for (x = 0;x < maskedsmalltiles->w;++x)
{
c = getpixel(maskedsmalltiles, x, y);
if(c == makecol(255, 0, 255))
  putpixel(maskedsmalltiles, x, y, makecol(255, 0, 255)-1);
else if(c == WHITE)
  putpixel(maskedsmalltiles, x, y, makecol(255, 0, 255));
}
}

greyicons = create_bitmap(icons->w,icons->h);
blit(icons, greyicons, 0, 0, 0, 0, icons->w,icons->h);
for (y = 0;y < greyicons->h;++y)
{
for (x = 0;x < greyicons->w;++x)
{
c = getpixel(greyicons, x, y);
if(c == GREY)
  putpixel(greyicons, x, y, GREY-1);
else if(c == makecol(74,41,8))
  putpixel(greyicons, x, y, GREY);
}
}

redicons = create_bitmap(icons->w,icons->h);
blit(icons, redicons, 0, 0, 0, 0, icons->w,icons->h);
for (y = 0;y < redicons->h;++y)
{
for (x = 0;x < redicons->w;++x)
{
c = getpixel(redicons, x, y);
if(c == RED)
  putpixel(redicons, x, y, RED-1);
else if(c == makecol(74,41,8))
  putpixel(redicons, x, y, RED);
}
}

inputfile = fopen("Saves//charlist.dat","r");

  if (!inputfile) {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Can't open Saves\\charlist.dat.  Exiting.");
  return 1;}

charcount=0; strcpy(tempstr,"none");
for (x=0; x<24; ++x)
{
fscanf(inputfile, "%[^\n]\n", &charlist[x]);
if (strcmp(charlist[x],tempstr)!=0) ++charcount;
}
fclose(inputfile);

/*
// keep this routine for when I need to update the saves
clearplayervar();
for (x=0; x<charcount; ++x)
{
strcpy(plyr.name, charlist[x]);
loadchar();
savechar();
}
return 1;
*/

total.mons=0; total.melee=0; total.armor=0; total.ammo=0; total.food=0; total.potion=0;
total.scroll=0; total.gem=0; total.ranged=0;


// Load tile definitions created by Endless Editor
inputfile = fopen("tiles.dat","r");
for (y=0; y<28; ++y)
{
for (x=0; x<64; ++x)
{
fscanf(inputfile, "%[^\n]\n", &tiledata[x][y].name);
fscanf(inputfile, "%i,%i,%i,%i,%i,%i,%i,%i\n", &tiledata[x][y].type, &tiledata[x][y].mtype, &tiledata[x][y].lvl, &tiledata[x][y].cls,
        &tiledata[x][y].elem, &tiledata[x][y].treasure, &tiledata[x][y].magic, &tiledata[x][y].bonus);
// populate NPC array for drawing the town
if (tiledata[x][y].type==3) {npcx[tiledata[x][y].mtype]=x; npcy[tiledata[x][y].mtype]=y;}
// section for correcting entry mistakes.  for example, polarms or spears should always be 2 handed
if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==0)&&
    ((tiledata[x][y].cls==4)||(tiledata[x][y].cls==5))) tiledata[x][y].treasure=1;
}
}
fclose(inputfile);


for (x=0; x<5; ++x)
{
dtiles[x].walls=0; dtiles[x].floors=0; dtiles[x].doors=0;
dtiles[x].supx=-1; dtiles[x].sdnx=-1; dtiles[x].supy=-1; dtiles[x].sdny=-1;
dtiles[x].doorhidx=-1; dtiles[x].doorhidy=-1;
for (y=0; y<4; ++y)
{
dtiles[x].doorx[y]=-1;
dtiles[x].wallx[y]=-1;
dtiles[x].floorx[y]=-1;
dtiles[x].doory[y]=-1;
dtiles[x].wally[y]=-1;
dtiles[x].floory[y]=-1;
}
}


// parse tile array into tilelookup, 0 = mons  1 = weapon  2 = ranged  3 = armor,
// 4 = ammo 5 = food 6 = potion 7 = scroll 8 = gem
// also parse wall, floor, and door tiles into dtiles player icons in picn and pull
// the skeleton key portal stone and gold pile icons
// and trade icons
c=0;
for (z=0; z<9; ++z)
{
for (y=0; y<28; ++y)
{
for (x=0; x<64; ++x)
{
if (z==0)
{
if ((tiledata[x][y].type==4)&&(tiledata[x][y].mtype<4))
{
if ((tiledata[x][y].mtype==0)&&(tiledata[x][y].elem==0))
{
dtiles[tiledata[x][y].cls].floorx[0]=x;
dtiles[tiledata[x][y].cls].floory[0]=y;
++dtiles[tiledata[x][y].cls].floors;
}
if ((tiledata[x][y].mtype==0)&&(tiledata[x][y].elem==1)
    &&(dtiles[tiledata[x][y].cls].floors<4))
{
q=1; while (dtiles[tiledata[x][y].cls].floorx[q]!=-1) ++q;
dtiles[tiledata[x][y].cls].floorx[q]=x;
dtiles[tiledata[x][y].cls].floory[q]=y;
++dtiles[tiledata[x][y].cls].floors;
}
if ((tiledata[x][y].mtype==1)&&(tiledata[x][y].elem==0))
{
dtiles[tiledata[x][y].cls].wallx[0]=x;
dtiles[tiledata[x][y].cls].wally[0]=y;
++dtiles[tiledata[x][y].cls].walls;
}
if ((tiledata[x][y].mtype==1)&&(tiledata[x][y].elem==1)
    &&(dtiles[tiledata[x][y].cls].walls<4))
{
q=1; while ((q<4)&&(dtiles[tiledata[x][y].cls].wallx[q]!=-1)) ++q;
dtiles[tiledata[x][y].cls].wallx[q]=x;
dtiles[tiledata[x][y].cls].wally[q]=y;
++dtiles[tiledata[x][y].cls].walls;
}
if ((tiledata[x][y].mtype==2)&&(tiledata[x][y].elem==0)
    &&(dtiles[tiledata[x][y].cls].doors<4))
{
q=0; while ((q<4)&&(dtiles[tiledata[x][y].cls].doorx[q]!=-1)) ++q;
dtiles[tiledata[x][y].cls].doorx[q]=x;
dtiles[tiledata[x][y].cls].doory[q]=y;
++dtiles[tiledata[x][y].cls].doors;
}
if ((tiledata[x][y].mtype==2)&&(tiledata[x][y].elem==1))
{
dtiles[tiledata[x][y].cls].doorhidx=x;
dtiles[tiledata[x][y].cls].doorhidy=y;
}
if ((tiledata[x][y].mtype==3)&&(tiledata[x][y].elem==0))
{
dtiles[tiledata[x][y].cls].supx=x;
dtiles[tiledata[x][y].cls].supy=y;
}
if ((tiledata[x][y].mtype==3)&&(tiledata[x][y].elem==1))
{
dtiles[tiledata[x][y].cls].sdnx=x;
dtiles[tiledata[x][y].cls].sdny=y;
}
}
if ((tiledata[x][y].type==4)&&(tiledata[x][y].mtype==5))
{
picn[tiledata[x][y].cls][tiledata[x][y].treasure].x = x;
picn[tiledata[x][y].cls][tiledata[x][y].treasure].y = y;
}
if ((tiledata[x][y].type==4)&&(tiledata[x][y].mtype==4))
{
if (tiledata[x][y].cls==0)
{
for (q=0; ((q<5)&&(chestx[q]!=-1)); q=q+2);
}
else
{
for (q=1; ((q<6)&&(chestx[q]!=-1)); q=q+2);
}
chestx[q] = x;
chesty[q] = y;
}
if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==4)&&(tiledata[x][y].cls==3))
{
skeyx=x; skeyy=y;
}
if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==4)&&(tiledata[x][y].cls==4))
{
pstonex=x; pstoney=y;
}
if ((tiledata[x][y].type==4)&&(tiledata[x][y].mtype==6))
{
gpilex=x; gpiley=y;
}
if ((tiledata[x][y].type==4)&&(tiledata[x][y].mtype==7))
{
xantosx=x; xantosy=y;
}
if ((tiledata[x][y].type==5)&&(tiledata[x][y].mtype==0))
{
if (tiledata[x][y].cls==0)
{
for (q=0; ((q<8)&&(trade.bonesx[q]!=-1)); ++q);
trade.bonesx[q]=x; trade.bonesy[q]=y;
}
if (tiledata[x][y].cls==1) {trade.ruinsx=x; trade.ruinsy=y;}
if (tiledata[x][y].cls==2)
{
if (tiledata[x][y].elem==0)
{
for (q=0; ((q<2)&&(trade.docx[q]!=-1)); ++q);
trade.docx[q]=x; trade.docy[q]=y;
}
if (tiledata[x][y].elem==1)
{
for (q=0; ((q<3)&&(trade.fragx[q]!=-1)); ++q);
trade.fragx[q]=x; trade.fragy[q]=y;
}
if (tiledata[x][y].elem==2)
{
for (q=0; ((q<3)&&(trade.sigilx[q]!=-1)); ++q);
trade.sigilx[q]=x; trade.sigily[q]=y;
}
if (tiledata[x][y].elem==3) {trade.scrapx=x; trade.scrapy=y;}
}
}
if ((tiledata[x][y].type==5)&&(tiledata[x][y].mtype==1))
{
for (q=0; ((q<7)&&(trade.bookx[q]!=-1)); ++q);
trade.bookx[q]=x; trade.booky[q]=y;
}
if ((tiledata[x][y].type==5)&&(tiledata[x][y].mtype==2))
{
if (tiledata[x][y].cls==0) {trade.nshadex=x; trade.nshadey=y;}
if (tiledata[x][y].cls==1) {trade.mossx=x; trade.mossy=y;}
if (tiledata[x][y].cls==2) {trade.pearlx=x; trade.pearly=y;}
}
if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==2)&&(tiledata[x][y].cls==7)&&(tiledata[x][y].treasure==1))
{
if (tiledata[x][y].lvl==1) {trade.lantx[0]=x; trade.lanty[0]=y;}
else {trade.lantx[1]=x; trade.lanty[1]=y;}
}
if ((tiledata[x][y].type==4)&&(tiledata[x][y].mtype==8))
{
runex[tiledata[x][y].cls]=x; runey[tiledata[x][y].cls]=y;
}
if ((tiledata[x][y].type==4)&&(tiledata[x][y].mtype==9))
{
sfieldx[tiledata[x][y].cls]=x; sfieldy[tiledata[x][y].cls]=y;
}
}
if ((z==0)&&(tiledata[x][y].type==1))
{
tlu[c].x=x; tlu[c].y=y;
++total.mons; ++c;
}
if ((z==1)&&(tiledata[x][y].type==2)&&(tiledata[x][y].mtype==0))
{
tlu[c].x=x; tlu[c].y=y;
++total.melee; ++c;
}
if ((z==2)&&(tiledata[x][y].type==2)&&(tiledata[x][y].mtype==1))
{
tlu[c].x=x; tlu[c].y=y;
++total.ranged; ++c;
}
if ((z==3)&&(tiledata[x][y].type==2)&&(tiledata[x][y].mtype==2))
{
tlu[c].x=x; tlu[c].y=y;
++total.armor; ++c;
}
if ((z==4)&&(tiledata[x][y].type==2)&&(tiledata[x][y].mtype==3))
{
tlu[c].x=x; tlu[c].y=y;
++total.ammo; ++c;
}
if ((z==5)&&(tiledata[x][y].type==2)&&(tiledata[x][y].mtype==4)&&(tiledata[x][y].cls==0))
{
tlu[c].x=x; tlu[c].y=y;
++total.food; ++c;
}
if ((z==6)&&(tiledata[x][y].type==2)&&(tiledata[x][y].mtype==4)&&(tiledata[x][y].cls==1))
{
tlu[c].x=x; tlu[c].y=y;
++total.potion; ++c;
}
if ((z==7)&&(tiledata[x][y].type==2)&&(tiledata[x][y].mtype==4)&&(tiledata[x][y].cls==2))
{
tlu[c].x=x; tlu[c].y=y;
++total.scroll; ++c;
}
if ((z==8)&&(tiledata[x][y].type==2)&&(tiledata[x][y].mtype==5))
{
tlu[c].x=x; tlu[c].y=y;
++total.gem; ++c;
}
}
}
}

// fill empty door-wall slots in dtiles with icons from other tilesets
// the base tileset must always contain 4 walls and 4 doors!

for (x=0; x<5; ++x)
{
for (y=0; y<4; ++y)
{
if (dtiles[x].doorx[y]==-1)
{dtiles[x].doorx[y]=dtiles[0].doorx[y]; dtiles[x].doory[y]=dtiles[0].doory[y];
++dtiles[x].doors;}
if (dtiles[x].wallx[y]==-1)
{dtiles[x].wallx[y]=dtiles[0].wallx[y]; dtiles[x].wally[y]=dtiles[0].wally[y];
++dtiles[x].walls;}
}
if (dtiles[x].floorx[0]==-1)
{dtiles[x].floorx[0]=dtiles[0].floorx[0]; dtiles[x].floory[0]=dtiles[0].floory[0];}
if (dtiles[x].supx==-1) {dtiles[x].supx=dtiles[0].supx; dtiles[x].supy=dtiles[0].supy;}
if (dtiles[x].sdnx==-1) {dtiles[x].sdnx=dtiles[0].sdnx; dtiles[x].sdny=dtiles[0].sdny;}
if (dtiles[x].doorhidx==-1) {dtiles[x].doorhidx=dtiles[0].doorhidx; dtiles[x].doorhidy=dtiles[0].doorhidy;}
}




for (x=0; x<40; ++x)
{
strcpy(plyrlog[x], "");
}
logc=0;
tempitem.l=0; tempitem.h=0; tempitem.type=0; tempitem.mtype=0; tempitem.cls=0; tempitem.bonus=0; tempitem.treasure=0; tempitem.magic=0;
tempitem.num=0; tempitem.elem=-1; tempitem.icnx=-1; tempitem.icny=-1; tempitem.lvl=0; strcpy(tempitem.name,"");
for (x=0; x<5; ++x) {tempitem.add[x]=-1; tempitem.addamt[x]=-1;}

//allegro_message("drawx=%d drawy=%d tiledraw.cx=%d tiledraw.cy=%d tiledraw.sx=%d tiledraw.sy=%d",drawx,drawy,tiledraw.cx,tiledraw.cy,tiledraw.sx,tiledraw.sy);

install_mouse();
install_timer();
set_alpha_blender();
state=0;
addlog("Welcome to Endless Dungeons 1.30d!",1);

// use d for tracking passage from right to left side of screen
// 0 = left 1 = right
d=-1;

// Main game loop
while (quit!=1)
{
if (gamescr==0)
{
clear_bitmap(screentemp);
playmusic("title.mid",-1);
stretch_blit(title,screentemp,0,0,title->w,title->h,((screen->w/2)-(((screenresy/3)*4)/2)),0,((screenresy/3)*4),screenresy);
if (state==1) {shadowtext((screenresx/2), screentemp->h/2-buttonl[0]->h-text_height(largefont), tempstr, WHITE,0, 1);}
draw_button("New Game",(screenresx/2),screentemp->h/2-buttonl[0]->h,0,1);
draw_button("Continue",(screenresx/2),screentemp->h/2,0,1);
draw_button("About",(screenresx/2),screentemp->h/2+buttonl[0]->h,0,1);
draw_button("Exit Game",(screenresx/2),screentemp->h/2+(buttonl[0]->h*2),0,1);
blitscreen();
for (x=0; x<4; ++x)
{
clickx[x][0]=(screenresx/2)-(buttonl[0]->w/2);
clickx[x][1]=(screenresx/2)+(buttonl[0]->w/2);
}
for (x=0; x<4; ++x)
{
clicky[x][0]=(screenresy/2)+((x-1)*buttonl[0]->h);
clicky[x][1]=(screenresy/2)+((1+(x-1))*buttonl[0]->h);
}
while (!(mouse_b & 1))
{
}
if ((fetchmousex()>=clickx[0][0])&&(fetchmousex()<clickx[0][1])&&(fetchmousey()>clicky[0][0])&&(fetchmousey()<clicky[0][1]))
{
draw_button("New Game",(screenresx/2),screentemp->h/2-buttonl[0]->h,1,1);
blitscreen();
while (mouse_b&1)
{
}
if (charcount==24)
{
strcpy(tempstr, "Can't create any more characters.  Delete one first.");
state=1;
}
else gamescr=1;
}
else if ((fetchmousex()>=clickx[1][0])&&(fetchmousex()<clickx[1][1])&&(fetchmousey()>clicky[1][0])&&(fetchmousey()<clicky[1][1]))
{
draw_button("Continue",(screenresx/2),screentemp->h/2,1,1);
blitscreen();
while (mouse_b&1)
{
}
if (charcount==0) {state=1; strcpy(tempstr, "No characters found.  Create one first.");}
else
{
for (x=0; x<charcount; ++x) strcpy(menuopt[x],charlist[x]);
if (charcount==1) x=0;
else
{
z=((charcount+1)/2);
x=draw_menu(CENTER, CENTER, charcount, z,"Select Character",0);
}
if (x!=-2)
{
strcpy(plyr.name, charlist[x]);
clearplayervar();
x=loadchar();
infoct=0;
if (x==1) {allegro_message("Problem loading char.  Exiting."); return 1;}
x=randshops();
if (x==1) {allegro_message("Problem randomizing shops.  Exiting."); return 1;}
if (plyr.hp==0)
{
x=resurrectoption();
if (x==1)
gamescr=2;
rightpane=0;
}
else
{
mission=-1;
gamescr=2;
rightpane=0;
}
}
}
}
else if ((fetchmousex()>=clickx[2][0])&&(fetchmousex()<clickx[2][1])&&(fetchmousey()>clicky[2][0])&&(fetchmousey()<clicky[2][1]))
{
blit(title, screentemp, 0, 0, (screentemp->w-title->w)/2, (screentemp->h-title->h)/2, title->w, title->h);
rectfill(screentemp,0,((screentemp->h/6)*1),screentemp->w,((screentemp->h/6)*5),BLACK); z=(screentemp->h/5);
//else {clear_bitmap(screentemp); z=(screentemp->h/8);}
show_mouse(NULL);
playmusic("credits.mid",-1);
//title crawl
q=-1;
for (x=0; x<15; ++x)
{
for (y=25; y<265; y=y+10)
{
if (key[KEY_ESC]) {q=0;}
if (q==0) y=255;
if (x==0)
regulartext((screenresx/2),z+(x*text_height(largefont)),"The Endless Dungeons.  Source of power and mystery for ages untold.",makecol(y,y,y),SCREEN,CENTER);
if (x==1)
regulartext((screenresx/2),z+(x*text_height(largefont)),"In the unassuming town of Velia lies it's entrance.  It is said that",makecol(y,y,y),SCREEN,CENTER);
if (x==2)
regulartext((screenresx/2),z+(x*text_height(largefont)),"at the bottom dwells Lord Xantos, who is leading the infernal creatures",makecol(y,y,y),SCREEN,CENTER);
if (x==3)
regulartext((screenresx/2),z+(x*text_height(largefont)),"therein in a campaign against the human realms.  Dare you venture into",makecol(y,y,y),SCREEN,CENTER);
if (x==4)
regulartext((screenresx/2),z+(x*text_height(largefont)),"it's depths, and make your way through 99 levels of winding and deadly",makecol(y,y,y),SCREEN,CENTER);
if (x==5)
regulartext((screenresx/2),z+(x*text_height(largefont)),"labyrinths to try and face him?",makecol(y,y,y),SCREEN,CENTER);
if (x==6)
regulartext((screenresx/2),z+(x*text_height(largefont)),"",makecol(y,y,y),SCREEN,CENTER);
if (x==7)
regulartext((screenresx/2),z+(x*text_height(largefont)),"The Endless Dungeons - an 8-bit style random dungeon game",makecol(y,y,y),SCREEN,CENTER);
if (x==8)
regulartext((screenresx/2),z+(x*text_height(largefont)),"by Litmus Dragon (Brian Conley)",makecol(y,y,y),SCREEN,CENTER);
if (x==9)
regulartext((screenresx/2),z+(x*text_height(largefont)),"Coded using Code::Blocks, MinGW, and Allegro 4.4.2",makecol(y,y,y),SCREEN,CENTER);
if (x==10)
regulartext((screenresx/2),z+(x*text_height(largefont)),"Tileset graphics by David Gervais",makecol(y,y,y),SCREEN,CENTER);
if (x==11)
regulartext((screenresx/2),z+(x*text_height(largefont)),"Boon and other icons from game-icons.net",makecol(y,y,y),SCREEN,CENTER);
if (x==12)
regulartext((screenresx/2),z+(x*text_height(largefont)),"Many other sound/gfx from Opengameart.org",makecol(y,y,y),SCREEN,CENTER);
if (x==13)
regulartext((screenresx/2),z+(x*text_height(largefont)),"See documentation for a complete list of attributions",makecol(y,y,y),SCREEN,CENTER);
if (x==14)
regulartext((screenresx/2),z+(x*text_height(largefont)),"Thanks to the open source community for making this possible!",makecol(y,y,y),SCREEN,CENTER);
//regulartext(offsetx,offsety+(x*text_height(largefont)),tempstr,makecol(y,y,y),SCREEN,CENTER);
rest(50);
//blitscreen();
blitscreen();
}
}
draw_button("BACK",screentemp->w-buttonl[0]->w,screentemp->h-(buttonl[0]->h*1.5),0,1);
blitscreen();
show_mouse(screen);
while ((!(mouse_b & 1))&&(!(mouse_b & 2)))
{
}
z=-1;
while (z==-1)
{
if (mouse_b&2)
{
while (mouse_b&2)
{
}
z=1;
}
if ((mouse_b&1)&&(fetchmousex()>screentemp->w-(buttonl[0]->w*1.5))
    &&(fetchmousex()<screentemp->w-(buttonl[0]->w*0.5))
    &&(fetchmousey()>screentemp->h-(buttonl[0]->h*1.5))
    &&(fetchmousey()<screentemp->h-(buttonl[0]->h*0.5)))
{
draw_button("BACK",screentemp->w-buttonl[0]->w,screentemp->h-(buttonl[0]->h*1.5),1,1);
blitscreen();
while (mouse_b&1)
{
}
z=1;
}
}
}
else if ((fetchmousex()>=clickx[3][0])&&(fetchmousex()<clickx[3][1])&&(fetchmousey()>clicky[3][0])&&(fetchmousey()<clicky[3][1]))
{
draw_button("Exit Game",(screenresx/2),screentemp->h/2+(buttonl[0]->h*2),1,1);
blitscreen();
while (mouse_b&1)
{
}
quit=1;
}
}
if (gamescr==1)
{
// character creation
playmusic("hall.mid",-1);
show_mouse(NULL);
c=0;
clear_bitmap(screentemp);
stretch_blit(ccreation,screentemp,0,0,ccreation->w,ccreation->h,((screen->w/2)-(((screenresy/3)*4)/2)),0,((screenresy/3)*4),screenresy);
blit(ccreation,screentemp,0,0,(screentemp->w-ccreation->w)/2,(screentemp->h-ccreation->h)/2,ccreation->w,ccreation->h);
blitscreen();
rest(2000);
textprintf_ex(screentemp, largefont, offsetx, offsety, WHITE, 0, "Create a New Character");
strcpy(menuopt[0], "Warrior");
strcpy(menuopt[1], "Cleric");
strcpy(menuopt[2], "Mage");
strcpy(menuopt[3], "Thief");
strcpy(menuopt[4], "Barbarian");
strcpy(menuopt[5], "Spellblade");
strcpy(menuopt[6], "Paladin");
strcpy(menuopt[7], "Warlock");
strcpy(menuopt[8], "Monk");
strcpy(menuopt[9], "Nomad");
//draw_trans_sprite(screentemp,bordtemp,bx-20,by-offsety+bordtemp->h);
c = draw_menu(LEFT, TOP, 10, 5,"Choose a Class",1);
clearplayervar();
if (c==-2) {gamescr=0; state=0;}
else
{
strcpy(plyr.clname, menuopt[c]);
blit(ccreation,screentemp,0,0,(screentemp->w-ccreation->w)/2,(screentemp->h-ccreation->h)/2,ccreation->w,ccreation->h);
strcpy(menuopt[0], "Male");
strcpy(menuopt[1], "Female");
x = draw_menu(LEFT, TOP, 2, 1,"Choose Sex",0);
if (x==-2) {gamescr=0; state=0;}
else
{
plyr.sex=x;
// class confirmed, fetch class stats
plyr.icnx=picn[c][x].x;
plyr.icny=picn[c][x].y;
difficulty = 1; mapsize = 1; bossflag = 1;
if (c==0)
{
plyr.spl=5; plyr.dpl=4; plyr.ipl=1; plyr.cpl=1; plyr.hpl=15; plyr.mpl=0; plyr.twoh=1;
for (x=0; x<7; ++x)
{
if (x<6) {plyr.equipa[x]=1; plyr.equipr[x]=1;}
for (y=0; y<3; ++y)
{
if (((x==0)||(x==5))&&(y<2)) plyr.equipmw[x][y]=1;
if ((x==1)||(x==2)||(x==4)||(x==6)) plyr.equipmw[x][y]=1;
if ((x==3)&&(y==0)) plyr.equipmw[x][y]=1;
}
}
}
else if (c==1)
{
plyr.spl=2; plyr.dpl=2; plyr.ipl=5; plyr.cpl=2; plyr.hpl=15; plyr.mpl=5;
plyr.equipmw[1][0]=1; plyr.equipmw[1][1]=1; plyr.equipmw[1][2]=1;
plyr.equipmw[6][1]=1; plyr.equipmw[6][2]=1; plyr.equipr[2]=1;
for (x=0; x<3; ++x) plyr.equipa[x]=1; plyr.equipa[5]=1;
plyr.magic[4]=1; plyr.magic[5]=1;
}
else if (c==2)
{
plyr.spl=1; plyr.dpl=1; plyr.ipl=6; plyr.cpl=3; plyr.hpl=8; plyr.mpl=12;
plyr.equipmw[0][0]=1; plyr.equipmw[0][1]=1; plyr.equipmw[6][2]=1;
plyr.equipr[3]=1; plyr.equipr[4]=1;
plyr.equipa[0]=1;
for (x=0; x<4; ++x) plyr.magic[x]=1;
}
else if (c==3)
{
plyr.spl=1; plyr.dpl=8; plyr.ipl=2; plyr.cpl=3; plyr.hpl=10; plyr.mpl=0;
plyr.equipmw[0][0]=1; plyr.equipmw[0][1]=1; plyr.equipmw[2][0]=1; plyr.equipmw[2][1]=1;
plyr.equipmw[6][0]=1; plyr.equipmw[6][1]=1;
plyr.equipr[1]=1; plyr.equipr[5]=1;
plyr.equipa[0]=1; plyr.equipa[1]=1;
plyr.dextodmg=1; plyr.lucky=1; plyr.poisonexpert=1; plyr.lockpick=1;
}
else if (c==4)
{
plyr.spl=6; plyr.dpl=3; plyr.ipl=1; plyr.cpl=1; plyr.hpl=20; plyr.mpl=0; plyr.twoh=1;
for (x=0; x<6; ++x)
{
for (y=0; y<3; ++y)
{
if (((x==0)||(x==5))&&(y<2)) plyr.equipmw[x][y]=1;
if ((x==1)||(x==2)||(x==4)||(x==6)) plyr.equipmw[x][y]=1;
if ((x==3)&&(y==0)) plyr.equipmw[x][y]=1;
}
}
plyr.equipa[0]=1; plyr.equipa[1]=1;
plyr.berserking=1;
}
else if (c==5)
{
plyr.spl=3; plyr.dpl=3; plyr.ipl=3; plyr.cpl=3; plyr.hpl=14; plyr.mpl=6; plyr.twoh=1;
plyr.equipmw[0][0]=1; plyr.equipmw[0][1]=1; plyr.equipmw[1][0]=1; plyr.equipmw[1][1]=1;
plyr.equipmw[1][2]=1; plyr.equipmw[2][0]=1; plyr.equipmw[2][1]=1; plyr.equipmw[2][2]=1;
plyr.equipmw[4][0]=1; plyr.equipmw[4][1]=1; plyr.equipmw[4][2]=1;
plyr.equipr[0]=1; plyr.equipr[1]=1;
for (x=0; x<4; ++x) plyr.equipa[x]=1; plyr.equipa[5]=1;
plyr.magic[1]=1; plyr.magic[2]=1;
}
else if (c==6)
{
plyr.spl=4; plyr.dpl=2; plyr.ipl=2; plyr.cpl=3; plyr.hpl=16; plyr.mpl=4; plyr.twoh=1;
for (x=0; x<6; ++x)
{
for (y=0; y<3; ++y)
{
if (((x==0)||(x==5))&&(y<2)) plyr.equipmw[x][y]=1;
if ((x==1)||(x==2)||(x==4)||(x==6)) plyr.equipmw[x][y]=1;
if ((x==3)&&(y==0)) plyr.equipmw[x][y]=1;
}
}
plyr.equipr[0]=1; plyr.equipr[1]=1;
for (x=0; x<6; ++x) plyr.equipa[x]=1;
plyr.magic[5]=1;
plyr.crusader=1;
}
else if (c==7)
{
plyr.spl=2; plyr.dpl=1; plyr.ipl=5; plyr.cpl=3; plyr.hpl=10; plyr.mpl=10; plyr.twoh=1;
plyr.equipmw[5][1]=1; plyr.equipmw[0][0]=1; plyr.equipmw[0][1]=1; plyr.equipmw[6][0]=1;
plyr.equipmw[6][2]=1;
plyr.equipr[3]=1; plyr.equipr[4]=1;
plyr.equipa[0]=1;
plyr.magic[1]=1; plyr.magic[3]=1; plyr.magic[4]=1;
plyr.siphon=1;
}
else if (c==8)
{
plyr.spl=3; plyr.dpl=8; plyr.ipl=3; plyr.cpl=3; plyr.hpl=10; plyr.mpl=0;
plyr.equipmw[6][0]=1; plyr.equipmw[6][1]=1; plyr.equipmw[6][2]=1;
plyr.equipa[0]=1;
plyr.dextodmg=1;
}
else if (c==9)
{
plyr.spl=4; plyr.dpl=3; plyr.ipl=3; plyr.cpl=2; plyr.hpl=14; plyr.mpl=6; plyr.twoh=1;
plyr.equipmw[2][0]=1; plyr.equipmw[2][1]=1; plyr.equipmw[2][2]=1;
plyr.equipmw[3][0]=1;
plyr.equipmw[4][0]=1; plyr.equipmw[4][1]=1; plyr.equipmw[4][2]=1;
plyr.equipmw[5][0]=1; plyr.equipmw[5][1]=1;
plyr.equipr[0]=1; plyr.equipr[1]=1;
for (x=0; x<3; ++x) plyr.equipa[x]=1; plyr.equipa[5]=1;
plyr.magic[0]=1; plyr.magic[3]=1;
}
// Total player abilities and calculate XP needed for level 2, from which all other levels are based
plyr.xpl=0;
// First look at attributes per level.  Fist 10 are free, every point over that adds 10 xp per level
plyr.xpl = ((plyr.spl+plyr.dpl+plyr.ipl+plyr.cpl)-10)*10;
// Next look at hitpoints per level + magic points per level.  First 10 free, every pont over 10 adds 5 xp
plyr.xpl = plyr.xpl + ((plyr.hpl+plyr.mpl)-10)*5;
// Now look at weapons.  These are weighted different so are done individually.
if (plyr.equipmw[0][0]!=-1) plyr.xpl = plyr.xpl+3;
if (plyr.equipmw[0][1]!=-1) plyr.xpl = plyr.xpl+3;
if (plyr.equipmw[1][0]!=-1) plyr.xpl = plyr.xpl+4;
if (plyr.equipmw[1][1]!=-1) plyr.xpl = plyr.xpl+4;
if (plyr.equipmw[1][2]!=-1) plyr.xpl = plyr.xpl+4;
if (plyr.equipmw[2][0]!=-1) plyr.xpl = plyr.xpl+5;
if (plyr.equipmw[2][1]!=-1) plyr.xpl = plyr.xpl+5;
if (plyr.equipmw[2][2]!=-1) plyr.xpl = plyr.xpl+5;
if (plyr.equipmw[3][0]!=-1) plyr.xpl = plyr.xpl+5;
if (plyr.equipmw[4][0]!=-1) plyr.xpl = plyr.xpl+5;
if (plyr.equipmw[4][1]!=-1) plyr.xpl = plyr.xpl+5;
if (plyr.equipmw[4][2]!=-1) plyr.xpl = plyr.xpl+5;
if (plyr.equipmw[5][0]!=-1) plyr.xpl = plyr.xpl+5;
if (plyr.equipmw[5][1]!=-1) plyr.xpl = plyr.xpl+5;
if (plyr.equipmw[6][0]!=-1) plyr.xpl = plyr.xpl+4;
if (plyr.equipmw[6][1]!=-1) plyr.xpl = plyr.xpl+4;
if (plyr.equipmw[6][2]!=-1) plyr.xpl = plyr.xpl+4;
// Armor.  Each class of armor (including shield) is worth 10 pts.
for (x=0; x<6; ++x) if (plyr.equipa[x]==1) plyr.xpl = plyr.xpl + 10;
// Ranged weapons.
if (plyr.equipr[0]!=-1) plyr.xpl=plyr.xpl+10;
if (plyr.equipr[1]!=-1) plyr.xpl=plyr.xpl+10;
if (plyr.equipr[2]!=-1) plyr.xpl=plyr.xpl+4;
if (plyr.equipr[3]!=-1) plyr.xpl=plyr.xpl+4;
if (plyr.equipr[4]!=-1) plyr.xpl=plyr.xpl+10;
if (plyr.equipr[5]!=-1) plyr.xpl=plyr.xpl+4;
// Magic.  Each school of magic available to the class adds 30 points.
for (x=0; x<6; ++x) if (plyr.magic[x]!=-1) plyr.xpl = plyr.xpl + 30;
// Special abilities values vary
if (plyr.berserking!=-1) plyr.xpl = plyr.xpl +20;
if (plyr.lockpick!=-1) plyr.xpl = plyr.xpl +30;
if (plyr.twoh!=-1) plyr.xpl = plyr.xpl +10;
if (plyr.dextodmg!=-1) plyr.xpl = plyr.xpl +15;
if (strcmp(plyr.clname,"Monk")==0) plyr.xpl = plyr.xpl + 90;
if (plyr.lucky!=-1) plyr.xpl = plyr.xpl +15;
if (plyr.crusader!=-1) plyr.xpl = plyr.xpl +10;
if (plyr.siphon!=-1) plyr.xpl = plyr.xpl +10;
if (plyr.poisonexpert!=-1) plyr.xpl = plyr.xpl +10;
plyr.xpn = plyr.xpl;
c=0;
while (c!=1)
{
clear_bitmap(screentemp);
blit(ccreation,screentemp,0,0,(screentemp->w-ccreation->w)/2,(screentemp->h-ccreation->h)/2,ccreation->w,ccreation->h);
textprintf_ex(screentemp, largefont, offsetx, offsety, WHITE, 0, "Create a New Character");
textprintf_ex(screentemp, largefont, offsetx, offsety+text_height(largefont)*2, WHITE, 0, "Choose a Class");
textprintf_ex(screentemp, largefont, offsetx, offsety+text_height(largefont)*3, WHITE, 0, "%s", plyr.clname);
textprintf_ex(screentemp, largefont, offsetx, offsety+text_height(largefont)*5, WHITE, 0, "Roll for stats:");
plyr.str=8+randomnum(plyr.spl, (plyr.spl*2)+2);
plyr.itl=8+randomnum(plyr.ipl, (plyr.ipl*2)+2);
plyr.dex=8+randomnum(plyr.dpl, (plyr.dpl*2)+2);
plyr.chr=8+randomnum(plyr.cpl, (plyr.cpl*2)+2);
plyr.hp=(plyr.hpl*2)+randomnum(1, plyr.hpl+2); plyr.hpm=plyr.hp;
if (plyr.mpl>0) {plyr.mp=(plyr.mpl*2)+randomnum(1, plyr.mpl+2); plyr.mpm=plyr.mp;}
else plyr.mp=0;
textprintf_ex(screentemp, largefont, offsetx, offsety+text_height(largefont)*7, WHITE, 0,
              "Strength: %i", plyr.str);
textprintf_ex(  screentemp, largefont, offsetx, offsety+text_height(largefont)*8, WHITE, 0,
              "Dexterity: %i", plyr.dex);
textprintf_ex(screentemp, largefont, offsetx, offsety+text_height(largefont)*9, WHITE, 0,
              "Intelligence: %i", plyr.itl);
textprintf_ex(screentemp, largefont, offsetx, offsety+text_height(largefont)*10, WHITE, 0,
              "Charisma: %i", plyr.chr);
textprintf_ex(screentemp, largefont, offsetx, offsety+text_height(largefont)*11, WHITE, 0,
              "Hit points: %i", plyr.hp);
textprintf_ex(screentemp, largefont, offsetx, offsety+text_height(largefont)*12, WHITE, 0,
              "Magic point: %i", plyr.mp);
draw_button("Accept",offsetx+(buttons[0]->w/2), offsety+text_height(largefont)*14,0,0);
draw_button("Re-roll",offsetx+(buttons[0]->w/2)+buttons[0]->w, offsety+text_height(largefont)*14,0,0);
draw_button("Go back",offsetx+(buttons[0]->w/2)+(buttons[0]->w*2), offsety+text_height(largefont)*14,0,0);
clickx[0][0]=offsetx;
clickx[0][1]=offsetx+buttons[0]->w;
clicky[0][0]=offsety+text_height(largefont)*14;
clicky[0][1]=offsety+text_height(largefont)*14+buttons[0]->h;
clickx[1][0]=offsetx+buttons[0]->w;
clickx[1][1]=offsetx+(buttons[0]->w*2);
clicky[1][0]=offsety+text_height(largefont)*14;
clicky[1][1]=offsety+text_height(largefont)*14+buttons[0]->h;
clickx[2][0]=offsetx+(buttons[0]->w*2);
clickx[2][1]=offsetx+(buttons[0]->w*3);
clicky[2][0]=offsety+text_height(largefont)*14;
clicky[2][1]=offsety+text_height(largefont)*14+buttons[0]->h;
blitscreen();
while (!(mouse_b&1))
{
}
for (x=0; x<3; ++x)
{
if ((fetchmousex()>=clickx[x][0])&&(fetchmousex()<clickx[x][1])&&(fetchmousey()>=clicky[x][0])&&(fetchmousey()<clicky[x][1]))
{
if (x==0) draw_button("Accept",offsetx+(buttons[0]->w/2), offsety+text_height(largefont)*14, 1,0);
if (x==1) draw_button("Re-roll",offsetx+(buttons[0]->w/2)+buttons[0]->w, offsety+text_height(largefont)*14,1,0);
if (x==2) draw_button("Go back",offsetx+(buttons[0]->w/2)+(buttons[0]->w*2), offsety+text_height(largefont)*14,1,0);
blitscreen();
while (mouse_b&1)
{
}
if (x==0)
{
c=1;
}
if (x==2) {state=0; gamescr=0; c=1;}
}
}
}
if (gamescr!=0)
{
show_mouse(NULL);
c=0;
for (x = 0; x < 24; ++x)
            plyr.name[x] = '\0'; // 'clean' the string
for (x=0; x<24; ++x)
{
textprintf_ex(screentemp, largefont, offsetx, offsety+text_height(largefont)*15+buttonl[0]->h, WHITE, -1,
              "Name your character: %s_", plyr.name);
blitscreen();
plyr.name[x]=readkey();
if ((!(key[KEY_ENTER]||key[KEY_BACKSPACE]))&&(((plyr.name[x] & 0xff) < 65)||((plyr.name[x] & 0xff)>122))) {plyr.name[x]='\0'; x=x-1;}
if (key[KEY_BACKSPACE])
{
plyr.name[x]='\0';
if (x>0) plyr.name[x-1]='\0';
if (x==0) --x;
else x=x-2;
}
if (key[KEY_ENTER])
{
z=0;
plyr.name[x]='\0';
for (y=0; y<charcount; ++y)
{
if (strcmp(plyr.name, charlist[y])==0)
{
textprintf_ex(screentemp, largefont, offsetx,  offsety+text_height(largefont)*16+buttonl[0]->h, WHITE, -1,
    "Name %s already taken, choose a different name.  Press any key to continue.", plyr.name);
blitscreen();
readkey();
for (z = 0; z < 24; ++z)
plyr.name[z] = '\0';
clear_keybuf();
x=-1;
y=charcount;
}
}
if (z==0)
{
if (x>1) x=24;
else x=x-1;
}
}
}
//textprintf_ex(screentemp, largefont, offsetx,  offsety+text_height(largefont)*15+buttonl[0]->h, WHITE, -1, "Name your character: %s", plyr.name);
textprintf_ex(screentemp, largefont, offsetx,  offsety+text_height(largefont)*17+buttonl[0]->h, WHITE, -1,
              "Saving %s ...", plyr.name);
blitscreen();
rest(250);
strcpy(tempstr,plyr.name);
strcat(tempstr," is born.");
addlog(tempstr,0);
z = rewarditem(1,-1);
if (z==1) return 1;
gotitem(-1,0);
z = rewarditem(2,-1);
if (z==1) return 1;
gotitem(-1,0);
z = rewarditem(3,-1);
if (z==1) return 1;
gotitem(-1,0);
strcpy(charlist[charcount], plyr.name);
++charcount;
inputfile = fopen("Saves//charlist.dat","w");

  if (!inputfile) {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Can't open Saves\\charlist.dat.  Exiting.");
  return 1;}

for (x=0; x<24; ++x) fprintf(inputfile, "%s\n", charlist[x]);
fclose(inputfile);
x = savechar();
if (x==1) {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Problem saving character.  Exiting.");
  return 1;}
x=randshops();
if (x==1) {allegro_message("Problem randomizing shops.  Exiting."); return 1;}
gamescr=2;
rightpane=0;
mission=-1;
}
}
}
}
if (gamescr==2)
{
state=1; lastmousex=fetchmousex(); lastmousey=fetchmousey(); lastinfo=-1;
redraw=1;
tileres=64;
recalcdraw();
//If no mission, add a mission
if (mission==-1)
{
if (plyr.llevel<99) x=randomnum(0,2);
else x=randomnum(0,1);
mission=x;
if (mission==0)
{
x=randomnum(1,12);
if (x==1) missiontyp=11;
else
{
if (plyr.llevel>89)
{
x=randomnum(1,5);
if (x==1) missiontyp=4;
else if (x==2) missiontyp=6;
else if (x==3) missiontyp=7;
else if (x==4) missiontyp=1;
else missiontyp=0;
}
else if (plyr.llevel>79)
{
x=randomnum(1,3);
if (x==1) missiontyp=4;
else if (x==2) missiontyp=1;
else missiontyp=6;
}
else if (plyr.llevel>69)
{
x=randomnum(1,3);
if (x==1) missiontyp=7;
else if (x==2) missiontyp=1;
else missiontyp=6;
}
else if (plyr.llevel>59)
{
x=randomnum(1,3);
if (x==1) missiontyp=4;
else if (x==2) missiontyp=1;
else missiontyp=0;
}
else if (plyr.llevel>49)
{
x=randomnum(1,3);
if (x==1) missiontyp=4;
else if (x==2) missiontyp=7;
else missiontyp=9;
}
else if (plyr.llevel>39)
{
x=randomnum(1,3);
if (x==1) missiontyp=5;
else if (x==2) missiontyp=7;
else missiontyp=2;
}
else if (plyr.llevel>29)
{
x=randomnum(1,3);
if (x==1) missiontyp=4;
else if (x==2) missiontyp=6;
else missiontyp=1;
}
else if (plyr.llevel>19)
{
x=randomnum(1,3);
if (x==1) missiontyp=7;
else if (x==2) missiontyp=4;
else missiontyp=10;
}
else if (plyr.llevel>9)
{
x=randomnum(1,3);
if (x==1) missiontyp=1;
else if (x==2) missiontyp=3;
else missiontyp=2;
}
else
{
x=randomnum(1,3);
if (x==1) missiontyp=1;
else if (x==2) missiontyp=10;
else missiontyp=3;
}
}
missioncount=5;
if (missiontyp==0)
addlog("New Mission: Defeat 5 Beasts.",1);
else if (missiontyp==1)
{
addlog("New Mission: Defeat 8 Humanoids.",1);
missioncount=8;
}
else if (missiontyp==2)
addlog("New Mission: Defeat 5 Reptiles.",1);
else if (missiontyp==3)
addlog("New Mission: Defeat 5 Insects.",1);
else if (missiontyp==4)
{
addlog("New Mission: Defeat 8 Undead.",1);
missioncount=8;
}
else if (missiontyp==5)
{
addlog("New Mission: Defeat 4 Apparitions.",1);
missioncount=4;
}
else if (missiontyp==6)
addlog("New Mission: Defeat 5 Golems.",1);
else if (missiontyp==7)
addlog("New Mission: Defeat 5 Dragons.",1);
else if (missiontyp==8)
{
addlog("New Mission: Defeat 4 Elementals.",1);
missioncount=4;
}
else if (missiontyp==9)
addlog("New Mission: Defeat 5 Demons.",1);
else if (missiontyp==10)
addlog("New Mission: Defeat 5 Ooze.",1);
else if (missiontyp==11)
{
addlog("New Mission: Defeat a random boss.",1);
missioncount=1;
}
}
else if (mission==1)
{
addlog("New Mission: Open a treasure chest.",1);
}
else
{
sprintf(tempstr,"New Mission: Reach dungeon level %d.",plyr.llevel+1);
addlog(tempstr,1);
}
}
x=randomnum(1,2);
sprintf(tempstr,"town%d.mid",x);
playmusic(tempstr,-1);
// town loop
while (state==1)
{
if (redraw==1)
{
clear_bitmap(screentemp);
statpane=0;
for (x=0; x<tiledraw.x; ++x)
{
for (y=0; y<tiledraw.y; ++y)
{
blit(tiles,screentemp,npcx[11]*tileres, npcy[11]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
if ((x==(tiledraw.cx-2))&&(y==(tiledraw.cy-2))) masked_blit(maskedtiles,screentemp,npcx[0]*tileres, npcy[0]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
else if ((x==(tiledraw.cx))&&(y==(tiledraw.cy-2))) masked_blit(maskedtiles,screentemp,npcx[5]*tileres, npcy[5]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
else if ((x==(tiledraw.cx+2))&&(y==(tiledraw.cy-2))) masked_blit(maskedtiles,screentemp,npcx[1]*tileres, npcy[1]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
else if ((x==(tiledraw.cx-2))&&(y==tiledraw.cy)) masked_blit(maskedtiles,screentemp,npcx[4]*tileres, npcy[4]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
else if ((x==(tiledraw.cx))&&(y==tiledraw.cy)) masked_blit(maskedtiles,screentemp,npcx[10]*tileres, npcy[10]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
else if ((x==(tiledraw.cx+2))&&(y==tiledraw.cy)) masked_blit(maskedtiles,screentemp,npcx[3]*tileres, npcy[3]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
else if ((x==(tiledraw.cx-2))&&(y==(tiledraw.cy+2))) masked_blit(maskedtiles,screentemp,npcx[2]*tileres, npcy[2]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
else if ((x==(tiledraw.cx))&&(y==(tiledraw.cy+2))) masked_blit(maskedtiles,screentemp,npcx[6]*tileres, npcy[6]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
else if ((x==(tiledraw.cx+2))&&(y==(tiledraw.cy+2))) masked_blit(maskedtiles,screentemp,npcx[9]*tileres, npcy[9]*tileres, tiledraw.sx+(x*tileres), tiledraw.sy+(y*tileres), tileres, tileres);
}
}
textprintf_centre_ex(screentemp, largefont, (tiledraw.sx+(tileres*(tiledraw.cx-2))+(tileres/2)), (tiledraw.sy+(tileres*(tiledraw.cy-1))), WHITE,  GREEN, " Trade Guild ");
textprintf_centre_ex(screentemp, largefont, (tiledraw.sx+(tileres*(tiledraw.cx))+(tileres/2)), (tiledraw.sy+(tileres*(tiledraw.cy-1))), WHITE,  GREEN, " Item Shop ");
textprintf_centre_ex(screentemp, largefont, (tiledraw.sx+(tileres*(tiledraw.cx+2))+(tileres/2)), (tiledraw.sy+(tileres*(tiledraw.cy-1))), WHITE,  GREEN, " Sage ");
textprintf_centre_ex(screentemp, largefont, (tiledraw.sx+(tileres*(tiledraw.cx-2))+(tileres/2)), (tiledraw.sy+(tileres*(tiledraw.cy+1))), WHITE,  GREEN, " Weapon Shop ");
textprintf_centre_ex(screentemp, largefont, (tiledraw.sx+(tileres*(tiledraw.cx))+(tileres/2)), (tiledraw.sy+(tileres*(tiledraw.cy+1))), WHITE,  GREEN, " Inn ");
textprintf_centre_ex(screentemp, largefont, (tiledraw.sx+(tileres*(tiledraw.cx+2))+(tileres/2)), (tiledraw.sy+(tileres*(tiledraw.cy+1))), WHITE,  GREEN, " Armor Shop ");
textprintf_centre_ex(screentemp, largefont, (tiledraw.sx+(tileres*(tiledraw.cx-2))+(tileres/2)), (tiledraw.sy+(tileres*(tiledraw.cy+3))), WHITE,  GREEN, " Heroes Hall ");
textprintf_centre_ex(screentemp, largefont, (tiledraw.sx+(tileres*(tiledraw.cx))+(tileres/2)), (tiledraw.sy+(tileres*(tiledraw.cy+3))), WHITE,  GREEN, " Dungeon ");
textprintf_centre_ex(screentemp, largefont, (tiledraw.sx+(tileres*(tiledraw.cx+2))+(tileres/2)), (tiledraw.sy+(tileres*(tiledraw.cy+3))), WHITE,  GREEN, " Exit ");
redraw=0;
if (rightpane==1) drawpaperdoll();
else if (rightpane==2) drawspellbook(-1,-1);
else if (rightpane!=-1) drawstatpane(0);
drawmaindock(0, -1);
drawlog(1);
drawinfopane(-1);
if (bagsopen==1)
{
drawbags(bagpage,0);
}
blitscreen();
redraw=0;
}
lastsquarex=-1; lastsquarey=-1;
while ((!(mouse_b&1))&&(!(mouse_b&2))&&(!(keypressed()))&&(redraw==0))
{
if ((fetchmousex()!=lastmousex)||(fetchmousey()!=lastmousey))
{
for (x=0; x<infoct; ++x)
{
if ((fetchmousex()>=infox[x][0])&&(fetchmousex()<=infox[x][1])&&(fetchmousey()>=infoy[x][0])&&(fetchmousey()<=infoy[x][1])&&(lastinfo!=x))
{
lastinfo=x;
show_mouse(NULL);
if ((rightpane==0)||(rightpane==2)) drawinfopane(x);
else if ((rightpane==1)&&(plyr.equipped[x]!=-1))
{
invtotemp(plyr.equipped[x]);
doiteminfo();
}
blit(screentemp,screen,guilocations.infox[0],guilocations.infoy[0],guilocations.infox[0],guilocations.infoy[0],
 (guilocations.infox[1]-guilocations.infox[0]),(guilocations.infoy[1]-guilocations.infoy[0]));
show_mouse(screen);
}
lastmousex=fetchmousex(); lastmousey=fetchmousey();
}
if ((bagsopen==1)&&(fetchmousex()<guilocations.bagx[1]))
{
lastinfo=-1;
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if ((((mx==0)||(mx==6))&&(my>0)&&(my<11))&&((lastsquarex!=mx)||(lastsquarey!=my)))
{
lastsquarex=mx;
lastsquarey=my;
dobaginfo(mx,my);
}
}
}
}

if ((key[KEY_I])||(key[KEY_E])||(key[KEY_B])||(key[KEY_S])||(key[KEY_Y])||(key[KEY_SLASH]))
{
if ((key[KEY_I])&&(bagsopen==0)) {bagsopen=1; bagpage=0;}
else if ((key[KEY_I])&&(bagsopen==1)) bagsopen=0;
else if ((key[KEY_E])&&(rightpane==1)) rightpane=0;
else if (key[KEY_E]) rightpane=1;
else if ((key[KEY_B])&&(rightpane==2)) rightpane=0;
else if (key[KEY_B]) rightpane=2;
else if ((key[KEY_S])&&(rightpane==0)) rightpane=4;
else if ((key[KEY_S])&&(rightpane==4)) rightpane=0;
else if (key[KEY_S]) rightpane=0;
else if (key[KEY_Y])
{
y=systemmenu();
if (y==1)
{
state=0;
gamescr=0;
redraw=1;
}
}
else if (key[KEY_SLASH])
{
if (help==0) help=1;
else help=0;
}
redraw=1;
}

if (((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(checknumkeys()!=-1))
{
z=checknumkeys();
if (bagsopen==1)
{
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
}
if ((bagsopen==1)&&((mx==0)||(mx==6))&&(my>0)&&(my<11))
hotkey(mx,my,z);
else
usehotkey(z);
redraw=1;
}
if (checkfunckeys()==1)
{
e=-1;
if ((bagsopen==1)&&(fetchmousex()<guilocations.bagx[1]))
{
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
z=my-1;  if (mx==6) z=z+10;
z=z+(bagpage*20);
if (plyr.invicnx[z]!=-1)
{
if ((plyr.invtype[z]==2)&&(plyr.invmtype[z]==4)
    &&((plyr.invcls[z]==1)||(plyr.invcls[z]==2)))
{
y=fetchfkeynum();
if (y!=-1)
{
assignfuncmacro(y, z,-1);
}
}
else
{
addlog("F-key macros require potion, scroll, or spell.",1);
}
e=0;
}
}
else if (rightpane==2)
{
z=32;
c=32;
mx=((fetchmousex()-((screen->w-312)+offsetx))/c);
my=(fetchmousey()-(offsety))/z;
if ((my==1)||(my==3)||(my==5)||(my==7))
{
--my;
my=my/2;
if ((my<4)&&(mx<6)&&(spellbutton[mx][my]!=-1))
{
drawspellbook(mx,my);
blitscreen();
y=fetchfkeynum();
if (y!=-1)
{
assignfuncmacro(y,my,mx);
}
e=0;
}
}
}
if (e!=0)
{
y=fetchfkeynum();
usefkeymacro(y);
redraw=1;
}
while (checkfunckeys()==1)
{
}
redraw=1;
}
if (mouse_b&2)
{
while (mouse_b&2)
{
}
if (bagsopen==0)
{
x=savechar();
gamescr=0; state=0;
}
else if ((bagsopen==1)&&(fetchmousex()<guilocations.bagx[1])&&(fetchmousey()<guilocations.bagy[1]))
{
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if (((mx==0)||(mx==6))&&(my>0))
{
z=my-1;  if (mx==6) z=z+10;
z=z+(bagpage*20);
if (plyr.invicnx[z]!=-1)
{
y=equipuse(z);
lastsquarex=-1; lastsquarey=-1;
if (y==255) {x=playerdied(-1); if (x==0) {randshops(); gamescr=2; rightpane=0; redraw=1;} else gamescr=0;}
if (y==254)
{
tileres=tilepref;
gamescr=4;
resumedungeonmusic();
redraw=1;
state=0;
bagsopen=0;
rightpane=-1;
sprintf(tempstr,"Used a portal stone to return to level %d.",plyr.dlvl);
addlog(tempstr,0);
}
redraw=1;
}
}
}
}
if (mouse_b & 1)
{
//toggle stat pane page
if ((fetchmousex()>guilocations.statpanex[0])&&(fetchmousey()<guilocations.statpaney[1])&&(rightpane==4))
{
playsound("confirm.wav");
while (mouse_b&1)
{
}
rightpane=0;
redraw=1;
}
else if ((fetchmousex()>guilocations.statpanex[0])&&(fetchmousey()<guilocations.statpaney[1])&&(rightpane==0))
{
playsound("confirm.wav");
while (mouse_b&1)
{
}
rightpane=4;
redraw=1;
}
else
{
for (x=0; x<6; ++x)
{
if ((fetchmousex()>=clickx[x][0])&&(fetchmousex()<=clickx[x][1])&&(fetchmousey()>=clicky[x][0])&&(fetchmousey()<=clicky[x][1]))
{
drawmaindock(0, x);
if ((x==1)&&(bagsopen==0)) {bagsopen=1; bagpage=0;}
else if ((x==1)&&(bagsopen==1)) bagsopen=0;
else if ((x==2)&&(rightpane==1)) rightpane=0;
else if (x==2) rightpane=1;
else if ((x==3)&&(rightpane==2)) rightpane=0;
else if (x==3) rightpane=2;
else if ((x==4)&&(rightpane==0)) rightpane=4;
else if (x==4) rightpane=0;
else if (x==5)
{
blitscreen();
while (mouse_b&1)
{
}
y=systemmenu();
if (y==1)
{
state=0;
redraw=1;
gamescr=0;
}
if (y==2)
{
tileres=tilepref;
recalcdraw();
gamescr=4;
redraw=1;
resumedungeonmusic();
state=0;
bagsopen=0;
rightpane=-1;
}
state=0;
redraw=1;
}
blitscreen();
while (mouse_b&1)
{
}
redraw=1;
}
}
if ((rightpane==2)&&(mouse_b&1))
{
z=32;
c=32;
mx=((fetchmousex()-((screen->w-312)+offsetx))/c);
my=(fetchmousey()-(offsety))/z;
if ((my==1)||(my==3)||(my==5)||(my==7))
{
--my;
my=my/2;
if ((my<4)&&(mx<6)&&(spellbutton[mx][my]!=-1))
{
drawspellbook(mx,my);
blitscreen();
while (mouse_b&1)
{
}
castspell(spellbutton[mx][my],mx);
redraw=1;
}
}
}
}
mx=-1;
mx = ((fetchmousex()-tiledraw.sx)/tileres);
my = ((fetchmousey()-tiledraw.sy)/tileres);
//allegro_message("mx: %d my %d",mx,my);
if ((bagsopen==1)&&(fetchmousex()<guilocations.bagx[1]))
{
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if ((mx==3)&&(my==12)&&(bagpage!=4)&&(plyr.invicnx[(bagpage*20)+20]!=-1))
{
++bagpage;
redraw=1;
blitinversebutton(3,13,offsetx+32*3,offsety+32*12,0);
blitscreen();
while (mouse_b&1)
{
}
redraw=1;
}
else if ((mx==2)&&(my==12)&&(bagpage>0))
{
--bagpage;
blitinversebutton(3,13,offsetx+32*2,offsety+32*12,1);
blitscreen();
while (mouse_b&1)
{
}
redraw=1;
}
else if ((mx==0)&&(my==12))
{
blitinversebutton(0,12,offsetx,offsety+32*12,0);
//else blitinversebutton(0,12,offsetx+tileres*10-4,offsety+tileres*1-4,1);
blitscreen();
while (mouse_b&1)
{
}
sortbags();
redraw=1;
}
}
else
{
while (mouse_b&1)
{
}
//shop routine
if (((mx==(tiledraw.cx-2))&&(my==tiledraw.cy))||((mx==tiledraw.cx)&&(my==(tiledraw.cy-2)))||((mx==(tiledraw.cx+2))&&(my==tiledraw.cy)))
{
bagpage=0;
if (mx==(tiledraw.cx-2)) z=0;
else if (mx==tiledraw.cx) z=2;
else z=1;
redraw=1;
c=0;
while (c==0)
{
if (redraw==1)
{
drawbags(bagpage,1);
drawshop(z);
drawlog(1);
blitscreen();
redraw=0;
}
if (fetchmousex()>guilocations.bagx[1])
{
mx = (fetchmousex()-guilocations.bagx[1]-offsetx)/32;
my = (fetchmousey()-offsety)/32;
--my;
if (((mx==0)&&(my>-1))&&((lastsquarex!=mx)||(lastsquarey!=my)))
{
lastsquarex=mx;
lastsquarey=my;
doshopinfo(z,my);
}
}
else
{
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if ((((mx==0)||(mx==6))&&(my>0)&&(my<11))&&((lastsquarex!=mx)||(lastsquarey!=my)))
{
lastsquarex=mx;
lastsquarey=my;
dobaginfo(mx,my);
}
}
if ((mouse_b&1)||((mouse_b&2)&&(fetchmousex()>guilocations.bagx[1]+300)
                  &&(fetchmousex()>guilocations.bagy[1])))
{
if (((fetchmousex()>clickx[0][0])&&(fetchmousex()<clickx[0][1])&&(fetchmousey()>clicky[0][0])&&
    (fetchmousey()<clicky[0][1]))||(mouse_b&2)||((fetchmousex()>guilocations.bagx[1]+300)
    &&(fetchmousex()>guilocations.bagy[1])))
{
//draw_button("EXIT", bknd->w+((screentemp->w-bknd->w)/2), 632-buttonl[0]->h-(tileres/2), 1, 1);
draw_button("EXIT", guilocations.bagx[1]+150, guilocations.bagy[1]-buttonl[0]->h-32, 1, 1);
blitscreen();
if (mouse_b&1)
{
while (mouse_b&1)
{
}
}
else
{
while (mouse_b&2)
{
}
}
c=1;
}

if ((fetchmousex()<guilocations.bagx[1])&&(mx==3)&&(my==12)&&(bagpage!=4)&&(plyr.invicnx[(bagpage*20)+20]!=-1))
{
++bagpage;
redraw=1;
blitinversebutton(3,13,offsetx+32*3,offsety+32*12,0);
blitscreen();
while (mouse_b&1)
{
}
redraw=1;
}
else if ((fetchmousex()<guilocations.bagx[1])&&(mx==2)&&(my==12)&&(bagpage>0))
{
--bagpage;
blitinversebutton(3,13,offsetx+32*2,offsety+32*12,1);
blitscreen();
while (mouse_b&1)
{
}
redraw=1;
}
else if ((fetchmousex()<guilocations.bagx[1])&&(mx==0)&&(my==12))
{
blitinversebutton(0,12,offsetx,offsety+32*12,0);
//else blitinversebutton(0,12,offsetx+tileres*10-4,offsety+tileres*1-4,1);
blitscreen();
while (mouse_b&1)
{
}
sortbags();
redraw=1;
}
}


if (((mouse_b&2)&&(saleoption==0))||
    ((mouse_b&1)&&(saleoption==1)))
{
while ((mouse_b&2)||(mouse_b&1))
{
}
if (fetchmousex()>guilocations.bagx[1])
{
if ((mx==0)&&(my>-1)&&(my<8))
if (shop[z].icnx[my]!=-1)
{
if (plyr.invtype[99]!=-1)
{
addlog("Your inventory is full.",1);
redraw=1;
}
else
{
purchaseitem(z,my);
redraw=1;
}
}
}
else
{
if (((mx==0)||(mx==6))&&(my>0))
{
y=my-1;
if (mx==6) y=y+10;
y=y+(bagpage*20);
sellitem(y);
lastsquarex=-1; lastsquarey=-1;
redraw=1;
}
}
}
}
bagsopen=0;
rightpane=0;
mx=0; my=0;
redraw=1;
}
if ((mx==(tiledraw.cx+2))&&(my==(tiledraw.cy+2)))
{
x=savechar();
gamescr=0; state=0;}
if ((mx==tiledraw.cx)&&(my==(tiledraw.cy+2)))
{
// place choice here
if (plyr.llevel>1)
{
strcpy(tempstr,"Level ");
sprintf(strint,"%d",plyr.llevel);
strcat(tempstr,strint);
strcpy(menuopt[0], "Level 1");
strcpy(menuopt[1], tempstr);
y = draw_menu(CENTER, CENTER, 2, 1,"Enter Dungeon",0);
}
else
y=-1;
if (y!=-2)
{
if (y==1)
plyr.dlvl=plyr.llevel;
else
plyr.dlvl=1;
if (plyr.llevel<plyr.dlvl) plyr.llevel=1;
if (plyr.savedlvl[plyr.dlvl-1]==-1)
{
x=randdungeon();
if (x==1) return 1;
savedlvl();
plyr.savedlvl[plyr.dlvl-1]=0;
}
else loaddlvl();
plyr.x=dgn.supy; plyr.y=dgn.supx;
tileres=tilepref;
recalcdraw();
gamescr=4;
resumedungeonmusic();
state=0;
rightpane=-1;
}
redraw=1;
}
if ((mx==(tiledraw.cx-2))&&(my==(tiledraw.cy+2))) {hallofheroes(); redraw=1;}
if ((mx==(tiledraw.cx+2))&&(my==(tiledraw.cy-2)))
{
strcpy(tempstr,"Uridium says: Shall I identify and");
y=strlen(tempstr);
strcat(tempstr,"combine your potions for 25 gold?");
x=draw_menu(CENTER, CENTER, y, 1, tempstr, 2);
if ((x==0)&&(plyr.gold<25))
{
addlog("You don't have enough gold",1);
}
else if (x==0)
{
plyr.gold=plyr.gold-25;
addlog("Had potions identified and combined in town.",0);
combinepotions();
playsound("loot1.wav");
for (x=0; x<100; ++x)
{
if ((plyr.invmtype[x]==4)&&(plyr.invcls[x]==1)&&(plyr.invbonus[x]==-1))
{
invtotemp(x);
getpotionname(); tempitem.bonus=0;
gotitem(x,1);
}
}
}
redraw=1;
}
if ((mx==(tiledraw.cx-2))&&(my==(tiledraw.cy-2)))
// Trade guild
{
tradeguild();
redraw=1;
}
if ((mx==tiledraw.cx)&&(my==tiledraw.cy))
// Inn
{
if (plyr.gold<5)
{
addlog("You don't have enough gold to stay at the Inn.",1);
redraw=1;
}
else
{
strcpy(tempstr,"Rest and heal at the Inn");
y=strlen(tempstr);
strcat(tempstr,"for 5 gold?");
z=draw_menu(CENTER, CENTER, y, 1, tempstr, 2);
if (z==0)
{
plyr.gold=plyr.gold-5;
plyr.hp=(plyr.hpm+plyr.adjhp); plyr.mp=(plyr.mpm+plyr.adjmp);
addlog("Stayed at the Inn and Recovered HP/MP.",0);
playsound("inn.wav");
for (x=0; x<10; ++x)
{
plyr.buffx[x]=-1;
plyr.buffdur[x]=0;
}
}
redraw=1;
}
}
}
}
clear_keybuf();
}
}
if (gamescr==4)
{
recalcdraw();
dungeonmusicloop();
// Redraw screen
if (automove>-1) c=automove;
else c=0;
while ((automove>-1)||(redraw==1))
{
if (automove>-1)
{
movex=-1; movey=-1;
// recalc the pathing blit overlay by figuring out which direction we are moving
if (plyr.x<movedxx[c-automove])
{
// moving east
for (y=0; y<tiledraw.y; ++y)
{
for (x=tiledraw.cx; x<tiledraw.x; ++x)
{
moveflag[x][y]=moveflag[x+1][y];
if (x==9) moveflag[10][y]=-1;
}
}
}
else if (plyr.x>movedxx[c-automove])
{
// moving west
for (y=0; y<tiledraw.y; ++y)
{
for (x=tiledraw.cx; x>0; --x)
{
moveflag[x][y]=moveflag[x-1][y];
if (x==1) moveflag[0][y]=-1;
}
}
}
else if (plyr.y<movedyy[c-automove])
{
// moving south
for (y=tiledraw.cy; y<tiledraw.y; ++y)
{
for (x=0; x<tiledraw.x; ++x)
{
moveflag[x][y]=moveflag[x][y+1];
if (y==9) moveflag[x][10]=-1;
}
}
}
else if (plyr.y>movedyy[c-automove])
{
// moving north
for (y=tiledraw.cy; y>0; --y)
{
for (x=0; x<tiledraw.x; ++x)
{
moveflag[x][y]=moveflag[x][y-1];
if (y==1) moveflag[x][0]=-1;
}
}
}
z=-1;
for (x=0; x<128; ++x)
{
if ((dgn.doorx[x]==movedxx[c-automove])&&(dgn.doory[x]==movedyy[c-automove])&&
    ((dgn.door[x]==2)||(dgn.door[x]==3))) z=x;
}
if (z!=-1) y=lockeddoorevent(z);
if ((z!=-1)&&(y==0))
{
automove=-1;
for (x=0; x<tiledraw.x; ++x)
{
for (y=0; y<tiledraw.y; ++y)
{
moveflag[x][y]=-1;
}
}
}
else
{
plyr.lastx=plyr.x;
plyr.lasty=plyr.y;
plyr.x=movedxx[c-automove];
plyr.y=movedyy[c-automove];
--automove;
x=endofturn();
if ((x==3)||(x==1)) {pausedungeonmusic(); gamescr=2; rightpane=0; redraw=1;}
if (x==2) {gamescr=0; state=0;}
}
}
// Pick up event
if ((dmap[plyr.x][plyr.y].trki!=-1)&&(plyr.invtype[99]==-1))
{
if (dgn.item[dmap[plyr.x][plyr.y].trki]==2000)
{
x=10+randomnum((plyr.dlvl/2),plyr.dlvl+5);
sprintf(tempstr,"Found %d gold.",x);
addlog(tempstr,0);
plyr.gold=plyr.gold+x;
dgnlostitem(dmap[plyr.x][plyr.y].trki);
x=randomnum(1,3);
sprintf(tempstr,"coin%d.wav",x);
playsound(tempstr);
}
else if (dgn.item[dmap[plyr.x][plyr.y].trki]==2001)
{
makecommon(skeyx,skeyy,1,-1);
gotitem(-1,0);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
}
else if (dgn.item[dmap[plyr.x][plyr.y].trki]==2002)
{
makecommon(pstonex,pstoney,1,-1);
gotitem(-1,0);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
}
else if ((dgn.item[dmap[plyr.x][plyr.y].trki]>2002)&&(dgn.item[dmap[plyr.x][plyr.y].trki]<3000))
{
if (plyr.invtype[99]!=-1)
addlog("Your inventory is full.",1);
else
x=chestevent(dgn.item[dmap[plyr.x][plyr.y].trki]);
if (x==1) dgnlostitem(dmap[plyr.x][plyr.y].trki);
}
else if ((dgn.item[dmap[plyr.x][plyr.y].trki]>2999)&&(dgn.item[dmap[plyr.x][plyr.y].trki]<3999))
{
makebook(dmap[plyr.x][plyr.y].trki);
gotitem(-1,0);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
}
else if ((dgn.item[dmap[plyr.x][plyr.y].trki]>3999)&&(dgn.item[dmap[plyr.x][plyr.y].trki]<4999))
{
archfind();
dgnlostitem(dmap[plyr.x][plyr.y].trki);
}
else if (dgn.item[dmap[plyr.x][plyr.y].trki]==5000)
{
makecommon(trade.nshadex, trade.nshadey,1,-1);
tempitem.cls=5000;
gotitem(-1,0);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
}
else if (dgn.item[dmap[plyr.x][plyr.y].trki]==5001)
{
makecommon(trade.mossx, trade.mossy,1,-1);
tempitem.cls=5001;
gotitem(-1,0);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
}
else if (dgn.item[dmap[plyr.x][plyr.y].trki]==5002)
{
makecommon(trade.pearlx, trade.pearly,1,-1);
tempitem.cls=5002;
gotitem(-1,0);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
}
else if (dgn.item[dmap[plyr.x][plyr.y].trki]==6000)
{
addlog("The rune heals your HP to full.",0);
plyr.hp=(plyr.hpm+plyr.adjhp);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
playsound("Spell-Light-Heal.wav");
}
else if (dgn.item[dmap[plyr.x][plyr.y].trki]==6001)
{
addlog("The rune removes your curses.",0);
curedebuff(-1);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
playsound("Spell-Cure.wav");
}
else if (dgn.item[dmap[plyr.x][plyr.y].trki]==6002)
{
addlog("The rune grants you a boon.",0);
gotbuff(3,6,0,1);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
playsound("Spell-Light-Boon.wav");
}
else
{
if ((tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].mtype==0)
    ||(tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].mtype==1))
{
makeweapon(0,0,0,plyr.dlvl,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y,-1);
}
else if ((tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].mtype==2))
{
makearmor(0,0,plyr.dlvl,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y);
}
else if ((tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].mtype==3))
{
makeammo(0,0,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y);
}
else if ((tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].mtype==4)
    &&(tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].cls==0))
{
makefood(plyr.dlvl,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y);
}
else if ((tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].mtype==4)
    &&(tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].cls==1))
{
makepotion(plyr.dlvl,0,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y,-1);
}
else if ((tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].mtype==4)
    &&(tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].cls==2))
{
makescroll(tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y);
}
else if ((tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].mtype==5))
{
makegem(tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x,tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y);
}
else
{
allegro_message("Unaccounted-for item type in pickup routine.  Trki%i  Item %i Name %s",dmap[plyr.x][plyr.y].trki, dgn.item[dmap[plyr.x][plyr.y].trki], tiledata[tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].x][tlu[dgn.item[dmap[plyr.x][plyr.y].trki]].y].name);
}
gotitem(-1,0);
dgnlostitem(dmap[plyr.x][plyr.y].trki);
}
}
else if ((dmap[plyr.x][plyr.y].trki!=-1)&&(plyr.invtype[99]!=-1))
{
addlog("Your bags are full.",1);
}
redrawdungeontiles();
drawlog(1); drawmaindock(0, -1); drawinfopane(-1);
if ((rightpane==0)||(rightpane==4)) drawstatpane(0);
else if (rightpane==1) drawpaperdoll();
else if (rightpane==2) drawspellbook(-1,-1);
if (minimap==1) drawminimap(mapscale);
if (bagsopen==1)
{
drawbags(bagpage, 0);
}
show_mouse(NULL);
//blit(tiletemp, screentemp, 0, 0, (tiledraw.cx*tileres), (tiledraw.cy*tileres), tileres, tileres);
blitscreen();
if (automove>-1) rest(50);
else redraw=0;
}
if (c!=0)
{
for (x=0; x<60; ++x)
{
for (y=0; y<60; ++y)
{
moveflag[x][y]=-1;
}
}
movex=-1; movey=-1;
}
z=-1;
for (x=0; x<128; ++x)
{
if ((dgn.doormn[x]!=-1)&&(dgn.doorx[x]==plyr.x)&&(dgn.doory[x]==plyr.y)) z=x;
}
if (z!=-1)
{
x=combatevent(z,dgn.doormn[z]);
tileres=tilepref;
recalcdraw();
if ((combatzoom==1)&&(tilepref!=64))
{
redrawdungeontiles();
zoomout();
}
iscombat=-1;
equiprecalc();
if (x==1)
{
plyr.x=plyr.lastx;
plyr.y=plyr.lasty;
}
if (x==2)
{
//delete char
gamescr=0;
state=0;
redraw=1;
}
if (x==3)
{
gamescr=2;
randshops();
redraw=1;
state=0;
rightpane=0;
}
redraw=1;
}
show_mouse(screen);
lastsquarex=-1; lastsquarey=-1;
while ((!keypressed())&&(!(mouse_b & 1))&&(!(mouse_b & 2))&&(gamescr!=2)&&(redraw==0))
{
if ((fetchmousex()!=lastmousex)||(fetchmousey()!=lastmousey))
{
for (x=0; x<infoct; ++x)
{
if ((fetchmousex()>=infox[x][0])&&(fetchmousex()<=infox[x][1])&&(fetchmousey()>=infoy[x][0])&&(fetchmousey()<=infoy[x][1])&&(lastinfo!=x))
{
lastinfo=x;
show_mouse(NULL);
if ((rightpane==0)||(rightpane==2)) drawinfopane(x);
else if ((rightpane==1)&&(plyr.equipped[x]!=-1))
{
invtotemp(plyr.equipped[x]);
doiteminfo();
}
blit(screentemp,screen,guilocations.infox[0],guilocations.infoy[0],guilocations.infox[0],guilocations.infoy[0],
 (guilocations.infox[1]-guilocations.infox[0]),(guilocations.infoy[1]-guilocations.infoy[0]));
show_mouse(screen);
}
lastmousex=fetchmousex(); lastmousey=fetchmousey();
}
if (bagsopen==1)
{
lastinfo=-1;
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if ((((mx==0)||(mx==6))&&(my>0)&&(my<11))&&((lastsquarex!=mx)||(lastsquarey!=my)))
{
lastsquarex=mx;
lastsquarey=my;
dobaginfo(mx,my);
}
}
}
}
if (redraw==0)
{
show_mouse(NULL);
if ((mouse_b&2)&&(bagsopen==1)&&(fetchmousex()<guilocations.bagx[1]))
{
while (mouse_b&2)
{
}
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if (((mx==0)||(mx==6))&&(my>0))
{
z=my-1;  if (mx==6) z=z+10;
z=z+(bagpage*20);
if (plyr.invicnx[z]!=-1)
{
y=equipuse(z);
lastsquarex=-1; lastsquarey=-1;
if (y==255) {x=playerdied(-1); if (x==0) {randshops(); gamescr=2; rightpane=0; redraw=1;} else gamescr=0;}
if (y==254)
{
bagsopen=0;
rightpane=0;
addlog("Used a portal stone to return to town.",0);
randshops();
if (plyr.curse[0]!=-1)
{
curedebuff(-1);
addlog("The healing aura of Velia removes your curses.",0);
}
}
redraw=1;
}
}
}
else if ((mouse_b&1)&&(fetchmousey()>guilocations.logy[0])&&(fetchmousex()<guilocations.logx[1]))
{
while ((mouse_b&1)||(key[KEY_ESC]))
{
}
drawlog(3);
blitscreen();
while ((!(mouse_b&1))&&(!(mouse_b&2))&&(!(key[KEY_ESC])))
{
}
while ((mouse_b&1)||(mouse_b&2)||(key[KEY_ESC]))
{
}
redraw=1;
}
else if (mouse_b & 1)
{
//toggle mini map scale
if ((fetchmousex()<guilocations.mapx[1])&&(fetchmousey()<guilocations.mapy[1])&&(minimap==1))
{
while (mouse_b&1)
{
}
playsound("confirm.wav");
drawfullscreenmap();
redraw=1;
}
//toggle stat pane page
if ((fetchmousex()>guilocations.statpanex[0])&&(fetchmousey()<guilocations.statpaney[1])&&(rightpane==4))
{
playsound("confirm.wav");
while (mouse_b&1)
{
}
rightpane=0;
redraw=1;
}
else if ((fetchmousex()>guilocations.statpanex[0])&&(fetchmousey()<guilocations.statpaney[1])&&(rightpane==0))
{
playsound("confirm.wav");
while (mouse_b&1)
{
}
rightpane=4;
redraw=1;
}
else
{
for (x=0; x<6; ++x)
{
if ((fetchmousex()>=clickx[x][0])&&(fetchmousex()<=clickx[x][1])&&(fetchmousey()>=clicky[x][0])&&(fetchmousey()<=clicky[x][1]))
{
drawmaindock(0, x);
blitscreen();
while (mouse_b&1)
{
}
if ((x==0)&&(minimap==1)) minimap=0;
else if ((x==0)&&(minimap==0)) minimap=1;
else if ((x==1)&&(bagsopen==0)) {bagsopen=1; bagpage=0;}
else if ((x==1)&&(bagsopen==1)) bagsopen=0;
else if ((x==2)&&(rightpane==1)) rightpane=-1;
else if (x==2) rightpane=1;
else if ((x==3)&&(rightpane==2)) rightpane=-1;
else if (x==3) rightpane=2;
else if ((x==4)&&(rightpane==0)) rightpane=4;
else if ((x==4)&&(rightpane==4)) rightpane=-1;
else if (x==4) rightpane=0;
else if (x==5)
{
y=systemmenu();
if (y==1)
{
state=0;
redraw=1;
gamescr=0;
}
}
redraw=1;
}
}
}
if ((rightpane==2)&&(mouse_b&1))
{
z=32;
c=32;
mx=((fetchmousex()-((screen->w-312)+offsetx))/c);
my=(fetchmousey()-(offsety))/z;
if ((my==1)||(my==3)||(my==5)||(my==7))
{
--my;
my=my/2;
if ((my<4)&&(mx<6)&&(spellbutton[mx][my]!=-1))
{
drawspellbook(mx,my);
blitscreen();
while (mouse_b&1)
{
}
castspell(spellbutton[mx][my],mx);
redraw=1;
}
}
}
if ((bagsopen==1)&&(fetchmousex()<guilocations.bagx[1])&&(fetchmousey()<guilocations.bagy[1]))
{
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
if (bagsopen==1)
{
if ((mx==3)&&(my==12)&&(bagpage!=4)&&(plyr.invicnx[(bagpage*20)+20]!=-1))
{
++bagpage;
redraw=1;
blitinversebutton(3,13,offsetx+tileres*(32*3),offsety+tileres*(32*12),0);
blitscreen();
while (mouse_b&1)
{
}
redraw=1;
}
else if ((mx==2)&&(my==12)&&(bagpage>0))
{
--bagpage;
blitinversebutton(3,13,offsetx+(32*2),offsety+tileres*(32*12),1);
blitscreen();
while (mouse_b&1)
{
}
redraw=1;
}
else if ((mx==0)&&(my==12))
{
blitinversebutton(0,12,offsetx,offsety+(12*32),0);
blitscreen();
while (mouse_b&1)
{
}
sortbags();
redraw=1;
}
}
}
if ((mouse_b & 1)&&((rightpane==-1)||(fetchmousex()<guilocations.statpanex[0]))&&(bagsopen!=1)&&
    ((minimap==0)||(fetchmousex()>guilocations.mapx[1]))&&(fetchmousey()<screen->h-buttonbord->h))
{
mx = divideround(fetchmousex()-(tileres/2),tileres);
my = divideround(fetchmousey()-(tileres/2),tileres);
//allegro_message("mx: %d my: %d cx: %d cy: %d mousex: %d mousey: %d sx: %d sy: %d",mx,my,tiledraw.cx,tiledraw.cy,fetchmousex(),fetchmousey(),tiledraw.sx,tiledraw.sy);
dx = (plyr.x-tiledraw.cx)+mx;
dy = (plyr.y-tiledraw.cy)+my;
//if ((mx==tiledraw.cx+1)&&(my==tiledraw.cy)&&(mx==movex)&&(my==movey)) addlog("Yep.",1);
if ((mx==movex)&&(my==movey))
{
for (c=0; movedx[c]!=-1; ++c) automove=c;
playsound("confirm.wav");
while (mouse_b & 1)
{
}
}
else if ((!((mx==tiledraw.cx)&&(my==tiledraw.cy)))&&(dgn.open[dx][dy]!=-1)&&(viewt[mx][my]!=-1))
{
//allegro_message("Yep");
bagsopen=0;
for (x=0; x<60; ++x)
{
for (y=0; y<60; ++y)
{
moveflag[x][y]=-1;
}
}
//player has opted to move using the mouse
movex=mx; movey=my;
//plot move from center to destination
movedx[0]=tiledraw.cx; movedy[0]=tiledraw.cy;
c=0;
x = tiledraw.cx;
y = tiledraw.cy;
while ((x!=mx)||(y!=my))
{
// figure out which direction to move
dx=(plyr.x-tiledraw.cx)+x;
dy=(plyr.y-tiledraw.cy)+y;
if ((mx<tiledraw.cx)&&(my==tiledraw.cy)&&(x!=mx)) x=x-1;
else if ((mx==tiledraw.cx)&&(my<tiledraw.cy)&&(y!=my)) y=y-1;
else if ((mx>tiledraw.cx)&&(my==tiledraw.cy)&&(x!=mx)) x=x+1;
else if ((mx==tiledraw.cx)&&(my>tiledraw.cy)&&(y!=my)) y=y+1;
else if ((mx<tiledraw.cx)&&(my<tiledraw.cy))
{
if ((dgn.open[dx][dy-1]==0)&&(dgn.open[dx-1][dy]==0)&&(y!=my)&&(x!=mx))
{
if ((y-my)>=(x-mx)) --y;
else --x;
}
else if ((dgn.open[dx][dy-1]==0)&&(y!=my)) --y;
else if ((dgn.open[dx-1][dy]==0)&&(x!=mx)) --x;
}
else if ((mx>tiledraw.cx)&&(my<tiledraw.cy))
{
if ((dgn.open[dx][dy-1]==0)&&(dgn.open[dx+1][dy]==0)&&(y!=my)&&(x!=mx))
{
if ((y-my)>=(mx-x)) --y;
else ++x;
}
else if ((dgn.open[dx][dy-1]==0)&&(y!=my)) --y;
else if ((dgn.open[dx+1][dy]==0)&&(x!=mx)) ++x;
}
else if ((mx>tiledraw.cx)&&(my>tiledraw.cy))
{
if ((dgn.open[dx][dy+1]==0)&&(dgn.open[dx+1][dy]==0)&&(y!=my)&&(x!=mx))
{
if ((my-y)>=(mx-x)) ++y;
else ++x;
}
else if ((dgn.open[dx][dy+1]==0)&&(y!=my)) ++y;
else if ((dgn.open[dx+1][dy]==0)&&(x!=mx)) ++x;
}
else if ((mx<tiledraw.cx)&&(my>tiledraw.cy))
{
if ((dgn.open[dx][dy+1]==0)&&(dgn.open[dx-1][dy]==0)&&(y!=my)&&(x!=mx))
{
if ((my-y)>=(x-mx)) ++y;
else --x;
}
else if ((dgn.open[dx][dy+1]==0)&&(y!=my)) ++y;
else if ((dgn.open[dx-1][dy]==0)&&(x!=mx)) --x;
}
/*
if ((x>10)||(y>10)||(x<0)||(y<0)) {
    set_gfx_mode(GFX_TEXT, 80, 25, 0, 0);
    printf("Move routine is broken. mx: %d my: %d x: %d y: %d", mx, my, x, y);
    return 1;
}
*/
movedx[c]=x; movedy[c]=y;
if (x<=tiledraw.cx) movedxx[c]=(plyr.x-(tiledraw.cx-x));
if (y<=tiledraw.cy) movedyy[c]=(plyr.y-(tiledraw.cy-y));
if (x>tiledraw.cx) movedxx[c]=(plyr.x+(x-tiledraw.cx));
if (y>tiledraw.cy) movedyy[c]=(plyr.y+(y-tiledraw.cy));
++c;
if (c==60)
{
allegro_message("Move routine is broken. c=60. mx: %d my: %d x: %d y: %d", mx, my, x, y);
return 1;
}
}
movedx[c]=-1;
// determine blit graphic for the move plotter
// 0 = east-west  1 = north-south  2 = destionation
// 3 = east-north  4 = west-north  5 = west-south  6 = east-south
--c;
if ((movedx[0]>tiledraw.cx)&&(movedx[1]>movedx[0])) moveflag[movedx[0]][tiledraw.cy]=0;
else if ((movedx[0]<tiledraw.cx)&&(movedx[1]<movedx[0])) moveflag[movedx[0]][movedy[0]]=0;
else if ((movedy[0]>tiledraw.cy)&&(movedy[1]>movedy[0])) moveflag[movedx[0]][movedy[0]]=1;
else if ((movedy[0]<tiledraw.cy)&&(movedy[1]<movedy[0])) moveflag[movedx[0]][movedy[0]]=1;
else if ((movedx[0]>tiledraw.cx)&&(movedy[1]<movedy[0])) moveflag[movedx[0]][movedy[0]]=3;
else if ((movedy[0]>tiledraw.cy)&&(movedx[1]<movedx[0])) moveflag[movedx[0]][movedy[0]]=3;
else if ((movedy[0]>tiledraw.cy)&&(movedx[1]>movedx[0])) moveflag[movedx[0]][movedy[0]]=4;
else if ((movedx[0]<tiledraw.cx)&&(movedy[1]<movedy[0])) moveflag[movedx[0]][movedy[0]]=4;
else if ((movedy[0]<tiledraw.cy)&&(movedx[1]>movedx[0])) moveflag[movedx[0]][movedy[0]]=5;
else if ((movedx[0]<tiledraw.cx)&&(movedy[1]>movedy[0])) moveflag[movedx[0]][movedy[0]]=5;
else if ((movedy[0]<tiledraw.cy)&&(movedx[1]<movedx[0])) moveflag[movedx[0]][movedy[0]]=6;
else if ((movedx[0]>tiledraw.cx)&&(movedy[1]>movedy[0])) moveflag[movedx[0]][movedy[0]]=6;
for (x=0; x<c-1; ++x)
{
if ((movedx[x+1]>movedx[x])&&(movedx[x+2]>movedx[x+1])) moveflag[movedx[x+1]][movedy[x]]=0;
else if ((movedx[x+1]<movedx[x])&&(movedx[x+2]<movedx[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=0;
else if ((movedy[x+1]>movedy[x])&&(movedy[x+2]>movedy[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=1;
else if ((movedy[x+1]<movedy[x])&&(movedy[x+2]<movedy[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=1;
else if ((movedx[x+1]>movedx[x])&&(movedy[x+2]<movedy[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=3;
else if ((movedy[x+1]>movedy[x])&&(movedx[x+2]<movedx[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=3;
else if ((movedy[x+1]>movedy[x])&&(movedx[x+2]>movedx[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=4;
else if ((movedx[x+1]<movedx[x])&&(movedy[x+2]<movedy[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=4;
else if ((movedy[x+1]<movedy[x])&&(movedx[x+2]>movedx[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=5;
else if ((movedx[x+1]<movedx[x])&&(movedy[x+2]>movedy[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=5;
else if ((movedy[x+1]<movedy[x])&&(movedx[x+2]<movedx[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=6;
else if ((movedx[x+1]>movedx[x])&&(movedy[x+2]>movedy[x+1])) moveflag[movedx[x+1]][movedy[x+1]]=6;
}
moveflag[movedx[c]][movedy[c]]=2;
redraw=1;
playsound("select.wav");
}
while (mouse_b & 1)
{
}
}

}

}

if (((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(key[KEY_R]))
{
if (tileres==64)
{
tileres=32;
tilepref=32;
recalcdraw();
redrawdungeontiles();
zoomout();
redraw=1;
}
else
{
redrawdungeontiles();
zoomin();
tileres=64;
tilepref=64;
recalcdraw();
redrawdungeontiles();
redraw=1;
}
}

if (((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(key[KEY_Q]))
{
x=draw_menu(CENTER, CENTER, 14, 1, "Quit and save?", 2);
if (x==0)
{
savechar();
savedlvl();
quit=1;
}
redraw=1;
}

if (((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(key[KEY_F]))
{
if (fastmode==1)
{
fastmode=0;
addlog("Fast combat mode disabled.",1);
redraw=1;
}
else
{
fastmode=1;
addlog("Fast combat mode enabled.",1);
redraw=1;
}
}


if ((key[KEY_I])||(key[KEY_M])||(key[KEY_E])||(key[KEY_B])||(key[KEY_S])||(key[KEY_Y])||(key[KEY_SLASH]))
{
if ((key[KEY_M])&&((key[KEY_LCONTROL])||(key[KEY_RCONTROL])))
drawfullscreenmap();
else if ((key[KEY_M])&&(minimap==1)) minimap=0;
else if (key[KEY_M]) minimap=1;
else if ((key[KEY_I])&&(bagsopen==0)) {bagsopen=1; bagpage=0;}
else if ((key[KEY_I])&&(bagsopen==1)) bagsopen=0;
else if ((key[KEY_E])&&(rightpane==1)) rightpane=-1;
else if (key[KEY_E]) rightpane=1;
else if ((key[KEY_B])&&(rightpane==2)) rightpane=-1;
else if (key[KEY_B]) rightpane=2;
else if ((key[KEY_S])&&(rightpane==0)) rightpane=4;
else if ((key[KEY_S])&&(rightpane==4)) rightpane=-1;
else if (key[KEY_S]) rightpane=0;
else if (key[KEY_Y])
{
y=systemmenu();
if (y==1)
{
state=0;
redraw=1;
gamescr=0;
}
}
else if (key[KEY_SLASH])
{
if (help==0) help=1;
else help=0;
}
redraw=1;
}

if (((key[KEY_LCONTROL])||(key[KEY_RCONTROL]))&&(checknumkeys()!=-1))
{
z=checknumkeys();
if (bagsopen==1)
{
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
}
if ((bagsopen==1)&&((mx==0)||(mx==6))&&(my>0)&&(my<11))
hotkey(mx,my,z);
else
usehotkey(z);
redraw=1;
}

if (checkfunckeys()==1)
{
e=-1;
if ((bagsopen==1)&&(fetchmousex()<guilocations.bagx[1]))
{
mx = ((fetchmousex()-offsetx)/32);
my = ((fetchmousey()-offsety)/32);
z=my-1;  if (mx==6) z=z+10;
z=z+(bagpage*20);
if (plyr.invicnx[z]!=-1)
{
if ((plyr.invtype[z]==2)&&(plyr.invmtype[z]==4)
    &&((plyr.invcls[z]==1)||(plyr.invcls[z]==2)))
{
y=fetchfkeynum();
if (y!=-1)
{
assignfuncmacro(y, z,-1);
}
}
else
{
addlog("F-key macros require potion, scroll, or spell.",1);
}
e=0;
}
}
else if ((rightpane==2)&&(fetchmousex()>guilocations.statpanex[0]))
{
z=32;
c=32;
mx=((fetchmousex()-((screen->w-312)+offsetx))/c);
my=(fetchmousey()-(offsety))/z;
if ((my==1)||(my==3)||(my==5)||(my==7))
{
--my;
my=my/2;
if ((my<4)&&(mx<6)&&(spellbutton[mx][my]!=-1))
{
drawspellbook(mx,my);
blitscreen();
y=fetchfkeynum();
if (y!=-1)
{
assignfuncmacro(y,my,mx);
}
e=0;
}
}
}
if (e!=0)
{
y=fetchfkeynum();
usefkeymacro(y);
redraw=1;
}
while (checkfunckeys()==1)
      {
      }
redraw=1;
}

if ((key[KEY_UP])&&(plyr.y>0)&&(dgn.open[plyr.x][plyr.y-1]==0))
{
z=-1;
for (x=0; x<128; ++x)
{
if ((dgn.doorx[x]==plyr.x)&&(dgn.doory[x]==plyr.y-1)&&
    ((dgn.door[x]==2)||(dgn.door[x]==3))) z=x;
}
y=0;
if (z!=-1) y=lockeddoorevent(z);
if ((z==-1)||(y==1))
{
plyr.lastx=plyr.x;
plyr.lasty=plyr.y;
plyr.y=plyr.y-1;
}
x=endofturn();
clearmoveflags();
if ((x==3)||(x==1)) {pausedungeonmusic(); gamescr=2; rightpane=0; redraw=1;}
if (x==2) {gamescr=0; state=0;}
redraw=1;
bagsopen=0;
}
else if ((key[KEY_DOWN])&&(plyr.y<128)&&(dgn.open[plyr.x][plyr.y+1]==0))
{
z=-1;
for (x=0; x<128; ++x)
{
if ((dgn.doorx[x]==plyr.x)&&(dgn.doory[x]==plyr.y+1)&&
    ((dgn.door[x]==2)||(dgn.door[x]==3))) z=x;
}
y=0;
if (z!=-1) y=lockeddoorevent(z);
if ((z==-1)||(y==1))
{
plyr.lastx=plyr.x;
plyr.lasty=plyr.y;
plyr.y=plyr.y+1;
}
x=endofturn();
clearmoveflags();
if ((x==3)||(x==1)) {pausedungeonmusic(); gamescr=2; rightpane=0; redraw=1;}
if (x==2) {gamescr=0; state=0;}
redraw=1;
bagsopen=0;
}
else if ((key[KEY_LEFT])&&(plyr.x>0)&&(dgn.open[plyr.x-1][plyr.y]==0))
{
z=-1;
for (x=0; x<128; ++x)
{
if ((dgn.doorx[x]==plyr.x-1)&&(dgn.doory[x]==plyr.y)&&
    ((dgn.door[x]==2)||(dgn.door[x]==3))) z=x;
}
y=0;
if (z!=-1) y=lockeddoorevent(z);
if ((z==-1)||(y==1))
{
plyr.lastx=plyr.x;
plyr.lasty=plyr.y;
plyr.x=plyr.x-1;
}
x=endofturn();
clearmoveflags();
if ((x==3)||(x==1)) {pausedungeonmusic(); gamescr=2; rightpane=0; redraw=1;}
if (x==2) {gamescr=0; state=0;}
redraw=1;
bagsopen=0;
}
else if ((key[KEY_RIGHT])&&(plyr.x<128)&&(dgn.open[plyr.x+1][plyr.y]==0))
{
z=-1;
for (x=0; x<128; ++x)
{
if ((dgn.doorx[x]==plyr.x+1)&&(dgn.doory[x]==plyr.y)&&
    ((dgn.door[x]==2)||(dgn.door[x]==3))) z=x;
}
y=0;
if (z!=-1) y=lockeddoorevent(z);
if ((z==-1)||(y==1))
{
plyr.lastx=plyr.x;
plyr.lasty=plyr.y;
plyr.x=plyr.x+1;
}
x=endofturn();
clearmoveflags();
if ((x==3)||(x==1)) {pausedungeonmusic(); gamescr=2; rightpane=0; redraw=1;}
if (x==2) {gamescr=0; state=0;}
redraw=1;
bagsopen=0;
}
clear_keybuf();
}
}
/* destroy the bitmap */
destroy_bitmap(tiles); destroy_bitmap(maskedtiles); destroy_bitmap(tiletemp); destroy_bitmap(screentemp);
for (x=0; x<2; ++x) {destroy_bitmap(buttons[x]); destroy_bitmap(buttonl[x]);}
destroy_bitmap(menutemp); destroy_bitmap(menu[0]); destroy_bitmap(menu[1]);
destroy_bitmap(title); destroy_bitmap(buttonbord); destroy_bitmap(paperdoll); destroy_bitmap(paper);
destroy_bitmap(iconsi); destroy_bitmap(greyicons);  destroy_bitmap(redicons);
destroy_font(largefont); destroy_bitmap(plusminus);
destroy_midi(musicfile); destroy_sample(soundfile);
return 0;
}


END_OF_MAIN()
