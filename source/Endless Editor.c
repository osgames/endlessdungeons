
#include <allegro.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
   BITMAP *tiles, *maskedtiles, *tiletemp, *screentemp;
   FONT *newfont;
   PALETTE the_palette, fontpalette;
   FILE *tilefile;
   char editx, edity, quit, view, typestr[24], mtypestr[24], clsstr[24], treasurestr[24], elementstr[24], magicstr[10], bonusstr[24];
   int x, y, c, z, screenres, tileres, uiorient, textinc;

   // The tile database structure
   typedef struct {
    int lvl, type, mtype, cls, elem, treasure, magic, bonus;
    char name[24];
   } tilearray;

      /*
   type
   0 = unusued tile
   1 = monster
   2 = inventory item
   3 = Npc
   4 = System
   IF MONSTER
   lvl
   = min level first appears
   mtype
   0 = beast
   1 = humanoid
   2 = reptile
   3 = insect
   4 = undead
   5 = apparition
   6 = golem
   7 = dragon
   8 = elemental
   9 = demon
   10 = Ooze
   11 = Plant
   cls
   0 = warrior
   1 = cleric
   2 = mage
   3 = thief
   4 = barbarian
   5 = spellblade
   6 = paladin
   elem
   0 = None
   1 = wind
   2 = fire
   3 = water
   4 = earth
   5 = shadow
   6 = light
   7 = Poison
   treasure
   0 = normal
   1 = less
   2 = more
   3 = hoard
   magic (temperament)
   0 = Evil
   1 = Neutral
   2 = Good
   bonus
   0 = None
   1 = Colossus
   2 = Paralyser
   3 = Physical Resist
   4 = Magic Resist
   5 = Vampire
   6 = Speed
   IF INVENTORY ITEM
   lvl
   = level first appears
   mtype
   0 = melee
   1 = ranged
   2 = armor
   3 = ammo
   4 = usable
   5 = gem
   IF MELEE
   cls
   0 = Dagger
   1 = Mace
   2 = Sword
   3 = Axe
   4 = Spear
   5 = Polearm
   6 = Martial arts
   elem
   same table as monster
   treasure
   0 = 1 Hand
   1 = 2 Hand
   bonus (subtype)
   IF DAGGER
   0 = Regular (slashing)
   1 = Puncture (stiletto)
   IF SWORD
   0 = Sword
   1 = Rapier
   2 = Katana
   IF MACE
   0 = Mace
   1 = Hammer
   2 = Flail
   IF SPEAR
   0 = Spear
   1 = Pitchfork
   2 = Lance
   IF POLEARM
   0 = Halberd
   1 = Scythe
   IF MARTIAL ARTS
   0 = Whip
   1 = Nunchuk (blunt 1hand)
   2 = Staff (blunt 2hand)
   IF RANGED
   cls
   0 = Bow
   1 = Crossbow
   2 = Sling
   3 = Darts
   4 = Wand
   5 = Shruiken
   elem
   same table as monster
   IF ARMOR
   cls
   0 = head
   1 = chest
   2 = hands
   3 = feet
   4 = back
   5 = ring
   6 = necklace
   7 = shield
   elem
   same table as monster
   treasure ( armor tpe)
   0 = cloth
   1 = leather
   2 = chain
   3 = scale
   4 = full plate
   IF AMMO
   cls
   same as ranged
   elem
   same as monster
   IF MELEE RANGED ARMOR OR AMMO
   magic
   0 = Normal
   1 = Magical
   IF USABLE
   cls
   0 = Food
   1 = Potion
   2 = Spell scroll
   IF POTION
   treasure
   0 = Healing
   1 = Cure Poison
   2 = Poison
   3 = Weakness
   4 = Strength
   5 = Dexterity
   6 = Intelligence
   7 = Charisma
   8 = Magic
   9 = Recklessness
   10 = Random stat
   11 = Darkness
   12 = Light
   IF GEM
   cls
   0 = Amethyst
   1 = Sapphire
   2 = Ruby
   3 = Quartz
   4 = Emerald
   IF NPC
   mtype
   0 = Healer
   1 = Quest Giver
   2 = Hall of records
   3 = Armor Shop
   4 = Weapon Shop
   5 = Item Shop
   6 = Dungeon Entrance
   7 = Sign
   8 = Gambling
   9 = Quit
   10 = Inn
   11 = Grass tile (fill tile for town)
   IF SYSTEM
   mtype
   0 = Floor
   1 = Wall
   2 = Door
   3 = Stairs
   4 = Chest
   5 = Player Icon
   IF FLOOR WALL DOOR STAIRS
   cls = sub-tileset
   IF FLOOR-WALL
   elem
   0 = Normal
   1 = Accent
   IF DOOR
   elem
   0 = Normal
   1 = Hidden
   2 = Magic
   IF STAIRS
   elem
   0 = Up
   1 = Down
   IF CHEST
   0 = Small
   1 = Large
   IF PLAYER ICON
   cls
   0 = Warrior
   1 = Cleric
   2 = Mage
   3 = Thief
   4 = Barbarian
   5 = Spellblade
   6 = Paladin
   7 = Warlock
   */


// The stat tracking structure
   typedef struct {
   int monsters[11], items[11], weapons[11], melee[11], ranged[11], potions[11], scrolls, rings, necklaces, walls[10], armor[11],
   armortype[11][5], food[11], cloaks[11], shields[11], ngood[11];
   } statarray;


// define basic varaibles
    tilearray tiledata[64][28];

    statarray stats;

    for (y=0; y<28; ++y)
    {
    for (x=0; x<64; ++x)
    {
    strcpy(tiledata[x][y].name,"Unused Tile"); tiledata[x][y].lvl = 1; tiledata[x][y].type = 0;
    tiledata[x][y].cls = 0; tiledata[x][y].treasure=0; tiledata[x][y].mtype=0; tiledata[x][y].elem=0;
    tiledata[x][y].magic = 0; tiledata[x][y].bonus=0;
    }
    }

    editx = 8; edity = 6; quit = 0; view=0;

   if (allegro_init() != 0)
      return 1;

//parse cfg
    set_config_file("dungeon.cfg");
    screenres = get_config_int("screen","screenres",0);
    tileres = get_config_int("screen","tilesize",0);
    if (screenres == 1) tileres=1;
    uiorient = get_config_int("screen","uiorient",0);

   install_keyboard();

    set_color_depth(32);

    extern FONT *font;

    if (uiorient == 0)
    {
    if (screenres==0) {x = 1088; y = 928;}
    if (screenres==1) {x = 544; y = 468;}
    }
    else
    {
    if (screenres==0) {x = 1280; y = 832;}
    if (screenres==1) {x = 736; y = 416;}
    }
    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, x, y, 0, 0) != 0) {
	 allegro_message("Unable to set graphic mode\n%s\n", allegro_error);
	 return 1;
   }

   if (tileres==1) {tileres=32; tiles = load_bitmap("tiles.bmp", the_palette);}
   else {tileres=64;
    screentemp=load_bitmap("tiles.bmp", the_palette);
   tiles = create_bitmap(4096,1792);
   stretch_blit(screentemp,tiles,0,0,screentemp->w,screentemp->h,0,0,tiles->w,tiles->h);
   destroy_bitmap(screentemp);
   }

    screentemp = create_bitmap(x,y);

    tiletemp = create_bitmap(tileres,tileres);

   if (!tiles) {
      set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
      allegro_message("Error reading bitmap file '%s'\n", argv[1]);
      return 1;
   }

/* select the bitmap palette */
   set_palette(the_palette);


    if (screenres == 1) {newfont = load_font("font1.pcx", fontpalette, NULL); textinc=16;}
    else {newfont = load_font("font2.pcx", fontpalette, NULL); textinc = 30;}
	if (
     newfont == NULL) {
		set_gfx_mode(GFX_TEXT, 80, 25, 0, 0);
		printf("Unable to load font.\n");
		return 1;
	}

   // Create a copy of the tileset for masked blitting
   // Substitute 255,255,255 (white)  255,0,255 in the tile file,  masked blitting
   maskedtiles = create_bitmap((64*tileres),(28*tileres));
   blit(tiles, maskedtiles, 0, 0, 0, 0, (64*tileres),(28*tileres));
   for (y = 0;y < maskedtiles->h;++y)
   {
   for (x = 0;x < maskedtiles->w;++x)
   {
    c = getpixel(maskedtiles, x, y);
    if(c == makecol(255, 0, 255))
      putpixel(maskedtiles, x, y, makecol(255, 0, 255)-1);
    else if(c == makecol(255,255,255))
      putpixel(maskedtiles, x, y, makecol(255, 0, 255));
   }
   }

   // Create another copy of the tileset with blue background, to show activated tiles
/*
    tilesblue = create_bitmap((64*tileres),(28*tileres));
   blit(tiles, tilesblue, 0, 0, 0, 0, (64*tileres),(28*tileres));
   for (y = 0;y < tilesblue->h;++y)
   {
   for (x = 0;x < tilesblue->w;++x)
   {
    c = getpixel(tilesblue, x, y);
    if(c == makecol(112,227,203))
      putpixel(tilesblue, x, y, makecol(112,227,203)-1);
    else if(c == makecol(255,255,255))
      putpixel(tilesblue, x, y, makecol(112,227,203));
   }
   }  */


// Main editor loop
   while (quit!=1)
   {
   // Redraw screen
    editx = editx-8;
    edity = edity-6;
    for (y=0; y<13; ++y)
    {
    for (x=0; x<17; ++x)
    {
    if ( ((editx+x)<0) || ((editx+x)>63) || ((edity+y)>27) || ((edity+y)<0) )
    {
    blit(tiles, tiletemp, (63*tileres), (27*tileres), 0, 0, tileres, tileres);
    }
    else
    {
    blit(tiles, tiletemp, ((editx+x)*tileres), ((edity+y)*tileres), 0, 0, tileres, tileres);
    if ((x==8)&&(y==6)) masked_blit(maskedtiles, tiletemp, (62*tileres), (27*tileres), 0, 0, tileres, tileres);
    else if (tiledata[editx+x][edity+y].type!=0) masked_blit(maskedtiles, tiletemp, (61*tileres), (27*tileres), 0, 0, tileres, tileres);
    if ((view==1)&&(tiledata[editx+x][edity+y].type>0)) blit(tiles, tiletemp, (63*tileres), (27*tileres), 0, 0, tileres, tileres);
    if ((view==2)&&(tiledata[editx+x][edity+y].type==0)) blit(tiles, tiletemp, (63*tileres), (27*tileres), 0, 0, tileres, tileres);
    }
    blit(tiletemp, screentemp, 0, 0, x*tileres, y*tileres, tileres, tileres);
    }
    }
    editx = editx+8; edity = edity+6;
    strcpy(mtypestr,"n/a");
    strcpy(clsstr,"n/a");
    strcpy(treasurestr,"n/a");
    strcpy(elementstr,"n/a");
    strcpy(magicstr,"n/a");
    strcpy(bonusstr,"n/a");
   if (tiledata[editx][edity].type==0)
   {
   strcpy(typestr,"Inactive Tile");
   }
   if (tiledata[editx][edity].type==1)
   {
   strcpy(typestr,"Monster");
   if (tiledata[editx][edity].mtype==0) strcpy(mtypestr, "Beast");
   if (tiledata[editx][edity].mtype==1) strcpy(mtypestr, "Humanoid");
   if (tiledata[editx][edity].mtype==2) strcpy(mtypestr, "Reptile");
   if (tiledata[editx][edity].mtype==3) strcpy(mtypestr, "Insect");
   if (tiledata[editx][edity].mtype==4) strcpy(mtypestr, "Undead");
   if (tiledata[editx][edity].mtype==5) strcpy(mtypestr, "Apparition");
   if (tiledata[editx][edity].mtype==6) strcpy(mtypestr, "Golem");
   if (tiledata[editx][edity].mtype==7) strcpy(mtypestr, "Dragon");
   if (tiledata[editx][edity].mtype==8) strcpy(mtypestr, "Elemental");
   if (tiledata[editx][edity].mtype==9) strcpy(mtypestr, "Demon");
   if (tiledata[editx][edity].mtype==10) strcpy(mtypestr, "Ooze");
   if (tiledata[editx][edity].mtype==11) strcpy(mtypestr, "Plant");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Warrior");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Cleric");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr, "Mage");
   if (tiledata[editx][edity].cls==3) strcpy(clsstr, "Thief");
   if (tiledata[editx][edity].cls==4) strcpy(clsstr, "Barbarian");
   if (tiledata[editx][edity].cls==5) strcpy(clsstr, "Spellblade");
   if (tiledata[editx][edity].cls==6) strcpy(clsstr, "Paladin");
   if (tiledata[editx][edity].cls==7) strcpy(clsstr, "Warlock");
   if (tiledata[editx][edity].elem==0) strcpy(elementstr, "None");
   if (tiledata[editx][edity].elem==1) strcpy(elementstr, "Wind");
   if (tiledata[editx][edity].elem==2) strcpy(elementstr, "Fire");
   if (tiledata[editx][edity].elem==3) strcpy(elementstr, "Water");
   if (tiledata[editx][edity].elem==4) strcpy(elementstr, "Earth");
   if (tiledata[editx][edity].elem==5) strcpy(elementstr, "Shadow");
   if (tiledata[editx][edity].elem==6) strcpy(elementstr, "Light");
   if (tiledata[editx][edity].elem==7) strcpy(elementstr, "Poison");
   if (tiledata[editx][edity].treasure==0) strcpy(treasurestr, "Normal");
   if (tiledata[editx][edity].treasure==1) strcpy(treasurestr, "Less");
   if (tiledata[editx][edity].treasure==2) strcpy(treasurestr, "More");
   if (tiledata[editx][edity].treasure==3) strcpy(treasurestr, "Hoard");
   if (tiledata[editx][edity].magic==0) strcpy(magicstr, "Evil");
   if (tiledata[editx][edity].magic==1) strcpy(magicstr, "Neutral");
   if (tiledata[editx][edity].magic==2) strcpy(magicstr, "Good");
   if (tiledata[editx][edity].bonus==0) strcpy(bonusstr, "None");
   if (tiledata[editx][edity].bonus==1) strcpy(bonusstr, "Colossus");
   if (tiledata[editx][edity].bonus==2) strcpy(bonusstr, "Paralyser");
   if (tiledata[editx][edity].bonus==3) strcpy(bonusstr, "Phys Resist");
   if (tiledata[editx][edity].bonus==4) strcpy(bonusstr, "Magic Resist");
   if (tiledata[editx][edity].bonus==5) strcpy(bonusstr, "Vampire");
   if (tiledata[editx][edity].bonus==6) strcpy(bonusstr, "Speed");
   }
   if (tiledata[editx][edity].type==2)
   {
   strcpy(typestr,"Inventory Item");
   if (tiledata[editx][edity].mtype==0)
   {
   strcpy(mtypestr, "Melee Weapon");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Dagger");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Mace");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr, "Sword");
   if (tiledata[editx][edity].cls==3) strcpy(clsstr, "Axe");
   if (tiledata[editx][edity].cls==4) strcpy(clsstr, "Spear");
   if (tiledata[editx][edity].cls==5) strcpy(clsstr, "Polearm");
   if (tiledata[editx][edity].cls==6) strcpy(clsstr, "Martial Arts");
   if (tiledata[editx][edity].treasure==0) strcpy(treasurestr, "1 Hnd");
   if (tiledata[editx][edity].treasure==1) strcpy(treasurestr, "2 Hnd");
   if (tiledata[editx][edity].cls==0)
   {
   if (tiledata[editx][edity].bonus==0) strcpy(bonusstr, "Slashing");
   if (tiledata[editx][edity].bonus==1) strcpy(bonusstr, "Puncture");
   }
   if (tiledata[editx][edity].cls==2)
   {
   if (tiledata[editx][edity].bonus==0) strcpy(bonusstr, "Broad");
   if (tiledata[editx][edity].bonus==1) strcpy(bonusstr, "Rapier");
   if (tiledata[editx][edity].bonus==2) strcpy(bonusstr, "Katana");
   }
   if (tiledata[editx][edity].cls==1)
   {
   if (tiledata[editx][edity].bonus==0) strcpy(bonusstr, "Mace");
   if (tiledata[editx][edity].bonus==1) strcpy(bonusstr, "Hammer");
   if (tiledata[editx][edity].bonus==2) strcpy(bonusstr, "Flail");
   }
   if (tiledata[editx][edity].cls==4)
   {
   if (tiledata[editx][edity].bonus==0) strcpy(bonusstr, "Spear");
   if (tiledata[editx][edity].bonus==1) strcpy(bonusstr, "Pitchfork");
   if (tiledata[editx][edity].bonus==2) strcpy(bonusstr, "Lance");
   }
   if (tiledata[editx][edity].cls==5)
   {
   if (tiledata[editx][edity].bonus==0) strcpy(bonusstr, "Halberd");
   if (tiledata[editx][edity].bonus==1) strcpy(bonusstr, "Scythe");
   }
   if (tiledata[editx][edity].cls==6)
   {
   if (tiledata[editx][edity].bonus==0) strcpy(bonusstr, "Whip");
   if (tiledata[editx][edity].bonus==1) strcpy(bonusstr, "Nunchuk");
   if (tiledata[editx][edity].bonus==2) strcpy(bonusstr, "Staff");
   }
   }
   if (tiledata[editx][edity].mtype==1)
   {
   strcpy(mtypestr, "Ranged Weapon");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Bow");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Crossbow");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr, "Sling");
   if (tiledata[editx][edity].cls==3) strcpy(clsstr, "Darts");
   if (tiledata[editx][edity].cls==4) strcpy(clsstr, "Wand");
   if (tiledata[editx][edity].cls==5) strcpy(clsstr, "Shruiken");
   if (tiledata[editx][edity].cls==4)
   {
   if (tiledata[editx][edity].treasure==0) strcpy(treasurestr, "Wand");
   if (tiledata[editx][edity].treasure==1) strcpy(treasurestr, "Scepter");
   }
   }
   if (tiledata[editx][edity].mtype==2)
   {
   strcpy(mtypestr, "Armor");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Head");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Chest");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr, "Hands");
   if (tiledata[editx][edity].cls==3) strcpy(clsstr, "Feet");
   if (tiledata[editx][edity].cls==4) strcpy(clsstr, "Back");
   if (tiledata[editx][edity].cls==5) strcpy(clsstr, "Ring");
   if (tiledata[editx][edity].cls==6) strcpy(clsstr, "Necklace");
   if (tiledata[editx][edity].cls==7) strcpy(clsstr, "Shield");
   if (tiledata[editx][edity].cls<4)
   {
   if (tiledata[editx][edity].treasure==0) strcpy(treasurestr, "Cloth");
   if (tiledata[editx][edity].treasure==1) strcpy(treasurestr, "Leather");
   if (tiledata[editx][edity].treasure==2) strcpy(treasurestr, "Chain");
   if (tiledata[editx][edity].treasure==3) strcpy(treasurestr, "Half-plate");
   if (tiledata[editx][edity].treasure==4) strcpy(treasurestr, "Full-plate");
   }
   if (tiledata[editx][edity].cls==7)
   {
   if (tiledata[editx][edity].treasure==0) strcpy(treasurestr, "Regular");
   if (tiledata[editx][edity].treasure==1) strcpy(treasurestr, "Lantern");
   }
   }
   if (tiledata[editx][edity].mtype==3)
   {
   strcpy(mtypestr, "Ammo");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Arrows");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Bolts");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr, "Pellets");
   }
   if (tiledata[editx][edity].mtype==4)
   {
   strcpy(mtypestr, "Usable");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Food");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Potion");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr, "Spell Scroll");
   if (tiledata[editx][edity].cls==3) strcpy(clsstr, "Skeleton Key");
   if (tiledata[editx][edity].cls==4) strcpy(clsstr, "Portal Stone");
   }
   if (tiledata[editx][edity].mtype==5)
   {
   strcpy(mtypestr, "Gem");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Amethyst");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Sapphire");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr, "Ruby");
   if (tiledata[editx][edity].cls==3) strcpy(clsstr, "Quartz");
   if (tiledata[editx][edity].cls==4) strcpy(clsstr, "Emerald");
   }
   if (tiledata[editx][edity].mtype<4)
   {
   if (tiledata[editx][edity].elem==0) strcpy(elementstr, "None");
   if (tiledata[editx][edity].elem==1) strcpy(elementstr, "Wind");
   if (tiledata[editx][edity].elem==2) strcpy(elementstr, "Fire");
   if (tiledata[editx][edity].elem==3) strcpy(elementstr, "Water");
   if (tiledata[editx][edity].elem==4) strcpy(elementstr, "Earth");
   if (tiledata[editx][edity].elem==5) strcpy(elementstr, "Shadow");
   if (tiledata[editx][edity].elem==6) strcpy(elementstr, "Light");
   if (tiledata[editx][edity].elem==7) strcpy(elementstr, "Poison");
   if (tiledata[editx][edity].magic==0) strcpy(magicstr, "No");
   if (tiledata[editx][edity].magic==1) strcpy(magicstr, "Yes");
   }
   }
   if (tiledata[editx][edity].type == 3)
   {
   strcpy(typestr, "NPC");
   if (tiledata[editx][edity].mtype==0) strcpy(mtypestr, "Trade Guild");
   if (tiledata[editx][edity].mtype==1) strcpy(mtypestr, "Quest Giver");
   if (tiledata[editx][edity].mtype==2) strcpy(mtypestr, "Hall of Records");
   if (tiledata[editx][edity].mtype==3) strcpy(mtypestr, "Armor Shop");
   if (tiledata[editx][edity].mtype==4) strcpy(mtypestr, "Weapon Shop");
   if (tiledata[editx][edity].mtype==5) strcpy(mtypestr, "Item Shop");
   if (tiledata[editx][edity].mtype==6) strcpy(mtypestr, "Dungeon Entrance");
   if (tiledata[editx][edity].mtype==7) strcpy(mtypestr, "Sign");
   if (tiledata[editx][edity].mtype==8) strcpy(mtypestr, "Gambling");
   if (tiledata[editx][edity].mtype==9) strcpy(mtypestr, "Quit");
   if (tiledata[editx][edity].mtype==10) strcpy(mtypestr, "Inn");
   if (tiledata[editx][edity].mtype==11) strcpy(mtypestr, "Grass");
   }
   if (tiledata[editx][edity].type==4)
   {
   strcpy(typestr, "System");
   if (tiledata[editx][edity].mtype==0) strcpy(mtypestr,"Floor");
   if (tiledata[editx][edity].mtype==1) strcpy(mtypestr,"Wall");
   if (tiledata[editx][edity].mtype==2) strcpy(mtypestr,"Door");
   if (tiledata[editx][edity].mtype==3) strcpy(mtypestr,"Stairs");
   if (tiledata[editx][edity].mtype==4) strcpy(mtypestr,"Chest");
   if (tiledata[editx][edity].mtype==5) strcpy(mtypestr,"Player Icon");
   if (tiledata[editx][edity].mtype==6) strcpy(mtypestr,"Gold Pile");
   if (tiledata[editx][edity].mtype==7) strcpy(mtypestr,"Lord Xantos");
   if (tiledata[editx][edity].mtype==8) strcpy(mtypestr,"Rune");
   if (tiledata[editx][edity].mtype==9) strcpy(mtypestr,"Starfield");
   if (tiledata[editx][edity].mtype<4)
   {
   itoa(tiledata[editx][edity].cls+1, elementstr, 10);
   strcpy(clsstr, "Tileset ");
   strcat(clsstr, elementstr);
   }
   if ((tiledata[editx][edity].mtype==0) || (tiledata[editx][edity].mtype==1))
   {
   if (tiledata[editx][edity].elem==0) strcpy(elementstr, "Normal");
   if (tiledata[editx][edity].elem==1) strcpy(elementstr, "Accent");
   }
   if (tiledata[editx][edity].mtype==2)
   {
   if (tiledata[editx][edity].elem==0) strcpy(elementstr, "Normal");
   if (tiledata[editx][edity].elem==1) strcpy(elementstr, "Hidden");
   if (tiledata[editx][edity].elem==2) strcpy(elementstr, "Magic");
   }
   if (tiledata[editx][edity].mtype==3)
   {
   if (tiledata[editx][edity].elem==0) strcpy(elementstr, "Up");
   if (tiledata[editx][edity].elem==1) strcpy(elementstr, "Down");
   }
   if (tiledata[editx][edity].mtype==4)
   {
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Small");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Large");
   }
   if (tiledata[editx][edity].mtype==5)
   {
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Warrior");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Cleric");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr, "Mage");
   if (tiledata[editx][edity].cls==3) strcpy(clsstr, "Thief");
   if (tiledata[editx][edity].cls==4) strcpy(clsstr, "Barbarian");
   if (tiledata[editx][edity].cls==5) strcpy(clsstr, "Spellblade");
   if (tiledata[editx][edity].cls==6) strcpy(clsstr, "Paladin");
   if (tiledata[editx][edity].cls==7) strcpy(clsstr, "Warlock");
   if (tiledata[editx][edity].treasure==0) strcpy(treasurestr, "Male");
   if (tiledata[editx][edity].treasure==1) strcpy(treasurestr, "Female");
   }
   if (tiledata[editx][edity].mtype==8)
   {
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Life");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Curing");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr, "Dragon");
   }
   if (tiledata[editx][edity].mtype==9)
   {
   if (tiledata[editx][edity].cls==0) strcpy(clsstr, "Regular");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr, "Accent");
   }
   }
   if (tiledata[editx][edity].type==5)
   {
   strcpy(typestr, "Trade");
   if (tiledata[editx][edity].mtype==1)
   {
   strcpy(mtypestr,"Scholar");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr,"Book of the Owl");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr,"Book of the Sloth");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr,"Book of the Lizard");
   if (tiledata[editx][edity].cls==3) strcpy(clsstr,"Book of the Turtle");
   if (tiledata[editx][edity].cls==4) strcpy(clsstr,"Book of the Gryphon");
   if (tiledata[editx][edity].cls==5) strcpy(clsstr,"Book of the Dragon");
   if (tiledata[editx][edity].cls==6) strcpy(clsstr,"Book of the Whale");
   }
   if (tiledata[editx][edity].mtype==2)
   {
   strcpy(mtypestr,"Alchemy");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr,"Deep Moss");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr,"Nightshade");
   if (tiledata[editx][edity].cls==2) strcpy(clsstr,"Pearl");
   }
   if (tiledata[editx][edity].mtype==0)
   {
   strcpy(mtypestr,"Archaeology");
   if (tiledata[editx][edity].cls==0) strcpy(clsstr,"Bones");
   if (tiledata[editx][edity].cls==1) strcpy(clsstr,"Ruins");
   if (tiledata[editx][edity].cls==2)
   {
   strcpy(clsstr,"Loot");
   if (tiledata[editx][edity].elem==0) strcpy(elementstr,"Document");
   if (tiledata[editx][edity].elem==1) strcpy(elementstr,"Weapon Fragment");
   if (tiledata[editx][edity].elem==2) strcpy(elementstr,"Sigil");
   if (tiledata[editx][edity].elem==3) strcpy(elementstr,"Scrap");
   }
   }
   }
   if (uiorient == 0)
   {
    textprintf_ex(screentemp,
                  newfont, 4, (13*tileres), makecol(255, 255, 255), 0,
                 "C: %d, %d  Name[Enter]: %s  Lvl[0]: %d  Type[1]: %s                                             "
                 , editx, edity,tiledata[editx][edity].name, tiledata[editx][edity].lvl, typestr);
   textprintf_ex(screentemp, newfont, 4, ((13*tileres)+textinc), makecol(255, 255, 255), 0,
                 "MType[2]: %s  Cls[3]: %s  Element[4]: %s  Treasure[5]: %s  MItem[6]: %s                             "
                 , mtypestr, clsstr, elementstr, treasurestr, magicstr);
   textprintf_ex(screentemp,
                 newfont, 4, ((13*tileres)+(textinc*2)), makecol(255, 255, 255), 0,
                 "Bonus[7]: %s  [Ctrl-Q] Exit  [F5] Toggle view  [S]ave  [L]oad  [X]Info  t[e]xt file               ", bonusstr);
   }
   else
   {
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), 0, makecol(255, 255, 255), 0,
                 "Coords:              ");
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*1), makecol(255, 255, 255), 0,
                 "%d, %d             ", editx, edity);
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*2), makecol(255, 255, 255), 0,
                 "Name[Enter]:                ");
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*3), makecol(255, 255, 255), 0,
                 "%s                   ", tiledata[editx][edity].name);
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*4), makecol(255, 255, 255), 0,
                 "Level[0]:             ");
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*5), makecol(255, 255, 255), 0,
                 "%d                    ", tiledata[editx][edity].lvl);
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*6), makecol(255, 255, 255), 0,
                 "Type[1]:             ");
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*7), makecol(255, 255, 255), 0,
                 "%s                    ", typestr);
   textprintf_ex(screentemp, newfont, ((17*tileres)+(0.50*textinc)), (textinc*8), makecol(255, 255, 255), 0,
                 "Mtype[2]:             ");
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*9), makecol(255, 255, 255), 0,
                 "%s                    ", mtypestr);
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*10), makecol(255, 255, 255), 0,
                 "Class[3]:              ");
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*11), makecol(255, 255, 255), 0,
                 "%s                     ", clsstr);
   textprintf_ex(screentemp, newfont, ((17*tileres)+(0.50*textinc)), (textinc*12), makecol(255, 255, 255), 0,
                 "Element[4]:             ");
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*13), makecol(255, 255, 255), 0,
                 "%s                       ", elementstr);
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*14), makecol(255, 255, 255), 0,
                 "Treasure[5]:            ");
   textprintf_ex(screentemp, newfont, ((17*tileres)+(0.50*textinc)), (textinc*15), makecol(255, 255, 255), 0,
                 "%s                    ", treasurestr);
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*16), makecol(255, 255, 255), 0,
                 "MItem[6]:            ");
   textprintf_ex(screentemp, newfont, ((17*tileres)+(0.50*textinc)), (textinc*17), makecol(255, 255, 255), 0,
                 "%s                    ", magicstr);
   textprintf_ex(screentemp,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*18), makecol(255, 255, 255), 0,
                 "Bonus[7]:            ");
   textprintf_ex(screentemp, newfont, ((17*tileres)+(0.50*textinc)), (textinc*19), makecol(255, 255, 255), 0,
                 "%s                    ", bonusstr);
   textprintf_ex(screentemp, newfont, ((17*tileres)+(0.50*textinc)), (textinc*20), makecol(255, 255, 255), 0,
                 "[Ctrl-Q] Exit");
   textprintf_ex(screentemp, newfont, ((17*tileres)+(0.50*textinc)), (textinc*21), makecol(255, 255, 255), 0,
                 "[S]ave  [L]oad");
   textprintf_ex(screentemp, newfont, ((17*tileres)+(0.50*textinc)), (textinc*22), makecol(255, 255, 255), 0,
                 "[X]Info [F5]View");
   textprintf_ex(screentemp, newfont, ((17*tileres)+(0.50*textinc)), (textinc*23), makecol(255, 255, 255), 0,
                 "t[e]xt file");
   }
   blit(screentemp,screen,0,0,0,0,screentemp->w,screentemp->h);
    readkey();
    if ((key[KEY_Q])&&(key[KEY_LCONTROL]))
   {
    quit = 1;
    }
    if (key[KEY_E])
    {
    textout_centre_ex(screen, newfont, "OUTPUTTING LIST.LOG",SCREEN_W / 2, SCREEN_H / 2, makecol(255, 255, 255), 0);
    rest(200);
    tilefile = fopen("list.log","w");
    fprintf(tilefile,"\n\nMonster list by level\n\n");
    for (z=1; z<91; z=z+10)
    {
    c=1;
    if (z==11) z=z-1;
    fprintf(tilefile,"Level %i\n--------\n",z);
    for (x=0; x<64; ++x)
    {
    for (y=0; y<28; ++y)
    {
    if ((tiledata[x][y].type==1)&&(tiledata[x][y].lvl==z))
    {
    fprintf(tilefile,"%i - %s\n",c,tiledata[x][y].name);
    ++c;
    }
    }
    }
    }
    fprintf(tilefile,"\n\nPlayer Icons\n\n");
    for (x=0; x<64; ++x)
    {
    for (y=0; y<28; ++y)
    {
    if ((tiledata[x][y].type==4)&&(tiledata[x][y].mtype==5))
    {
    if (tiledata[x][y].cls==0) strcpy(clsstr, "Warrior");
    if (tiledata[x][y].cls==1) strcpy(clsstr, "Cleric");
    if (tiledata[x][y].cls==2) strcpy(clsstr, "Mage");
    if (tiledata[x][y].cls==3) strcpy(clsstr, "Thief");
    if (tiledata[x][y].cls==4) strcpy(clsstr, "Barbarian");
    if (tiledata[x][y].cls==5) strcpy(clsstr, "Spellblade");
    if (tiledata[x][y].cls==6) strcpy(clsstr, "Paladin");
    if (tiledata[x][y].cls==7) strcpy(clsstr, "Warlock");
    if (tiledata[x][y].treasure==0) strcpy(treasurestr, "Male");
    if (tiledata[x][y].treasure==1) strcpy(treasurestr, "Female");
    fprintf(tilefile,"%i,%i - %s %s\n",x,y,clsstr,treasurestr);
    }
    }
    }
    fclose(tilefile);
    }
    if (key[KEY_X])
    {
    // Compiles and displays stats to measure progress of the tile database
    clear_to_color(screentemp, makecol(0,0,0));
    for (x=0; x<11; ++x)
    {
    stats.monsters[x]=0;
    stats.items[x]=0;
    stats.weapons[x]=0;
    stats.melee[x]=0;
    stats.ranged[x]=0;
    stats.potions[x]=0;
    stats.armor[x]=0;
    stats.food[x]=0;
    stats.cloaks[x]=0;
    stats.shields[x]=0;
    stats.ngood[x]=0;
    for (c=0; c<5; ++c)
    {
    stats.armortype[x][c]=0;
    }
    if (x!=10) stats.walls[x]=0;
    }
    stats.scrolls=0;
    stats.rings = 0;
    stats.necklaces = 0;
    for (x=0; x<64; ++x)
    {
    for (y=0; y<28; ++y)
    {
    if (tiledata[x][y].lvl==1) c=0;
    else c=tiledata[x][y].lvl/10;
    if (tiledata[x][y].type==1)
    {
    ++stats.monsters[c];
    ++stats.monsters[10];
    if (tiledata[x][y].magic>0)
    {
    ++stats.ngood[c];
    ++stats.ngood[10];
    }
    }
    if (tiledata[x][y].type==2) {++stats.items[c]; ++stats.items[10];}
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==0)) {++stats.melee[c]; ++stats.melee[10];}
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==1)) {++stats.ranged[c]; ++stats.ranged[10];}
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==2))
        {++stats.armor[c]; ++stats.armor[10]; ++stats.armortype[c][tiledata[x][y].treasure]; ++stats.armortype[10][tiledata[x][y].treasure];}
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==2)&&(tiledata[x][y].cls<5))
        {
        ++stats.armor[c]; ++stats.armor[10];
        if (tiledata[x][y].cls!=4) {++stats.armortype[c][tiledata[x][y].treasure]; ++stats.armortype[10][tiledata[x][y].treasure];}
        if (tiledata[x][y].cls==4) {++stats.cloaks[c]; ++stats.cloaks[10];}
        }
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==2)&&(tiledata[x][y].cls==5)) ++stats.rings;
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==2)&&(tiledata[x][y].cls==6)) ++stats.necklaces;
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==2)&&(tiledata[x][y].cls==7)) {++stats.shields[c]; ++stats.shields[10];}
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==4)&&(tiledata[x][y].cls==0)) {++stats.food[c]; ++stats.food[10];}
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==4)&&(tiledata[x][y].cls==1)) {++stats.potions[c]; ++stats.potions[10];}
    if ((tiledata[x][y].type==2)&&(tiledata[x][y].mtype==4)&&(tiledata[x][y].cls==2)) ++stats.scrolls;
    }
    }
    textprintf_ex(screentemp, newfont, 0, 0, makecol(255, 255, 255), 0, "Endless Editor- Stats");
    c=text_length(newfont,"Lev");
    textprintf_ex(screentemp, newfont, 0, (text_height(newfont)*2), makecol(255, 255, 255), 0,
    "Level  Monsters Weapons Melee Ranged Armor Cloaks Shields Food Potions Neut+Good");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*3), makecol(255, 255, 255), 0, "1-10");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*4), makecol(255, 255, 255), 0, "10-20");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*5), makecol(255, 255, 255), 0, "20-30");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*6), makecol(255, 255, 255), 0, "30-40");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*7), makecol(255, 255, 255), 0, "40-50");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*8), makecol(255, 255, 255), 0, "50-60");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*9), makecol(255, 255, 255), 0, "60-70");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*10), makecol(255, 255, 255), 0, "70-80");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*11), makecol(255, 255, 255), 0, "80-90");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*12), makecol(255, 255, 255), 0, "90-99");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*13), makecol(255, 255, 255), 0, "Total");
    for (x=0; x<11; ++x)
    {
    c=text_length(newfont,"Level  Monst");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", stats.monsters[x]);
    c=text_length(newfont,"Level  Monsters Weap");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", (stats.melee[x]+stats.ranged[x]));
    c=text_length(newfont,"Level  Monsters Weapons Mel");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", stats.melee[x]);
    c=text_length(newfont,"Level  Monsters Weapons Melee Rang");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", stats.ranged[x]);
    c=text_length(newfont,"Level  Monsters Weapons Melee Ranged Arm");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", stats.armor[x]);
    c=text_length(newfont,"Level  Monsters Weapons Melee Ranged Armor Cloa");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", stats.cloaks[x]);
    c=text_length(newfont,"Level  Monsters Weapons Melee Ranged Armor Cloaks Shie");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", stats.shields[x]);
    c=text_length(newfont,"Level  Monsters Weapons Melee Ranged Armor Cloaks Shields Fo");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", stats.food[x]);
    c=text_length(newfont,"Level  Monsters Weapons Melee Ranged Armor Cloaks Shields Food Poti");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", stats.potions[x]);
    c=text_length(newfont,"Level  Monsters Weapons Melee Ranged Armor Cloaks Shields Food Potions Neut");
    textprintf_centre_ex(screentemp, newfont, c, (text_height(newfont)*(3+x)), makecol(255, 255, 255), 0, "%d", stats.ngood[x]);
    }
    textprintf_ex(screentemp, newfont, 0, (text_height(newfont)*14), makecol(255, 255, 255), 0, "Plus %d rings, %d necklaces, and %d scrolls.", stats.rings, stats.necklaces, stats.scrolls);
    textprintf_centre_ex(screentemp, newfont, SCREEN_W/2, (text_height(newfont)*16), makecol(255, 255, 255), 0, "Press F1 to continue.");
    blit(screentemp,screen,0,0,0,0,screentemp->w,screentemp->h);
    while (!key[KEY_F1])
    {
    }
    }
    if (key[KEY_0])
    {
    tiledata[editx][edity].lvl=tiledata[editx][edity].lvl+10;
    if (tiledata[editx][edity].lvl==11) tiledata[editx][edity].lvl =10;
    if (tiledata[editx][edity].lvl==100) tiledata[editx][edity].lvl =1;
    }
    if (key[KEY_1])
    {
    ++tiledata[editx][edity].type;
    tiledata[editx][edity].mtype=0; tiledata[editx][edity].cls=0; tiledata[editx][edity].elem=0; tiledata[editx][edity].lvl=1;
    tiledata[editx][edity].treasure = 0;
    if (tiledata[editx][edity].type>5) tiledata[editx][edity].type=0;
    }
    if (key[KEY_2])
    {
    if (tiledata[editx][edity].type!=0)
    {
    ++tiledata[editx][edity].mtype;
    if ((tiledata[editx][edity].type==1) && (tiledata[editx][edity].mtype==12)) tiledata[editx][edity].mtype=0;
    if ((tiledata[editx][edity].type==2) && (tiledata[editx][edity].mtype==6)) tiledata[editx][edity].mtype=0;
    if ((tiledata[editx][edity].type==3) && (tiledata[editx][edity].mtype==12)) tiledata[editx][edity].mtype=0;
    if ((tiledata[editx][edity].type==4) && (tiledata[editx][edity].mtype==10)) tiledata[editx][edity].mtype=0;
    if ((tiledata[editx][edity].type==5) && (tiledata[editx][edity].mtype==3)) tiledata[editx][edity].mtype=0;
    }
    }
    if (key[KEY_3])
    {
    if (tiledata[editx][edity].type!=3)
    {
    ++tiledata[editx][edity].cls;
    if ((tiledata[editx][edity].type==1) && (tiledata[editx][edity].cls==8)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype==0)&&(tiledata[editx][edity].cls==7)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype==1)&&(tiledata[editx][edity].cls==6)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype==2)&&(tiledata[editx][edity].cls==8)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype==3)&&(tiledata[editx][edity].cls==6)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype==4)&&(tiledata[editx][edity].cls==5)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype==5)&&(tiledata[editx][edity].cls==5)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==4)&&(tiledata[editx][edity].mtype<4)&&(tiledata[editx][edity].cls==10)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==4)&&(tiledata[editx][edity].mtype==4)&&(tiledata[editx][edity].cls==2)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==4)&&(tiledata[editx][edity].mtype==5)&&(tiledata[editx][edity].cls==8)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==4)&&(tiledata[editx][edity].mtype==8)&&(tiledata[editx][edity].cls==3)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==4)&&(tiledata[editx][edity].mtype==8)&&(tiledata[editx][edity].cls==2)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==5)&&(tiledata[editx][edity].mtype==0)&&(tiledata[editx][edity].cls==3)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==5)&&(tiledata[editx][edity].mtype==1)&&(tiledata[editx][edity].cls==8)) tiledata[editx][edity].cls=0;
    if ((tiledata[editx][edity].type==5)&&(tiledata[editx][edity].mtype==2)&&(tiledata[editx][edity].cls==3)) tiledata[editx][edity].cls=0;
    }
    }
    if (key[KEY_4])
    {
    if ((tiledata[editx][edity].type==1)||((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype<4))
        ||((tiledata[editx][edity].type==4)&&(tiledata[editx][edity].mtype!=4))
        ||((tiledata[editx][edity].type==5)&&(tiledata[editx][edity].mtype==0)&&(tiledata[editx][edity].cls==2)))
    {
    ++tiledata[editx][edity].elem;
    if ((tiledata[editx][edity].type==1)||(tiledata[editx][edity].type==2))
    {
    if (tiledata[editx][edity].elem == 8) tiledata[editx][edity].elem = 0;
    }
    else if (tiledata[editx][edity].type==5) {if (tiledata[editx][edity].elem==4) tiledata[editx][edity].elem=0;}
    else
    {
    if ((tiledata[editx][edity].mtype<2)&&(tiledata[editx][edity].elem==2)) tiledata[editx][edity].elem=0;
    if ((tiledata[editx][edity].mtype==2)&&(tiledata[editx][edity].elem==3)) tiledata[editx][edity].elem=0;
    if ((tiledata[editx][edity].mtype==3)&&(tiledata[editx][edity].elem==2)) tiledata[editx][edity].elem=0;
    }
    }
    }
    if (key[KEY_5])
    {
    if ((tiledata[editx][edity].type==1)||((tiledata[editx][edity].type==2)&&((tiledata[editx][edity].mtype<3)))
        ||((tiledata[editx][edity].type==4)&&((tiledata[editx][edity].mtype==5))))
    {
    ++tiledata[editx][edity].treasure;
    if ((tiledata[editx][edity].type==1)&&(tiledata[editx][edity].treasure==4)) tiledata[editx][edity].treasure=0;
    if (((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype<2))&&(tiledata[editx][edity].treasure==2)) tiledata[editx][edity].treasure=0;
    if (((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype==2))&&(tiledata[editx][edity].treasure==5)) tiledata[editx][edity].treasure=0;
    if ((tiledata[editx][edity].type==4)&&(tiledata[editx][edity].treasure==2)) tiledata[editx][edity].treasure=0;
    }
    }
   if (key[KEY_6])
   {
    if (((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype<4))
         ||(tiledata[editx][edity].type==1))
    ++tiledata[editx][edity].magic;
    if ((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].magic==2)) tiledata[editx][edity].magic=0;
    if ((tiledata[editx][edity].type==1)&&(tiledata[editx][edity].magic==3)) tiledata[editx][edity].magic=0;
   }
    if ((key[KEY_7])&&((tiledata[editx][edity].type==1)
                      ||((tiledata[editx][edity].type==2)&&(tiledata[editx][edity].mtype==0)&&(tiledata[editx][edity].cls!=3))
                      ))

   {
    ++tiledata[editx][edity].bonus;
    if ((tiledata[editx][edity].type==1)&&(tiledata[editx][edity].bonus==7)) tiledata[editx][edity].bonus=0;
    if (tiledata[editx][edity].type==2)
    {
    if (((tiledata[editx][edity].cls==0)||(tiledata[editx][edity].cls==5))&&
         (tiledata[editx][edity].bonus==2)) tiledata[editx][edity].bonus=0;
    else if (tiledata[editx][edity].bonus==3) tiledata[editx][edity].bonus=0;
    }
   }
   if ((key[KEY_UP])&&(edity>0))
   {
    edity=edity-1;
   }
   if ((key[KEY_DOWN])&&(edity<27))
   {
   edity=edity+1;
   }
   if ((key[KEY_LEFT])&&(editx>0))
   {
   editx=editx-1;
   }
   if ((key[KEY_RIGHT])&&(editx<63))
   {
   editx=editx+1;
   }
   if (key[KEY_F5])
   {
   ++view;
   if (view==3) view=0;
   }
   if (key[KEY_ENTER])
   {
   for (x = 0; x < 24; x++)
                tiledata[editx][edity].name[x] = '\0'; // 'clean' the string

   for (x=0; x<24; ++x)
   {
   if (uiorient == 0)
   {
    textprintf_ex(screen,
                  newfont, 4, (13*tileres), makecol(255, 255, 255), 0,
                 "C: %d, %d  Name[Enter]: %s_                                            "
                 , editx, edity,tiledata[editx][edity].name);
   }
   else
   {
   textprintf_ex(screen,
                 newfont, ((17*tileres)+(0.50*textinc)), (textinc*3), makecol(255, 255, 255), 0,
                 "%s_                     ", tiledata[editx][edity].name);
   }
   tiledata[editx][edity].name[x]=readkey();
   if ((key[KEY_BACKSPACE])&&(x>0)) {tiledata[editx][edity].name[x]='\0'; tiledata[editx][edity].name[x-1]='\0'; x=x-2;}
   if (key[KEY_ENTER]) {tiledata[editx][edity].name[x]='\0'; x=24;}
   }
   }
   if (key[KEY_S])
   {
   textout_centre_ex(screen, newfont, "SAVING",SCREEN_W / 2, SCREEN_H / 2, makecol(255, 255, 255), 0);
   rest(200);
   tilefile = fopen("tiles.dat","w");
    for (y=0; y<28; ++y)
    {
    for (x=0; x<64; ++x)
    {
    fprintf(tilefile, "%s\n%i,%i,%i,%i,%i,%i,%i,%i\n", tiledata[x][y].name, tiledata[x][y].type, tiledata[x][y].mtype, tiledata[x][y].lvl, tiledata[x][y].cls,
            tiledata[x][y].elem, tiledata[x][y].treasure, tiledata[x][y].magic, tiledata[x][y].bonus);
    }
    }
    fclose(tilefile);
   }
   if (key[KEY_L])
   {
   textout_centre_ex(screen, newfont, "LOADING",SCREEN_W / 2, SCREEN_H / 2, makecol(255, 255, 255), 0);
   rest(200);
    tilefile = fopen("tiles.dat","r");
    for (y=0; y<28; ++y)
    {
    for (x=0; x<64; ++x)
    {
    fscanf(tilefile, "%[^\n]s", tiledata[x][y].name); fscanf(tilefile, "%i%*c", &tiledata[x][y].type); fscanf(tilefile, "%i%*c", &tiledata[x][y].mtype);
    fscanf(tilefile, "%i%*c", &tiledata[x][y].lvl); fscanf(tilefile, "%i%*c", &tiledata[x][y].cls); fscanf(tilefile, "%i%*c", &tiledata[x][y].elem);
    fscanf(tilefile, "%i%*c", &tiledata[x][y].treasure); fscanf(tilefile, "%i%*c", &tiledata[x][y].magic); fscanf(tilefile, "%i%*c", &tiledata[x][y].bonus);
    }
    }
    fclose(tilefile);
   }
   }
    /* destroy the bitmap */
    destroy_bitmap(tiles); destroy_bitmap(maskedtiles); destroy_bitmap(tiletemp); destroy_bitmap(screentemp);
    destroy_font(newfont);
   return 0;
}

END_OF_MAIN()
